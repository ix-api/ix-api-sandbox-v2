# Warning: This repository is still under heavy development and does not contain all features of the v2 specification yet

# IX-API - Sandbox v2

This is a reference implementation for the joint external api effort.
The sandbox implements version 2 of the `ix-api-schema`.

## Running the Sandbox 

The easiest way to run the sandbox is to clone the
repository and use docker-compose.

    git clone https://gitlab.com/ix-api/ix-api-sandbox-v2.git

    make up


Ensure the Sandbox database is up-to-date

    make migrate

Create a superuser account for the admin interface

    make superuser

Populate the Sandbox with mock data

    make bootstrap

The Sandbox is now running at [http://localhost:8000](http://localhost:8000)
    

The `bin/sandbox` wrapper around docker-compose can be used to
easily run commands in a sandbox container.


## Testing

Run all tests with

    ./bin/sandbox pytest

Add flags for more verbose output

    ./bin/sandbox pytest -s -v


## Getting the OpenAPI specs

Visit [https://docs.ix-api.net/](https://docs.ix-api.net/) to get
the latest version of the IX-API spec.

## Using the API

Both (legacy) token and oauth2 authentication is supported
in the sandbox.

After bootstrapping, you can use the `api_key` and `api_secret`
provided to access the API.
In case you forgot your key and secret, you can retrieve them
from the test suite config:

    curl http://localhost:8000/static/test-suite-config.json


### Token Authentication

To authenticate with the api you can `POST` a json document
with the payload:

```json
{
    "api_key": "<api key>",
    "api_secret": "<secret>"
}
```

Authenticate with your API key and secret using `curl`:

```bash
export API_KEY=...
export SECRET=...

curl -X POST --json "{\"api_key\":\"$API_KEY\",\"api_secret\":\"$SECRET\"}" http://localhost:8000/api/v2/auth/token
```

**Deprecation Notice**: *The token authentication is considered
legacy and future implementations are encuraged to use OAuth2.*


### OAuth2

The sandbox provides the `OAuth2` flows: `client_credentials` and
`refresh_token` under the endpoint `/auth/oauth2/tokens`.


Authenticate with your API key and secret using `curl`:

```bash
export API_KEY=...
export SECRET=...

curl -X POST -d grant_type=client_credentials http://$API_KEY:$SECRET@localhost:8000/auth/oauth2/token
```

You will receive an access and refresh token. The access token can be used as a `Bearer`
for all requests to the API.
The refresh token can be used to receive a ne access and refresh token
using the `refresh_token` grant type.


## Project Structure

All sanbox related apps are located within the `src/jea` module.
The sandbox is split into the following apps:

 - `jea.public` A minimal landing page
 - `jea.auth` The authentication backend application providing authentication services
 - `jea.config` Network service configuration
 - `jea.catalog` The service catalog
 - `jea.crm` All crm related services
 - `jea.service` Package for network services and features
 - `jea.api.v2` The v2 implementation of the JointExternalApi.

### Tests

Tests are grouped by app and a `tests/` directory is located in the
first level of the application's root (e.g. `src/jea/api/v1/tests/`).

The test folder structure should reflect the structure of the app.

### The `jea.api.v2` application

The API application reflects to some degree the overall project structure.

All resources are grouped in their specific module: A resource usually
consists of a `views.py` and a `serializers.py`. Additional resource
related modules should be placed within the directory of the resource.

Example:
    
    src/jea/api/v2/crm/serializers.py
    src/jea/api/v2/crm/views.py


## Bootstrapping

The IX-API sandbox comes with management command to setup a
demonstration / testing environment with a populated catalog
and test customers.

This is done by running the `jea.ctrl.managment` command: `bootstrap`.

### Commandline Arguments

You can provide various settings directly on invocation.
If some information is missing, bootstrap will ask you for it
or will generate some made up values.

    --yes
        Assume yes when asked, e.g. when clearing the database

    --api-key
        Provide a fixed api key

    --api-secret
        Provide a fixed api secret for the reseller customer

    --exchange-name
        The name of your exchange.

        Bootstrap will prompt for input when in interactive mode, 
        or will suggest some random name.

    --exchange-asn
        The IXPs ASN.

        Bootstrap will prompt for input or will make
        some ASN up from the private AS range.

    --api-customer
        The name of the reseller / API customer

    --defaults
        Don't prompt for any input, just make something up in case
        it's not provided by the above flags.
