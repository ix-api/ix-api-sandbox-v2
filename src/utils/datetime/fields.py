
from django.db.models import CharField
from django.core.exceptions import ValidationError
from dateutil.relativedelta import relativedelta

from utils.datetime.encoding import (
    relativedelta_to_iso,
    iso_to_relativedelta,
)

from utils.datetime.forms import RelativeDeltaFormField


class RelativeDeltaField(CharField):
    """Serialize a relativedelta into the database"""
    description = "A relative time delta"
    default_error_messages = {
        "invalid": (
            "“%(value)s” value has an invalid format. It must be in "
            "iso8601 period format."
        ),
    }

    def __init__(self, *args, **kwargs):
        """Initialize relativedelta field"""
        # Set max_length
        if "max_length" not in kwargs:
            kwargs["max_length"] = 255

        # Encode default value, if its a relative delta
        default = kwargs.get("default")
        if default is not None and isinstance(default, str):
            kwargs["default"] = self.to_python(default)


        super().__init__(*args, **kwargs)

        # Clear validators, because max_length validation is
        # actually not needed here.
        self.validators = []

    def to_python(self, value):
        """Decode string to relativedelta"""
        if value is None:
            return None

        if isinstance(value, relativedelta):
            return value

        try:
            return iso_to_relativedelta(value)
        except ValueError:
            raise ValidationError(
                self.error_messages["invalid"],
                code="invalid_period",
                params={"value": value},
            )

    def get_prep_value(self, value):
        """Encode relativedelta to string"""
        if value is None:
            return None

        return relativedelta_to_iso(value)

    def from_db_value(self, value, expression, connection):
        """Decode string to relativedelta"""
        return self.to_python(value)

    def value_from_object(self, obj):
        """Get value from object"""
        value = super().value_from_object(obj)
        return value

    def value_to_string(self, obj):
        """Encode relativedelta to string"""
        value = self.value_from_object(obj)
        if value is None:
            return ""

        return self.get_prep_value(value)

    def formfield(self, **kwargs):
        """Custom form field for relativedelta"""
        fieldargs = {
            "form_class": RelativeDeltaFormField,
        }
        return super().formfield(**fieldargs)
