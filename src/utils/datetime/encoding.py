"""
Encoding and decoding functions for the time and date
formats. Notably relative delta in iso8601 period format.
"""

from dateutil.relativedelta import relativedelta

def relativedelta_to_iso(delta: relativedelta) -> str:
    """
    Encode a relative delta as an iso8601 period string.

    :params delta: relativedelta
    :return: str
    """
    delta = delta.normalized()
    years = "%dY" % delta.years if delta.years else ""
    months = "%dM" % delta.months if delta.months else ""
    days = "%dD" % delta.days if delta.days else ""
    hours = "%dH" % delta.hours if delta.hours else ""
    minutes = "%dM" % delta.minutes if delta.minutes else ""
    seconds = "%dS" % delta.seconds if delta.seconds else ""

    ddate = "".join([years, months, days])
    dtime = "".join([hours, minutes, seconds])
    if dtime:
        dtime = "T" + dtime

    return "P" + ddate + dtime


def iso_to_relativedelta(value) -> relativedelta:
    """
    Parse an iso8601 period string into a relativedelta.

    :params period: str
    :return: relativedelta
    """
    period = value.upper()
    if not period.startswith("P"):
        raise ValueError("Invalid iso8601 period format: %s" % period)

    period = period[1:] # Strip prefix

    years = months = weeks = days = 0
    hours = minutes = seconds = 0

    pdate, ptime = period.split("T") if "T" in period else (period, "")

    # Parse date by parsing and removing a block
    if pdate:
        if "Y" in pdate:
            years = int(pdate.split("Y", 1)[0])
            pdate = pdate.split("Y", 1)[1]
        if "M" in pdate:
            months = int(pdate.split("M", 1)[0])
            pdate = pdate.split("M", 1)[1]
        if "W" in pdate:
            weeks = int(pdate.split("W", 1)[0])
            pdate = pdate.split("W", 1)[1]
        if "D" in pdate:
            days = int(pdate.split("D", 1)[0])
            pdate = pdate.split("D", 1)[1]

    # Same for time
    if ptime:
        if "H" in ptime:
            hours = int(ptime.split("H")[0])
            ptime = ptime.split("H")[1]
        if "M" in ptime:
            minutes = int(ptime.split("M")[0])
            ptime = ptime.split("M")[1]
        if "S" in ptime:
            seconds = int(ptime.split("S")[0])
            ptime = ptime.split("S")[1]

    # Check if there are residuals in date and time
    if pdate or ptime:
        print(pdate, ptime)
        raise ValueError("Invalid iso8601 period format: %s" % value)

    # Construct relativedelta object
    return relativedelta(
        years=years, months=months, weeks=weeks, days=days,
        hours=hours, minutes=minutes, seconds=seconds
    )
