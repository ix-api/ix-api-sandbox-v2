"""
Relative Delta Form Fields
"""
from django.forms import Field, ValidationError
from django.forms.widgets import TextInput
from dateutil.relativedelta import relativedelta

from utils.datetime.encoding import (
    relativedelta_to_iso,
    iso_to_relativedelta,
)

class RelativeDeltaInput(TextInput):
    """
    Relative Delta Input
    """
    def format_value(self, value):
        """Format value"""
        if isinstance(value, relativedelta):
            return relativedelta_to_iso(value)

        return value


class RelativeDeltaFormField(Field):
    """
    Relative Delta Form Field
    """
    widget = RelativeDeltaInput

    def __init__(self, *args, **kwargs):
        """Initialize field"""
        kwargs["help_text"] = (
            "ISO 8601 period format: "
            "P&lt;years&gt;Y&lt;months&gt;M&lt;days&gt;D"
        )
        del kwargs["max_length"]
        super().__init__(*args, **kwargs)

