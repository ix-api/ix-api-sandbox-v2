
"""
Test datetime encoding
"""

from dateutil.relativedelta import relativedelta

from utils.datetime.encoding import (
    relativedelta_to_iso,
    iso_to_relativedelta,
)

def test_relativedelta_to_iso():
    """Test iso period encoding"""
    delta = relativedelta(years=23, months=2, days=3, hours=4, minutes=5, seconds=6)
    assert relativedelta_to_iso(delta) == "P23Y2M3DT4H5M6S"

    delta = relativedelta(years=5, months=2, days=3)
    assert relativedelta_to_iso(delta) == "P5Y2M3D"

    delta = relativedelta(minutes=5, seconds=23)
    assert relativedelta_to_iso(delta) == "PT5M23S"


def test_iso_to_relativedelta():
    """Test iso period decoding"""
    delta = iso_to_relativedelta("P23Y2M3DT4H5M6S")
    assert delta == relativedelta(
        years=23, months=2, days=3,
        hours=4, minutes=5, seconds=6)

    delta = iso_to_relativedelta('P5Y2M3D')
    assert delta == relativedelta(years=5, months=2, days=3)

    delta = iso_to_relativedelta('P2W3DT5H')
    assert delta == relativedelta(weeks=2, days=3, hours=5)

    delta = iso_to_relativedelta('PT5M23S')
    assert delta == relativedelta(minutes=5, seconds=23)
