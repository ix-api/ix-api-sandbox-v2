

"""
Test custom filters.

Caveat: this is using a jea model, which is a bit of a bad
design. Yet I don't want to have this piece of code untested.

"""

import pytest
import django_filters
from model_bakery import baker
from ixapi_schema.v2.schema import (
    NETWORK_SERVICE_TYPE_EXCHANGE_LAN,
    NETWORK_SERVICE_TYPE_P2P,
    NETWORK_SERVICE_TYPE_MP2MP,
)

from jea.service.models import (
    NETWORK_SERVICE_MODELS,
    NetworkService,
)
from jea.catalog import models
from utils import filters


@pytest.mark.django_db
def test_bulk_id_filter():
    """Test bulk id filtering"""

    # Make some models
    p1 = baker.make("catalog.ProductOffering")
    p2 = baker.make("catalog.ProductOffering")
    p3 = baker.make("catalog.ProductOffering")

    bulk_filter = filters.BulkIdFilter()

    # Test single value
    qs = models.ProductOffering.objects.all()
    qs = bulk_filter.filter(qs, str(p1.pk))

    assert qs.count() == 1
    assert qs.first() == p1

    # Test multivalue
    pks = f"{p3.pk},{p2.pk}"
    query_set = models.ProductOffering.objects.all()
    query_set = bulk_filter.filter(query_set, pks)

    assert query_set.count() == 2
    assert not p1 in query_set
    assert p2 in query_set
    assert p3 in query_set


@pytest.mark.django_db
def test_bulk_id_filterset():
    """Test bulk filter in FilterSet context"""
    class FooFilter(django_filters.FilterSet):
        id = filters.BulkIdFilter()

        class Meta:
            model = models.ProductOffering
            exclude = []

    # Make some models
    p1 = baker.make("catalog.ProductOffering")
    p2 = baker.make("catalog.ProductOffering")
    p3 = baker.make("catalog.ProductOffering")

    filtered = FooFilter({
        "id": f"{p3.pk},{p2.pk}",
    })
    query_set = filtered.qs

    assert query_set.count() == 2
    assert not p1 in query_set
    assert p2 in query_set
    assert p3 in query_set



@pytest.mark.django_db
def test_polymorphic_model_type_filter():
    """Test filtering by polymorphic model type"""
    class ModelFilter(django_filters.FilterSet):
        type = filters.PolymorphicModelTypeFilter(NETWORK_SERVICE_MODELS)

        class Meta:
            model = NetworkService
            exclude = []

    s1 = baker.make("service.P2PNetworkService")
    s2 = baker.make("service.MP2MPNetworkService")
    s3 = baker.make("service.ExchangeLanNetworkService")

    # Type Exchange Lan
    filtered = ModelFilter({
        "type": NETWORK_SERVICE_TYPE_EXCHANGE_LAN,
    }).qs
    assert not s1 in filtered
    assert not s2 in filtered
    assert s3 in filtered

    # Type P2P
    filtered = ModelFilter({
        "type": NETWORK_SERVICE_TYPE_P2P,
    }).qs
    assert s1 in filtered
    assert not s2 in filtered
    assert not s3 in filtered

    # Type unknown
    filtered = ModelFilter({
        "type": "AAAAAAAAAAAAAAAAAAAAAAAAAAA",
    }).qs
    assert not s1 in filtered
    assert not s2 in filtered
    assert not s3 in filtered
