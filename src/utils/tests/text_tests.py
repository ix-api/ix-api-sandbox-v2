
"""
Test text utilities
"""

from utils import text



def test_trim_docstring():
    """Test left aligning by trimming the docstring"""

    class ExampleA:
        """This is A docstring"""

    class ExampleB:
        """This is B docstring
        And It is multiline!

            >>> b = ExampleB()
        """

    class ExampleC:
        """
        This is C Docstirng
            >>> print("just some example")

        End of documentation.
        """

    doc_a = text.trim_docstring(ExampleA.__doc__)
    assert doc_a == "This is A docstring"
    doc_b = text.trim_docstring(ExampleB.__doc__)
    assert doc_b
    doc_c = text.trim_docstring(ExampleC.__doc__)
    assert doc_c[0] != " "
