
"""
Tests for datastructure utilities
"""

from utils import datastructures


def test_reverse_mapping():
    """Test reverse mapping"""
    mapping = {
        "foo": 23,
        "bar": "baz",
    }

    inverse = datastructures.reverse_mapping(mapping)

    assert inverse[23] == "foo"
    assert inverse["baz"] == "bar"


def test_filter_fields():
    """Test dict key filtering"""
    data = {
        "foo": 23,
        "bar": 42,
        "baz": {
            "bam": 12,
        }
    }

    filtered = datastructures.filter_fields(data, [
        "foo", "baz"
    ])

    assert not id(filtered) == id(data), "filtered should be a copy of data"
    assert data["bar"], "The original data should not be touched."
    assert not filtered.get("bar")
    assert not id(filtered["baz"]) == id(data["baz"])


def test_must_list__single_value():
    """Test assert list from input: single value"""
    value = "foo"
    assert ["foo"] == datastructures.must_list(value)

    value = 42
    assert [42] == datastructures.must_list(value)

    value = None
    assert [] == datastructures.must_list(value)


def test_must_list__list_value():
    """Test assert list from input: list"""
    value = ["foo", 42]
    assert value == datastructures.must_list(value)


def test_must_list__csv():
    """Test assert list from input: CSV"""
    csv = "23,42,5"
    result = datastructures.must_list(csv)
    assert result == ["23", "42", "5"]

