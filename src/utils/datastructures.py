
"""
Datastructure Utilities
"""

from typing import List
from copy import deepcopy



def reverse_mapping(mapping: dict) -> dict:
    """
    Creates the inverse mapping of a dict type
    """
    return {v: k for k, v in mapping.items()}


def filter_fields(data: dict, fields: List[str]) -> dict:
    """
    Remove fields from data, if fields are not explicitly
    allowed. Creates a copy to prevent mutations.

    :param data: The data dict
    :param fields: A list of allowed fields
    """
    filtered = deepcopy(data)
    for key in data.keys():
        if not key in fields:
            del filtered[key]

    return filtered


def must_list(value):
    """
    We require a parameters list.
    If the value is a CSV-string, we will unpack
    the content as a list.

    :param value: The incoming query value
    :returns: A list.
    """
    if value is None:
        return []
    if isinstance(value, str):
        value = [v.strip() for v in value.split(",")]
    if not isinstance(value, list):
        value = [value]

    return value
