
"""
Custom Filters, not only JEA specific
"""

import django_filters


class BulkIdFilter(django_filters.Filter):
    """
    Treat value as a csv list of ids and match inclusion in set.
    """
    def filter(self, qs, value):
        """
        Filter by list of ids

        :param qs: A queryset
        :param value: The passed value
        """
        if not value:
            return qs

        field_name = self.field_name
        if not field_name:
            field_name = "pk"

        lookup = f"{field_name}__in"
        pk_set = [pk.strip() for pk in value.split(",")]

        return qs.filter(**{lookup: pk_set})


class PolymorphicModelTypeFilter(django_filters.Filter):
    """Filter by polymorphic model type"""
    def __init__(self, models):
        self.models = models
        super().__init__()

    def filter(self, qs, value):
        """Filter by polymorphic type"""
        # Apply only when value is set
        if not value:
            return qs

        model = self.models.get(value)
        if not model:
            return qs.none()

        # Apply filter
        return qs.instance_of(model)
