import logging

from django.apps import AppConfig

logger = logging.getLogger(__name__)


class CatalogConfig(AppConfig):
    name = 'jea.catalog'
    verbose_name = "JEA :: Catalog"

    def ready(self):
        logger.info("Initializing app: {}".format(self.name))

