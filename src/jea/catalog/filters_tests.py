
"""
Test Model Filters
"""

import pytest
from model_bakery import baker
from ixapi_schema.v2.schema import ServiceProviderWorkflow

from jea.catalog.filters import (
    MetroAreaFilter,
    MetroAreaNetworkFilter,
    FacilityFilter,
    DeviceFilter,
    ProductOfferingFilter,
    PointOfPresenceFilter,
)

@pytest.mark.django_db
def test_metro_area_filter__id():
    """Test filtering metro areas: ID"""
    ma1 = baker.make("catalog.MetroArea")
    ma2 = baker.make("catalog.MetroArea")
    ma3 = baker.make("catalog.MetroArea")

    filtered = MetroAreaFilter({
        "id": "{},{}".format(ma1.pk, ma3.pk),
    }).qs
    assert ma1 in filtered
    assert ma2 not in filtered
    assert ma3 in filtered


@pytest.mark.django_db
def test_metro_area_network_filter__id():
    """Test filter metro area networks: ID"""
    # We only allow bulk ids
    man1 = baker.make("catalog.MetroAreaNetwork")
    man2 = baker.make("catalog.MetroAreaNetwork")
    man3 = baker.make("catalog.MetroAreaNetwork")

    filtered = MetroAreaNetworkFilter({
        "id": "{},{}".format(man1.pk, man3.pk),
    }).qs
    assert man1 in filtered
    assert man2 not in filtered
    assert man3 in filtered


@pytest.mark.django_db
def test_metro_area_network_filter__metro_area():
    """Test filter metro area networks: ID"""
    # We only allow bulk ids
    ma1 = baker.make("catalog.MetroArea")
    ma2 = baker.make("catalog.MetroArea")
    man1 = baker.make(
        "catalog.MetroAreaNetwork", metro_area=ma1)
    man2 = baker.make(
        "catalog.MetroAreaNetwork", metro_area=ma2)

    filtered = MetroAreaNetworkFilter({
        "metro_area": ma2.pk,
    }).qs

    assert man1 not in filtered
    assert man2 in filtered


@pytest.mark.django_db
def test_metro_area_network_filter__name():
    """Test filter metro area network: name"""
    man1 = baker.make(
        "catalog.MetroAreaNetwork", name="foo-loc-1")
    man2 = baker.make(
        "catalog.MetroAreaNetwork", name="bar-loc-3")

    filtered = MetroAreaNetworkFilter({
        "name": "bar-loc-3",
    }).qs

    assert man1 not in filtered
    assert man2 in filtered


@pytest.mark.django_db
def test_metro_area_network_filter__service_provider():
    """Test filter: service_provider"""
    man1 = baker.make(
        "catalog.MetroAreaNetwork", service_provider="barcix")
    man2 = baker.make(
        "catalog.MetroAreaNetwork", service_provider="vnet9")

    filtered = MetroAreaNetworkFilter({
        "service_provider": "vnet9",
    }).qs

    assert man1 not in filtered
    assert man2 in filtered


@pytest.mark.django_db
def test_facility_filter():
    """Test facility filtering"""
    facility_a = baker.make("catalog.Facility")
    facility_b = baker.make("catalog.Facility")
    pop = baker.make("catalog.PointOfPresence",
                     facility=facility_b)
    device = baker.make("catalog.Device",
                        pop=pop)

    filter_requests = [
        ({"metro_area": facility_b.metro_area.pk}, facility_b),
        ({"address_country": facility_a.address_country}, facility_a),
        ({"address_locality": facility_a.address_locality}, facility_a),
        ({"postal_code": facility_a.postal_code}, facility_a),
    ]

    for request, expected_facility in filter_requests:
        filtered = FacilityFilter(request)
        assert expected_facility == filtered.qs.first(), \
            (filtered.qs.first(), (request, expected_facility))


@pytest.mark.django_db
def test_facility_filter__metro_area():
    """test filtering facilities by metro area"""
    area1 = baker.make("catalog.MetroArea")
    area52 = baker.make("catalog.MetroArea")
    facility1 = baker.make(
        "catalog.Facility",
        metro_area=area1)
    facility2 = baker.make(
        "catalog.Facility",
        metro_area=area52)

    filtered = FacilityFilter({
        "metro_area": area1.pk,
    }).qs
    assert facility1 in filtered
    assert facility2 not in filtered


@pytest.mark.django_db
def test_facility_filter__metro_area_network():
    """test filtering by metro area network"""
    man1 = baker.make("catalog.MetroAreaNetwork")
    man2 = baker.make("catalog.MetroAreaNetwork")
    facility1 = baker.make(
        "catalog.Facility",
        metro_area=man1.metro_area)
    facility2 = baker.make(
        "catalog.Facility",
        metro_area=man2.metro_area)
    filtered = FacilityFilter({
        "metro_area_network": man1.pk,
    }).qs
    assert facility1 in filtered
    assert facility2 not in filtered


@pytest.mark.django_db
def test_devices_filter():
    """Test filtering the devices"""
    device_a = baker.make("catalog.Device")
    device_b = baker.make("catalog.Device")

    # filter by id
    filtered = DeviceFilter({"id": f"{device_b.pk},12345689"})
    assert device_b in filtered.qs
    assert not device_a in filtered.qs


@pytest.mark.django_db
def test_product_offering_filter__type():
    """Test product filtering"""
    product_a = baker.make("catalog.ExchangeLanNetworkProductOffering")
    product_b = baker.make("catalog.CloudNetworkProductOffering")

    filtered = ProductOfferingFilter({"type": "exchange_lan"})
    assert product_a in filtered.qs
    assert not product_b in filtered.qs


@pytest.mark.django_db
def test_product_offering_filter__bandwidth():
    """Test bandwidth filtering"""
    product1 = baker.make(
        "catalog.P2PNetworkProductOffering",
        bandwidth_min=None,
        bandwidth_max=None)

    product2 = baker.make(
        "catalog.P2PNetworkProductOffering",
        bandwidth_min=1000,
        bandwidth_max=None)

    product3 = baker.make(
        "catalog.P2PNetworkProductOffering",
        bandwidth_min=None,
        bandwidth_max=10000)

    product4 = baker.make(
        "catalog.P2MPNetworkProductOffering",
        bandwidth_min=100000,
        bandwidth_max=200000)

    filtered = ProductOfferingFilter({"bandwidth": 1000}).qs
    assert product1 in filtered
    assert product2 in filtered
    assert product3 in filtered
    assert not product4 in filtered

    filtered = ProductOfferingFilter({"bandwidth": 105000}).qs
    assert product1 in filtered
    assert product2 in filtered
    assert not product3 in filtered
    assert product4 in filtered

    filtered = ProductOfferingFilter({"bandwidth": 500}).qs
    assert product1 in filtered
    assert not product2 in filtered
    assert product3 in filtered
    assert not product4 in filtered


@pytest.mark.django_db
def test_product_offering_filter__upgrade_downgrade():
    """Test upgrade / downgrade filtering"""
    product1 = baker.make(
        "catalog.P2PNetworkProductOffering",
        upgrade_allowed=True,
        downgrade_allowed=False)

    product2 = baker.make(
        "catalog.P2PNetworkProductOffering",
        upgrade_allowed=False,
        downgrade_allowed=True)

    filtered = ProductOfferingFilter({"upgrade_allowed": False}).qs
    assert not product1 in filtered
    assert product2 in filtered

    filtered = ProductOfferingFilter({"upgrade_allowed": True}).qs
    assert product1 in filtered
    assert not product2 in filtered

    filtered = ProductOfferingFilter({"downgrade_allowed": False}).qs
    assert product1 in filtered
    assert not product2 in filtered

    filtered = ProductOfferingFilter({"downgrade_allowed": True}).qs
    assert not product1 in filtered
    assert product2 in filtered


@pytest.mark.django_db
def test_point_of_presence_filter__facility():
    """Test pop filter: facility"""
    facility1 = baker.make("catalog.Facility")
    pop1 = baker.make(
        "catalog.PointOfPresence",
        facility=facility1)
    pop2 = baker.make("catalog.PointOfPresence")

    filtered = PointOfPresenceFilter({
        "facility": facility1.pk,
    }).qs
    assert pop1 in filtered
    assert pop2 not in filtered


@pytest.mark.django_db
def test_point_of_presence_filter__metro_area_network():
    """Test pop filter: facility"""
    man = baker.make("catalog.MetroAreaNetwork")
    pop1 = baker.make("catalog.PointOfPresence")
    pop2 = baker.make(
        "catalog.PointOfPresence",
        metro_area_network=man)

    filtered = PointOfPresenceFilter({
        "metro_area_network": man.pk,
    }).qs
    assert pop1 not in filtered
    assert pop2 in filtered


@pytest.mark.django_db
def test_point_of_presence_filter__capabilities():
    """Test pop filter: facility"""
    pop1 = baker.make("catalog.PointOfPresence")
    pop2 = baker.make("catalog.PointOfPresence")
    dev = baker.make("catalog.Device", pop=pop2)
    baker.make(
        "catalog.DeviceCapability",
        device=dev,
        media_type="foo")

    filtered = PointOfPresenceFilter({
        "capability_media_type": "foo",
    }).qs
    assert pop1 not in filtered
    assert pop2 in filtered


@pytest.mark.django_db
def test_product_offering_filter__service_provider_pop():
    """Test filtering by service provider pop"""
    offering1 = baker.make(
        "catalog.CloudNetworkProductOffering",
        service_provider_pop="pop3")
    offering2 = baker.make(
        "catalog.CloudNetworkProductOffering",
        service_provider_pop="imap")

    filtered = ProductOfferingFilter({
        "service_provider_pop": "pop3",
    }).qs

    assert offering1 in filtered
    assert offering2 not in filtered


@pytest.mark.django_db
def test_product_offering_filter__fields():
    """Test distinct filtering"""
    aaa = baker.make("catalog.MetroArea")
    bbb = baker.make("catalog.MetroArea")
    ffa = baker.make("catalog.MetroArea")
    ffb = baker.make("catalog.MetroArea")

    baker.make(
        "catalog.P2PNetworkProductOffering",
        handover_metro_area=aaa,
        service_metro_area=ffa)
    product2 = baker.make(
        "catalog.P2PNetworkProductOffering",
        handover_metro_area=bbb,
        service_metro_area=ffa)
    product3 = baker.make(
        "catalog.ExchangeLanNetworkProductOffering",
        handover_metro_area=aaa,
        service_metro_area=ffb)

    filtered = ProductOfferingFilter(
        {"fields": "handover_metro_area"}).qs
    assert len(filtered) == 2
    assert product2 in filtered

    filtered = ProductOfferingFilter(
        {"fields": "type,service_metro_area"}).qs
    assert len(filtered) == 2
    assert product3 in filtered

    filtered = ProductOfferingFilter({"fields": "service_metro_area"}).qs
    assert len(filtered) == 2


@pytest.mark.django_db
def test_product_offering_filter__cloud_key():
    """Test Cloud Key Filter"""
    product1 = baker.make(
        "catalog.ExchangeLanNetworkProductOffering")
    product2 = baker.make(
        "catalog.CloudNetworkProductOffering",
        service_provider_workflow=ServiceProviderWorkflow.EXCHANGE_FIRST)
    product3 = baker.make(
        "catalog.CloudNetworkProductOffering",
        service_provider_workflow=ServiceProviderWorkflow.PROVIDER_FIRST)

    filtered = ProductOfferingFilter({"cloud_key": "FFFFFFF"}).qs
    assert product1 not in filtered
    assert product2 not in filtered
    assert product3 in filtered
