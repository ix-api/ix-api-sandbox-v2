
"""
Test Catalog Models and Relations
"""

import pytest
from model_bakery import baker

from dateutil.relativedelta import relativedelta

from jea.catalog.models import (
    Facility,
    FacilityOperator,
    Device,
    DeviceCapability,
    PointOfPresence,
    ProductOffering,
    ExchangeLanNetworkProductOffering,
    P2PNetworkProductOffering,
    P2MPNetworkProductOffering,
    MP2MPNetworkProductOffering,
    CloudNetworkProductOffering,
    MediaType,
)


@pytest.mark.django_db
def test_facilities():
    """Test Facilities and Facility Clusters"""
    operator = baker.make(FacilityOperator)

    # Create some facility
    facility = baker.make(Facility, operator=operator)
    assert facility.operator == operator

    # Make another facility
    baker.make(Facility, operator=operator)

    # Find facilities operated by a specific operator
    facilities = operator.facilities.all()
    assert len(facilities) == 2


@pytest.mark.django_db
def test_facility_devices():
    """Test getting all devices in a facility"""
    facility = baker.make(Facility)
    pop = baker.make(PointOfPresence, facility=facility)
    dev = baker.make(Device, pop=pop)

    # assert dev in facility.devices


@pytest.mark.django_db
def test_facility_available_devices():
    """Test getting all devices in a facility"""
    facility = baker.make(Facility)
    pop = baker.make(PointOfPresence, facility=facility)
    dev_a = baker.make(Device, pop=pop)
    dev_b = baker.make(Device)

    assert dev_a in facility.devices.all()
    assert dev_b not in facility.devices.all()


@pytest.mark.django_db
def test_devices():
    """Test Basic Devices"""
    pop = baker.make(PointOfPresence)

    # Make some devices
    baker.make(Device, pop=pop)

    # Find devices as devices in a facility
    assert pop.devices.count() == 1


@pytest.mark.django_db
def test_device_capabilities():
    """Test device capabilities"""
    capa = baker.make(DeviceCapability, speed=1000)
    device = capa.device

    # Make more capabilities
    baker.make(DeviceCapability,
               device=device,
               speed=400000)

    baker.make(DeviceCapability,
               device=device,
               media_type=MediaType.TYPE_100GBASE_LR.value,
               speed=100000)

    # Check filters
    capabilities = device.capabilities.filter(speed=1000)
    assert capabilities.count() == 1

    capabilities = device.capabilities.filter(speed=400000)
    assert capabilities.count() == 1

    capabilities = device.capabilities.filter(
        speed=100000,
        media_type=MediaType.TYPE_100GBASE_LR.value)
    assert capabilities.count() == 1


@pytest.mark.django_db
def test_pop_facilities():
    """Test PointOfPresence model with facilities"""
    facility_a = baker.make(Facility)
    facility_b = baker.make(Facility)

    # Create pops
    baker.make(
        PointOfPresence,
        facility=facility_a)
    baker.make(
        PointOfPresence,
        facility=facility_a)
    baker.make(
        PointOfPresence,
        facility=facility_b)

    # Find portation points
    assert facility_a.pops.all().count() == 2
    assert facility_b.pops.all().count() == 1


@pytest.mark.django_db
def test_pop_devices():
    """Test PointOfPresence model with devices"""
    d_1 = baker.make(Device)
    d_2 = baker.make(Device)
    d_3 = baker.make(Device)

    pop_1 = baker.make(PointOfPresence,
                       devices=[d_1, d_2])

    pop_2 = baker.make(PointOfPresence,
                       devices=[d_3])

    # Check related
    assert d_1.pop == pop_1
    assert d_2.pop == pop_1
    assert d_3.pop == pop_2


@pytest.mark.django_db
def test_pop_reachable_devices():
    """Test devices relation from PointOfPresence"""
    d_1 = baker.make(Device)
    d_2 = baker.make(Device)

    pop_1 = baker.make(PointOfPresence,
                       devices=[d_1])

    assert d_1 in pop_1.devices.all()
    assert d_2 not in pop_1.devices.all()


@pytest.mark.django_db
def test_polymorphic_product_offerings():
    """Test products and derived polymorphic products"""
    prod1 = baker.make(ExchangeLanNetworkProductOffering)
    prod2 = baker.make(P2PNetworkProductOffering)

    offering = ProductOffering.objects.get(pk=prod1.pk)
    assert isinstance(offering, ExchangeLanNetworkProductOffering)

    offering = ProductOffering.objects.get(pk=prod2.pk)
    assert isinstance(offering, P2PNetworkProductOffering)


def test_product_offering__repr__():
    """Test product offering repr"""
    prod = baker.prepare(CloudNetworkProductOffering)
    assert repr(prod)


def test_product_offering__str__():
    """Test product offering to str"""
    prod = baker.prepare(ExchangeLanNetworkProductOffering)
    assert str(prod)


@pytest.mark.django_db
def test_exchange_lan_network_product_offering():
    """Test creating an exchange lan product offering"""
    baker.make(ExchangeLanNetworkProductOffering)


@pytest.mark.django_db
def test_p2p_network_product_offering():
    """Test creating a product offering"""
    baker.make(P2PNetworkProductOffering)


@pytest.mark.django_db
def test_p2mp_network_product_offering():
    """Test creating a product offering"""
    baker.make(P2MPNetworkProductOffering)


@pytest.mark.django_db
def test_mp2mp_network_product_offering():
    """Test creating a product offering"""
    baker.make(MP2MPNetworkProductOffering)


@pytest.mark.django_db
def test_cloud_network_product_offering():
    """Test creating an exchange lan product offering"""
    baker.make(CloudNetworkProductOffering)

@pytest.mark.django_db
def test_product_offering_cancellation_relative_delta():
    """Test relative delta fields"""
    prod = baker.make(
        CloudNetworkProductOffering,
        cancellation_notice_period=relativedelta(days=30),
        cancellation_charge_period=relativedelta(months=6),
    )

    # Load from db
    dbprod = CloudNetworkProductOffering.objects.get(pk=prod.pk)

    assert dbprod.cancellation_notice_period == relativedelta(days=30)
    assert dbprod.cancellation_charge_period == relativedelta(months=6)

