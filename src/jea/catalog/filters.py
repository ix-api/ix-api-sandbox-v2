
"""
Filters for Catalog Models
"""

import django_filters
from django.db import models
from django.db.models import Q
from ixapi_schema.v2.constants.catalog import (
    ServiceProviderWorkflow,
)

from utils import filters
from utils.datastructures import must_list
from jea.catalog.models import (
    PRODUCT_OFFERING_MODELS,
    VLanProductOfferingMixin,
    MetroArea,
    MetroAreaNetwork,
    PointOfPresence,
    Facility,
    Device,
    ProductOffering,
)


class MetroAreaFilter(django_filters.FilterSet):
    """Filter MetroAreas"""
    id = filters.BulkIdFilter()

    class Meta:
        model = MetroArea
        fields = []


class MetroAreaNetworkFilter(django_filters.FilterSet):
    """MetroAreaNetwork Filter"""
    id = filters.BulkIdFilter()
    metro_area = django_filters.CharFilter(
        field_name="metro_area_id")
    name = django_filters.CharFilter(
        lookup_expr="iexact")
    service_provider = django_filters.CharFilter(
        lookup_expr="iexact")

    class Meta:
        model = MetroAreaNetwork
        fields = []


class FacilityFilter(django_filters.FilterSet):
    """Filter Facilities"""
    id = filters.BulkIdFilter()

    capability_speed = django_filters.NumberFilter(
        distinct=True,
        field_name="physical_pops__physical_devices__capabilities__speed")

    capability_speed__lt = django_filters.NumberFilter(
        distinct=True,
        field_name="physical_pops__physical_devices__capabilities__speed",
        lookup_expr="lt")

    capability_speed__lte = django_filters.NumberFilter(
        distinct=True,
        field_name="physical_pops__physical_devices__capabilities__speed",
        lookup_expr="lte")

    capability_speed__gt = django_filters.NumberFilter(
        distinct=True,
        field_name="physical_pops__physical_devices__capabilities__speed",
        lookup_expr="gt")

    capability_speed__gte = django_filters.NumberFilter(
        distinct=True,
        field_name="physical_pops__physical_devices__capabilities__speed",
        lookup_expr="gte")

    organisation_name = django_filters.CharFilter(
        distinct=True,
        field_name="operator__name",
        lookup_expr="iexact")

    metro_area = django_filters.CharFilter(
        field_name="metro_area_id")

    metro_area_network = django_filters.CharFilter(
        method="filter_metro_area_networks")

    def filter_metro_area_networks(self, queryset, _name, value):
        """Filter by metro area networks"""
        return queryset.filter(
            metro_area__metro_area_networks__in=[value])

    # TODO:
    # network_service = django_filters.CharFilter(
    #   distinct=True,
    #   field_name="huh."
    #

    class Meta:
        model = Facility
        fields = [
            "address_country",
            "address_locality",
            "postal_code"
        ]

        filter_overrides = {
            models.CharField: {
                'filter_class':
                    django_filters.CharFilter,
                'extra': lambda f: {
                    'lookup_expr': 'iexact',
                },
            },
        }


class DeviceFilter(django_filters.FilterSet):
    """Filters for devices"""
    id = filters.BulkIdFilter()

    capability_media_type = django_filters.CharFilter(
        distinct=True,
        field_name="capabilities__media_type",
        lookup_expr="iexact")

    capability_speed = django_filters.NumberFilter(
        distinct=True,
        field_name="capabilities__speed")

    capability_speed__lt = django_filters.NumberFilter(
        distinct=True,
        field_name="capabilities__speed",
        lookup_expr="lt")

    capability_speed__lte = django_filters.NumberFilter(
        distinct=True,
        field_name="capabilities__speed",
        lookup_expr="lte")

    capability_speed__gt = django_filters.NumberFilter(
        distinct=True,
        field_name="capabilities__speed",
        lookup_expr="gt")

    capability_speed__gte = django_filters.NumberFilter(
        distinct=True,
        field_name="capabilities__speed",
        lookup_expr="gte")

    facility = django_filters.CharFilter(
        distinct=True,
        field_name="pop__facility")

    pop = django_filters.CharFilter(
        distinct=True,
        field_name="pop")

    class Meta:
        model = Device
        fields = [
            "name",
        ]

        filter_overrides = {
            models.CharField: {
                'filter_class':
                    django_filters.CharFilter,
                'extra': lambda f: {
                    'lookup_expr': 'iexact',
                },
            },
        }


class PointOfPresenceFilter(django_filters.FilterSet):
    """Filters for pops"""
    id = filters.BulkIdFilter()

    facility = django_filters.CharFilter(
        field_name="facility")

    metro_area_network = django_filters.CharFilter(
        field_name="metro_area_network")

    capability_media_type = django_filters.CharFilter(
        distinct=True,
        field_name="devices__capabilities__media_type",
        lookup_expr="iexact")

    capability_speed = django_filters.NumberFilter(
        distinct=True,
        field_name="devices__capabilities__speed")

    capability_speed__lt = django_filters.NumberFilter(
        distinct=True,
        field_name="devices__capabilities__speed",
        lookup_expr="lt")

    capability_speed__lte = django_filters.NumberFilter(
        distinct=True,
        field_name="devices__capabilities__speed",
        lookup_expr="lte")

    capability_speed__gt = django_filters.NumberFilter(
        distinct=True,
        field_name="devices__capabilities__speed",
        lookup_expr="gt")

    capability_speed__gte = django_filters.NumberFilter(
        distinct=True,
        field_name="devices__capabilities__speed",
        lookup_expr="gte")


    class Meta:
        model = PointOfPresence
        fields = [] # allow only the fields above


class ProductOfferingFilter(django_filters.FilterSet):
    """Filter Products"""

    # Field Filters
    id = filters.BulkIdFilter()

    type = filters.PolymorphicModelTypeFilter(PRODUCT_OFFERING_MODELS)

    name = django_filters.CharFilter(
        field_name="name", lookup_expr="iexact")

    # Legacy filter
    handover_metro_area = django_filters.CharFilter(
        field_name="handover_metro_area_id")
    handover_metro_area_network = django_filters.CharFilter(
        field_name="handover_metro_area_network_id")

    service_metro_area = django_filters.CharFilter(
        method="filter_service_metro_area")

    def filter_service_metro_area(self, queryset, _name, value):
        """Filter service metro area"""
        producttypes = [c.__name__.lower()
            for c in VLanProductOfferingMixin.__subclasses__()]
        query = Q()
        for p in producttypes:
            query = query | Q(**{f"{p}__service_metro_area_id": value})

        return queryset.filter(query)

    service_metro_area_network = django_filters.CharFilter(
        method="filter_service_metro_area_network")

    def filter_service_metro_area_network(self, queryset, _name, value):
        """Filter service metro area network"""
        producttypes = [c.__name__.lower()
            for c in VLanProductOfferingMixin.__subclasses__()]
        query = Q()
        for p in producttypes:
            query = query | Q(**{f"{p}__service_metro_area_network_id": value})

        return queryset.filter(query)

    service_provider = django_filters.CharFilter(
        field_name="cloudnetworkproductoffering__service_provider",
        lookup_expr="iexact")
    service_provider_region = django_filters.CharFilter(
        field_name="cloudnetworkproductoffering__service_provider_region",
        lookup_expr="iexact")
    service_provider_pop = django_filters.CharFilter(
        field_name="cloudnetworkproductoffering__service_provider_pop",
        lookup_expr="iexact")

    upgrade_allowed = django_filters.BooleanFilter(
        field_name="upgrade_allowed")
    downgrade_allowed = django_filters.BooleanFilter(
        field_name="downgrade_allowed")

    bandwidth = django_filters.NumberFilter(
        method="filter_bandwidth")
    def filter_bandwidth(self, queryset, _name, value):
        """Filter bandwidth"""
        if not value:
            return queryset

        qmax = Q(bandwidth_max=None)
        qmax.add(Q(bandwidth_max__gte=value), Q.OR)
        qmin = Q(bandwidth_min=None)
        qmin.add(Q(bandwidth_min__lte=value), Q.OR)

        return queryset.filter(qmin & qmax)

    physical_port_speed = django_filters.NumberFilter(
        field_name="physical_port_speed")

    delivery_method = django_filters.CharFilter(
        field_name="cloudnetworkproductoffering__delivery_method",
        lookup_expr="iexact")

    fields = django_filters.CharFilter(method="filter_fields")
    def filter_fields(self, queryset, _name, value):
        """Make queyset distinct on some fields"""
        if not value:
            return queryset

        keys = must_list(value)
        key_lookup = {
            "type": "__polymorphic_type__",
        }

        # This is a bit tricky to implement as a queryset
        # The naiive implementation is to for now get the entire set,
        # collect distinct ids and return a new queryset matching the ids.
        # This is kind of expensive, and thus has some room for improvment.
        ids = []
        values = set()
        for offering in queryset:
            attr = tuple(getattr(offering, key_lookup.get(k, k), None)
                         for k in keys)
            if attr not in values:
                values.add(attr)
                ids.append(offering.pk)

        # Response queryset
        return queryset.filter(pk__in=ids)

    cloud_key = django_filters.CharFilter(method="filter_cloud_key")
    def filter_cloud_key(self, queryset, _name, value):
        """CloudKey Filter"""
        # The applies only to provider workflows
        workflow = ServiceProviderWorkflow.PROVIDER_FIRST
        queryset = queryset.filter(
            cloudnetworkproductoffering__service_provider_workflow=workflow)

        # TODO: Actually simulate the entire cloud backend.
        return queryset

    class Meta:
        model = ProductOffering
        fields = []


