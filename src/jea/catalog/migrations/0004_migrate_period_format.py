
import json

from django.db import migrations
from django.db.migrations import (
    RunPython,
)

def convert_period(period):
    """
    Convert old period format [months, days] to
    iso format.
    """
    try:
        months, days = json.loads(period)
    except:
        print("Invalid period format:", period)
        return None

    return f"P{months}M{days}D"


def migrate_period_format(apps, schema_editor):
    """Migrate existing period serialized data to new format."""
    # We need to use direct database access to do this
    qry = """
        SELECT
            id,
            cancellation_notice_period,
            cancellation_charge_period
        FROM
            catalog_productoffering
    """
    with schema_editor.connection.cursor() as cursor:
        cursor.execute(qry)
        for row in cursor.fetchall():
            row_id, notice_period, charge_period = row
            notice_period = convert_period(notice_period)
            charge_period = convert_period(charge_period)

            if notice_period is None or charge_period is None:
                continue

            # Update the row with new period format
            update = """
                UPDATE catalog_productoffering
                SET
                    cancellation_notice_period = %s,
                    cancellation_charge_period = %s
                WHERE
                    id = %s
            """
            cursor.execute(update, [
                notice_period,
                charge_period,
                row_id,
            ])


class Migration(migrations.Migration):
    """Migrate existing period format."""
    dependencies = [
        ('catalog', '0003_alter_availabilityzone_options_and_more'),
    ]

    operations = [
        RunPython(migrate_period_format),
    ]
