
"""
Catalog Admin
-------------

Provide a minimal admin interface for catalog models.
"""

from django.contrib import admin

from jea.admin import site
from jea.catalog.models import (
    ExchangeLanNetworkProductOffering,
    P2PNetworkProductOffering,
    P2MPNetworkProductOffering,
    MP2MPNetworkProductOffering,
    CloudNetworkProductOffering,
    Facility,
    FacilityOperator,
    Device,
    DeviceCapability,
    PointOfPresence,
    AvailabilityZone,
    MetroArea,
    MetroAreaNetwork,
)


#
# Inline Admins
#


#
# Model Admins
#

class ExchangeLanNetworkProductOfferingAdmin(admin.ModelAdmin):

    def save_formset(self, request, form, formset, change):
        """Handle saving of custom props"""
        print("save_formset")


class P2PNetworkProductOfferingAdmin(admin.ModelAdmin):
    pass

class P2MPNetworkProductOfferingAdmin(admin.ModelAdmin):
    pass

class MP2MPNetworkProductOfferingAdmin(admin.ModelAdmin):
    pass

class CloudNetworkProductOfferingAdmin(admin.ModelAdmin):
    pass

class FacilityOperatorAdmin(admin.ModelAdmin):
    pass

class FacilityAdmin(admin.ModelAdmin):
    pass

class DeviceAdmin(admin.ModelAdmin):
    pass

class DeviceCapabilityAdmin(admin.ModelAdmin):
    pass

class PhysicalDevicesInline(admin.TabularInline):
    model = Device
    extra = 0

class PointOfPresenceAdmin(admin.ModelAdmin):
    inlines = (PhysicalDevicesInline,)

class AvailabilityZoneAdmin(admin.ModelAdmin):
    pass

class MetroAreaAdmin(admin.ModelAdmin):
    pass

class MetroAreaNetworkAdmin(admin.ModelAdmin):
    pass



#
# Register models
#
site.register(FacilityOperator, FacilityOperatorAdmin)
site.register(Facility, FacilityAdmin)

site.register(ExchangeLanNetworkProductOffering,
              ExchangeLanNetworkProductOfferingAdmin)
site.register(P2PNetworkProductOffering,
              P2PNetworkProductOfferingAdmin)
site.register(P2MPNetworkProductOffering,
              P2MPNetworkProductOfferingAdmin)
site.register(MP2MPNetworkProductOffering,
              MP2MPNetworkProductOfferingAdmin)
site.register(CloudNetworkProductOffering,
              CloudNetworkProductOfferingAdmin)

site.register(Device, DeviceAdmin)
site.register(DeviceCapability, DeviceCapabilityAdmin)

site.register(PointOfPresence, PointOfPresenceAdmin)

site.register(AvailabilityZone, AvailabilityZoneAdmin)
site.register(MetroArea, MetroAreaAdmin)
site.register(MetroAreaNetwork, MetroAreaNetworkAdmin)

