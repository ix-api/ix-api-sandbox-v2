
"""
Catalog Models
--------------

Provide models for a service catalog implementation.
This implementation might differ vastly from a real
world setup in an IX.
"""
from typing import Optional
from datetime import date, timedelta

import enumfields
from django.db import models
from django.db.models import expressions
from polymorphic.models import PolymorphicModel
from dateutil.relativedelta import relativedelta
from utils.datetime.fields import RelativeDeltaField
from utils.datetime.encoding import relativedelta_to_iso

from ixapi_schema.v2.constants.catalog import (
    DeliveryMethod,
    ProviderVlanTypes,
    ResourceTypes,
    ServiceProviderWorkflow,
    CrossConnectInitiator,

    PRODUCT_TYPE_CONNECTION,
    PRODUCT_TYPE_EXCHANGE_LAN,
    PRODUCT_TYPE_P2P,
    PRODUCT_TYPE_P2MP,
    PRODUCT_TYPE_MP2MP,
    PRODUCT_TYPE_CLOUD,
)
from jea.crm.models import Role


class MediaType(enumfields.Enum):
    """
    Enumeration of supported media types.
    This will most likely be moved into the schema in phase 3.
    """
    TYPE_1000BASE_LX = "1000BASE-LX"
    TYPE_10GBASE_LR = "10GBASE-LR"
    TYPE_100GBASE_LR = "100GBASE-LR"
    TYPE_100GBASE_LR4 = "100GBASE-LR4"
    TYPE_400GBASE_LR = "400GBASE-LR"
    TYPE_400GBASE_LR8 = "400GBASE-LR8"


class AvailabilityZone(models.Model):
    """An availability zone"""
    name = models.CharField(max_length=255)

    def __str__(self) -> str:
        """String representation"""
        return f"{self.name} ({self.pk})"

    def __repr__(self) -> str:
        """Representation"""
        return f"<AvailabilityZone id='{self.pk}' name='{self.name}'>"

    class Meta:
        verbose_name = "Availability Zone"


class MetroArea(models.Model):
    """Metro Area Model"""
    id = models.CharField(
        primary_key=True,
        max_length=8)

    un_locode = models.CharField(
        max_length=8)
    iata_code = models.CharField(
        max_length=8)
    display_name = models.CharField(
        max_length=64)

    def __str__(self):
        """MetroArea String Representation"""
        return f"MetroArea: {self.iata_code} ({self.id})"

    class Meta:
        verbose_name = "Metro Area"


class MetroAreaNetwork(models.Model):
    """Metro Area Network Model"""
    name = models.CharField(max_length=32)
    service_provider = models.CharField(max_length=128)

    metro_area = models.ForeignKey(
        MetroArea,
        related_name="metro_area_networks",
        on_delete=models.CASCADE)

    def __str__(self):
        """MetroAreaNetwork String Representation"""
        return f"{self.name} ({self.pk})"

    class Meta:
        verbose_name = "Metro Area Network"


#
# Infrastructure
#
class CloudProvider(models.Model):
    """A model for cloud providers"""
    name = models.CharField(max_length=80)

    def __str__(self) -> str:
        """Make string representation"""
        return str(self.name)

    def __repr__(self) -> str:
        """Make representation"""
        return f"<CloudProvider id='{self.pk}' name='{self.name}'>"

    class Meta:
        verbose_name = "Cloud Provider"


class FacilityOperator(models.Model):
    """A facility is run by some operator"""
    name = models.CharField(max_length=80)

    def __str__(self) -> str:
        """Make string representation"""
        return str(self.name)

    def __repr__(self) -> str:
        """Make representation"""
        return f"<FacilityOperator id='{self.pk}' name='{self.name}'>"

    class Meta:
        verbose_name = "Facility Operator"


class Facility(models.Model):
    """A model for facilities"""
    name = models.CharField(max_length=80)
    metro_area = models.ForeignKey(
        MetroArea,
        related_name="facilities",
        on_delete=models.CASCADE)

    # We derive the "organisationName" from the
    # operator of the facility.
    operator = models.ForeignKey(
        FacilityOperator,
        related_name="facilities",
        on_delete=models.CASCADE)

    # Address
    address_country = models.CharField(max_length=2)
    address_locality = models.CharField(max_length=80)
    address_region = models.CharField(max_length=80)
    postal_code = models.CharField(max_length=18)
    street_address = models.CharField(max_length=80)

    # External references
    peeringdb_facility_id = models.PositiveIntegerField(
        blank=True,
        null=True,
        default=None)

    def __str__(self) -> str:
        """Make facility string representation"""
        return str(self.name)

    def __repr__(self) -> str:
        """Make representation"""
        return f"<Facility id='{self.pk}' name='{self.name}'>"

    @property
    def operator_name(self) -> Optional[str]:
        """Retrieve the operator name"""
        if self.operator:
            return self.operator.name
        return None

    @property
    def devices(self):
        """Get a queryset of all devices in this facility"""
        return Device.objects.filter(pop__facility=self)


    class Meta:
        verbose_name = "Facility"
        verbose_name_plural = "Facilities"


class Device(models.Model):
    """A model for a network device"""
    name = models.CharField(max_length=180)

    # Relations
    pop = models.ForeignKey(
        "catalog.PointOfPresence",
        on_delete=models.CASCADE,
        related_name="devices")


    def __str__(self):
        """Make string representation"""
        return str(self.name)

    def __repr__(self):
        """Make repr of Device"""
        return f"<Device id='{self.pk}' name='{self.name}'>"

    class Meta:
        verbose_name = "Device"


class DeviceCapability(models.Model):
    """Capabilities of a device"""
    media_type = models.CharField(max_length=20)
    speed = models.PositiveIntegerField()

    q_in_q = models.BooleanField()
    max_lag = models.PositiveSmallIntegerField()  # Max: 32767

    availability_count = models.PositiveIntegerField()

    # Relations
    device = models.ForeignKey(
        Device,
        related_name="capabilities",
        on_delete=models.CASCADE)

    def __str__(self) -> str:
        """Device Capability To String"""
        return f"DeviceCapability ({self.pk}, {self.media_type})"

    def __repr__(self) -> str:
        """DeviceCapability representation"""
        return (f"<DeviceCapability id='{self.pk}' "
                f"media_type='{self.media_type}' "
                f"speed='{self.speed}'>")

    class Meta:
        verbose_name = "Device Capability"
        verbose_name_plural = "Device Capabilities"


class PointOfPresence(models.Model):
    """A point of presence"""
    name = models.CharField(max_length=40)

    # Relations
    metro_area_network = models.ForeignKey(
        MetroAreaNetwork,
        related_name="pops",
        on_delete=models.PROTECT)

    facility = models.ForeignKey(
        Facility,
        related_name="pops",
        on_delete=models.CASCADE)

    availability_zone = models.ForeignKey(
        AvailabilityZone,
        related_name="pops",
        null=True,
        on_delete=models.CASCADE)

    def __str__(self) -> str:
        """PoP string representation"""
        return str(self.name)

    def __repr__(self) -> str:
        """Make representation"""
        return f"<PointOfPresence id='{self.pk}' name='{self.name}'>"

    class Meta:
        verbose_name = "Point Of Presence"

#
# Products
#
class ProductOffering(PolymorphicModel):
    """
    The product catalog is flattened into a product offerings
    list. This might be a facade to a more complex database
    model though.
    """
    name = models.CharField(max_length=160)
    display_name = models.CharField(max_length=256)

    resource_type = enumfields.EnumField(
        ResourceTypes, max_length=64)

    handover_metro_area_network = models.ForeignKey(
        MetroAreaNetwork,
        on_delete=models.CASCADE,
        related_name="handover_product_offerings")

    handover_metro_area = models.ForeignKey(
        MetroArea,
        on_delete=models.CASCADE,
        related_name="handover_product_offerings")

    bandwidth_min = models.PositiveIntegerField(null=True, blank=True)
    bandwidth_max = models.PositiveIntegerField(null=True, blank=True)

    physical_port_speed = models.PositiveIntegerField(null=True, blank=True)
    physical_media_type = enumfields.EnumField(
        MediaType,
        max_length=24,
        default=MediaType.TYPE_10GBASE_LR)

    # Note - this [sh/c]ould be a foreign key
    service_provider = models.CharField(max_length=128)

    downgrade_allowed = models.BooleanField()
    upgrade_allowed = models.BooleanField()

    # Cancellation policy
    cancellation_notice_period = RelativeDeltaField(
        default=relativedelta(months=1))
    cancellation_charge_period = RelativeDeltaField(
        default=relativedelta(days=1))


    @property
    def notice_period(self):
        """Alias of cancellation_notice_period"""
        value = self.cancellation_notice_period
        if isinstance(value, relativedelta):
            return relativedelta_to_iso(value)

        return value

    def cancellation_policy(
            self,
            started_at : date,
            decommission_at : Optional[date] = None,
        ):
        """
        Calculate decommission policy dates based on the day
        the billing for this product offering started,
        notice and charge period, for a given date.

        If the decommission_at parameter is None, the current
        day will be used.

        :param started_at: Billing for this product started
            at this day.
        :param decommission_at: The product shall be decommissioned
            at this day.
        """
        today = date.today()
        notice_period = self.cancellation_notice_period

        if not decommission_at:
            decommission_at = today

        # We don't allow for decommission requests in the past
        if decommission_at < today:
            decommission_at = today

        started_at = date(started_at.year, started_at.month, 1)

        # When we have a daily notice period, the next
        # cancellation will be the requested decommission
        # day + notice period.
        if notice_period.days > 0:
            if today + notice_period > decommission_at:
                decommission_at = today + notice_period

        else:
            # Handle full month notice periods: The decommission
            # is dependent on the starting month.
            next_decommission = started_at
            while next_decommission < decommission_at:
                next_decommission += notice_period
            decommission_at = next_decommission

            if today + notice_period > decommission_at:
                decommission_at += notice_period


        # The service will be charged in a given interval. If this
        # is daily, the charged_until will be equal to the
        # decommission date. It will never be before the decommission
        # date.
        charged_until = started_at
        while charged_until < decommission_at:
            charged_until += self.cancellation_charge_period

        return {
            "decommission_at": decommission_at,
            "charged_until": charged_until,
        }

    def __repr__(self) -> str:
        """ProductOffering representation"""
        return "<{} id={}>".format(self.__class__.__name__, self.pk)

    def __str__(self) -> str:
        """ProductOffering to str"""
        type_name = self.__class__.__name__
        offering_name = self.display_name
        if not offering_name:
            offering_name = type_name

        return f"{offering_name} ({self.pk})"


class VLanProductOfferingMixin(models.Model):
    """VLan Product Offering Trait"""
    provider_vlans = enumfields.EnumField(
        ProviderVlanTypes, max_length=64)

    service_metro_area_network = models.ForeignKey(
        MetroAreaNetwork,
        related_name="service_metro_area_network_%(class)ss",
        on_delete=models.CASCADE)
    service_metro_area = models.ForeignKey(
        MetroArea,
        related_name="service_metro_area_%(class)ss",
        on_delete=models.CASCADE)

    class Meta:
        abstract = True


class ConnectionProductOffering(ProductOffering):
    """A connection product offering"""
    cross_connect_initiator = enumfields.EnumField(
        CrossConnectInitiator, max_length=96)

    handover_pop = models.ForeignKey(
        PointOfPresence,
        null=True,
        on_delete=models.CASCADE)

    @property
    def all_connection_required_contact_roles(self):
        """Get required contact roles"""
        return Role.objects.filter(name__in=["implementation"])


    class Meta:
        verbose_name = "Connection Product"

    __polymorphic_type__ = PRODUCT_TYPE_CONNECTION


class ExchangeLanNetworkProductOffering(
        VLanProductOfferingMixin,
        ProductOffering):
    """Exchange Lan Network Product"""
    class Meta:
        verbose_name = "ExchangeLAN Network Product"

    @property
    def exchange_lan_network_service(self):
        """Retrieve the related exchange lan network service"""
        # Usually between the exchange lan network product
        # offering and network service is a 1:1 relation.
        # However, a M:N relation already exists (see network_services
        # property of a product offering)
        return self.network_services.first()

    __polymorphic_type__ = PRODUCT_TYPE_EXCHANGE_LAN


class P2PNetworkProductOffering(
        VLanProductOfferingMixin,
        ProductOffering):
    """P2P Network Product"""

    class Meta:
        verbose_name = "PointToPoint Network Product"

    __polymorphic_type__ = PRODUCT_TYPE_P2P


class P2MPNetworkProductOffering(
        VLanProductOfferingMixin,
        ProductOffering):
    """P2MP Network Product"""
    class Meta:
        verbose_name = "PointToMultiPoint Network Product"

    __polymorphic_type__ = PRODUCT_TYPE_P2MP


class MP2MPNetworkProductOffering(
        VLanProductOfferingMixin,
        ProductOffering):
    """MP2MP Network Product"""
    class Meta:
        verbose_name = "MultiPointToMultiPoint Network Product"

    __polymorphic_type__ = PRODUCT_TYPE_MP2MP


class CloudNetworkProductOffering(
        VLanProductOfferingMixin,
        ProductOffering):
    """Cloud Network Product"""
    service_provider_workflow = enumfields.EnumField(
        ServiceProviderWorkflow,
        max_length=64)
    service_provider_region = models.CharField(null=True, max_length=128)

    service_provider_pop = models.CharField(
        null=True,
        max_length=128)

    delivery_method = enumfields.EnumField(DeliveryMethod, max_length=64)

    cloud_key = models.CharField(null=True, max_length=256)

    # This is populated from the product offering
    diversity = models.PositiveIntegerField(default=1)

    class Meta:
        verbose_name = "Cloud Network Product"

    __polymorphic_type__ = PRODUCT_TYPE_CLOUD


PRODUCT_OFFERING_MODELS = {
    PRODUCT_TYPE_CONNECTION: ConnectionProductOffering,
    PRODUCT_TYPE_EXCHANGE_LAN: ExchangeLanNetworkProductOffering,
    PRODUCT_TYPE_P2P: P2PNetworkProductOffering,
    PRODUCT_TYPE_P2MP: P2MPNetworkProductOffering,
    PRODUCT_TYPE_MP2MP: MP2MPNetworkProductOffering,
    PRODUCT_TYPE_CLOUD: CloudNetworkProductOffering,
}

