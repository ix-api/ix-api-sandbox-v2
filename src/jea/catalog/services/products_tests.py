
"""
Test Products Service
"""

import pytest
from model_bakery import baker
from ixapi_schema.v2.schema import (
    NETWORK_SERVICE_TYPE_EXCHANGE_LAN,
    NETWORK_SERVICE_TYPE_P2P,
    NETWORK_SERVICE_TYPE_P2MP,
    NETWORK_SERVICE_TYPE_MP2MP,
    NETWORK_SERVICE_TYPE_CLOUD,
)

from jea.exceptions import ValidationError
from jea.catalog.services import (
    products as products_svc,
)

@pytest.mark.django_db
def test_get_product_offerings():
    """Just list all products"""
    product = baker.make("catalog.ExchangeLanNetworkProductOffering")
    products = products_svc.get_product_offerings()

    assert product in products


@pytest.mark.django_db
def test_list_products_filter_type():
    """List products of a specific type"""
    exchange_lan = baker.make("catalog.ExchangeLanNetworkProductOffering")
    p2p = baker.make("catalog.P2PNetworkProductOffering")
    p2mp = baker.make("catalog.P2MPNetworkProductOffering")
    mp2mp = baker.make("catalog.MP2MPNetworkProductOffering")
    cloud = baker.make("catalog.CloudNetworkProductOffering")

    # Exchange LAN
    offerings = products_svc.get_product_offerings(
        filters={"type": NETWORK_SERVICE_TYPE_EXCHANGE_LAN})
    assert exchange_lan in offerings
    assert not p2p in offerings
    assert not p2mp in offerings
    assert not mp2mp in offerings
    assert not cloud in offerings

    # P2P
    offerings = products_svc.get_product_offerings(
        filters={"type": NETWORK_SERVICE_TYPE_P2P})
    assert not exchange_lan in offerings
    assert p2p in offerings
    assert not p2mp in offerings
    assert not mp2mp in offerings
    assert not cloud in offerings

    # P2MP
    offerings = products_svc.get_product_offerings(
        filters={"type": NETWORK_SERVICE_TYPE_P2MP})
    assert not exchange_lan in offerings
    assert not p2p in offerings
    assert p2mp in offerings
    assert not mp2mp in offerings
    assert not cloud in offerings

    # MP2MP
    offerings = products_svc.get_product_offerings(
        filters={"type": NETWORK_SERVICE_TYPE_MP2MP})
    assert not exchange_lan in offerings
    assert not p2p in offerings
    assert not p2mp in offerings
    assert mp2mp in offerings
    assert not cloud in offerings

    # Cloud
    offerings = products_svc.get_product_offerings(
        filters={"type": NETWORK_SERVICE_TYPE_CLOUD})
    assert not exchange_lan in offerings
    assert not p2p in offerings
    assert not p2mp in offerings
    assert not mp2mp in offerings
    assert cloud in offerings


@pytest.mark.django_db
def test_list_products_filters():
    """List products of a specific type"""
    mtt_metro_area = baker.make("catalog.MetroArea")
    ttm_metro_area = baker.make("catalog.MetroArea")
    exchange_lan = baker.make(
        "catalog.ExchangeLanNetworkProductOffering",
        service_metro_area=mtt_metro_area,
        handover_metro_area=ttm_metro_area)
    mp2mp = baker.make(
        "catalog.MP2MPNetworkProductOffering")
    p2p = baker.make(
        "catalog.P2PNetworkProductOffering")
    cloud_foo = baker.make(
        "catalog.CloudNetworkProductOffering",
        service_provider_region="foo-centraal-2")
    cloud_bar = baker.make(
        "catalog.CloudNetworkProductOffering",
        service_provider_region="bar-centraal-2")

    offerings = products_svc.get_product_offerings(filters={
        "service_provider_region": "foo-centraal-2"})
    assert not exchange_lan in offerings
    assert not mp2mp in offerings
    assert not p2p in offerings
    assert cloud_foo in offerings
    assert not cloud_bar in offerings

    offerings = products_svc.get_product_offerings(filters={
        "service_metro_area": mtt_metro_area.pk})
    assert exchange_lan in offerings
    assert not mp2mp in offerings
    assert not p2p in offerings
    assert not cloud_bar in offerings
    assert not cloud_foo in offerings

    offerings = products_svc.get_product_offerings(filters={
        "handover_metro_area": ttm_metro_area.pk})
    assert exchange_lan in offerings
    assert not mp2mp in offerings
    assert not p2p in offerings
    assert not cloud_bar in offerings
    assert not cloud_foo in offerings


@pytest.mark.django_db
def test_assert_capacity_constraints():
    """Test capacity validation"""
    offering = baker.make(
        "catalog.CloudNetworkProductOffering",
        bandwidth_min=10,
        bandwidth_max=100)

    products_svc.assert_capacity_constraints(
        product_offering=offering,
        capacity=None)
    products_svc.assert_capacity_constraints(
        product_offering=offering,
        capacity=10)
    products_svc.assert_capacity_constraints(
        product_offering=offering,
        capacity=50)
    products_svc.assert_capacity_constraints(
        product_offering=offering,
        capacity=100)

    with pytest.raises(ValidationError):
        products_svc.assert_capacity_constraints(
            product_offering=offering,
            capacity=1)

    with pytest.raises(ValidationError):
        products_svc.assert_capacity_constraints(
            product_offering=offering,
            capacity=101)

    # No upper limit
    offering = baker.make(
        "catalog.CloudNetworkProductOffering",
        bandwidth_min=10,
        bandwidth_max=None)

    products_svc.assert_capacity_constraints(
        product_offering=offering,
        capacity=101)

    with pytest.raises(ValidationError):
        products_svc.assert_capacity_constraints(
            product_offering=offering,
            capacity=1)

    # No lower limit
    offering = baker.make(
        "catalog.CloudNetworkProductOffering",
        bandwidth_min=None,
        bandwidth_max=100)

    products_svc.assert_capacity_constraints(
        product_offering=offering,
        capacity=1)

    with pytest.raises(ValidationError):
        products_svc.assert_capacity_constraints(
            product_offering=offering,
            capacity=101)

    # No limit whatsoever
    offering = baker.make(
        "catalog.CloudNetworkProductOffering",
        bandwidth_min=None,
        bandwidth_max=None)

    products_svc.assert_capacity_constraints(
        product_offering=offering,
        capacity=1)

    products_svc.assert_capacity_constraints(
        product_offering=offering,
        capacity=1000000)

    # Check zero constraint
    with pytest.raises(ValidationError):
        products_svc.assert_capacity_constraints(
            product_offering=offering,
            capacity=0)


@pytest.mark.django_db
def test_assert_valid_cloud_key_provider_first():
    """Test cloud key validation"""
    # Provider first
    offering = baker.make(
        "catalog.CloudNetworkProductOffering",
        cloud_key="demo_cloud_key",
        service_provider_workflow="provider_first")
    products_svc.assert_valid_cloud_key(
        product_offering=offering,
        cloud_key="demo_cloud_key")

    with pytest.raises(ValidationError):
        products_svc.assert_valid_cloud_key(
            product_offering=offering,
            cloud_key=None)

    with pytest.raises(ValidationError):
        products_svc.assert_valid_cloud_key(
            product_offering=offering,
            cloud_key="foo")


@pytest.mark.django_db
def test_assert_valid_cloud_key_exchange_first():
    """Test cloud key validation"""
    # Provider first
    offering = baker.make(
        "catalog.CloudNetworkProductOffering",
        service_provider_workflow="exchange_first")
    products_svc.assert_valid_cloud_key(
        product_offering=offering,
        cloud_key="DEMOadc83b19e793491b1c6ea0fd8b46cd9f32e592fc")

    with pytest.raises(ValidationError):
        products_svc.assert_valid_cloud_key(
            product_offering=offering, cloud_key=None)
        products_svc.assert_valid_cloud_key(
            product_offering=offering, cloud_key="")
        products_svc.assert_valid_cloud_key(
            product_offering=offering, cloud_key="foo")
