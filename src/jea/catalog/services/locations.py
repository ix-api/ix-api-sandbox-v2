"""
Locations Service

Manages metro areas and metro area networks
"""

from jea.catalog.models import (
    MetroArea,
    MetroAreaNetwork,
)
from jea.catalog.filters import (
    MetroAreaFilter,
    MetroAreaNetworkFilter,
)


def get_metro_areas(filters=None):
    """Get all metro areas"""
    if filters:
        return MetroAreaFilter(filters).qs
    return MetroArea.objects.all()


def get_metro_area(metro_area=None):
    """Get MetroArea"""
    if isinstance(metro_area, MetroArea):
        return metro_area
    return MetroArea.objects.get(pk=metro_area)


def get_metro_area_networks(filters=None):
    """Get metro area networks"""
    if filters:
        return MetroAreaNetworkFilter(filters).qs
    return MetroAreaNetwork.objects.all()


def get_metro_area_network(metro_area_network=None):
    """Get a metro area network"""
    if isinstance(metro_area_network, MetroAreaNetwork):
        return metro_area_network
    return MetroAreaNetwork.objects.get(pk=metro_area_network)
