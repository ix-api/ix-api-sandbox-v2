
"""
Devices Service Tests
"""

import pytest
from model_bakery import baker

from jea.catalog.services import devices as devices_svc


@pytest.mark.django_db
def test_get_devices():
    """Test listing all devices"""
    device = baker.make("catalog.Device")
    devices = devices_svc.get_devices()
    assert device in devices


@pytest.mark.django_db
def test_list_devices_filter_physical_facility():
    """Test filtering device by physical facility id"""
    facility = baker.make("catalog.Facility")
    pop = baker.make("catalog.PointOfPresence", facility=facility)
    device1 = baker.make("catalog.Device", pop=pop)
    device2 = baker.make("catalog.Device")

    devices = devices_svc.get_devices(filters={
        "facility": facility.pk,
    })

    assert devices.count() == 1
    assert device1 in devices
    assert device2 not in devices


@pytest.mark.django_db
def test_get_device_by_id():
    """Test getting a device by it's primary key"""
    device = baker.make("catalog.Device")
    device_ = devices_svc.get_device(device=str(device.id))
    assert device.id == device_.id
