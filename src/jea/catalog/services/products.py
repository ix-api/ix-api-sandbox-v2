
"""
Products Service
"""

from typing import Optional

from django.db.models.query import QuerySet
from ixapi_schema.v2.constants.catalog import (
    ServiceProviderWorkflow,
)

from jea.catalog.models import (
    ProductOffering,
)
from jea.catalog.filters import (
    ProductOfferingFilter,
)
from jea.catalog.exceptions import (
    CloudKeyInvalid,
)
from jea.exceptions import ValidationError


def get_product_offerings(scoping_account=None, filters=None) -> QuerySet:
    """
    List and filter products.
    This returns a polymorphic query set including:
     - ExchangeLanProducts,
     - VirtualCircuit Products,
     - CloudProducts
    """
    filtered = ProductOfferingFilter(filters)
    return filtered.qs


def get_product_offering(
        scoping_account=None,
        product_offering=None,
    ) -> ProductOffering:
    """
    Retrieve a single product identified by a unique
    property. In this case: The primary key / id.

    :param product_offering: The id of the product to fetch.
    :raises Product.DoesNotExist: When the product is not found
    """
    if isinstance(product_offering, ProductOffering):
        # Maybe trigger a refresh here?
        return product_offering

    return ProductOffering.objects.get(pk=product_offering)


def assert_valid_cloud_key(
        scoping_account=None,
        product_offering=None,
        cloud_key=None,
    ):
    """
    Validate the cloud key: In a provider first scenario
    the cloud key may be an empty string,
    for exchange first workflows the cloud key must be an
    API key for interacting with the providers API.
    """
    if not cloud_key:
        raise CloudKeyInvalid  # May not be empty

    offering = get_product_offering(
        scoping_account=scoping_account,
        product_offering=product_offering)

    workflow = offering.service_provider_workflow

    if workflow == ServiceProviderWorkflow.PROVIDER_FIRST:
        # DEMO mode:
        # Provider first cloud key must be the same as the
        # one associated with the product.
        if cloud_key != offering.cloud_key:
            raise CloudKeyInvalid
    else:
        # Exchange First Worflow:
        # DEMO mode:
        # Validate exchange first cloud keys by pattern.
        if not cloud_key.startswith("DEMO"):
            raise CloudKeyInvalid


def assert_capacity_constraints(
        scoping_account=None,
        product_offering=None,
        capacity=None,
    ):
    """
    Make sure the capacity provided is compatible with
    the required bandwidth in the product offering.

    :raises ValidationError: When the constraints are violated.

    :param scoping_account: A scope limiting account
    :param product_offering: The product offering to check
    :param capacity: The provided capacity
    """
    product_offering = get_product_offering(
        scoping_account=scoping_account,
        product_offering=product_offering)

    if capacity is None:
        return # Maximum capacity is allowed

    # However, when a capacity is provided is must be > 0
    if capacity <= 0:
        raise ValidationError(
            "The capacity must be > 0 or null",
            field="capacity")

    # Check minimum capacity
    if product_offering.bandwidth_min is not None:
        if capacity < product_offering.bandwidth_min:
            raise ValidationError(
                ("capacity is not sufficient for mimimum "
                 "bandwidth requirement."),
                field="capacity")

    if product_offering.bandwidth_max is not None:
        if capacity > product_offering.bandwidth_max:
            raise ValidationError(
                "capacity is above maximum bandwidth requirement.",
                field="capacity")

