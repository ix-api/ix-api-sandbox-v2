
"""
Points Of Presence Service
"""

from typing import Optional

from django.db.models.query import QuerySet

from jea.catalog.exceptions import MediaTypeUnavailable
from jea.catalog.models import (
    PointOfPresence,
)
from jea.catalog.filters import (
    PointOfPresenceFilter,
)


def get_pops(scoping_account=None, filters=None) -> QuerySet:
    """
    List all pops - apply filters.

    :param scoping_account: The current account
    :param filters: A dict of filter criteria
    """
    return PointOfPresenceFilter(filters).qs


def get_pop(
        scoping_account=None,
        pop=None) -> PointOfPresence:
    """
    Get a specific pop.
    Implemented lookup methods are:
     - pop_id

    :param pop: The pop to lookup
    :raises PointOfPresence.DoesNotExist: When pop is not found
    """
    if isinstance(pop, PointOfPresence):
        return pop

    return PointOfPresence.objects.get(pk=pop)


def get_media_type_availability(
        scoping_account=None,
        pop=None,
        media_type=None):
    """
    Count ports with given properties available on a dmearc.

    :param pop: The PoP to query
    :param media_type: The connection type e.g. 10GBASE-LR

    :raises MediaTypeUnavailable: When no device could be found.
    """
    if not media_type:
        raise MediaTypeUnavailable()

    pop = get_pop(
        pop=pop,
        scoping_account=scoping_account)

    # Filter devices with connection type
    devices = pop.devices \
        .filter(capabilities__media_type__iexact=media_type) \
        .prefetch_related("capabilities")

    if not devices.exists():
        raise MediaTypeUnavailable()

    # Let's sum the availabilities
    count = 0
    for device in devices:
        capabilities = device.capabilities \
            .filter(media_type__iexact=media_type)
        count += sum(cap.availability_count for cap in capabilities)

    return count
