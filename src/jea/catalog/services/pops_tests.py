
import pytest
from model_bakery import baker

from jea.catalog.models import (
    MediaType,
)
from jea.catalog.exceptions import MediaTypeUnavailable
from jea.catalog.services import pops as pops_svc


@pytest.mark.django_db
def test_get_pops():
    """Test listing points of presence"""
    pop = baker.make("catalog.PointOfPresence")
    pops = pops_svc.get_pops()
    assert pop in pops


@pytest.mark.django_db
def test_list_pops_filter_facility():
    """Test filtering by facility id"""
    facility = baker.make("catalog.Facility")
    pop = baker.make("catalog.PointOfPresence", facility=facility)

    pops = pops_svc.get_pops(filters={
        "facility": facility.pk,
    })

    assert pops.count() == 1
    assert pop in pops



@pytest.mark.django_db
def test_get_pops_by_id():
    """Test retrieving a pop by id"""
    pop = baker.make("catalog.PointOfPresence")
    pop_ = pops_svc.get_pop(pop=str(pop.id))
    assert pop.id == pop_.id


@pytest.mark.django_db
def test_get_media_type_availability():
    """Test counting available ports with given capabilities"""
    pop = baker.make("catalog.PointOfPresence")
    device_a = baker.make("catalog.Device",
                          pop=pop)
    baker.make("catalog.DeviceCapability",
               device=device_a,
               media_type="10GSTUFF",
               availability_count=10)

    baker.make("catalog.DeviceCapability",
               device=device_a,
               media_type="100GSTUFF",
               availability_count=1)

    device_b = baker.make(
        "catalog.Device",
        pop=pop)
    baker.make("catalog.DeviceCapability",
               device=device_b,
               media_type="10GSTUFF",
               availability_count=13)

    count = pops_svc.get_media_type_availability(
        pop=pop,
        media_type="10GSTUFF",
    )
    assert count == 23

    count = pops_svc.get_media_type_availability(
        pop=pop,
        media_type="100GSTUFF",
    )
    assert count == 1


@pytest.mark.django_db
def test_get_media_type_availability__invalid_media_type():
    """Try getting the capabilities of an unknown media type"""
    pop = baker.make("catalog.PointOfPresence")
    device_a = baker.make("catalog.Device",
                          pop=pop)
    baker.make("catalog.DeviceCapability",
               device=device_a,
               media_type="10GBASE-LR",
               availability_count=10)

    with pytest.raises(MediaTypeUnavailable):
        pops_svc.get_media_type_availability(
            pop=pop,
            media_type="100GFOO-MX")

