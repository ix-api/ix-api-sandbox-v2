
from jea.exceptions import ValidationError


class MediaTypeUnavailable(ValidationError):
    default_detail = ("The requsted media type is not available "
                      "on any device reachable from the pop.")
    default_code = "media_type_unavailable"
    default_field = "media_type"


class CloudKeyInvalid(ValidationError):
    default_detail = "The provided cloud key was invalid."
    default_field = "cloud_key"
