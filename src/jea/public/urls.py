
from django.urls import path

from jea.public import views

urlpatterns = [
    path(r"", views.index_page),
]

