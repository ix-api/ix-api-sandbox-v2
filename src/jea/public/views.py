
"""
JEA :: Sandbox Public Interface

While this is most likely only useful in development,
it is nice to provide some root level entry page to the project.
"""

from django.shortcuts import render

from ixapi_schema.v2 import __version__ as SCHEMA_VERSION

from backend.settings import VERSION as SANDBOX_VERSION

def index_page(request):
    """Render welcome / entry / root page"""

    return render(request, "jea/index/index.html", {
        "schema_version": SCHEMA_VERSION,
        "sandbox_version": SANDBOX_VERSION,
    })

