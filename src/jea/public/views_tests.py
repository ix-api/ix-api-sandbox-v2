
from django.test.client import RequestFactory

from jea.public.views import index_page


def test_index_page():
    """Test render index page"""
    request = RequestFactory().get('/')
    response = index_page(request)
    assert response.status_code == 200
