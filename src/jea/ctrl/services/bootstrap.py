"""
The Sanbox Bootstrapping Service
--------------------------------

Populates the catalog and services with
everything our demo exchange needs.
"""

import re
import time
import json
import random
import secrets
import ipaddress

from faker import Faker
from dateutil.relativedelta import relativedelta
from django.utils import text as text_utils
from django.db import transaction
from ixapi_schema.v2.constants.catalog import (
    DeliveryMethod,
    ProviderVlanTypes,
    ResourceTypes,
    ServiceProviderWorkflow,

    RESOURCE_TYPE_NETWORK_SERVICE,
    DELIVERY_METHOD_DEDICATED,
    DELIVERY_METHOD_SHARED,
)

from utils.prompt import (
    progress_exec_tasks,
    task,
)
from jea.crm.services import contacts as contacts_svc
from jea.auth import models as auth_models
from jea.config import models as access_models
from jea.catalog import models as catalog_models
from jea.crm import models as crm_models
from jea.service import models as service_models
from jea.ipam import models as ipam_models
from jea.stateful import models as stateful_models
from jea.stateful.models import State
from jea.ctrl.services import (
    snapshot as snapshot_svc,
    secrets as secrets_svc,
)


def generate_airport_code():
    """Make some airport code"""
    faker = Faker()
    return "".join(faker.random_letters(3)).upper()


def generate_ix_name():
    """Generate a fake ix name"""
    faker = Faker()
    phrase = faker.catch_phrase().split(" ")
    suffix = random.choice(["AB", "Inc", "GmbH", "AG"])
    prefix = " ".join(phrase[:2]).title()
    net = random.choice(["Networks", "Connectivity", "Communications"])

    return f"{prefix} {net} {suffix}"


def generate_dc_name():
    """Generate a fake ix name"""
    faker = Faker()
    phrase = faker.catch_phrase().split(" ")
    suffix = faker.company_suffix()
    prefix = " ".join(phrase[:1]).title()
    datacenter = random.choice(["Datacenters", "Dataction", "Data Operations"])

    return f"{prefix} {datacenter} {suffix}"


def generate_pop_name(facility, exchange_name):
    """Generate a name for a point of presence"""
    ex_tokens = exchange_name.split()
    ex_name = ex_tokens[0].lower()

    fac_tokens = facility.name.split()
    fac_name = fac_tokens[-1].lower()
    pop_count = facility.pops.count()

    return "{}-{}-{}".format(fac_name, ex_name, pop_count + 1)


def generate_exchange_lan_name(exchange_name, metro_area_network):
    """Generate exchange lan name"""
    prefix = "-".join(exchange_name.split(" ")[:2]).upper()
    metro_area = metro_area_network.metro_area.iata_code
    return f"{prefix}-{metro_area}"


def generate_device_name(pop, exchange_name):
    """Generate a name for a device in a facility"""
    # Naming schema:
    #  <dev>.<fac>.<metro>.<exchange>.com
    dev_count = pop.facility.devices.count()
    fac_tokens = pop.facility.name.split()
    fac_name = fac_tokens[-1].lower()
    return "device{}.{}.{}.{}.com".format(
        dev_count + 1,
        fac_name,
        pop.facility.metro_area.iata_code.lower(),
        text_utils.slugify(exchange_name))


def generate_asn():
    """Make some fake private ASN"""
    return random.randint(64512, 65534)


def generate_email(user=None):
    """Create some email address with an optional fixed user"""
    faker = Faker()
    if not user:
        return faker.email()

    return user + "@" + faker.domain_name()


def generate_pbox_number():
    """Generate some post office box number"""
    faker = Faker()
    return "PB " + re.sub(r"\W", ".", faker.phone_number())


def generate_vat_number():
    """Generate a vat number"""
    faker = Faker()
    return faker.country_code() + " " + str(faker.random_number(12, True))


def get_account_domain(account):
    """Make up some domain from the account name"""
    tokens = account.name.split(" ")
    prefix = "-".join(tokens[:2]).lower()

    return f"{prefix}.net"


def create_random_ip4_prefix(account, fqdn=None):
    """Generate a random ip v4 prefix"""
    if not fqdn:
        fqdn = "net.noc.provider.example.net"

    addr = [str(random.randint(100, 150)),
            str(random.randint(0, 200)),
            str(random.randint(0, 200)),
            "0"]

    ip_addr = ipam_models.IpAddress(
        version=4,
        address=".".join(addr),
        prefix_length=24,
        fqdn=fqdn,
        ixp_allocated=True,
        managing_account=account,
        consuming_account=account)
    ip_addr.save()
    return ip_addr


def create_random_ip6_prefix(account, fqdn=None):
    """Create a random ip prefix"""
    if not fqdn:
        fqdn = "net.noc.provider.example.net"

    addr = "2001:" + secrets.token_hex(2) + "::"

    ip_addr = ipam_models.IpAddress(
        version=6,
        address=addr,
        prefix_length=64,
        fqdn=fqdn,
        ixp_allocated=True,
        managing_account=account,
        consuming_account=account)
    ip_addr.save()
    return ip_addr


def create_random_host(account, net, fqdn=None):
    """Create a random host in network"""
    cidr = f"{net.address}/{net.prefix_length}"
    ip_net = ipaddress.ip_network(cidr)

    max_hosts = 254
    host_generator = ip_net.hosts()

    hosts = [host_generator.send(None)
             for _ in range(max_hosts)]

    faker = Faker()
    host_ip = faker.random_element(hosts)

    prefix_length = 32
    if net.version == 6:
        prefix_length = 128

    ip_addr = ipam_models.IpAddress(
        version=net.version,
        fqdn=fqdn,
        ixp_allocated=True,
        prefix_length=prefix_length,
        address=str(host_ip),
        managing_account=account,
        consuming_account=account)
    ip_addr.save()
    return ip_addr


@transaction.atomic
def _clear_model(model):
    """Clear a model"""
    for mdl in model.objects.all().order_by("-pk"):
        mdl.delete()


@task("nop")
def nop(_options):
    """Do nothing."""
    # If a task is done too fast, the progress indicator
    # glitches and keeps hanging. Maybe a racecondition.
    time.sleep(0.25)


@task()
def clear_database(_options):
    """Remove all exchange data"""
    # Remove all changes if there are any left
    _clear_model(stateful_models.Change)

    # Deleting all users
    auth_models.User.objects \
        .exclude(is_superuser=True).delete()

    # Clear access
    _clear_model(access_models.NetworkFeatureConfig)
    _clear_model(access_models.NetworkServiceConfig)
    _clear_model(access_models.Port)
    _clear_model(access_models.Connection)

    # Clear services
    _clear_model(service_models.NetworkFeature)
    _clear_model(service_models.NetworkService)

    # Clear catalog
    _clear_model(catalog_models.ProductOffering)
    _clear_model(catalog_models.PointOfPresence)
    _clear_model(catalog_models.DeviceCapability)
    _clear_model(catalog_models.Device)
    _clear_model(catalog_models.Facility)
    _clear_model(catalog_models.FacilityOperator)
    _clear_model(catalog_models.CloudProvider)
    _clear_model(catalog_models.MetroArea)
    _clear_model(catalog_models.MetroAreaNetwork)

    # IPAM
    _clear_model(ipam_models.IpAddress)
    _clear_model(ipam_models.MacAddress)

    # Clear accounts and contacts
    _clear_model(crm_models.RoleAssignment)
    _clear_model(crm_models.Contact)

    # Clear accounts
    for acc in crm_models.Account.objects.all():
        acc.scoping_account = None
        acc.save()

    _clear_model(crm_models.Account)



#
# Accounts
#
def _create_noc_contact(account):
    """Create a noc contact"""
    noc_role = contacts_svc.get_default_role("noc")
    faker = Faker()
    contact = crm_models.Contact(
        telephone=faker.phone_number(),
        email=generate_email("noc"),
        scoping_account=account.scoping_account,
        consuming_account=account,
        managing_account=account)
    contact.save()
    assignment = crm_models.RoleAssignment(
        contact=contact,
        role=noc_role)
    assignment.save()

    return contact


def _create_implementation_contact(account):
    """Create an implementation contact"""
    faker = Faker()

    impl_role = contacts_svc.get_default_role("implementation")
    contact = crm_models.Contact(
        name=faker.name(),
        email=faker.email(),
        telephone=faker.phone_number(),
        scoping_account=account.scoping_account,
        managing_account=account,
        consuming_account=account)
    contact.save()
    assignment = crm_models.RoleAssignment(
        role=impl_role,
        contact=contact)
    assignment.save()

    return contact


def _create_contact(account, roles):
    """Create a contact and assign all roles"""
    faker = Faker()
    contact = crm_models.Contact(
        name=faker.name(),
        email=faker.email(),
        telephone=faker.phone_number(),
        scoping_account=account.scoping_account,
        managing_account=account,
        consuming_account=account)
    contact.save()

    for role in roles:
        assignment = crm_models.RoleAssignment(
            contact=contact,
            role=role)
        assignment.save()

    return contact


def _create_address():
    """Create some address data"""
    faker = Faker()
    address = crm_models.Address(
        country=faker.country_code(),
        locality=faker.city(),
        region=faker.country_code(),
        street_address=faker.street_address(),
        postal_code=faker.postalcode(),
        post_office_box_number=None)
    address.save()
    return address


def _create_billing_info(account_name):
    """Create a billing information contact"""
    address = _create_address()
    billing = crm_models.BillingInformation(
        name=account_name,
        address=address,
        vat_number=generate_vat_number())
    billing.save()
    return billing


@task("public_account")
def create_public_account(_options):
    """
    Create a account discoverable through the API
    """
    # Create account name
    account_name = generate_ix_name()

    address = _create_address()
    billing = _create_billing_info(account_name)

    account = crm_models.Account(
        state=State.REQUESTED,
        name=account_name,
        billing_information=billing,
        address=address,
        discoverable=True,
        external_ref="discoverable_account")
    account.save()

    # Add all contacts
    _create_contact(account, roles=[
        contacts_svc.get_default_role("noc"),
        contacts_svc.get_default_role("implementation"),
    ])

    account.state = State.PRODUCTION
    account.save()

    return account


@task("ix_account")
def create_ix_account(options):
    """Create the IX's own account"""
    address = _create_address()
    billing = _create_billing_info(options["exchange_name"])
    account = crm_models.Account(
        address=address,
        billing_information=billing,
        state=State.REQUESTED,
        name=options["exchange_name"],
        external_ref="demo_ix")
    account.save()

    # Add some contacts
    _create_noc_contact(account)
    _create_implementation_contact(account)

    account.state = State.PRODUCTION
    account.save()

    return account


@task("ix_account")
def load_root_account(_options):
    """Get the IX's own account"""
    account = crm_models.Account.objects.order_by("id").first()
    assert account, "IX account not found. Sandbox bootstrapped?"
    assert not account.managing_account, \
        "IX account should not have a managing_account."
    return account


@task("api_user")
def create_reseller_account(options, state):
    """Create the reseller account"""
    address = _create_address()
    billing = _create_billing_info(options["api_account"])
    account = crm_models.Account(
        state=State.REQUESTED,
        managing_account=state["ix_account"],
        billing_information=billing,
        address=address,
        name=options["api_account"],
        external_ref="demo_reseller")
    account.save()

    # We are our own scope
    account.scoping_account = account
    account.save()

    # Add some contacts
    _create_noc_contact(account)
    _create_implementation_contact(account)

    # Create api access
    params = {
        "username": text_utils.slugify(account.name),
        "account": account,
    }
    if options["api_key"]:
        params["api_key"] = options["api_key"]
    if options["api_secret"]:
        params["api_secret"] = options["api_secret"]

    user = auth_models.User(**params)
    user.save()

    account.state = State.PRODUCTION
    account.save()

    return user


@task("account")
def create_subaccount1(options, state):
    """Create first subaccount"""
    return create_subaccount(options, state)


@task("subaccount2")
def create_subaccount2(options, state):
    """Create second subaccount"""
    return create_subaccount(options, state)


def create_subaccount(options, state):
    """
    Create a subaccount for the reseller.
    Add ports and connections.
    """
    faker = Faker()

    reseller = state["api_user"].account

    address = _create_address()
    account = crm_models.Account(
        state=State.PRODUCTION,
        managing_account=reseller,
        scoping_account=reseller,
        address=address,
        name=generate_ix_name(),
        external_ref="via reseller")
    account.save()

    # Create user with authentication capabilities
    username = text_utils.slugify(account.name)[:16]
    password = faker.first_name().lower() + faker.numerify("#")

    # For now we store the "password" as plaintext in the
    # api secret attribute of the user in order to show it
    # after the bootstrap process. This is unacceptable in a
    # real world application, however this is a sandbox.
    user = auth_models.User(
        username=username,
        account=account,
        api_secret=password)
    user.set_password(password)
    user.save()

    # Add some contacts
    _create_contact(account, roles=[
        contacts_svc.get_default_role("implementation"),
        contacts_svc.get_default_role("noc"),
    ])

    return account

#
# Catalog
# - Facilities
#

def _create_facility(operator=None, metro_area=None):
    """Create a facility"""
    faker = Faker()

    if not operator:
        operator = catalog_models.FacilityOperator(
            name=generate_dc_name())
        operator.save()

    facility_name = " ".join(operator.name.split(" ")[:2]) \
        + " DC" + str(random.randint(1, 100))

    facility_input = {
        "name": facility_name,
        "metro_area": metro_area,
        "address_country": faker.country_code(),
        "address_locality": faker.city(),
        "address_region": faker.country_code(),
        "postal_code": faker.postalcode(),
        "street_address": faker.street_address(),
        "operator": operator,
    }

    facility = catalog_models.Facility(**facility_input)
    facility.save()

    return facility


@task("metro_areas")
def create_metro_areas(_options):
    """Create relevant metro areas"""
    metro_areas = {
        "LON": catalog_models.MetroArea(
            id="LON",
            un_locode="GBLON",
            iata_code="LON",
            display_name="London"),
        "FRA": catalog_models.MetroArea(
            id="FRA",
            un_locode="DEFRA",
            iata_code="FRA",
            display_name="Frankfurt am Main"),
        "AMS": catalog_models.MetroArea(
            id="AMS",
            un_locode="NLAMS",
            iata_code="AMS",
            display_name="Amsterdam"),
        "MAD": catalog_models.MetroArea(
            id="MAD",
            un_locode="ESMAD",
            iata_code="MAD",
            display_name="Madrid"),
        "MRS": catalog_models.MetroArea(
            id="MRS",
            un_locode="FRMRS",
            iata_code="MRS",
            display_name="Marseille"),
        "MUC": catalog_models.MetroArea(
            id="MUC",
            un_locode="DEMUC",
            iata_code="MUC",
            display_name="München"),
        "NYC": catalog_models.MetroArea(
            id="NYC",
            un_locode="USNYC",
            iata_code="NYC",
            display_name="New York")
    }
    for _, area in metro_areas.items():
        area.save()
    return metro_areas


@task("metro_area_networks")
def create_metro_area_networks(options, state):
    """Create metro area networks"""
    provider = options["exchange_name"]

    # Metro area network for each metro area
    metro_area_networks = [
        catalog_models.MetroAreaNetwork(
            metro_area=metro_area,
            service_provider=provider,
            name="{}1".format(metro_area.iata_code))
        for _, metro_area in state["metro_areas"].items()]
    for man in metro_area_networks:
        man.save()

    # Create another metro area network for LON
    net = catalog_models.MetroAreaNetwork(
        metro_area=state["metro_areas"]["LON"],
        service_provider=provider,
        name="LON2")
    net.save()
    metro_area_networks.append(net)

    return metro_area_networks


@task("catalog_facilities")
def create_catalog_facilities(options, state):
    """Create facilities where services are provided"""
    metro_areas = state["metro_areas"]

    # Create standalone facilities
    facility_a = _create_facility(metro_area=metro_areas["FRA"])
    facility_b = _create_facility(metro_area=metro_areas["LON"])

    # Create facilities in the same metro area
    _create_facility(metro_area=metro_areas["MAD"])
    _create_facility(metro_area=metro_areas["MAD"])

    # Operated by a operator
    operator = catalog_models.FacilityOperator(
        name=generate_dc_name())
    operator.save()

    facility_op_1 = _create_facility(
        operator=operator, metro_area=metro_areas["NYC"])
    facility_op_2 = _create_facility(
        operator=operator, metro_area=metro_areas["NYC"])

    facility_c3_op_1 = _create_facility(
        operator=operator,
        metro_area=metro_areas["MRS"])

    return {
        "standalone": [facility_a, facility_b],
        "op_1": [facility_op_1, facility_op_2, facility_c3_op_1],
    }


def _create_pop(facility, network):
    """Create PoP"""
    name = "{}-{}".format(
        facility.name.lower().replace(" ", "-"),
        network.name.lower())
    pop = catalog_models.PointOfPresence(
        name=name,
        metro_area_network=network,
        facility=facility)
    pop.save()
    return pop


@task("catalog_pops")
def create_catalog_pops(_options, state):
    """Install pops"""
    # We assume a pop for each metro area network
    # in each facility.
    pops = []
    for facility in catalog_models.Facility.objects.all():
        networks = facility.metro_area.metro_area_networks.all()
        for net in networks:
            pop = _create_pop(facility, net)

    # Add some reachable pops to facilities
    nyc = state["metro_areas"]["NYC"]
    fra = state["metro_areas"]["FRA"]
    net = nyc.metro_area_networks.first()
    fac = fra.facilities.first()
    pop = _create_pop(fac, net)
    pops.append(pop)

    return pops


#
# - Devices
#
def _create_device(
        pop,
        exchange_name,
        bandwidth=None):
    """Create a device in a facility"""
    device = catalog_models.Device(
        name=generate_device_name(pop, exchange_name),
        pop=pop)
    device.save()

    # Add device capabilities
    capabilitiy_1g = catalog_models.DeviceCapability(
        device=device,
        media_type="1000Base-LX",
        speed=10000,
        q_in_q=True,
        max_lag=8,
        availability_count=25)
    capabilitiy_1g.save()

    capabilitiy_10g = catalog_models.DeviceCapability(
        device=device,
        media_type="10GBase-LR",
        speed=10000,
        q_in_q=True,
        max_lag=8,
        availability_count=25)
    capabilitiy_10g.save()

    capabilitiy_100g = catalog_models.DeviceCapability(
        device=device,
        media_type="10GBase-LR4",
        speed=100000,
        q_in_q=True,
        max_lag=8,
        availability_count=25)
    capabilitiy_100g.save()

    if bandwidth == 400000:
        capabilitiy_400g = catalog_models.DeviceCapability(
            device=device,
            media_type="400GBase-LR8",
            speed=400000,
            q_in_q=True,
            max_lag=8,
            availability_count=15)
        capabilitiy_400g.save()

    return device


@task("catalog_devices")
def create_catalog_devices(options, state):
    """Install devices in facilities"""
    pops = catalog_models.PointOfPresence.objects.all()
    devices = []
    for pop in pops:
        # Create two devices per pop
        devices += [
            _create_device(pop, options["exchange_name"]),
            _create_device(pop, options["exchange_name"]),
        ]

    # Create 400G device
    fac = state["metro_areas"]["FRA"].facilities.first()
    device = _create_device(fac.pops.first(),
                            options["exchange_name"],
                            bandwidth=400000)
    devices.append(device)

    return devices

#
# Allocate reseller ports
#

def _allocate_port(account, pop, ref):
    """Create a port"""
    port_num = access_models.Port.objects.filter(
        consuming_account=account.pk).count() + 1
    name = "{} c({}) d({})".format(
        pop.name,
        account.pk,
        port_num)

    for dev in pop.devices.all():
        # Allocate port in each device. We start with
        # all ports in production, as we configure connections
        # later.
        port = access_models.Port(
            state=State.PRODUCTION,
            device=dev,
            speed=10000,
            operational_state=access_models.PortState.UP,
            managing_account=account,
            consuming_account=account,
            scoping_account=account,
            billing_account=account,
            media_type="10GBase-LR",
            external_ref=ref,
            name=name)
        port.save()

    return name


@task("reseller_ports")
def create_reseller_ports(options, state):
    """Allocate ports for a reseller"""
    time.sleep(0.1)
    reseller = state["api_user"].account

    pops = catalog_models.PointOfPresence.objects.all()
    ports = [
        _allocate_port(reseller, pop, "port-set-1")
        for pop in pops]
    ports += [
        _allocate_port(reseller, pop, "port-set-2")
        for pop in pops]
    # Allocate a second port for
    # Allocate another round of ports,
    # but only in the first two pop
    ports += [
        _allocate_port(reseller, pop, "port-set-extra")
        for pop in pops[:2]]

    return ports


@task("connections")
def create_connections(options, state):
    """Create all connections"""
    return create_customer_connections(
        state["account"], "port-set-1", options, state)


@task("subaccount2_connections")
def create_subaccount2_connections(options, state):
    """Create all connections"""
    return create_customer_connections(
        state["subaccount2"], "port-set-2", options, state)


def create_customer_connections(customer, portset, options, state):
    """Create customer connections"""
    connections = []

    reseller = state["api_user"].account

    initiator = catalog_models.CrossConnectInitiator.EXCHANGE

    # Create a customer connection at each device with
    # all ports available
    pop = None
    dcount = 0
    for dev in catalog_models.Device.objects.all():
        if dev.pop == pop:
            dcount += 1
        else:
            dcount = 1
            pop = dev.pop

        ports = dev.ports.filter(
            external_ref=portset,
            consuming_account=reseller)

        name = "{}-{}-{}".format(
            dev.pop.name,
            customer.name.split(" ")[0].lower(),
            dcount)

        # Get appropriate product offering
        po = catalog_models.ConnectionProductOffering.objects \
            .filter(handover_pop=pop) \
            .filter(cross_connect_initiator=initiator) \
            .first()

        lag = access_models.Connection(
            state=State.PRODUCTION,
            scoping_account=reseller.scoping_account,
            billing_account=reseller,
            consuming_account=customer,
            managing_account=reseller,
            name=name,
            product_offering=po,
            pop=pop,
            vlan_types=["port", "dot1q", "qinq"],
            outer_vlan_ethertypes=[
                "0x8100", "0x88a8", "0x9100",
            ],
            mode="lag_lacp",
            lacp_timeout="fast")
        lag.save()
        lag.ports.set(ports)

        # Create port reservations
        for port in ports:
            reservation = access_models.PortReservation(
                state=State.PRODUCTION,
                connection=lag,
                scoping_account=lag.scoping_account,
                port=port)
            reservation.save()

        connections.append(lag)

    return connections


#
# - Products

# -- Connection
@task("connection_products")
def create_connection_products(options, state):
    """Create connection product offerings"""
    products = []
    # For each pop create two connection product
    # offerings. One with cross connect initiator IX and 
    # one with subscriber
    pops = catalog_models.PointOfPresence.objects.all()

    for pop in pops:
        prod = _make_connection_productoffering(
            options, pop, catalog_models.CrossConnectInitiator.SUBSCRIBER)
        prod.save()
        products.append(prod)
        prod = _make_connection_productoffering(
            options, pop, catalog_models.CrossConnectInitiator.EXCHANGE)
        prod.save()
        products.append(prod)

    return products


def _make_connection_productoffering(options, pop, initiator):
    """create a connection product offering"""
    speed = 10000
    man = pop.metro_area_network
    ma = man.metro_area

    name = "PoPConn {}".format(speed)
    display_name = "{} Gbit Port @ {} ({})".format(
        speed / 1000, pop.name, pop.facility.name)

    if initiator == catalog_models.CrossConnectInitiator.SUBSCRIBER:
        display_name += " (Subscriber Initiated CrossConnect)"

    prod = catalog_models.ConnectionProductOffering(
        name=name,
        display_name=display_name,
        service_provider=options["exchange_name"],
        handover_pop=pop,
        handover_metro_area=ma,
        handover_metro_area_network=man,
        bandwidth_min=0,
        bandwidth_max=speed,
        downgrade_allowed=True,
        upgrade_allowed=True,
        physical_port_speed=speed,
        physical_media_type=catalog_models.MediaType.TYPE_10GBASE_LR,
        cross_connect_initiator=initiator,
        resource_type="connection")
    return prod

# -- Peering / ExchangeLan
#
@task("exchange_lan_products")
def create_exchange_lan_products(_options, state):
    """Create exchange lan product(s)"""
    products = []
    # For each metro area network, we offer peering:
    mans = catalog_models.MetroAreaNetwork.objects.all()
    for man in mans:
        metro_area = man.metro_area
        name = "Planet Peering ({})".format(
            man.metro_area.display_name)

        offering = catalog_models.ExchangeLanNetworkProductOffering(
            name="PlanetPEER",
            display_name=name,
            provider_vlans="single",
            resource_type=RESOURCE_TYPE_NETWORK_SERVICE,
            service_metro_area=metro_area,
            handover_metro_area=metro_area,
            downgrade_allowed=True,
            upgrade_allowed=True,
            cancellation_notice_period=relativedelta(months=1),
            cancellation_charge_period=relativedelta(months=3),
            service_metro_area_network=man,
            handover_metro_area_network=man)
        offering.save()
        products.append(offering)

        # Add multi vlan product
        name = "Planet Peering MVLAN ({})".format(
            man.metro_area.display_name)

        offering = catalog_models.ExchangeLanNetworkProductOffering(
            name="PlanetPEER",
            display_name=name,
            service_metro_area=metro_area,
            handover_metro_area=metro_area,
            provider_vlans="multi",
            resource_type=RESOURCE_TYPE_NETWORK_SERVICE,
            downgrade_allowed=True,
            upgrade_allowed=True,
            cancellation_notice_period=relativedelta(months=1),
            cancellation_charge_period=relativedelta(months=3),
            service_metro_area_network=man,
            handover_metro_area_network=man)
        offering.save()
        products.append(offering)

    # Offer remote peering from FRA to NYC
    fra = state["metro_areas"]["FRA"]
    nyc = state["metro_areas"]["NYC"]
    product = catalog_models.ExchangeLanNetworkProductOffering(
        name="RemoteCONNECT",
        display_name="RemoteCONNECT NYC accessed from FRA",
        provider_vlans="single",
        resource_type=RESOURCE_TYPE_NETWORK_SERVICE,
        downgrade_allowed=True,
        upgrade_allowed=True,
        cancellation_notice_period=relativedelta(months=1),
        cancellation_charge_period=relativedelta(months=3),
        service_metro_area=fra,
        service_metro_area_network=fra.metro_area_networks.first(),
        handover_metro_area=nyc,
        handover_metro_area_network=nyc.metro_area_networks.first())
    product.save()
    products.append(product)

    # We provide some local peering product in MAD
    mad = state["metro_areas"]["MAD"]
    product = catalog_models.ExchangeLanNetworkProductOffering(
        name="OptimalREACH",
        display_name="OptimalREACH in MAD",
        provider_vlans="single",
        resource_type=RESOURCE_TYPE_NETWORK_SERVICE,
        downgrade_allowed=True,
        upgrade_allowed=True,
        cancellation_notice_period=relativedelta(months=1),
        cancellation_charge_period=relativedelta(months=3),
        handover_metro_area=mad,
        handover_metro_area_network=mad.metro_area_networks.first(),
        service_metro_area=mad,
        service_metro_area_network=mad.metro_area_networks.first())
    product.save()
    products.append(product)

    return products


# Setup Exchange Lans:

def _create_route_server_feature(options, exchange_lan):
    """Create a routeserver on the exchange lan"""
    asn = options["exchange_asn"]
    rsnum = exchange_lan.network_features.instance_of(
        service_models.RouteServerNetworkFeature).count() + 1
    required = True
    if rsnum > 1:
        required = False
    account = exchange_lan.consuming_account
    domain = get_account_domain(account)
    metro_area = exchange_lan.metro_area_network.metro_area.iata_code.lower()
    fqdn = f"rs{rsnum}.{metro_area}.noc.{domain}"
    looking_glass_url = (f"https://lg.{domain}/routeservers/rs{rsnum}_"
                         f"{metro_area}")

    # Get exchange lan ip ranges
    exchange_lan_ips = exchange_lan.ip_addresses.all()
    # Check address families supported
    ip_versions = [ip.version.value for ip in exchange_lan_ips]
    address_families = []
    if 4 in ip_versions:
        address_families.append(service_models.AddressFamilies.AF_INET)
    if 6 in ip_versions:
        address_families.append(service_models.AddressFamilies.AF_INET6)

    bgp_session_types = [
        access_models.BGPSessionType.TYPE_ACTIVE,
        access_models.BGPSessionType.TYPE_PASSIVE,
    ]

    rs = service_models.RouteServerNetworkFeature(
        name=f"rs{rsnum}-{exchange_lan.metro_area_network.metro_area.iata_code.lower()}",
        session_mode=access_models.RouteServerSessionMode.MODE_PUBLIC,
        address_families=address_families,
        available_bgp_session_types=bgp_session_types,
        asn=asn,
        fqdn=fqdn,
        looking_glass_url=looking_glass_url,
        required=required,
        network_service=exchange_lan)
    rs.save()
    rs.nfc_required_contact_roles.add(
        contacts_svc.get_default_role("implementation"))

    ip_addresses = [
        create_random_host(account, net, fqdn=fqdn)
        for net in exchange_lan.ip_addresses.all()]

    rs.ip_addresses.set(ip_addresses)

    return rs


def _create_exchange_lan_network_service(
        options,
        exchange_name,
        exchange_account,
        product,
        metro_area_network,
    ):
    """Create an exchange lan network service"""
    exchange_lan = service_models.ExchangeLanNetworkService(
        state=State.PRODUCTION,
        name=generate_exchange_lan_name(
            exchange_name,
            metro_area_network),
        metro_area_network=metro_area_network,
        managing_account=exchange_account,
        consuming_account=exchange_account,
        product_offering=product)
    exchange_lan.save()
    exchange_lan.nsc_required_contact_roles.add(
        contacts_svc.get_default_role("implementation"))
    exchange_lan.nsc_required_contact_roles.add(
        contacts_svc.get_default_role("noc"))

    domain = get_account_domain(exchange_account)
    fqdn = f"noc.{domain}"

    # Assign IP addresses
    net_v4 = create_random_ip4_prefix(exchange_account, fqdn=fqdn)
    net_v6 = create_random_ip6_prefix(exchange_account, fqdn=fqdn)

    exchange_lan.ip_addresses.add(net_v4)
    exchange_lan.ip_addresses.add(net_v6)

    # Create features:
    _create_route_server_feature(
        options,
        exchange_lan)
    _create_route_server_feature(
        options,
        exchange_lan)

    return exchange_lan


@task("exchange_lan_services")
def create_exchange_lan_services(options, state):
    """Create services for the product"""
    services = []
    # Get our own exchange account
    exchange_name = options["exchange_name"]
    ix_account = state["ix_account"]
    products = state["exchange_lan_products"]

    # Create peering services in each metro area
    for product in products:
        man = product.service_metro_area_network
        service = _create_exchange_lan_network_service(
            options,
            exchange_name,
            ix_account,
            product,
            man)
        services.append(service)

    return services
#
# - Virtual Network Products
# -- Eline
#
@task("p2p_products")
def create_p2p_products(options, state):
    """Create eline network product(s)"""
    products = []

    lon = state["metro_areas"]["LON"]
    name = "Private Connect P2P (London)"
    product = catalog_models.P2PNetworkProductOffering(
        name=name,
        display_name=name,
        provider_vlans="single",
        service_provider=options["exchange_name"],
        upgrade_allowed=False,
        downgrade_allowed=False,
        cancellation_notice_period=relativedelta(days=30),
        cancellation_charge_period=relativedelta(days=1),
        resource_type=RESOURCE_TYPE_NETWORK_SERVICE,
        service_metro_area=lon,
        service_metro_area_network=lon.metro_area_networks.first(),
        handover_metro_area=lon,
        handover_metro_area_network=lon.metro_area_networks.first(),
        )
    product.save()
    products.append(product)

    return products


@task("mp2mp_products")
def create_mp2mp_products(options, state):
    """Create elan network product(s)"""
    products = []
    lon = state["metro_areas"]["LON"]
    name = "Private Peering MP2MP (London)"
    product = catalog_models.MP2MPNetworkProductOffering(
        name=name,
        display_name=name,
        provider_vlans="single",
        upgrade_allowed=False,
        downgrade_allowed=False,
        cancellation_notice_period=relativedelta(days=30),
        cancellation_charge_period=relativedelta(days=1),
        resource_type=RESOURCE_TYPE_NETWORK_SERVICE,
        service_metro_area=lon,
        service_metro_area_network=lon.metro_area_networks.first(),
        handover_metro_area=lon,
        handover_metro_area_network=lon.metro_area_networks.first(),
        )

    product.save()
    products.append(product)

    return products


@task("p2mp_products")
def create_p2mp_products(options, state):
    """Create elan network product(s)"""
    products = []

    fra = state["metro_areas"]["FRA"]
    name = "Private Secure Connect P2MP (Frankfurt)"
    product = catalog_models.P2MPNetworkProductOffering(
        name=name,
        display_name=name,
        provider_vlans="single",
        upgrade_allowed=False,
        downgrade_allowed=False,
        resource_type=RESOURCE_TYPE_NETWORK_SERVICE,
        cancellation_notice_period=relativedelta(days=30),
        cancellation_charge_period=relativedelta(days=1),
        service_metro_area=fra,
        service_metro_area_network=fra.metro_area_networks.first(),
        handover_metro_area=fra,
        handover_metro_area_network=fra.metro_area_networks.first(),
        )
    product.save()
    products.append(product)

    return products


def _create_awesome_cloud_product(
        bandwidth,
        delivery_method,
        region,
        service_metro_area,
        handover_metro_area):
    """Create an AwesomeCloudServices product"""
    service_man = service_metro_area.metro_area_networks.first()
    handover_man = handover_metro_area.metro_area_networks.first()

    pop = "dc-pop3"

    name = (
        "InstantCLOUD A.W. and Services {} ({}) "
        "accessed from {} {} Mbit/s on {}"
    ).format(
        region,
        pop,
        handover_metro_area.iata_code,
        bandwidth,
        delivery_method)

    product = catalog_models.CloudNetworkProductOffering(
        name="InstantCLOUD",
        display_name=name,
        diversity=1,
        delivery_method=delivery_method,
        service_provider="AwesomeCloud Inc.",
        service_provider_pop=pop,
        bandwidth_min=bandwidth,
        bandwidth_max=bandwidth,
        provider_vlans="single",
        cancellation_notice_period=relativedelta(days=30),
        cancellation_charge_period=relativedelta(days=1),
        service_metro_area=service_metro_area,
        service_metro_area_network=service_man,
        handover_metro_area=handover_metro_area,
        handover_metro_area_network=handover_man,
        service_provider_workflow="exchange_first",
        resource_type=RESOURCE_TYPE_NETWORK_SERVICE,
        upgrade_allowed=False,
        downgrade_allowed=False)
    product.save()
    return product


@task("awesome_cloud_products")
def create_awesome_cloud_products(options, state):
    """Create cloud products"""
    products = []

    bandwidths = [
        50, 100, 200, 300, 400, 500,
        1000, 10000]
    delivery_methods = [
        DELIVERY_METHOD_DEDICATED,
        DELIVERY_METHOD_SHARED,
    ]
    handovers = [
        state["metro_areas"]["FRA"],
        state["metro_areas"]["LON"],
    ]

    # Create Awesome Web and Services
    for bwidth in bandwidths:
        for method in delivery_methods:
            for handover in handovers:
                prod = _create_awesome_cloud_product(
                    bwidth,
                    method,
                    "eu-central-1",
                    state["metro_areas"]["FRA"],
                    handover)
                products.append(prod)

                prod = _create_awesome_cloud_product(
                    bwidth,
                    method,
                    "uk-east-1",
                    state["metro_areas"]["LON"],
                    handover)
                products.append(prod)

    return products


def _create_oceanblue_cloud_product(
        bandwidth,
        delivery_method,
        region,
        service_metro_area,
        handover_metro_area,
        counter):
    """Create a blue colored cloud product"""
    service_man = service_metro_area.metro_area_networks.first()
    handover_man = handover_metro_area.metro_area_networks.first()
    pop = "dc-pop3"

    name = (
        "InstantCLOUD OceanBlue {} ({}) "
        "accessed from {} {} Mbit/s on {}"
    ).format(
        region,
        pop,
        handover_metro_area.iata_code,
        bandwidth,
        delivery_method)


    cloud_key = "demo{}".format(counter+1)

    product = catalog_models.CloudNetworkProductOffering(
        name="InstantCLOUD",
        display_name=name,
        delivery_method=delivery_method,
        diversity=2,
        service_provider="OCEANBLUE",
        service_provider_pop=pop,
        bandwidth_min=bandwidth,
        bandwidth_max=bandwidth,
        provider_vlans="multi",
        service_metro_area=service_metro_area,
        service_metro_area_network=service_man,
        handover_metro_area=handover_metro_area,
        handover_metro_area_network=handover_man,
        cancellation_notice_period=relativedelta(days=30),
        cancellation_charge_period=relativedelta(days=1),
        service_provider_workflow="provider_first",
        resource_type=RESOURCE_TYPE_NETWORK_SERVICE,
        cloud_key=cloud_key,
        upgrade_allowed=False,
        downgrade_allowed=False)
    product.save()
    return product


@task("oceanblue_cloud_products")
def create_oceanblue_cloud_products(options, state):
    """Create cloud products"""
    products = []

    bandwidths = [
        50, 100, 200, 300, 400, 500,
        1000, 10000]
    delivery_methods = [
        DELIVERY_METHOD_DEDICATED,
        DELIVERY_METHOD_SHARED,
    ]
    handovers = [
        state["metro_areas"]["FRA"],
        state["metro_areas"]["LON"],
    ]

    # Create Awesome Web and Services
    counter = 0
    for bwidth in bandwidths:
        for method in delivery_methods:
            for handover in handovers:
                prod = _create_oceanblue_cloud_product(
                    bwidth,
                    method,
                    "Europe",
                    state["metro_areas"]["FRA"],
                    handover,
                    counter)
                products.append(prod)
                counter += 1

                prod = _create_oceanblue_cloud_product(
                    bwidth,
                    method,
                    "Europe",
                    state["metro_areas"]["LON"],
                    handover,
                    counter)
                products.append(prod)
                counter += 1

    return products

@task("test_suite_config")
def create_test_suite_config(_options, state):
    """Create the test-suite-config and add it to the webstatic"""
    time.sleep(0.1)
    exchange_lan = state["exchange_lan_services"][0]
    p2p_products = state["p2p_products"]
    mp2mp_products = state["mp2mp_products"]
    p2mp_products = state["p2mp_products"]

    reseller = state["api_user"].account
    reseller_customer = state["account"]
    subaccount2 = state["subaccount2"]

    oceanblue_cloud_products = state["oceanblue_cloud_products"]
    awesome_cloud_products = state["awesome_cloud_products"]

    noc_role = contacts_svc.get_default_role("noc")
    impl_role = contacts_svc.get_default_role("implementation")

    reseller_impl_role_assignment = crm_models.RoleAssignment.objects.filter(
        contact__consuming_account=reseller,
        role=impl_role).first()
    subcustomer_impl_role_assignment = crm_models.RoleAssignment \
        .objects \
        .filter(
            contact__consuming_account=reseller_customer,
            role=impl_role).first()
    subcustomer_noc_role_assignment = crm_models.RoleAssignment \
        .objects \
        .filter(
            contact__consuming_account=reseller_customer,
            role=noc_role).first()

    subaccount2_noc_role_assignment = crm_models.RoleAssignment \
        .objects.filter(
            contact__consuming_account=subaccount2,
            role=noc_role).first()
    subaccount2_impl_role_assignment = crm_models.RoleAssignment \
        .objects.filter(
            contact__consuming_account=subaccount2,
            role=impl_role).first()

    reset_token = secrets_svc.generate_token()
    reset_trigger_path = f"api/ctrl/reset-{reset_token}"

    pop = exchange_lan.metro_area_network.pops.first()
    subaccount_connection = access_models.Connection \
        .objects \
        .filter(
            ports__device__pop=pop,
            consuming_account=reseller_customer) \
        .first()
    assert subaccount_connection

    subaccount2_connection = access_models.Connection \
        .objects \
        .filter(
            ports__device__pop=pop,
            consuming_account=subaccount2) \
        .first()
    assert subaccount2_connection

    pop = exchange_lan.metro_area_network.pops.first()

    # Cloud connections
    provider_first_cloud_product = oceanblue_cloud_products[0]

    cloud_connections = access_models.Connection \
        .objects \
        .filter(
            ports__device__pop__metro_area_network__metro_area=provider_first_cloud_product.handover_metro_area,
            consuming_account=reseller_customer) \
        .all()

    # Connection product offerings
    subscriber_initiated_connection_po = \
        catalog_models.ConnectionProductOffering.objects \
            .filter(cross_connect_initiator="subscriber").first()
    exchange_initiated_connection_po = \
        catalog_models.ConnectionProductOffering.objects \
            .filter(cross_connect_initiator="exchange").first()


    config = {
        "account": {
            "id":
                str(reseller.pk),
            "implementation_role_assignment_id":
                str(reseller_impl_role_assignment.pk),
        },
        "subaccounts": [
            {
                "id":
                    str(reseller_customer.pk),
                "noc_role_assignment_id":
                    str(subcustomer_noc_role_assignment.pk),
                "implementation_role_assignment_id":
                    str(subcustomer_impl_role_assignment.pk),
                "connection_id":
                    str(subaccount_connection.pk),
            },
            {
                "id":
                    str(subaccount2.pk),
                "noc_role_assignment_id":
                    str(subaccount2_noc_role_assignment.pk),
                "implementation_role_assignment_id":
                    str(subaccount2_impl_role_assignment.pk),
                "connection_id":
                    str(subaccount2_connection.pk),
            },
        ],
        "cloud_handover_connections": [
            str(cloud_connections[0].pk),
            str(cloud_connections[1].pk),
        ],
        "exchange_lan_network_service_id":
            str(exchange_lan.pk),
        "exchange_lan_network_service_product_offering_id":
            str(exchange_lan.product_offering.pk),
        "subscriber_initated_connection_product_offering_id":
            str(subscriber_initiated_connection_po.pk),
        "exchange_initated_connection_product_offering_id":
            str(exchange_initiated_connection_po.pk),
        "p2p_product_offering_id":
            str(p2p_products[0].pk),
        "p2mp_product_offering_id":
            str(p2mp_products[0].pk),
        "mp2mp_product_offering_id":
            str(mp2mp_products[0].pk),
        "cloud_workflow_exchange_first_product_offering_id":
            str(awesome_cloud_products[0].pk),
        "cloud_workflow_provider_first_product_offering_cloud_key":
            str(oceanblue_cloud_products[0].cloud_key),
        "api_base_url":
            "http://localhost:8000/api/v2",
        "api_key":
            state["api_user"].api_key,
        "api_secret":
            state["api_user"].api_secret,
        "reset_trigger_url":
            f"http://localhost:8000/{reset_trigger_path}",
    }

    configfile = "src/jea/public/static/test-suite-config.json"
    with open(configfile, "w") as outfile:
        json.dump(config, outfile)


@task("db_snapshot_exec_result")
def create_db_snapshot(_options, _state):
    """Create database snapshot"""
    snapshot_svc.create_template()


def setup_sandbox(**options):
    """
    Setup the sandbox.

    Create required models and populate
    our catalog and services.
    """
    tasks = [
        "Clearing database...", [clear_database],
        "Creating accounts...", [
            create_ix_account,
            create_reseller_account,
            create_subaccount1,
            create_subaccount2,
            create_public_account,
        ],
        "Building facilities...", [
            create_metro_areas,
            create_metro_area_networks,
            create_catalog_facilities,
            create_catalog_pops,
        ],
        "Installing switches and splicing fibers...", [
            create_catalog_devices,
        ],
        "Getting products from marketing...", [
            create_connection_products,
            create_exchange_lan_products,
            create_p2p_products,
            create_mp2mp_products,
            create_p2mp_products,
        ],
        "Inflating cloud providers...", [
            create_awesome_cloud_products,
            create_oceanblue_cloud_products,
        ],
        "Provisioning services...", [
            create_exchange_lan_services,
        ],
        "Installing optics...", [
            create_reseller_ports,
            create_connections,
            create_subaccount2_connections,
        ],
        "Rendering test-suite-config.json...", [
            create_test_suite_config,
        ],
        "Creating snapshot... (please smile)", [
            create_db_snapshot,
        ]
    ]

    return progress_exec_tasks(tasks, options)


def setup_account(**options):
    """Create a account"""
    tasks = [
        "Getting IXP account...", [
            load_root_account,
        ],
        "Creating account...", [
            create_reseller_account,
            create_subaccount1,
            create_subaccount2,
            create_public_account,
        ],
    ]

    # Reseller ports
    tasks += [
        "Installing optics...", [
            create_reseller_ports,
        ]
    ]

    return progress_exec_tasks(tasks, options)
