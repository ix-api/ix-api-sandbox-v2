
"""
Sandbox Secret Service
"""

import secrets
import hmac
import hashlib

from django.conf import settings

MSG_BYTES = 2

class InvalidTokenException(Exception):
    pass


def _hash_msg(key, msg):
    """Create hmac digest of message"""
    token = hmac.HMAC(
        key=bytes(key, "utf-8"),
        msg=bytes(msg, "utf-8"),
        digestmod=hashlib.sha1)
    return token.hexdigest()


def generate_token():
    """Generate a signed token"""
    key = settings.SECRET_KEY
    msg = secrets.token_hex(MSG_BYTES)
    msg_hash = _hash_msg(key, msg)

    token = msg + str(msg_hash)
    return token


def validate(token):
    """Validate a secret token"""
    key = settings.SECRET_KEY
    token_msg = token[:MSG_BYTES*2]
    token_msg_hash = token[MSG_BYTES*2:]

    msg_hash = _hash_msg(key, token_msg)

    if not hmac.compare_digest(msg_hash, token_msg_hash):
        raise InvalidTokenException

    return True

