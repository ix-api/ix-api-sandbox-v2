
"""
Database snapshot service.

This module requires the postgres database client tools
installed on the application server machine.
"""

import subprocess
import os

from django.conf import settings
from django.db import connection


def _create_tmpl_datbase(tmpl_name):
    """Create the template database"""
    cur = connection.cursor()
    # I know it's dangerous to pass variables
    # directly into the querystring; somehow the variables
    # can not be bound.
    #
    # tl;dr - don't pass user data into this function.
    cur.execute(f"DROP DATABASE IF EXISTS {tmpl_name}")
    cur.execute(f"CREATE DATABASE {tmpl_name}")
    return cur


def _create_postgres_env(conn_params):
    """
    Taken from `django.db.conncetion.client.runshell()`.
    """
    passwd = conn_params.get('password', '')
    sslmode = conn_params.get('sslmode', '')
    sslrootcert = conn_params.get('sslrootcert', '')
    sslcert = conn_params.get('sslcert', '')
    sslkey = conn_params.get('sslkey', '')

    subprocess_env = os.environ.copy()
    if passwd:
        subprocess_env['PGPASSWORD'] = str(passwd)
    if sslmode:
        subprocess_env['PGSSLMODE'] = str(sslmode)
    if sslrootcert:
        subprocess_env['PGSSLROOTCERT'] = str(sslrootcert)
    if sslcert:
        subprocess_env['PGSSLCERT'] = str(sslcert)
    if sslkey:
        subprocess_env['PGSSLKEY'] = str(sslkey)

    return subprocess_env


def _create_postgres_cmd(client, conn_params, *args):
    """Create the postgres client command."""
    user = conn_params.get('user', '')
    host = conn_params.get('host', '')
    port = conn_params.get('port', '')

    cmd = [client]
    if user:
        cmd += ['-U', user]
    if host:
        cmd += ['-h', host]
    if port:
        cmd += ['-p', str(port)]

    cmd += args

    return cmd


def _clone_database(db_settings, src, dst):
    """Clone database"""
    # Create authorized environment
    postgres_env = _create_postgres_env(db_settings)
    # Create pg_dump and psql command
    pg_dump_cmd = _create_postgres_cmd(
        "pg_dump", db_settings, "--clean", src)
    psql_cmd = _create_postgres_cmd(
        "psql", db_settings, dst)

    # Execute and pipe
    pg_dump = subprocess.Popen(
        pg_dump_cmd,
        stdout=subprocess.PIPE,
        env=postgres_env)
    psql = subprocess.Popen(
        psql_cmd,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,  # silence.
        stdin=pg_dump.stdout,
        env=postgres_env)

    return (pg_dump.wait(), psql.wait())


def create_template():
    """Create a snapshot from the current database"""
    db_settings = connection.get_connection_params()
    db_name = db_settings.get("database", "")
    tmpl_name = f"{db_name}_tmpl"

    # Create template database
    _create_tmpl_datbase(tmpl_name)
    return _clone_database(db_settings, db_name, tmpl_name)


def restore():
    """Restore the snapshot"""
    db_settings = connection.get_connection_params()
    db_name = db_settings.get("database", "")
    tmpl_name = f"{db_name}_tmpl"

    # Restore through cloning the template
    return _clone_database(db_settings, tmpl_name, db_name)
