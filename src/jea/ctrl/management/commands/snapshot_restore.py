"""
Expose restore snapshot for convenience as a
management command.
"""

from django.core.management import BaseCommand

from jea.ctrl.services import snapshot as snapshot_svc


class Command(BaseCommand):
    """Snapshot Restore Management Command"""
    def handle(self, *args, **options):
        """Restore the snapshot"""
        if (0, 0) == snapshot_svc.restore():
            print("ok.")
