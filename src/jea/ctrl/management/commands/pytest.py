
import os
import sys
import subprocess

from django.core.management import BaseCommand


def _strip_prefix(filename):
    """Remove prefix"""
    if filename.startswith("src/"):
        filename = filename[4:]
    if filename.startswith("jea/"):
        filename = filename[4:]
    return filename

class Command(BaseCommand):
    """Run PyTest"""
    script_path = os.path.dirname(os.path.abspath(__file__))
    project_root = os.path.abspath(
        os.path.join(script_path, "..", "..", ".."))

    test_files = [_strip_prefix(f) for f in sys.argv[2:]]

    cmd = ["pytest"] + test_files
    result = subprocess.run(
        cmd,
        cwd=project_root,
        env=os.environ,
        universal_newlines=True)

    sys.exit(result.returncode)
