
from prompt_toolkit import prompt, print_formatted_text
from prompt_toolkit.formatted_text import HTML
from prompt_toolkit.shortcuts import confirm
from django.core import management

from jea.ctrl.services import bootstrap as bootstrap_svc
from utils.prompt import (
    toolbar_help,
    print_value_table,
    NumberValidator,
)

"""
Create a fresh api account. Like in bootstrap.
Just only the api account part.
And add contacts.
And some optics.
"""

class Command(management.BaseCommand):
    """Create a new account."""

    def add_arguments(self, parser):
        """Add setup commandline arguments"""
        parser.add_argument(
            "-k", "--api-key",
            help="Provide an optional fixed API key for the new user")
        parser.add_argument(
            "-s", "--api-secret",
            help="Provide an optional api secret")
        parser.add_argument(
            "-c", "---api-account",
            help="The api account name")
        parser.add_argument(
            "-D", "--defaults",
            dest="use_defaults",
            action="store_true",
            help="Just use the defaults, don't prompt.")
        parser.add_argument(
            "--no-ports",
            dest="allocate_ports",
            action="store_false",
            help="Allocate some ports")
        parser.add_argument(
            "-y", "--yes",
            action="store_true",
            help="Assume yes when asked.")


    def generate_required_options(self, options):
        """Just make up some things."""
        if not options["api_account"]:
            options["api_account"] = bootstrap_svc.generate_ix_name()

        return options


    def ask_required_options(self, options):
        """
        Prompt the user for inputting the required options
        not provided by commandline arguments.
        """
        if not options["api_account"]:
            api_account_default = bootstrap_svc.generate_ix_name()
            api_account = prompt(
                "Api Account: ",
                bottom_toolbar=toolbar_help(
                    api_account_default,
                    "The reseller's name"))
            options["api_account"] = api_account or api_account_default

        return options

    def handle(self, *args, **options):
        """Create a new account"""

        print("")
        print_formatted_text(HTML(
            "<white><b>---------------------------------------------</b></white>"))
        print_formatted_text(HTML(
            "<cyan><b>Joint External API :: Create Sandbox Account</b></cyan>"))
        print_formatted_text(HTML(
            "<white><b>---------------------------------------------</b></white>"))
        print("")

        if options["use_defaults"]:
            # Just generate required options
            options = self.generate_required_options(options)
        else:
            # Ask for options
            options = self.ask_required_options(options)

        print_formatted_text(HTML(
            "\n<b>  Creating account</b>\n"))

        print_value_table([
            ("            Name", options["api_account"]),
            ("Allocate ports", options["allocate_ports"]),
        ])
        print("")

        if options["api_key"]:
            print_value_table([("   Api Key", options["api_key"])])
        if options["api_secret"]:
            print_value_table([("Api Secret", "*SET*")])
        print("")

        if not options["yes"]:

            start = confirm("Looking good?")
            if not start:
                print("kthxbye.")
                return
            print("")


        result = bootstrap_svc.setup_account(
            api_account=options["api_account"],
            api_key=options["api_key"],
            api_secret=options["api_secret"],
            allocate_ports=options["allocate_ports"],
        )

        print("")
        print_formatted_text(HTML("  <green><b>Done!</b></green>"))
        print("")
        print("  You can now use your client or the test suite")
        print("  to access the API using the following credentials.")
        print("")
        print_formatted_text(HTML("  <b>API-Credentials</b>"))
        print("")

        api_user = result["api_user"]
        print_value_table([
            ("    Api Key", api_user.api_key),
            (" Api Secret", api_user.api_secret)
        ])
        print("")
        print_value_table([
            ("Account Id", api_user.account_id),
        ])
        print("")
