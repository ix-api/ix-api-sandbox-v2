"""
Expose create snapshot for convenience as a
management command.
"""

from django.core.management import BaseCommand

from jea.ctrl.services import snapshot as snapshot_svc


class Command(BaseCommand):
    """Snapshot Management Command"""
    def handle(self, *args, **options):
        """Create the snapshot template"""
        if (0, 0) == snapshot_svc.create_template():
            print("ok.")
