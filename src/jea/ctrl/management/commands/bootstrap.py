
import random

from prompt_toolkit import prompt, print_formatted_text
from prompt_toolkit.formatted_text import HTML
from prompt_toolkit.shortcuts import confirm
from django.core.management import BaseCommand

from jea.ctrl.services import bootstrap as bootstrap_svc
from utils.prompt import (
    toolbar_help,
    print_value_table,
    NumberValidator,
)

#
# Management Command
#
class Command(BaseCommand):

    def add_arguments(self, parser):
        """Add setup commandline arguments"""
        parser.add_argument(
            "-k", "--api-key",
            help="Provide an optional fixed API key for the new user")
        parser.add_argument(
            "-s", "--api-secret",
            help="Provide an optional api secret")
        parser.add_argument(
            "-c", "---api-account",
            help="The api account name")
        parser.add_argument(
            "-n", "--exchange-name",
            help="The name of the demo exchange")
        parser.add_argument(
            "-a", "--exchange-asn",
            type=int,
            help="Our own exchange ASN")
        parser.add_argument(
            "--ports",
            dest="allocate_ports",
            action="store_true",
            help="Allocate some portation points for the reseller.")
        parser.add_argument(
            "-D", "--defaults",
            dest="use_defaults",
            action="store_true",
            help="Just use the defaults, don't prompt.")
        parser.add_argument(
            "-y", "--yes",
            action="store_true",
            help="Assume yes when asked. E.g. when clearing the db")


    def generate_required_options(self, options):
        """Just make up some parameters."""
        if not options["exchange_name"]:
            options["exchange_name"] = bootstrap_svc.generate_ix_name()

        if not options["exchange_asn"]:
            options["exchange_asn"] = bootstrap_svc.generate_asn()

        if not options["api_account"]:
            options["api_account"] = bootstrap_svc.generate_ix_name()

        return options

    def ask_required_options(self, options):
        """
        Prompt the user for inputting the required options
        not provided by commandline arguments.
        """
        if not options["exchange_name"]:
            exchange_name_default = bootstrap_svc.generate_ix_name()
            exchange_name = prompt(
                "Exchange Name: ",
                bottom_toolbar=toolbar_help(
                    exchange_name_default,
                    "The name of the demo exchange"))
            options["exchange_name"] = exchange_name or exchange_name_default

        if not options["exchange_asn"]:
            exchange_asn_default = bootstrap_svc.generate_asn()
            exchange_asn = prompt(
                "Exchange ASN: ",
                validator=NumberValidator(),
                bottom_toolbar=toolbar_help(
                    exchange_asn_default,
                    "The ASN of the demo exchange"))
            options["exchange_asn"] = int(exchange_asn or exchange_asn_default)

        if not options["api_account"]:
            api_account_default = bootstrap_svc.generate_ix_name()
            api_account = prompt(
                "Api Account: ",
                bottom_toolbar=toolbar_help(
                    api_account_default,
                    "The reseller's name"))
            options["api_account"] = api_account or api_account_default

        return options


    def handle(self, *args, **options):
        """Run JEA sandbox setup"""

        print("")
        print_formatted_text(HTML(
            "<white><b>-----------------------------------</b></white>"))
        print_formatted_text(HTML(
            "<cyan><b>Joint External API :: Sandbox Setup</b></cyan>"))
        print_formatted_text(HTML(
            "<white><b>-----------------------------------</b></white>"))
        print("")

        if options["use_defaults"]:
            # Just generate required options
            options = self.generate_required_options(options)
        else:
            # Ask for options
            options = self.ask_required_options(options)


        print_formatted_text(HTML(
            "\n<b>  Setup Configuration</b>\n"))

        print_value_table([
            ("  Exchange", options["exchange_name"]),
            ("       ASN", options["exchange_asn"]),
            (""),
            ("  Reseller", options["api_account"]),
        ])
        print("")

        # Inform our user if an api key or secret was set
        if options["api_key"]:
            print_value_table([("   Api Key", options["api_key"])])
        if options["api_secret"]:
            print_value_table([("Api Secret", "*SET*")])
        print("")

        print_formatted_text(HTML(
            "  <red>Setup will clear the database in the next step!</red>"))
        print("  Any superusers will be kept.")
        print("")

        if not options["yes"]:

            start = confirm("Looking good?")
            if not start:
                print("kthxbye.")
                return
            print("")

        setup_result = bootstrap_svc.setup_sandbox(
            api_account=options["api_account"],
            api_key=options["api_key"],
            api_secret=options["api_secret"],
            allocate_ports=options["allocate_ports"],
            exchange_name=options["exchange_name"],
            exchange_asn=options["exchange_asn"],
            reseller_name=options["api_account"],
        )
        print("")

        print_formatted_text(HTML("  <green><b>Done!</b></green>"))
        print("")
        print("  You can now use your client or the test suite")
        print("  to access the API using the following credentials.")
        print("")
        print_formatted_text(HTML("  <b>API-Credentials</b>"))
        print("")

        api_user = setup_result["api_user"]
        print_value_table([
            ("    Api Key", api_user.api_key),
            (" Api Secret", api_user.api_secret)
        ])
        print("")
        print_value_table([
            ("Account Id", api_user.account_id),
        ])

        print("")
        sub1 = setup_result["account"]
        sub2 = setup_result["subaccount2"]
        print_formatted_text(HTML("  <b>Customer Logins</b>"))
        print("")
        print_value_table([
            ("Subaccount 1 (username password)",
             f"{sub1.user.username} {sub1.user.api_secret}"),
            ("Subaccount 2 (username password)",
             f"{sub2.user.username} {sub2.user.api_secret}"),
        ])
        print("")
        print_formatted_text(HTML("  <red><b>NOTICE!</b></red>"))
        print_formatted_text(HTML("  <red><b>NOTICE!</b></red> The "
            "test-suite-config.json contains the api key and secret"))
        print_formatted_text(HTML("  <red><b>NOTICE!</b></red> and "
            "is accessible from: "
            "https://&lt;sandboxurl&gt;/static/test-suite-config.json"))
        print_formatted_text(HTML("  <red><b>NOTICE!</b></red>"))
        print("")
        print("You can delete the file "
            "src/jea/public/static/test-suite-config.json")
        print("or protect the path with e.g. basic-auth.")

        print("")
