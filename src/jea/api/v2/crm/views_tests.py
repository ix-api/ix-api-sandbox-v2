
"""
Test CRM Views
"""

import pytest
from model_bakery import baker

from jea.test import authorized_requests
from jea.api.v2.crm import views
from jea.crm.services import contacts as contacts_svc

#
# Accounts
#

@pytest.mark.django_db
def test_accounts_view_set__list():
    """Test listing all accounts"""
    account = baker.make(
        "crm.Account",
        address=baker.make("crm.Address"))
    request = authorized_requests.get(account)
    view = views.AccountsViewSet.as_view({"get": "list"})

    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_accounts_view_set__retrieve():
    """Test retriving a specific account"""
    account = baker.make("crm.Account")
    request = authorized_requests.get(account)
    view = views.AccountsViewSet.as_view({"get": "retrieve"})

    # Account should be able to see itself
    response = view(request, pk=account.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_accounts_view_set__create():
    """Test creating a new account"""
    view = views.AccountsViewSet.as_view({"post": "create"})
    account = baker.make("crm.Account")
    new_account = baker.prepare("crm.Account")
    man = baker.make("catalog.MetroAreaNetwork")

    request = authorized_requests.post(account, {
        "name": new_account.name[:50],
        "external_ref": new_account.external_ref,
        "address": {
            "country": "DE",
            "locality": "Berlin",
            "postal_code": "10202",
            "street_address": "Wegweg 23",
        },
        "metro_area_network_presence": [man.pk],
    })

    response = view(request)
    assert response.status_code == 201


@pytest.mark.django_db
def test_accounts_view_set__create_billing_information():
    """Test creating a new account"""
    view = views.AccountsViewSet.as_view({"post": "create"})
    account = baker.make("crm.Account")
    new_account = baker.prepare("crm.Account")

    request = authorized_requests.post(account, {
        "name": new_account.name[:50],
        "external_ref": new_account.external_ref,
        "address": {
            "country": "DE",
            "locality": "Berlin",
            "postal_code": "10202",
            "street_address": "Wegweg 23",
        },
        "billing_information": {
            "name": "Account Accounting Inc.",
            "address": {
                "country": "GB",
                "locality": "Westernshire",
                "postal_code": "L2391",
                "street_address": "23 Billingstreet",
            },
        }
    })

    response = view(request)
    assert response.status_code == 201


@pytest.mark.django_db
def test_accounts_view_set__update():
    """Test updating a account"""
    view = views.AccountsViewSet.as_view({"put": "update"})
    account = baker.make("crm.Account")
    man = baker.make("catalog.MetroAreaNetwork")

    request = authorized_requests.put(account, {
        "id": account.pk,
        "name": "NewAccountName",
        "address": {
            "country": "DE",
            "locality": "Berlin",
            "postal_code": "10202",
            "street_address": "Wegweg 23",
        },
        "metro_area_network_presence": [man.pk],
    })

    response = view(request, pk=account.pk)
    assert response.status_code == 202, response.__dict__


@pytest.mark.django_db
def test_accounts_view_set__update_billing_information():
    """Test updating a account"""
    view = views.AccountsViewSet.as_view({"put": "update"})
    account = baker.make("crm.Account")

    # Should not be a billing account
    assert not account.billing_information

    request = authorized_requests.put(account, {
        "id": account.pk,
        "name": "NewAccountName",
        "address": {
            "country": "DE",
            "locality": "Berlin",
            "postal_code": "10202",
            "street_address": "Wegweg 23",
        },
        "billing_information": {
            "name": "Account Accounting Inc.",
            "address": {
                "country": "GB",
                "locality": "Westernshire",
                "postal_code": "L2391",
                "street_address": "23 Billingstreet",
            },
        }
    })

    response = view(request, pk=account.pk)
    assert response.status_code == 202, response.__dict__

    # Should be a billing account
    assert not account.billing_information


@pytest.mark.django_db
def test_accounts_view_set__partial_update():
    """Test updating parts of the account"""
    view = views.AccountsViewSet.as_view({"patch": "partial_update"})
    account = baker.make("crm.Account")

    request = authorized_requests.patch(account, {
        "external_ref": "FOO-BAR-23",
    })

    response = view(request, pk=account.pk)
    assert response.status_code == 202, response.__dict__


@pytest.mark.django_db
def test_account_view_set__destroy():
    """Test account deletion"""
    view = views.AccountsViewSet.as_view({"delete": "destroy"})
    account = baker.make("crm.Account")

    request = authorized_requests.delete(account)

    response = view(request, pk=account.pk)
    assert response.status_code == 200, response.__dict__


#
# Contacts
#

@pytest.mark.django_db
def test_account_view_set__create__with_parent():
    """Provide a different parent"""
    view = views.AccountsViewSet.as_view({"post": "create"})
    account = baker.make("crm.Account")
    new_account = baker.prepare("crm.Account")
    # With parent id
    parent = baker.make("crm.Account", scoping_account=account)
    request = authorized_requests.post(account, {
        "name": new_account.name[:23],
        "managing_account": parent.pk,
        "address": {
            "country": "DE",
            "locality": "Berlin",
            "postal_code": "10202",
            "street_address": "Wegweg 23",
        },
    })
    response = view(request)
    assert response.status_code == 201, response.__dict__


@pytest.mark.django_db
def test_contacts_view_set__list():
    """Test listing all contacts"""
    view = views.ContactsViewSet.as_view({"get": "list"})
    account = baker.make("crm.Account")
    request = authorized_requests.get(account)

    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_contact_view_set__retrieve():
    """Test getting a specific account contact"""
    view = views.ContactsViewSet.as_view({"get": "retrieve"})
    account = baker.make(
        "crm.Account")
    contact = baker.make(
        "crm.Contact",
        scoping_account=account)

    request = authorized_requests.get(account)
    response = view(request, pk=contact.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_contact_view_set__create():
    """Test creating a contact"""
    view = views.ContactsViewSet.as_view({"post": "create"})
    account = baker.make("crm.Account")
    request = authorized_requests.post(account, {
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "name": "Test Implementation Contact",
        "email": "foo@bar.net",
    })

    response = view(request)
    assert response.status_code == 201


@pytest.mark.django_db
def test_contact_view_set__update():
    """Test updating a contact"""
    view = views.ContactsViewSet.as_view({"put": "update"})
    account = baker.make("crm.Account")
    contact = baker.make(
        "crm.Contact",
        scoping_account=account)
    request = authorized_requests.put(account, {
        "id": contact.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "email": "noc@test.example.net",
    })

    response = view(request, pk=contact.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_contact_view_set__partial_update():
    """Test partial updating a contact"""
    view = views.ContactsViewSet.as_view({"patch": "partial_update"})
    account = baker.make("crm.Account")
    contact = baker.make(
        "crm.Contact",
        scoping_account=account)
    request = authorized_requests.patch(account, {
        "email": "noc@test.example.net",
    })

    response = view(request, pk=contact.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_contact_view_set__destroy():
    """Test deleting a contact"""
    view = views.ContactsViewSet.as_view({"delete": "destroy"})
    account = baker.make("crm.Account")
    contact = baker.make(
        "crm.Contact",
        scoping_account=account)
    request = authorized_requests.delete(account)

    response = view(request, pk=contact.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_roles_view_set__list():
    """
    Test getting a list of roles
    """
    account = baker.make("crm.Account")
    view = views.RolesViewSet.as_view({"get": "list"})
    request = authorized_requests.get(account)
    response = view(request)

    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_roles_view_set__retrieve():
    """
    Test getting a single role
    """
    noc_role = contacts_svc.get_default_role("noc")
    account = baker.make("crm.Account")
    view = views.RolesViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(account)
    response = view(request, pk=noc_role.pk)

    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_role_assignments_view_set__list():
    """Test listing all role assignments"""
    account = baker.make("crm.Account")
    contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=account)

    baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=contacts_svc.get_default_role("noc"))
    baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))

    # Make request
    view = views.RoleAssignmentsViewSet.as_view({"get": "list"})
    request = authorized_requests.get(account)
    response = view(request)

    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_role_assignments_view_set__retrieve():
    """Test getting a role assignment"""
    account = baker.make("crm.Account")
    role = contacts_svc.get_default_role("noc")
    contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=account)

    assignment = baker.make(
        "crm.RoleAssignment",
        role=role,
        contact=contact)

    # Make request
    view = views.RoleAssignmentsViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(account)
    response = view(request, pk=assignment.pk)

    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_role_assignments_view_set__create():
    """Test assigning a role"""
    account = baker.make("crm.Account")
    role = contacts_svc.get_default_role("noc")
    contact = baker.make(
        "crm.Contact",
        name="some noc",
        email="noc@example.net",
        telephone="+123 123 123",
        scoping_account=account,
        consuming_account=account)

    # Make request
    view = views.RoleAssignmentsViewSet.as_view({"post": "create"})
    request = authorized_requests.post(account, {
        "contact": contact.pk,
        "role": role.pk,
    })
    response = view(request)
    assert response.status_code == 201, response.__dict__


@pytest.mark.django_db
def test_role_assignments_view_set__destroy():
    """Test removal of a role assignment"""
    account = baker.make("crm.Account")
    role = contacts_svc.get_default_role("noc")
    contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=account)
    assignment = baker.make(
        "crm.RoleAssignment",
        role=role,
        contact=contact)

    # Make request
    view = views.RoleAssignmentsViewSet.as_view({"delete": "destroy"})
    request = authorized_requests.delete(account)
    response = view(request, pk=assignment.pk)

    assert response.status_code == 200, response.__dict__
