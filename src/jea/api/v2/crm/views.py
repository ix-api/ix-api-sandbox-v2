
"""
CRM :: Views

Manage accounts and account contacts.
"""

from rest_framework import exceptions, status
from django.db.models.deletion import ProtectedError

from ixapi_schema.v2 import schema

from jea.api.v2.permissions import require_account
from jea.api.v2.response import tags
from jea.api.v2.viewsets import IxApiViewSet
from jea.api.v2.response import ApiSuccess, ApiError
from jea.crm.services import (
    accounts as accounts_svc,
    contacts as contacts_svc,
)
from jea.crm.models import Account
from jea.crm.changes import (
    create_account,
    update_account,
)
from jea.stateful import dispatch
from jea.exceptions import UnableToFulfillError

def _filter_protected_account_data(
        scoping_account,
        account,
    ):
    """
    If the account was included in the result set, because
    it was flagged as discoverable, we still do not want
    to present all information.

    :param scoping_account: The account from the request context.
    :param account: The account to present to the view
    """
    # The account is within the current accounts scope,
    # everything is fine.
    if account.scoping_account == scoping_account:
        return account

    # Outside the current account scope,
    # create minimal account representation
    return {
        "id": account.pk,
        "name": account.name,
        "metro_area_network_presence": account.metro_area_network_presence,
    }


class AccountsViewSet(IxApiViewSet):
    """
    A `Account` is a company using services from an IXP. The accounts
    can have a hierarchy. A account can have subaccounts. Each account needs
    to have different contacts, depending on the exchange and the service he
    wants to use.

    For a account to become operational, you need to provide a
    `legal` contact.
    """
    @require_account
    def list(self, request, account=None):
        """
        Retrieve a list of accounts.

        This includes all accounts the current authorized account
        is managing and the current account itself.
        """
        accounts = accounts_svc.get_accounts(
            scoping_account=account,
            filters=request.query_params,
            include_discoverable=True)

        # Filter sensitive information about the account,
        # if the scoping account does not match the account.
        # This can happen if the account was discoverable.
        accounts = (_filter_protected_account_data(account, c)
                    for c in accounts)
        serializer = schema.Account(accounts, many=True)
        return serializer.data

    @require_account
    def retrieve(self, request, account=None, pk=None):
        """
        Get a single account.
        """
        # Retrieve account
        requested_account = accounts_svc.get_account(
            allow_discoverable=True,
            scoping_account=account,
            account=pk)
        serializer = schema.Account(_filter_protected_account_data(
            account, requested_account))

        return serializer.data

    @require_account
    def create(self, request, account=None):
        """
        Create a new account.

        Please remember that a account may require some `contacts`
        to be created. Otherwise it will stay in an `error` state.
        """
        # Make a change request
        new_account = dispatch.request(account, create_account(request.data))
        serializer = schema.Account(new_account)
        return ApiSuccess(serializer.data, status=status.HTTP_201_CREATED)

    @require_account
    def update(self, request, account=None, pk=None):
        """Update a account."""
        # Load account and deserialize request. Of course we
        # do not allow for discoverable accounts here.
        upd_account = accounts_svc.get_account(
            scoping_account=account,
            account=pk)

        # Make an update change request
        upd_account = dispatch.request(
            account,
            update_account(upd_account, request.data))

        serializer = schema.Account(upd_account)
        return ApiSuccess(serializer.data, status=status.HTTP_202_ACCEPTED)

    @require_account
    def partial_update(self, request, account=None, pk=None):
        """Update some fields of a account."""
        # Load account and deserialize request. Of course we
        # do not allow for discoverable accounts here.
        upd_account = accounts_svc.get_account(
            scoping_account=account,
            account=pk)
        # Make an update change request
        upd_account = dispatch.request(
            account,
            update_account(upd_account, request.data, partial=True))
        serializer = schema.Account(upd_account)
        return ApiSuccess(serializer.data, status=status.HTTP_202_ACCEPTED)

    @require_account
    def destroy(self, request, account=None, pk=None):
        """
        Delete an account.

        Accounts will be deleted when they are not in use as a
        `managing_account` or `billing_account`.

        If the preconditions are not met, the request will fail.

        Note: This is not stateful.
        """
        try:
            destroyed_account = accounts_svc.delete_account(
                account=pk,
                scoping_account=account)

            serializer = schema.Account(destroyed_account)
            return ApiSuccess(serializer.data)

        except ProtectedError:
            raise UnableToFulfillError(
                detail="the account is still in use")



class ContactsViewSet(IxApiViewSet):
    """
    A `Contact` is a role undertaking a specific responsibility within a
    account, typically a department or agent of the account company.

    These can be `implementation`,`noc`, `legal`, `peering` or `billing`.

    A contact is bound to the account by the `consuming_account`
    property.
    """
    @require_account
    def list(self, request, account=None):
        """List available contacts managed by the authorized account"""
        contacts = contacts_svc.get_contacts(
            scoping_account=account,
            filters=request.query_params)

        serializer = schema.Contact(contacts, many=True)
        return serializer.data

    @require_account
    def retrieve(self, request, account=None, pk=None):
        """Get a contact by it's id"""
        contact = contacts_svc.get_contact(
            scoping_account=account,
            contact=pk)
        serializer = schema.Contact(contact)

        return serializer.data

    @require_account
    def create(self, request, account=None):
        """
        Create a new contact.

        Please remember to set the correct `type` of the contact
        when sending the request. Available types are `noc`, `billing`,
        `legal` and `implementation`.
        """
        # Validate input
        serializer = schema.ContactRequest(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Everythings fine, lets save our contact
        contact = contacts_svc.create_contact(
            scoping_account=account,
            contact_request=serializer.validated_data)

        # And respond with our fresh contact
        serializer = schema.Contact(contact)

        return ApiSuccess(serializer.data, status=status.HTTP_201_CREATED)

    @require_account
    def update(self, request, account=None, pk=None):
        """Update a contact"""
        contact = contacts_svc.get_contact(
            scoping_account=account,
            contact=pk)

        # Input validation
        serializer = schema.ContactRequest(data=request.data)

        serializer.is_valid(raise_exception=True)
        update = serializer.validated_data

        # Perform update
        contact = contacts_svc.update_contact(
            scoping_account=account,
            contact=contact,
            contact_update=update)

        serializer = schema.Contact(contact)

        return serializer.data

    @require_account
    def partial_update(self, request, account=None, pk=None):
        """Update parts of a contact"""
        contact = contacts_svc.get_contact(
            scoping_account=account,
            contact=pk)

        # Input validation
        serializer = schema.ContactRequest(
            data=request.data,
            partial=True)

        # Validate input
        serializer.is_valid(raise_exception=True)

        # Update contact
        contact = contacts_svc.update_contact(
            scoping_account=account,
            contact=contact,
            contact_update=serializer.validated_data)
        serializer = schema.Contact(contact)

        return serializer.data

    @require_account
    def destroy(self, request, account=None, pk=None):
        """Remove a contact"""
        contact = contacts_svc.delete_contact(
            scoping_account=account,
            contact=pk)

        # Serialize old data
        serializer = schema.Contact(contact)
        return serializer.data


class RolesViewSet(IxApiViewSet):
    """
    View set for all roles
    """
    @require_account
    def list(self, request, account=None):
        """List available roles"""
        roles = contacts_svc.get_roles()
        serializer = schema.Role(roles, many=True)

        return serializer.data

    @require_account
    def retrieve(self, request, account=None, pk=None):
        """Get a single role by it's id"""
        role = contacts_svc.get_role(role=pk)
        serializer = schema.Role(role)

        return serializer.data


class RoleAssignmentsViewSet(IxApiViewSet):
    """
    Manage role assignments for contacts
    """
    @require_account
    def list(self, request, account=None):
        """
        List all role assignments for all contacts
        of the requesting account.
        """
        assignments = contacts_svc.get_role_assignments(
            scoping_account=account)

        serializer = schema.RoleAssignment(
            assignments, many=True)

        return serializer.data

    @require_account
    def retrieve(self, request, account=None, pk=None):
        """Get a single role assignment"""
        assignment = contacts_svc.get_role_assignment(
            scoping_account=account,
            role_assignment=pk)

        serializer = schema.RoleAssignment(assignment)
        return serializer.data


    @require_account
    def create(self, request, account=None):
        """Create a new role assignment"""
        assignment = schema.RoleAssignmentRequest(data=request.data)
        assignment.is_valid(raise_exception=True)

        role = assignment.validated_data["role"]
        contact = assignment.validated_data["contact"]

        # Assign role, if possible
        assignment = contacts_svc.assign_role(
            role=role,
            contact=contact,
            scoping_account=account)

        serializer = schema.RoleAssignment(assignment)
        return serializer.data, status.HTTP_201_CREATED


    @require_account
    def destroy(self, request, account=None, pk=None):
        """
        Remove a role assignment
        """
        # Unassign role
        assignment = contacts_svc.destroy_role_assignment(
            role_assignment=pk,
            scoping_account=account)

        serializer = schema.RoleAssignment(assignment)
        return serializer.data
