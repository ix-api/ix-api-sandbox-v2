"""
Test CRM entities
"""

import pytest
from model_bakery import baker
from rest_framework import serializers
from ixapi_schema.v2 import schema


@pytest.mark.django_db
def test_account_serializer():
    """Test account serializer"""
    root_account = baker.make("crm.Account")
    account = baker.make("crm.Account", managing_account=root_account)

    serializer = schema.Account(account)

    # Assertations
    assert serializer.data["managing_account"] == str(root_account.pk)


@pytest.mark.django_db
def test_contact_serialization():
    """Test polymorphic contact serializer"""
    contact = baker.make("crm.Contact")

    serializer = schema.Contact(contact)
    data = serializer.data

    # Check response
    assert data, "Serializer should ouput data"


@pytest.mark.django_db
def test_polymorphic_contact_deserialization():
    """Test polymorphic contact deserialization"""
    account = baker.make("crm.Account")
    # Request data:
    data = {
        "email": "e@mail.com",
        "managing_account": account.pk,
        "consuming_account": account.pk,
    }

    # Deserialization
    serializer = schema.ContactRequest(data=data)
    assert serializer.is_valid(raise_exception=True), \
        "Serializer should accept valid data"

    # Some invalid data
    serializer = schema.ContactRequest(data={"foo": 23})
    with pytest.raises(serializers.ValidationError):
        serializer.is_valid(raise_exception=True)
