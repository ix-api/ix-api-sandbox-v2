import logging

from ixapi_schema import v2
from django.apps import AppConfig

logger = logging.getLogger(__name__)


class ApiConfig(AppConfig):
    name = 'jea.api.v2'

    def ready(self):
        """Application ready"""
        logger.info("Initializing app: {}".format(self.name))
        logger.info(
            "Using ix-api-schema v.{}".format(v2.__version__))


