
"""
JEA Api Permissions
"""

import logging
from functools import wraps

from rest_framework.permissions import BasePermission

from jea.auth import roles
from jea.auth.exceptions import (
    AuthorizationError,
    AuthorizationIncompleteError,
)
from jea.crm.models import Account

logger = logging.getLogger(__name__)


#
# TODO: v2. stilll not elegant. make elegant.
#

class HasAccessRole(BasePermission):
    """
    Check if an accessing ApiUser has the 'access' role.
    """
    def has_permission(self, request, view):
        """Test if role is assigned"""
        logger.debug(
            "checking for role '%s' in '%s'",
            roles.ACCESS,
            request.user.access_roles,
        )

        return roles.ACCESS in request.user.access_roles


class HasTokenRefreshRole(BasePermission):
    """
    Check if an accessing User has the 'token_refresh' role.
    """
    def has_permission(self, request, view):
        """Test if role is assigned"""
        logger.debug(
            "checking for role '%s' in '%s'",
            roles.TOKEN_REFRESH,
            request.user.access_roles,
        )

        return roles.TOKEN_REFRESH in request.user.access_roles


def require_account(view):
    """
    The view requires a account set in the request.

    If a account is not available in the request,
    raise a permission denied access error.

    :param view: a django view
    """
    @wraps(view)
    def _wrapper(self, request, *args, **kwargs):
        """Get account and root account"""
        user = request.user
        try:
            if not user.account:
                raise AuthorizationIncompleteError
        except Account.DoesNotExist:
            logger.error("account missing for user: %s", user)
            raise AuthorizationIncompleteError
        except Exception as e:
            logger.error("authentication user error: %s", e)
            raise AuthorizationIncompleteError

        # Add account to kwargs
        kwargs["account"] = request.user.account

        return view(self, request, *args, **kwargs)

    return _wrapper

