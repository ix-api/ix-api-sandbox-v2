
import pytest
from model_bakery import baker
from ixapi_schema.v2 import schema


@pytest.mark.django_db
def test_status_serializer():
    """Test status serializer"""
    status = baker.prepare("stateful.StatusMessage")
    serializer = schema.Status(status)

    assert serializer.data, "Serializer should serialize without crashing"

