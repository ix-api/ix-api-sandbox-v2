
"""
IX-API ViewSets
---------------

API (v2) ViewSets are like normal view sets, but expect an
implicit Success or explicit ErrorResponse.
"""

from functools import wraps

from rest_framework import exceptions, status
from rest_framework import serializers
from rest_framework.metadata import SimpleMetadata
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ViewSet
from rest_framework.authentication import SessionAuthentication
from ixapi_schema.openapi import components

from jea.api.v2.auth.handlers import (
    JWTAuthentication,
    ApiSessionAuthentication,
)
from jea.api.v2.permissions import HasAccessRole
from jea.api.v2.errors.views import exception_handler
from jea.api.v2.response import (
    ApiResponse,
    ApiError,
)


def _get_meta_refs(view):
    """
    Get meta references from viewset's serializer class.

    :param view: a jea view set
    :type view: IxApiViewSet
    """
    serializer_class = getattr(view, "serializer_class", None)
    if not serializer_class:
        return {}

    serializer_meta = getattr(serializer_class, "Meta", None)
    if not serializer_meta:
        return {}

    refs = getattr(serializer_meta, "refs", {})

    return refs


def wrap_response(response):
    """
    Ensure response is wrapped in an ApiResponse object
    """
    response_status = status.HTTP_200_OK
    # We allow to return a tuple with the response data
    # and an http status code
    if isinstance(response, tuple):
        response, response_status = response

    # If the response is an ixapi component
    # we wrap the data:
    if isinstance(response, components.Component):
        response = response.data
    elif isinstance(response, serializers.ListSerializer):
        response = response.data

    # If response is not an api response, wrap it.
    if not isinstance(response, ApiResponse):
        response = ApiResponse(response, status=response_status)

    return response


class IxApiMetadata(SimpleMetadata):
    """Generate endpoint metadata"""

    def get_filters_metadata(self, view):
        """
        Get available filters.

        CAVEAT: The way this is done right now is a classic
                leaky abstraction, where the service should
                be encapsulated from the API.

                However in the API the filters from the service
                are directly imported.

                While being pragmatic right now and getting things
                done, the way of communicating the available filters
                to the API layer should be improved.
        """
        filterset_class = getattr(view, "filterset_class", None)
        if not filterset_class:
            return []

        return [name for name in filterset_class.get_filters()]

    def get_references_metadata(self, view):
        """
        Get reference resolve hints: If there is some other
        resource referenced in the serialization result,
        it can be resolved by using the hint from the metadata.
        """
        refs = getattr(view, "_meta_refs", {})

        return refs

    def get_schema(self, request, view) -> dict:
        """Get schema info"""
        serializer_class = getattr(view, "serializer_class", None)
        if not serializer_class:
            return {}

        if not isinstance(view, IxApiViewSet):
            return {}

        result_schema = self.get_serializer_info(serializer_class())

        get_action = view.action_map.get("get")
        if not get_action:
            return {}

        if get_action == "list":
            result_schema = [result_schema]

        return result_schema

    def determine_metadata(self, request, view):
        """
        Generate metadata schema encapsulated in
        a standard api envelope.
        """
        metadata = {
            "name": view.get_view_name(),
            "description": view.get_view_description(),
            # "result_schema": self.get_schema(request, view),
            "query_filters": self.get_filters_metadata(view),
            # "refs": self.get_references_metadata(view),
        }

        return metadata


class IxApiViewSetMeta(type):
    """
    IX-API ViewSet Metaclass

    Decorate IxAPiViewSet derived classes with a response
    wrapper for ensuring the result is an ApiResponse.
    """
    @classmethod
    def _decorate_handler(cls, handler):
        @wraps(handler)
        def wrapper(self, *args, **kwargs):
            response = handler(self, *args, **kwargs)
            return wrap_response(response)

        return wrapper


    def __new__(cls, name, bases, attrs):
        """Create new IxApiViewSet class, decorate handlers"""
        # Decorate all handler functions
        handlers = [
            "list", "create", "retrieve", "update",
            "partial_update", "destroy",
        ]
        for method in handlers:
            handler = attrs.get(method)
            if handler:
                attrs[method] = cls._decorate_handler(handler)

        view_set = super(IxApiViewSetMeta, cls). \
            __new__(cls, name, bases, attrs)

        # Set meta refs
        view_set._meta_refs = _get_meta_refs(view_set)

        return view_set


class IxApiViewSet(ViewSet, metaclass=IxApiViewSetMeta):
    """IX-API ViewSet"""
    # Set default permission classes for all IxApiViewSets
    permission_classes = (
        IsAuthenticated,
        HasAccessRole,
    )
    authentication_classes = (
        ApiSessionAuthentication,
        JWTAuthentication,
    )
    metadata_class = IxApiViewSetMeta

    def get_exception_handler(self):
        """Override exception handler for jea viewsets"""
        return exception_handler

