"""
IX-API v2 urls
"""

from django.conf.urls import handler404
from django.urls import path, include, re_path
from rest_framework import routers

from jea.api.v2.auth import (
    handlers as auth_handlers,
    views as auth_views
)
from jea.api.v2.errors import views as errors_views
from jea.api.v2.config import views as config_views
from jea.api.v2.catalog import views as catalog_views
from jea.api.v2.service import views as service_views
from jea.api.v2.ipam import views as ipam_views
from jea.api.v2.crm import views as crm_views
from jea.api.v2.introspection import views as introspection_views


class IxApiSandboxView(routers.APIRootView):
    """IX-API"""
    authentication_classes = (
        auth_handlers.ApiSessionAuthentication,
        auth_handlers.JWTAuthentication,
    )


class IxApiRouter(routers.DefaultRouter):
    """API Router"""
    APIRootView = IxApiSandboxView

# Create router and register our viewsets
router = IxApiRouter(trailing_slash=False)

# == Auth:
router.register(r"auth/token",
                auth_views.TokenViewSet,
                basename="auth_token")

router.register(r"auth/refresh",
                auth_views.RefreshTokenViewSet,
                basename="auth_refresh")

router.register(r"account",
                auth_views.AccountViewSet,
                basename="account")

# == CRM:
router.register(r"accounts",
                crm_views.AccountsViewSet,
                basename="accounts")

router.register(r"contacts",
                crm_views.ContactsViewSet,
                basename="contacts")

router.register(r"roles",
                crm_views.RolesViewSet,
                basename="roles")

router.register(r"role-assignments",
                crm_views.RoleAssignmentsViewSet,
                basename="role-assignments")


# == Catalog:
router.register(r"facilities",
                catalog_views.FacilitiesViewSet,
                basename="facilties")

router.register(r"devices",
                catalog_views.DevicesViewSet,
                basename="devices")

router.register(r"product-offerings",
                catalog_views.ProductOfferingsViewSet,
                basename="product-offerings")

router.register(r"pops",
                catalog_views.PointsOfPresenceViewSet,
                basename="pops")

router.register(r"metro-areas",
                catalog_views.MetroAreasViewSet,
                basename="pops")

router.register(r"metro-area-networks",
                catalog_views.MetroAreaNetworksViewSet,
                basename="pops")


# == Ports and Connections
router.register(r"ports",
                config_views.PortsViewSet,
                basename="ports")

router.register(r"port-reservations",
                config_views.PortReservationsViewSet,
                basename="port-reservations")

router.register(r"connections",
                config_views.ConnectionsViewSet,
                basename="connections")

# == Network Services and Features
router.register(r"network-services",
                service_views.NetworkServicesViewSet,
                basename="network-services")

router.register(r"network-features",
                service_views.NetworkFeaturesViewSet,
                basename="network-features")

# == Member joining rules
router.register(r"member-joining-rules",
                service_views.MemberJoiningRulesViewSet,
                basename="member-joining-rules")

# == Network Services and Feature Configurations
router.register(r"network-service-configs",
                config_views.NetworkServiceConfigsViewSet,
                basename="network-service-configs")

router.register(r"network-feature-configs",
                config_views.NetworkFeatureConfigsViewSet,
                basename="network-service-configs")

# == Ip and Mac addresses:
router.register(r"ips",
                ipam_views.IpAddressViewSet,
                basename="ip-addresses")

router.register(r"macs",
                ipam_views.MacAddressViewSet,
                basename="mac-addresses")

# == Service Introspection
router.register(r"implementation",
                introspection_views.ImplementationViewSet,
                basename="implementation")

router.register(r"health",
                introspection_views.HealthViewSet,
                basename="health")

router.register(r"extensions",
                introspection_views.ExtensionsViewSet,
                basename="extensions")


urlpatterns = [
    path("", include(router.urls)),
    re_path("(?P<path>.*)", errors_views.resource_not_found),
]

