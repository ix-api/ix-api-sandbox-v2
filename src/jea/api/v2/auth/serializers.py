
from rest_framework import serializers

from jea.auth import roles
from jea.auth.exceptions import TokenError
from jea.auth.services import token_authentication as token_authentication_svc

#
# Fields
#
class JWTField(serializers.Field):
    """
    Serializer field for Json Web tokens
    """
    def to_representation(self, payload):
        """Encode payload as JWT"""
        return token_authentication_svc.encode(payload)


    def to_internal_value(self, data):
        """Decode the JWT"""
        return token_authentication_svc.decode(data)


class RefreshTokenField(JWTField):
    """
    Serializer field for the refresh token:
    This acts like a JWT field, however it is checked if
    the token payload contains the TOKEN_REFRESH role.
    """
    def to_internal_value(self, data):
        """Decode and check role"""
        payload = super(RefreshTokenField, self).to_internal_value(data)

        if not roles.TOKEN_REFRESH in payload["roles"]:
            raise TokenError("Token has no refresh permissions")

        return payload

#
# Serializers
#

class AuthTokenRequestSerializer(serializers.Serializer):
    """AuthTokenRequest"""
    api_key = serializers.CharField(max_length=16,
                                    min_length=16,
                                    required=True)
    api_secret = serializers.CharField(max_length=86,
                                       min_length=86,
                                       required=True)



class AuthTokensSerializer(serializers.Serializer):
    """AuthToken"""
    access_token = serializers.CharField(required=True)
    refresh_token = serializers.CharField(required=True)


class RefreshTokenRequestSerializer(serializers.Serializer):
    """RefreshTokenRequest"""
    refresh_token = RefreshTokenField(required=True)

