
"""
JEA API Authentication

Get your access and refresh tokens here.
"""

import logging

from rest_framework.request import Request
from ixapi_schema.v2.schema import Account

from jea.auth.services import (
    token_authentication as token_authentication_svc,
)
from jea.api.v2.viewsets import  IxApiViewSet
from jea.api.v2.response import ApiSuccess, ApiResponse
from jea.api.v2.auth import serializers
from jea.api.v2.auth.handlers import JWTAuthentication
from jea.api.v2.permissions import require_account

logger = logging.getLogger(__name__)



class TokenViewSet(IxApiViewSet):
    """
    Open an authorized session with the IX-API by
    sending an `api_key` and `api_secret` pair to the
    `/api/v2/tokens` resource.

    This is equivalent to a 'login' endpoint.
    """
    authentication_classes = ()
    permission_classes = ()

    def create(self, request: Request) -> ApiResponse:
        """
        Authenticate a account identified by `api_key` and `api_secret`.
        """
        serializer = serializers.AuthTokenRequestSerializer(
            data=request.data)

        # Validate create token request
        serializer.is_valid(raise_exception=True)
        valid_request = serializer.validated_data

        logger.debug("Authenticating token request: %s", valid_request)

        # Authenticate via api key
        access_token, refresh_token = \
            token_authentication_svc.authenticate_api_key(
                valid_request["api_key"], valid_request["api_secret"])

        tokens_serializer = serializers.AuthTokensSerializer({
            "access_token": access_token,
            "refresh_token": refresh_token,
        })

        return ApiSuccess(tokens_serializer.data)


class RefreshTokenViewSet(IxApiViewSet):
    """
    The `auth_token` (and `refresh_token`) have limited lifetimes.
    As both are JWTs, you can directy get this information from them.

    When the session (`access_token`) expires, you can (within
    the lifetime of the `refresh_token`) reauthenticate by
    providing only the `refresh_token`.
    """
    authentication_classes = ()
    permission_classes = ()

    def create(self, request: Request) -> ApiResponse:
        """
        Reauthenticate the API user, issue a new `access_token`
        and `refresh_token` pair by providing the `refresh_token`
        in the request body.
        """
        serializer = serializers.RefreshTokenRequestSerializer(
            data=request.data)

        # Validate refresh token request
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        # Issue new tokens
        access_token, refresh_token = \
            token_authentication_svc.refresh(validated_data["refresh_token"])

        tokens_serializer = serializers.AuthTokensSerializer({
            "access_token": access_token,
            "refresh_token": refresh_token,
        })

        return ApiSuccess(tokens_serializer.data)


class AccountViewSet(IxApiViewSet):
    """
    Show the currently authenticated account
    """
    @require_account
    def list(self, request, account=None):
        """Show the currently authenticated account"""
        serializer = Account(account)

        return ApiSuccess(serializer.data)


