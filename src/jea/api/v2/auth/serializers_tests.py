
from datetime import timedelta

import pytest

from jea.auth import roles
from jea.auth.exceptions import TokenError
from jea.auth.services import token_authentication as token_authentication_svc
from jea.api.v2.auth import serializers

#
# Serializer Field Test
#
def test_jwt_field_to_representation():
    field = serializers.JWTField()
    data = field.to_representation({
        "foo": "bar",
        "baz": 42,
    })

    assert data
    assert isinstance(data, str)


def test_jwt_field_to_internal_value():
    ttl = timedelta(minutes=30)
    token = token_authentication_svc.issue_token(23, lifetime=ttl)

    field = serializers.JWTField()
    data = field.to_internal_value(token)

    assert data
    assert isinstance(data, dict)


def test_rrefresh_token_field_to_internal_value():
    field = serializers.RefreshTokenField()
    ttl = timedelta(minutes=30)

    # This should be ok: Valid token with refresh role
    token = token_authentication_svc.issue_token(
        23, # Account id
        roles=[roles.TOKEN_REFRESH],
        lifetime=ttl)
    data = field.to_internal_value(token)

    assert data
    assert isinstance(data, dict)

    # This should not work: Token with access role
    token = token_authentication_svc.issue_token(
        23, # account id
        roles=[roles.ACCESS],
        lifetime=ttl)

    with pytest.raises(TokenError):
        field.to_internal_value(token)


#
# Auth Token Request Serializer
#
def test_auth_token_request_serializer_serialize():
    """Serialize a token request"""
    serializer = serializers.AuthTokenRequestSerializer({
        "api_key": "test00001",
        "api_secret": "secret00001",
    })

    # Assertions 
    result = serializer.data
    assert result is not None


def test_auth_token_request_serializer_deserialize():
    """Deserialize a token request"""
    api_key = "A" * 16
    api_secret = "A" * 86

    serializer = serializers.AuthTokenRequestSerializer(data={
        "api_key": api_key,
        "api_secret": api_secret,
    })

    assert serializer.is_valid()
    assert serializer.validated_data is not None


def test_auth_token_request_serializer_validation():
    """Trigger validation errors"""
    # Missing required fields
    serializer = serializers.AuthTokenRequestSerializer(data={})
    assert not serializer.is_valid(), \
        "Missing fields should trigger errors"

    # Input length validation
    serializer = serializers.AuthTokenRequestSerializer(data={
        "api_key": "A",
        "api_secret": "B",
    })
    assert not serializer.is_valid(), \
        "Invalid input length should trigger errors"

    serializer = serializers.AuthTokenRequestSerializer(data={
        "api_key": "A" * 23,
    }, partial=True)

    assert not serializer.is_valid(), \
        "Input length validation for 'api_key'"

    serializer = serializers.AuthTokenRequestSerializer(data={
        "api_secret": "B" * 90,
    }, partial=True)

    assert not serializer.is_valid(), \
        "Input length validation for 'api_key'"

    # The positive path is covered by
    # `test_token_request_serializer_deserialize.


#
# Auth Token Serializer
#
def test_auth_tokens_serializer():
    """Auth Token Serializer smoke test"""
    serializer = serializers.AuthTokensSerializer({
        "access_token": "AAAAA...",
        "refresh_token": "AAAAA...",
    })

    assert serializer.data is not None, \
        "Serializer should serialize."

