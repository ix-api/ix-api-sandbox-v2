
"""
Test Authentication Views
"""

import pytest
import mock
from model_bakery import baker
import json

from rest_framework import status
from rest_framework.test import APIRequestFactory

from jea.test import authorized_requests
from jea.auth import models as auth_models
from jea.auth.services import token_authentication as token_authentication_svc
from jea.crm import models as crm_models
from jea.api.v2.auth import views
from jea.api.v2.response import tags


@pytest.mark.django_db
def tests_token_viewset_create_valid():
    """Make a valid request to the create endpoint"""
    # Setup user
    account = baker.make(crm_models.Account)
    user = baker.make(auth_models.User, account=account)

    # Setup view
    view = views.TokenViewSet.as_view({"post": "create"})

    # Make request
    request = APIRequestFactory().post("/auth/token", {
        "api_key": user.api_key,
        "api_secret": user.api_secret,
    })

    response = view(request)
    assert response.status_code == 200, \
        "Response should be OK"

    assert response.data["access_token"]
    assert response.data["refresh_token"]


def tests_token_viewset_create_invalid_input():
    """Make a request to the create endpoint with malformed data"""
    view = views.TokenViewSet.as_view({"post": "create"})
    request = APIRequestFactory().post("/auth/token", {
        "api_key": "A" * 5,
    })

    response = view(request)

    # Assertations
    assert response.status_code == status.HTTP_400_BAD_REQUEST, \
        "Request should fail with an validation error"


@pytest.mark.django_db
def tests_token_viewset_create_credentails_error():
    """Make a valid request to the create endpoint"""
    # Setup view
    view = views.TokenViewSet.as_view({"post": "create"})

    # Make request
    request = APIRequestFactory().post("/auth/token", {
        "api_key": "A" * 16,
        "api_secret": "A" * 86,
    })

    response = view(request)
    assert response.status_code == 403, \
        "Response should be Forbidden"


@pytest.mark.django_db
def tests_refresh_token_viewset_create_valid():
    """Make a valid request to the create endpoint"""
    # Setup user
    account = baker.make(crm_models.Account)

    # Create token
    _, refresh_token = \
        token_authentication_svc.issue_tokens(account.pk)

    # Setup view
    view = views.RefreshTokenViewSet.as_view({"post": "create"})

    # Make request
    request = APIRequestFactory().post("/auth/refresh", {
        "refresh_token": refresh_token,
    })

    response = view(request)
    assert response.status_code == 200, \
        "Response should be OK"

    assert response.data["access_token"]
    assert response.data["refresh_token"]


@pytest.mark.django_db
def tests_refresh_token_viewset_create_invalid():
    """Make a invalid request to the create endpoint"""
    # Setup user
    account = baker.make(crm_models.Account)

    # Create token
    access_token, _ = \
        token_authentication_svc.issue_tokens(account.pk)

    # Setup view
    view = views.RefreshTokenViewSet.as_view({"post": "create"})

    # Make request
    request = APIRequestFactory().post("/auth/refresh", {
        "refresh_token": access_token,
    })

    response = view(request)
    assert response.status_code == 403, \
        "Response should be forbidden"


@pytest.mark.django_db
def test_account_viewset_list():
    """Test the account retrieve endpoint"""
    # Setup user
    account = baker.make(crm_models.Account)
    request = authorized_requests.get(account)

    # Setup view
    view = views.AccountViewSet.as_view({"get": "list"})
    response = view(request)

    assert response.status_code == 200, \
        "Response should be OK"

    assert response.data["id"] == str(account.pk)


