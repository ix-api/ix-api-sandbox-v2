
"""
Test IPAM serializers
"""

import pytest
from model_bakery import baker
from rest_framework import serializers
from ixapi_schema.v2 import schema
from ixapi_schema.v2.schema import (
    IpVersion,
    AddressFamilies,
)
from ixapi_schema.openapi.components import (
    IpVersionField,
    MacAddressField,
)

from jea.ipam import models as ipam_models


def test_mac_address_field__to_internal_value():
    """Test mac address validation"""
    field = MacAddressField()
    mac = "00:29:18:2f:45:2b"
    assert mac == field.to_internal_value(mac)

    # Validate length
    with pytest.raises(serializers.ValidationError):
        field.to_internal_value("aa:bb")

    with pytest.raises(serializers.ValidationError):
        field.to_internal_value("00:2f:28:f8:1f:95:4b:95")

    with pytest.raises(serializers.ValidationError):
        field.to_internal_value("00:aa:bb:cc:xx:yy")


def test_ip_version_field__to_representation():
    """Test IP version field"""
    field = IpVersionField()
    version = IpVersion(4)
    result = field.to_representation(version)
    assert isinstance(result, int)
    assert result == 4

    version = IpVersion(6)
    result = field.to_representation(version)
    assert isinstance(result, int)
    assert result == 6


def test_ip_version_field__to_internal_value():
    """Test IP version to internal value"""
    field = IpVersionField()
    result = field.to_internal_value(4)
    assert result == IpVersion(4)

    result = field.to_internal_value(6)
    assert result == IpVersion(6)


def test_ip_address_serializer():
    """Test ip Address serializer"""
    ip_address = baker.prepare("ipam.IpAddress")
    serializer = schema.IpAddress(ip_address)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"


def test_mac_address_serializer():
    """Test mac aaddress serializer"""
    mac_address = baker.prepare("ipam.MacAddress")
    serializer = schema.MacAddress(mac_address)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"

