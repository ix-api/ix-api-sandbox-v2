
"""
JEA IPAM

Manage ip and mac addresses.
"""

import logging

from rest_framework import request, status
from ixapi_schema.v2 import schema

from jea.ipam.services import (
    ip_addresses as ip_addresses_svc,
    mac_addresses as mac_addresses_svc,
)
from jea.ipam.filters import IpAddressFilter, MacAddressFilter
from jea.api.v2.permissions import require_account
from jea.api.v2.viewsets import IxApiViewSet
from jea.api.v2.response import (
    ApiSuccess,
    ApiError
)


class IpAddressViewSet(IxApiViewSet):
    """
    An `IP` is a IPv4 or 6 addresses, with a given validity period.
    Some services require IP addresses to work.

    When you are joining an `exchange_lan` network service
    for example, addresses on the peering lan will be assigned
    to you.
    """
    @require_account
    def list(self, request, account=None):
        """
        List all ip addresses (and prefixes)
        """
        ips = ip_addresses_svc.get_ip_addresses(
            scoping_account=account,
            filters=request.query_params)

        return schema.IpAddress(ips, many=True)

    @require_account
    def retrieve(self, request, account=None, pk=None):
        """
        Get a signle ip addresses object.
        """
        ip_address = ip_addresses_svc.get_ip_address(
            scoping_account=account,
            ip_address=pk)

        return schema.IpAddress(ip_address)

    @require_account
    def create(self, request, account=None):
        """
        Add an IP address.
        """
        ip_address_request = schema.IpAddressRequest(data=request.data)
        ip_address_request.is_valid(raise_exception=True)

        ip_address = ip_addresses_svc.add_ip_address(
            scoping_account=account,
            ip_address_request=ip_address_request.validated_data)

        return schema.IpAddress(ip_address), status.HTTP_201_CREATED

    @require_account
    def update(self, request, account=None, pk=None):
        """
        Update an ip address object.

        You can only update
        IP addresses within your current scope. Not all
        addresses you can read you can update.
        """
        ip_address = ip_addresses_svc.get_ip_address(
            scoping_account=account,
            ip_address=pk)

        # For now only updating the fqdn is allowed
        serializer = schema.IpAddressUpdate(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Perform update
        ip_address = ip_addresses_svc.update_ip_address(
            scoping_account=account,
            ip_address=ip_address,
            ip_address_update=serializer.validated_data)

        # Serialize result
        return schema.IpAddress(ip_address)

    @require_account
    def partial_update(self, request, account=None, pk=None):
        """
        Update parts of an ip address.

        As with the `PUT` opertaion, IP addresses, where you
        don't have update rights, will yield a `resource access denied`
        error when attempting an update.
        """
        ip_address = ip_addresses_svc.get_ip_address(
            scoping_account=account,
            ip_address=pk)

        # For now only updating the fqdn is allowed
        serializer = schema.IpAddressUpdate(data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        # Perform update
        ip_address = ip_addresses_svc.update_ip_address(
            scoping_account=account,
            ip_address=ip_address,
            ip_address_update=serializer.validated_data)

        # Serialize result
        return schema.IpAddress(ip_address)

    @require_account
    def destroy(self, request, account=None, pk=None):
        """
        Remove an IP address object.
        """
        ip_address = ip_addresses_svc.delete_ip_address(
            scoping_account=account,
            ip_address=pk)

        return schema.IpAddress(ip_address)


class MacAddressViewSet(IxApiViewSet):
    """
    A `MAC` is a MAC addresses with a given validity period.

    Some services require MAC addresses to work.

    The address itself can not be updated after creation.
    However: It can be expired by changing the `valid_not_before`
    and `valid_not_after` properties.
    """
    @require_account
    def list(self, request, account=None):
        """List all mac addresses managed by the authorized account."""
        mac_addresses = mac_addresses_svc.get_mac_addresses(
            scoping_account=account,
            filters=request.query_params)

        return schema.MacAddress(mac_addresses, many=True)

    @require_account
    def retrieve(self, request, account=None, pk=None):
        """Get a signle mac address by id"""
        mac_address = mac_addresses_svc.get_mac_address(
            scoping_account=account,
            mac_address=pk)

        return schema.MacAddress(mac_address)

    @require_account
    def create(self, request, account=None):
        """Create a new mac address."""
        serializer = schema.MacAddressRequest(data=request.data)
        serializer.is_valid(raise_exception=True)

        # So far so good, create a fresh mac:
        mac_address = mac_addresses_svc.create_mac_address(
            scoping_account=account,
            mac_address_input=serializer.validated_data)

        return schema.MacAddress(mac_address), status.HTTP_201_CREATED

    @require_account
    def destroy(self, request, account=None, pk=None):
        """Remove a mac address"""
        mac_address = mac_addresses_svc.get_mac_address(
            scoping_account=account,
            mac_address=pk)

        mac_address = mac_addresses_svc.remove_mac_address(
            scoping_account=account,
            mac_address=mac_address)

        return schema.MacAddress(mac_address)

