
"""
Tests for IPAM views
"""

from datetime import timedelta

import pytest
from model_bakery import baker
from django.utils import timezone

from jea.test import authorized_requests
from jea.api.v2.ipam import views


@pytest.mark.django_db
def test_ip_addresses_view_set__list():
    """Test listing all ip addresss"""
    account = baker.make("crm.Account")

    view = views.IpAddressViewSet.as_view({"get": "list"})
    request = authorized_requests.get(account)
    response = view(request)

    assert response.status_code == 200


@pytest.mark.django_db
def test_ip_addresses_view_set__retrieve():
    """Test getting a specific ip address"""
    account = baker.make(
        "crm.Account")
    ip_addr = baker.make(
        "ipam.IpAddress",
        scoping_account=account)
    view = views.IpAddressViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(account)
    response = view(request)

    assert response.status_code == 200


@pytest.mark.django_db
def test_ip_addresses_view_set__create():
    """Test adding an IP address"""
    account = baker.make("crm.Account")

    view = views.IpAddressViewSet.as_view({"post": "create"})
    request = authorized_requests.post(account, {
        "address": "1.2.3.5",
        "version": 4,
        "prefix_length": 32,
        "managing_account": account.pk,
        "consuming_account": account.pk,
    })

    response = view(request)
    assert response.status_code == 201


@pytest.mark.django_db
def test_ip_addresses_view_set__update():
    """Test updating properties of the ip"""
    account = baker.make(
        "crm.Account")
    ip_addr = baker.make(
        "ipam.IpAddress",
        prefix_length=8,
        version=4,
        address="2.3.4.5",
        managing_account=account,
        consuming_account=account,
        scoping_account=account)
    view = views.IpAddressViewSet.as_view({"put": "update"})
    request = authorized_requests.put(account, {
        "id": ip_addr.pk,
        "address": "fe23::42",
        "version": 6,
        "prefix_length": 8,
        "fqdn": "foo.bar",
        "managing_account": account.pk,
        "consuming_account": account.pk,
    })
    response = view(request, pk=ip_addr.pk)
    print(response.__dict__)
    assert response.status_code == 200


@pytest.mark.django_db
def test_ip_addresses_view_set__partial_update():
    """Test partially updating properties of the ip"""
    account = baker.make(
        "crm.Account")
    ip_addr = baker.make(
        "ipam.IpAddress",
        scoping_account=account)
    view = views.IpAddressViewSet.as_view({"patch": "partial_update"})
    request = authorized_requests.patch(account, {
        "fqdn": "foo.bar",
    })
    response = view(request, pk=ip_addr.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_ip_addresses_view_set__partial_update__address():
    """Test partially updating properties of the ip"""
    account = baker.make(
        "crm.Account")
    ip_addr = baker.make(
        "ipam.IpAddress",
        scoping_account=account)
    view = views.IpAddressViewSet.as_view({"patch": "partial_update"})
    request = authorized_requests.patch(account, {
        "version": 4,
        "address": "10.23.42.100",
        "prefix_length": 32,
    })
    response = view(request, pk=ip_addr.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_ip_addresses_view_set__partial_update__expiration():
    """Test partially updating properties of the ip"""
    account = baker.make(
        "crm.Account")
    ip_addr = baker.make(
        "ipam.IpAddress",
        scoping_account=account)
    view = views.IpAddressViewSet.as_view({"patch": "partial_update"})
    request = authorized_requests.patch(account, {
        "valid_not_after": timezone.now() + timedelta(days=10),
    })
    response = view(request, pk=ip_addr.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_ip_addresses_view_set__destroy():
    """Test ip address removal"""
    account = baker.make("crm.Account")
    ip_addr = baker.make(
        "ipam.IpAddress",
        scoping_account=account,
        ixp_allocated=False)

    view = views.IpAddressViewSet.as_view({"delete": "destroy"})
    request = authorized_requests.delete(account)
    response = view(request, pk=ip_addr.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_mac_addresses_view_set__list():
    """Test listing all the mac addresses"""
    account = baker.make("crm.Account")

    view = views.MacAddressViewSet.as_view({"get": "list"})
    request = authorized_requests.get(account)
    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_mac_addresses_view_set__retrieve():
    """Test getting a specific mac address"""
    account = baker.make("crm.Account")
    mac = baker.make(
        "ipam.MacAddress",
        managing_account=account,
        consuming_account=account,
        scoping_account=account)

    view = views.MacAddressViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(account)
    response = view(request, pk=mac.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_mac_address_view_set__create():
    """Test creating a mac address"""
    account = baker.make("crm.Account")
    view = views.MacAddressViewSet.as_view({"post": "create"})
    request = authorized_requests.post(account, {
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "address": "ff:00:ff:00:ff:00",
    })
    response = view(request)
    assert response.status_code == 201


@pytest.mark.django_db
def test_mac_address_view_set__destroy():
    """Test removing a mac"""
    account = baker.make(
        "crm.Account")
    mac = baker.make(
        "ipam.MacAddress",
        scoping_account=account)
    view = views.MacAddressViewSet.as_view({"delete": "destroy"})
    request = authorized_requests.delete(account)
    response = view(request, pk=mac.pk)
    assert response.status_code == 200
