
import pytest
import mock
from model_bakery import baker

from jea.auth import roles, exceptions
from jea.auth.models import User
from jea.crm.models import Account
from jea.api.v2.permissions import (
    HasAccessRole,
    HasTokenRefreshRole,
    require_account,
)


def test_has_access_role():
    check = HasAccessRole()
    request = mock.MagicMock()

    # Test with access role present
    request.user = User()
    request.user.access_roles = [
        roles.ACCESS,
    ]

    assert check.has_permission(request, None)

    request.user.access_roles = [
        roles.TOKEN_REFRESH,
    ]
    assert not check.has_permission(request, None)



def test_has_token_refresh_role():
    check = HasTokenRefreshRole()
    request = mock.MagicMock()

    # Test with access role present
    request.user = User()
    request.user.access_roles = [
        roles.ACCESS,
    ]

    assert not check.has_permission(request, None)

    request.user.access_roles = [
        roles.TOKEN_REFRESH,
    ]
    assert check.has_permission(request, None)


@pytest.mark.django_db
def test_require_account():
    view = mock.MagicMock()
    request = mock.MagicMock()

    account = baker.make(Account)
    user = baker.make(User, account=account)

    # We mock all required fields.
    request.user = user
    require_account(view)(None, request) # Should be ok

    view.assert_called_with(None, request, account=account)

    with pytest.raises(exceptions.AuthorizationError):
        request.user.account = None
        require_account(view)(None, request)

