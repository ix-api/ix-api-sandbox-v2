
"""
Test Catalog Views
"""

import pytest
from model_bakery import baker
from rest_framework import test

from jea.test import authorized_requests
from jea.api.v2.introspection import views


@pytest.mark.django_db
def test_health_view_set_list():
    """Retrieve health status"""
    account = baker.make("crm.Account")
    request = authorized_requests.get(account)

    # Make request
    view = views.HealthViewSet.as_view({"get": "list"})
    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_implementation_view_set_list():
    """Retrieve health status"""
    account = baker.make("crm.Account")
    request = authorized_requests.get(account)

    # Make request
    view = views.ImplementationViewSet.as_view({"get": "list"})
    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_extensions_view_set_list():
    """Get a list of extensions"""
    account = baker.make("crm.Account")
    request = authorized_requests.get(account)
    view = views.ExtensionsViewSet.as_view({"get": "list"})
    response = view(request)
    assert response.status_code == 200
