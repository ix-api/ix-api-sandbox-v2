

from django.conf import settings

from ixapi_schema import v2
from ixapi_schema.v2.schema import (
    ApiHealth,
    ApiImplementation,
    HealthStatus,
    NetworkService,
    NetworkServiceConfig,
    NetworkFeature,
    NetworkFeatureConfig,
)
from ixapi_schema.openapi import schema
from jea.api.v2.viewsets import IxApiViewSet


class HealthViewSet(IxApiViewSet):
    """Show a short health status response"""
    permission_classes = ()
    authentication_classes = ()
    def list(self, request):
        """Public health status"""
        return ApiHealth({
            "status": HealthStatus.PASS,
            "version": "2",
            "releaseId": settings.VERSION,
        })


class ImplementationViewSet(IxApiViewSet):
    """
    Show api and service information.
    Also provide a list of implemented operations
    """
    permission_classes = ()
    authentication_classes = ()
    def list(self, request):
        """Show api implementation details and capabilities"""
        # Generate the api schema and use this to get this list
        # of supported operations. The sandbox is assumed to implement
        # all of them.
        scm = schema.generate(v2)

        ops = [op["operationId"]
               for _, endpoint in scm["paths"].items()
               for _, op in endpoint.items()]

        ns_types = [t for t in NetworkService.serializer_classes]
        nsc_types = [t for t in NetworkServiceConfig.serializer_classes]
        nf_types = [t for t in NetworkFeature.serializer_classes]
        nfc_types = [t for t in NetworkFeatureConfig.serializer_classes]

        return ApiImplementation({
            "schema_version": v2.__version__,
            "service_version": settings.VERSION,
            "supported_operations": ops,
            "supported_network_service_types": ns_types,
            "supported_network_service_config_types": nsc_types,
            "supported_network_feature_types": nf_types,
            "supported_network_feature_config_types": nfc_types,
        })


class ExtensionsViewSet(IxApiViewSet):
    """Show a list of supported extensions"""
    permission_classes = ()
    authentication_classes = ()

    def list(self, request):
        """Show a list of extensions"""
        # No extensions in the sandbox
        return []
