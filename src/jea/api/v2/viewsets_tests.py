
"""
Test IX-API ViewSet extensions
"""
from django.http import HttpRequest
from ixapi_schema.openapi import components

from jea.api.v2.response import ApiResponse, ApiError, ApiSuccess
from jea.api.v2.viewsets import IxApiViewSet


#
# Assertation helpers
#
def _assert_response_wrapping(response):
    """Test if the response is correctly wrapped"""
    assert isinstance(response, ApiResponse), \
        "IX-API ViewSets results should be wrappen in an ApiResponse object"


#
# API Response tests
#
def test_viewset_response_enveloping():
    """The viewsets response should be wrapped"""

    class TestViewSet(IxApiViewSet):
        def list(self, request):
            return ApiSuccess("everything's fine!")

        def retrieve(self, request, pk=None):
            return ApiError("Nothing's right")

        def update(self, request, pk=None):
            return 42

    viewset = TestViewSet()
    req = HttpRequest()

    # Execute handlers
    handlers = ['list', 'retrieve', 'update']
    for method in handlers:
        handler = getattr(viewset, method)

        response = handler(req)

        # Check wrapping and envelope
        _assert_response_wrapping(response)


def test_viewset_component_response():
    """
    Check if the viewset allows for components
    and status to be returned.
    """
    class Foo(components.Component):
        name = components.CharField()


    class TestViewSet(IxApiViewSet):
        def list(self, request):
            return Foo({"name": "foo"})

        def create(self, request):
            return Foo({"name": "foo"}), 201

    viewset = TestViewSet()
    req = HttpRequest()

    handlers = ['list', 'create']
    for method in handlers:
        handler = getattr(viewset, method)
        response = handler(req)

        # Check wrapping and envelope
        _assert_response_wrapping(response)
        assert response.data, "data shold be present"

