
"""
Service Access Serialization
"""

from ixapi_schema.v2 import schema

from jea.config import models

# TODO: Check if deprecated
def get_config_update_serializer(config):
    """
    Get the right network service config serializer
    for a given config object.

    :param config: A network service or feature config
    """
    # Network Services
    if isinstance(config, models.ExchangeLanNetworkServiceConfig):
        return schema.ExchangeLanNetworkServiceConfigUpdate
    if isinstance(config, models.P2PNetworkServiceConfig):
        return schema.P2PNetworkServiceConfigUpdate
    if isinstance(config, models.MP2MPNetworkServiceConfig):
        return schema.MP2MPNetworkServiceConfigUpdate
    if isinstance(config, models.P2MPNetworkServiceConfig):
        return schema.P2MPNetworkServiceConfigUpdate
    if isinstance(config, models.CloudNetworkServiceConfig):
        return schema.CloudNetworkServiceConfigUpdate

    # Network Features
    if isinstance(config, models.RouteServerNetworkFeatureConfig):
        return schema.RouteServerNetworkFeatureConfigUpdate

    raise TypeError("Unsupported config type: {}".format(config.__class__))

