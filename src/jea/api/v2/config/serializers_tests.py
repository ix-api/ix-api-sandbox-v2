
import pytest
from model_bakery import baker
from ixapi_schema.v2 import schema

from jea.api.v2.config.serializers import get_config_update_serializer


@pytest.mark.django_db
def test_get_config_update_serializer():
    """Get serializer based on config type"""
    # Network Services
    #  - Exchange Lan
    config = baker.make("config.ExchangeLanNetworkServiceConfig")
    assert get_config_update_serializer(config) == \
        schema.ExchangeLanNetworkServiceConfigUpdate

    #  - P2P / ELine
    config = baker.make("config.P2PNetworkServiceConfig")
    assert get_config_update_serializer(config) == \
        schema.P2PNetworkServiceConfigUpdate

    #  - MP2MP / ELan
    config = baker.make("config.MP2MPNetworkServiceConfig")
    assert get_config_update_serializer(config) == \
        schema.MP2MPNetworkServiceConfigUpdate

    #  - P2MP / ETree
    config = baker.make("config.P2MPNetworkServiceConfig")
    assert get_config_update_serializer(config) == \
        schema.P2MPNetworkServiceConfigUpdate

    # - Cloud
    config = baker.make("config.CloudNetworkServiceConfig")
    assert get_config_update_serializer(config) == \
        schema.CloudNetworkServiceConfigUpdate

    # Network Features
    #  - Route Server
    config = baker.make("config.RouteServerNetworkFeatureConfig")
    assert get_config_update_serializer(config) == \
        schema.RouteServerNetworkFeatureConfigUpdate

    with pytest.raises(TypeError):
        config = baker.make("config.NetworkServiceConfig")
        get_config_update_serializer(config)
