
"""
Test IX-API Access Entities
"""

import pytest
from model_bakery import baker
from ixapi_schema.v2 import schema
from ixapi_schema.v2.schema import (
    NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
    NETWORK_SERVICE_CONFIG_TYPE_P2P,
    NETWORK_SERVICE_CONFIG_TYPE_P2MP,
    NETWORK_SERVICE_CONFIG_TYPE_MP2MP,
    NETWORK_SERVICE_CONFIG_TYPE_CLOUD,

    VLAN_CONFIG_TYPE_QINQ,
    VLAN_CONFIG_TYPE_DOT1Q,
    VLAN_CONFIG_TYPE_PORT,
)

from jea.crm.services import contacts as contacts_svc
from jea.api.v2.config.serializers import (
    get_config_update_serializer,
)


@pytest.mark.django_db
def test_connection_serialization():
    """Test serialization of a connection"""
    conn = baker.make("config.Connection")
    port = baker.make("config.Port", connection=conn)

    serializer = schema.Connection(conn)
    data = serializer.data

    assert data, "Serializer should serialize without crashing"
    assert str(port.id) in data["ports"]


@pytest.mark.django_db
def test_connection_request_serializer():
    """Test create connection request serializer"""
    account = baker.make("crm.Account")
    contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=account)
    impl = baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))
    po = baker.make("catalog.ConnectionProductOffering")

    payload = {
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "mode": "lag_lacp",
        "lacp_timeout": "slow",
        "product_offering": po.pk,
        "role_assignments": [impl.pk],
        "port_quantity": 1,
    }

    serializer = schema.ConnectionRequest(data=payload)
    serializer.is_valid()
    assert not serializer.errors


@pytest.mark.django_db
def test_connection_update_serializer():
    """Test create connection request serializer"""
    account = baker.make("crm.Account")
    contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=account)
    impl = baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))

    po = baker.make("catalog.ConnectionProductOffering")

    payload = {
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "mode": "lag_static",
        "role_assignments": [impl.pk],
        "port_quantity": 2,
        "product_offering": po.pk,
    }

    serializer = schema.ConnectionUpdate(data=payload)
    serializer.is_valid()
    assert not serializer.errors


@pytest.mark.django_db
def test_polymorphic_network_service_config_serializer():
    """Test Polymorphic network service config serializer"""
    config = baker.make("config.ExchangeLanNetworkServiceConfig")
    serializer = schema.NetworkServiceConfig(config)

    assert serializer.data
    assert serializer.data["type"] == NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN

    config = baker.make(
        "config.P2PNetworkServiceConfig",
        vlan_config=baker.make("config.PortVLanConfig"))

    serializer = schema.NetworkServiceConfig(config)

    assert serializer.data
    assert serializer.data["type"] == NETWORK_SERVICE_CONFIG_TYPE_P2P

    config = baker.make(
        "config.MP2MPNetworkServiceConfig",
        vlan_config=baker.make("config.QinQVLanConfig"))
    serializer = schema.NetworkServiceConfig(config)

    assert serializer.data
    assert serializer.data["type"] == NETWORK_SERVICE_CONFIG_TYPE_MP2MP

    config = baker.make(
        "config.P2MPNetworkServiceConfig",
        vlan_config=baker.make("config.Dot1QVLanConfig"))
    serializer = schema.NetworkServiceConfig(config)

    assert serializer.data
    assert serializer.data["type"] == NETWORK_SERVICE_CONFIG_TYPE_P2MP


@pytest.mark.django_db
def test_polymorphic_network_service_config_deserialization():
    """Test deserialization of network service input"""
    account = baker.make("crm.Account")
    conn = baker.make("config.Connection")
    product = baker.make(
        "catalog.ExchangeLanNetworkProductOffering")
    service = baker.make(
        "service.ExchangeLanNetworkService",
        product_offering=product)
    contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=account)
    impl = baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))
    mac = baker.make("ipam.MacAddress")

    data = {
        "type": NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
        "capacity": None,
        "role_assignments": [impl.pk],
        "connection": conn.pk,
        "network_service": service.pk,
        "asns": [2342],
        "macs": [mac.pk],
        "listed": True,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "product_offering": product.pk,
        "vlan_config": {
            "vlan_type": VLAN_CONFIG_TYPE_DOT1Q,
            "vlan": 1234,
        }
    }

    serializer = schema.NetworkServiceConfigRequest(data=data)
    serializer.is_valid()
    assert not serializer.errors
    assert serializer.validated_data["type"] == \
        NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN

    data = {
        "type": NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
        "capacity": 1,
        "role_assignments": [impl.pk],
        "asns": [2342],
        "vlan_config": {"vlan_type": VLAN_CONFIG_TYPE_PORT},
        "connection": "",
        "listed": True,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
    }

    serializer = schema.NetworkServiceConfigRequest(data=data)
    assert not serializer.is_valid()


@pytest.mark.django_db
def test_port_reservation_serializer():
    """PortReservation serializer"""
    reservation = baker.make("config.PortReservation")
    serializer = schema.PortReservation(reservation)
    assert serializer.data


@pytest.mark.django_db
def test_port_serialization():
    """Test serialization of a port"""
    port = baker.make("config.Port")
    serializer = schema.Port(port)

    data = serializer.data
    assert data
    assert isinstance(data["role_assignments"], list)


@pytest.mark.django_db
def test_port_reservation_request_serializer():
    """Test portation point request serializer"""
    connection = baker.make("config.Connection")
    account = baker.make("crm.Account")
    contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=account)
    impl = baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))
    pop = baker.make("catalog.PointOfPresence")

    payload = {
        "pop": pop.pk,
        "media_type": "100GBASE-LR",
        "connection": connection.pk,
        "role_assignments": [impl.pk],
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
    }

    serializer = schema.PortReservationRequest(data=payload)
    assert serializer.is_valid()


@pytest.mark.django_db
def test_port_reservation_update_serializer():
    """Test port reservation update serializer"""
    connection = baker.make("config.Connection")
    account = baker.make("crm.Account")
    contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=account)
    impl = baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))

    payload = {
        "connection": connection.pk,
        "role_assignments": [impl.pk],
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
    }

    serializer = schema.PortReservationUpdate(data=payload)
    assert serializer.is_valid()


@pytest.mark.django_db
def test_feature_config_serialization():
    """Test serialization of feature config objects"""
    service_config = baker.make("config.ExchangeLanNetworkServiceConfig")
    feature = baker.make("service.RouteServerNetworkFeature")
    feature_config = baker.make(
        "config.RouteServerNetworkFeatureConfig",
        network_service_config=service_config,
        network_feature=feature)
    serializer = schema.NetworkFeatureConfig(feature_config)
    assert serializer.data, "Serializer should serialize without crashing"
    assert serializer.data["type"] == "route_server"


@pytest.mark.django_db
def test_network_service_config_udpate_base_serializer():
    """Test service config update base serializer"""
    account = baker.make("crm.Account")
    contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=account)
    impl = baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))
    connection = baker.make("config.Connection")

    serializer = schema.NetworkServiceConfigUpdateBase(data={
        "role_assignments": [impl.pk],
        "connection": connection.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "vlan_config": {"vlan_type": VLAN_CONFIG_TYPE_PORT},
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_exchange_lan_network_service_config_update_serializer():
    """Test update serializer for network service config"""
    account = baker.make("crm.Account")
    contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=account)
    impl = baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))
    connection = baker.make("config.Connection")
    mac = baker.make("ipam.MacAddress")

    serializer = schema.ExchangeLanNetworkServiceConfigUpdate(data={
        "connection": connection.pk,
        "macs": [mac.pk],
        "asns": [42],
        "listed": False,
        "vlan_config": {
            "vlan_type": VLAN_CONFIG_TYPE_PORT,
        },
        "role_assignments": [impl.pk],
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_p2p_network_service_config_request_serializer():
    """Test request serializer for network service config"""
    account = baker.make("crm.Account")
    contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=account)
    impl = baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))
    connection = baker.make("config.Connection")
    network_service = baker.make("service.P2PNetworkService")

    serializer = schema.P2PNetworkServiceConfigRequest(data={
        "role_assignments": [impl.pk],
        "connection": connection.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "network_service": network_service.pk,
        "vlan_config": {
            "vlan_type": VLAN_CONFIG_TYPE_QINQ,
            "inner_vlan": 23,
            "outer_vlan": 42,
            "outer_vlan_ethertype": "0x8100",
        },
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_p2p_network_service_config_update_serializer():
    """Test update serializer for network service config"""
    account = baker.make("crm.Account")
    contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=account)
    impl = baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))
    connection = baker.make("config.Connection")

    serializer = schema.P2PNetworkServiceConfigUpdate(data={
        "role_assignments": [impl.pk],
        "connection": connection.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "vlan_config": {
            "vlan_type": VLAN_CONFIG_TYPE_DOT1Q,
            "vlan": 23,
        },
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_mp2mp_network_service_config_request_serializer():
    """Test request serializer for network service config"""
    account = baker.make("crm.Account")
    contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=account)
    impl = baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))
    connection = baker.make("config.Connection")
    network_service = baker.make("service.MP2MPNetworkService")

    serializer = schema.MP2MPNetworkServiceConfigRequest(data={
        "role_assignments": [impl.pk],
        "connection": connection.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "network_service": network_service.pk,
        "vlan_config": {
            "vlan_type": VLAN_CONFIG_TYPE_DOT1Q,
            "vlan": 23,
        },
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_mp2mp_network_service_config_update_serializer():
    """Test update serializer for network service config"""
    account = baker.make("crm.Account")
    contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=account)
    impl = baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))
    connection = baker.make("config.Connection")

    serializer = schema.MP2MPNetworkServiceConfigUpdate(data={
        "role_assignments": [impl.pk],
        "connection": connection.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "vlan_config": {
            "vlan_type": VLAN_CONFIG_TYPE_DOT1Q,
            "vlan": 254,
        },
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_p2mp_network_service_config_request_serializer():
    """Test request serializer for network service config"""
    account = baker.make("crm.Account")
    contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=account)
    impl = baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))
    connection = baker.make("config.Connection")
    network_service = baker.make("service.P2MPNetworkService")

    serializer = schema.P2MPNetworkServiceConfigRequest(data={
        "role_assignments": [impl.pk],
        "connection": connection.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "network_service": network_service.pk,
        "vlan_config": {
            "vlan_type": VLAN_CONFIG_TYPE_DOT1Q,
            "vlan": 123,
        },
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_p2mp_network_service_config_update_serializer():
    """Test update serializer for network service config"""
    account = baker.make("crm.Account")
    contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=account)
    impl = baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))
    connection = baker.make("config.Connection")

    serializer = schema.P2MPNetworkServiceConfigUpdate(data={
        "role_assignments": [impl.pk],
        "connection": connection.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "vlan_config": {
            "vlan_type": VLAN_CONFIG_TYPE_DOT1Q,
            "vlan": 123,
        },
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_route_server_network_feature_config_update_serializer():
    """Test update serializer for route server feature configs"""
    account = baker.make("crm.Account")
    ip_address = baker.make("ipam.IpAddress", scoping_account=account)

    serializer = schema.RouteServerNetworkFeatureConfigUpdate(data={
        "asn": 2342,
        "ip": ip_address.pk,
        "as_set": "AS-FOO",
        "max_prefix_v4": 5000,
        "insert_ixp_asn": True,
        "session_mode": "public",
        "bgp_session_type": "active",
        "billing_account": account.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
    })

    assert serializer.is_valid(raise_exception=True)

