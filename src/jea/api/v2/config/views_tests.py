
"""
Test JEA Access Views
"""

import pytest
import mock
from model_bakery import baker
from ixapi_schema.v2.schema import (
    NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
    NETWORK_SERVICE_CONFIG_TYPE_P2P,
    NETWORK_SERVICE_CONFIG_TYPE_P2MP,
    NETWORK_SERVICE_CONFIG_TYPE_MP2MP,
    NETWORK_SERVICE_CONFIG_TYPE_CLOUD,

    VLAN_CONFIG_TYPE_QINQ,
    VLAN_CONFIG_TYPE_DOT1Q,
)

from jea.test import authorized_requests
from jea.crm.services import contacts as contacts_svc
from jea.api.v2.config import views
from jea.config import models as config_models
from jea.catalog import models as catalog_models
from jea.service.models import (
    RouteServerNetworkFeature,
    ExchangeLanNetworkService,
)
from jea.crm.models import (
    Account,
    Contact,
    RoleAssignment,
    BillingInformation,
)
from jea.catalog.models import (
    Device,
    DeviceCapability,
    MediaType,
    ConnectionProductOffering,
    CrossConnectInitiator,
    PointOfPresence,
)
from jea.config.models import (
    Connection,
    Port,
    PortReservation,
    ExchangeLanNetworkServiceConfig,
    RouteServerNetworkFeatureConfig,
    PortVLanConfig,
)
from jea.ipam.models import (
    IpAddress,
    MacAddress,
)

#
# Ports and PortReservations
#
@pytest.mark.django_db
def test_port_reservations__list():
    """List all PortReservations"""
    account = baker.make(Account)
    baker.make(
        PortReservation,
        scoping_account=account)
    baker.make(
        PortReservation,
        scoping_account=account)

    # Run view
    view = views.PortReservationsViewSet.as_view({"get": "list"})
    request = authorized_requests.get(account)

    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_port_reservations__retrieve():
    """Get a specific port"""
    account = baker.make(Account)
    reservation = baker.make(
        PortReservation,
        scoping_account=account)

    view = views.PortReservationsViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(account)
    response = view(request, pk=reservation.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_port_reservations__create():
    """Get a specific PortReservation"""
    account = baker.make(
        Account,
        billing_information=baker.make(BillingInformation))
    pop = baker.make(PointOfPresence)
    device = baker.make(
        Device,
        pop=pop)
    baker.make(
        DeviceCapability,
        device=device,
        media_type=MediaType.TYPE_10GBASE_LR.value,
        availability_count=100)
    product_offering = baker.make(
        ConnectionProductOffering,
        handover_pop=pop,
        physical_port_speed=10000,
        physical_media_type=MediaType.TYPE_10GBASE_LR,
        cross_connect_initiator="subscriber")
    connection = baker.make(
        Connection,
        product_offering=product_offering,
        scoping_account=account,
        billing_account=account,
        managing_account=account,
        consuming_account=account)

    view = views.PortReservationsViewSet.as_view({
        "post": "create"})
    request = authorized_requests.post(account, {
        "connection": connection.pk,
    })

    response = view(request)
    assert response.status_code == 201, response.__dict__


@pytest.mark.django_db
def test_port_reservations__update():
    """Update a PortReservation"""
    account = baker.make(Account)
    reservation = baker.make(
        PortReservation,
        scoping_account=account)

    view = views.PortReservationsViewSet.as_view({
        "put": "update"})
    request = authorized_requests.put(account, {
        "purchase_order": "PO42",
        "external_ref": "extref",
    })

    response = view(request, pk=reservation.pk)
    assert response.status_code == 202, response.__dict__


@pytest.mark.django_db
def test_port_reservations__partial_update():
    """Update a PortReservation"""
    account = baker.make(Account)
    reservation = baker.make(
        PortReservation,
        scoping_account=account)

    view = views.PortReservationsViewSet.as_view({
        "patch": "partial_update"})
    request = authorized_requests.patch(account, {
        "external_ref": "extref",
    })

    response = view(request, pk=reservation.pk)
    assert response.status_code == 202, response.__dict__


@pytest.mark.django_db
def test_port_reservations__destroy():
    """Update a PortReservation"""
    account = baker.make(Account)
    reservation = baker.make(
        PortReservation,
        scoping_account=account)

    view = views.PortReservationsViewSet.as_view({
        "delete": "destroy"})
    request = authorized_requests.delete(account)
    response = view(request, pk=reservation.pk)

    assert response.status_code == 202, response.__dict__


@pytest.mark.django_db
def test_port_reservations__cancellation_policy():
    """Test cancellation policy retrieval"""
    account = baker.make(Account)
    reservation = baker.make(
        PortReservation,
        scoping_account=account)

    view = views.PortReservationsViewSet.as_view({
        "get": "cancellation_policy"})
    request = authorized_requests.get(account)
    response = view(request, pk=reservation.pk)

    assert response.status_code == 200, response.__dict__
    print(response.__dict__)


@pytest.mark.django_db
def test_port_reservations__loa_download():
    """Test downloading a LOA for a single port reservation"""
    account = baker.make(Account)
    offering = baker.make(
            ConnectionProductOffering,
            cross_connect_initiator=CrossConnectInitiator.SUBSCRIBER)
    connection = baker.make(
            Connection,
            product_offering=offering)
    reservation = baker.make(
        PortReservation,
        connection=connection,
        exchange_side_demarc="Port-23-42",
        connecting_party="demo party",
        scoping_account=account)

    view = views.PortReservationsViewSet.as_view({
        "get": "loa_download",
    })

    request = authorized_requests.get(account)
    response = view(request, pk=reservation.pk)

    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_port_reservations__loa_upload():
    """Test uploading a LOA for a port reservation"""
    account = baker.make(Account)
    offering = baker.make(
            ConnectionProductOffering,
            cross_connect_initiator=CrossConnectInitiator.SUBSCRIBER)
    connection = baker.make(
            Connection,
            product_offering=offering)
    reservation = baker.make(
        PortReservation,
        connection=connection,
        exchange_side_demarc="Port-23-42",
        connecting_party="demo party",
        scoping_account=account)
    assert not reservation.loa_received_at

    view = views.PortReservationsViewSet.as_view({
        "post": "loa_upload",
    })

    with open("config/testdata/loa.pdf", "rb") as file:
        loa = file.read()


    request = authorized_requests.post(
        account,
        data=loa,
        format=None,
        content_type="application/pdf")

    response = view(request, pk=reservation.pk)
    assert response.status_code == 200, response.__dict__

    reservation.refresh_from_db()
    assert reservation.loa_received_at


@pytest.mark.django_db
def test_ports_view_set__list():
    """List all ports"""
    account = baker.make(Account)
    baker.make(
        Port,
        scoping_account=account,
        managing_account=account,
        consuming_account=account)
    baker.make(
        Port,
        scoping_account=account,
        managing_account=account,
        consuming_account=account)

    # Run view 
    view = views.PortsViewSet.as_view({"get": "list"})
    request = authorized_requests.get(account)

    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_ports_view_set__retrieve():
    """Get a specific port"""
    view = views.PortsViewSet.as_view({"get": "retrieve"})
    account = baker.make(Account)
    port = baker.make(
        Port,
        scoping_account=account)
    request = authorized_requests.get(account)
    response = view(request, pk=port.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_ports_view_set__statistics_read():
    """Read the snapshot statistics for a port"""
    account = baker.make(Account)
    port = baker.make(
         Port,
         speed=10000,
         operational_state="up",
         scoping_account=account)
    view = views.PortsViewSet.as_view({"get": "statistics_read"})
    request = authorized_requests.get(account)
    response = view(request, pk=port.pk)
    print(response.__dict__)
    assert response.status_code == 200


@pytest.mark.django_db
def test_ports_view_set__statistics_timerseries_read():
    """Read the timeseries for a port"""
    account = baker.make(Account)
    port = baker.make(
        Port,
        speed=10000,
        operational_state="up",
        scoping_account=account)
    view = views.PortsViewSet.as_view({"get": "statistics_timeseries_read"})
    request = authorized_requests.get(account)
    response = view(request, pk=port.pk)
    print(response.__dict__)
    assert response.status_code == 200

#
# Connections
#

@pytest.mark.django_db
def test_connections_view_set__list():
    """Test listing connections"""
    account = baker.make(
        Account)
    # Have some connections to list
    baker.make(
        Connection,
        scoping_account=account,
        managing_account=account,
        consuming_account=account,
        billing_account=account)
    baker.make(
        Connection,
        scoping_account=account,
        managing_account=account,
        consuming_account=account,
        billing_account=account)

    request = authorized_requests.get(account)
    view = views.ConnectionsViewSet.as_view({"get": "list"})
    response = view(request)

    assert response.status_code == 200


@pytest.mark.django_db
def test_connections_view_set__retrieve():
    """Test listing connections"""
    account = baker.make(
        Account)
    connection = baker.make(
        Connection,
        scoping_account=account)
    request = authorized_requests.get(account)
    view = views.ConnectionsViewSet.as_view({"get": "retrieve"})
    response = view(request, pk=connection.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_connections_view_set__create():
    """Test connection create"""
    account = baker.make(
        Account,
        billing_information=baker.make(BillingInformation))
    contact = baker.make(
        Contact,
        scoping_account=account,
        consuming_account=account)
    impl = baker.make(
        RoleAssignment,
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))
    pop = baker.make(
        "catalog.PointOfPresence")
    device = baker.make(
        Device,
        pop=pop)
    baker.make(
        DeviceCapability,
        device=device,
        media_type=MediaType.TYPE_10GBASE_LR.value,
        availability_count=100)
    product = baker.make(
        ConnectionProductOffering,
        handover_pop=pop,
        physical_port_speed=1000,
        cross_connect_initiator="subscriber")

    request = authorized_requests.post(account, {
        "mode": "lag_lacp",
        "role_assignments": [impl.pk],
        "product_offering": product.pk,
        "port_quantity": 2,
        "billing_account": account.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
    })
    view = views.ConnectionsViewSet.as_view({"post": "create"})
    response = view(request)
    assert response.status_code == 201, response.__dict__


@pytest.mark.django_db
def test_connections_view_set__update():
    """Test updating a connection"""
    account = baker.make(
        Account,
        billing_information=baker.make(BillingInformation))
    contact = baker.make(
        Contact,
        scoping_account=account,
        consuming_account=account)
    impl = baker.make(
        RoleAssignment,
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))
    connection = baker.make(
        Connection,
        scoping_account=account)

    request = authorized_requests.put(account, {
        "mode": "lag_lacp",
        "lacp_timeout": "slow",
        "role_assignments": [impl.pk],
        "billing_account": account.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "product_offering": connection.product_offering.pk,
        "external_ref": "Test2048",
    })
    view = views.ConnectionsViewSet.as_view({"put": "update"})
    response = view(request, pk=connection.pk)
    assert response.status_code == 202, response.__dict__


@pytest.mark.django_db
def test_connections_view_set__partial_update():
    """Test partially updating a connection"""
    account = baker.make(
        Account)
    connection = baker.make(
        Connection,
        scoping_account=account)

    request = authorized_requests.patch(account, {
        "lacp_timeout": "fast",
    })
    view = views.ConnectionsViewSet.as_view({"patch": "partial_update"})
    response = view(request, pk=connection.pk)
    assert response.status_code == 202


@pytest.mark.django_db
def test_connections_view_set__destroy():
    """Destroy a connection"""
    account = baker.make(
        Account)
    connection = baker.make(
        Connection,
        managing_account=account,
        consuming_account=account,
        scoping_account=account)

    request = authorized_requests.delete(account)
    view = views.ConnectionsViewSet.as_view({"delete": "destroy"})
    response = view(request, pk=connection.pk)
    assert response.status_code == 202


@pytest.mark.django_db
def test_connections__cancellation_policy():
    """Test cancellation policy retrieval"""
    account = baker.make(Account)
    connection = baker.make(
        Connection,
        scoping_account=account)

    view = views.ConnectionsViewSet.as_view({
        "get": "cancellation_policy"})
    request = authorized_requests.get(account)
    response = view(request, pk=connection.pk)

    assert response.status_code == 200, response.__dict__
    print(response.__dict__)


@pytest.mark.django_db
def test_connections__loa_download():
    """Test downloading a LOA for a connection"""
    account = baker.make(Account)
    offering = baker.make(
            ConnectionProductOffering,
            cross_connect_initiator=CrossConnectInitiator.SUBSCRIBER)
    connection = baker.make(
            Connection,
            product_offering=offering,
            managing_account=account,
            consuming_account=account,
            scoping_account=account)
    baker.make(
        PortReservation,
        connection=connection,
        exchange_side_demarc="Port-123-142-23",
        connecting_party="demo party",
        scoping_account=account)

    view = views.ConnectionsViewSet.as_view({
        "get": "loa_download",
    })

    request = authorized_requests.get(account)
    response = view(request, pk=connection.pk)

    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_connections__loa_upload():
    """Test downloading a LOA for a connection"""
    account = baker.make(Account)
    offering = baker.make(
            ConnectionProductOffering,
            cross_connect_initiator=CrossConnectInitiator.SUBSCRIBER)
    connection = baker.make(
            Connection,
            product_offering=offering,
            managing_account=account,
            consuming_account=account,
            scoping_account=account)

    view = views.ConnectionsViewSet.as_view({
        "post": "loa_upload",
    })

    with open("config/testdata/loa.pdf", "rb") as file:
        loa = file.read()

    request = authorized_requests.post(
        account,
        data=loa,
        format=None,
        content_type="application/pdf")
    response = view(request, pk=connection.pk)

    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_connection_view_set__statistics_read():
    """Read the snapshot statistics for a connection"""
    account = baker.make(Account)
    connection = baker.make(
            Connection,
            managing_account=account,
            consuming_account=account,
            scoping_account=account)
    baker.make(
         Port,
         speed=10000,
         operational_state="up",
         scoping_account=account)

    view = views.ConnectionsViewSet.as_view({"get": "statistics_read"})
    request = authorized_requests.get(account)
    response = view(request, pk=connection.pk)
    print(response.__dict__)
    assert response.status_code == 200


@pytest.mark.django_db
def test_connection_view_set__statistics_timeseries_read():
    """Read the snapshot statistics for a connection"""
    account = baker.make(Account)
    connection = baker.make(
            Connection,
            managing_account=account,
            consuming_account=account,
            scoping_account=account)
    baker.make(
         Port,
         speed=10000,
         operational_state="up",
         scoping_account=account)

    view = views.ConnectionsViewSet.as_view({"get": "statistics_timeseries_read"})
    request = authorized_requests.get(account)
    response = view(request, pk=connection.pk)
    print(response.__dict__)
    assert response.status_code == 200


#
# Configurations:
# NetworkServiceConfig
#
@pytest.mark.django_db
def test_network_service_configs_view_set__list():
    """List all the network service configs"""
    account = baker.make(Account)
    # Have some network service configs to list
    baker.make(
        ExchangeLanNetworkServiceConfig,
        scoping_account=account,
        managing_account=account,
        billing_account=account,
        consuming_account=account)
    baker.make(
        "config.P2PNetworkServiceConfig",
        scoping_account=account,
        managing_account=account,
        billing_account=account,
        consuming_account=account)
    baker.make(
        "config.MP2MPNetworkServiceConfig",
        scoping_account=account,
        managing_account=account,
        billing_account=account,
        consuming_account=account)
    baker.make(
        "config.P2MPNetworkServiceConfig",
        scoping_account=account,
        managing_account=account,
        billing_account=account,
        consuming_account=account)

    request = authorized_requests.get(account)
    view = views.NetworkServiceConfigsViewSet.as_view({"get": "list"})
    response = view(request)

    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_network_service_configs_view_set__retrieve():
    """Get a specific network service config"""
    account = baker.make(
        Account)
    config = baker.make(
        ExchangeLanNetworkServiceConfig,
        scoping_account=account)

    request = authorized_requests.get(account)
    view = views.NetworkServiceConfigsViewSet.as_view({"get": "retrieve"})
    response = view(request, pk=config.pk)
    assert response.status_code == 200

    # Eline
    config = baker.make(
        "config.P2PNetworkServiceConfig",
        scoping_account=account)

    request = authorized_requests.get(account)
    view = views.NetworkServiceConfigsViewSet.as_view({"get": "retrieve"})
    response = view(request, pk=config.pk)
    assert response.status_code == 200

    # ELan
    config = baker.make(
        "config.MP2MPNetworkServiceConfig",
        scoping_account=account)

    request = authorized_requests.get(account)
    view = views.NetworkServiceConfigsViewSet.as_view({"get": "retrieve"})
    response = view(request, pk=config.pk)
    assert response.status_code == 200

    # ETree
    config = baker.make(
        "config.P2MPNetworkServiceConfig",
        scoping_account=account)

    request = authorized_requests.get(account)
    view = views.NetworkServiceConfigsViewSet.as_view({"get": "retrieve"})
    response = view(request, pk=config.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_network_service_configs_view_set__create():
    """Create a network service config"""
    account = baker.make(
        Account,
        billing_information=baker.make(BillingInformation))
    contact = baker.make(
        Contact,
        scoping_account=account,
        consuming_account=account)
    impl = baker.make(
        RoleAssignment,
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))
    connection = baker.make(
        Connection,
        scoping_account=account)
    net_addr = baker.make(
        IpAddress,
        address="10.23.42.0",
        version=4,
        prefix_length=24)
    product_offering = baker.make(
        "catalog.ExchangeLanNetworkProductOffering")
    network_service = baker.make(
        ExchangeLanNetworkService,
        product_offering=product_offering,
        ip_addresses=[net_addr],
        nsc_required_contact_roles=[
            contacts_svc.get_default_role("implementation"),
        ])

    # Make a create request
    view = views.NetworkServiceConfigsViewSet.as_view({"post": "create"})
    request = authorized_requests.post(account, {
        "type": NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
        "connection": connection.pk,
        "network_service": network_service.pk,
        "product_offering": product_offering.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "role_assignments": [impl.pk],
        "listed": True,
        "vlan_config": {
            "vlan_type": VLAN_CONFIG_TYPE_QINQ,
            "inner_vlan": 23,
            "outer_vlan": 42,
        }
    })
    response = view(request)
    assert response.status_code == 201


@pytest.mark.django_db
def test_network_service_configs_view_set__update():
    """Update a network service config"""
    account = baker.make(
        Account,
        billing_information=baker.make(BillingInformation))
    contact = baker.make(
        Contact,
        scoping_account=account,
        consuming_account=account)
    impl = baker.make(
        RoleAssignment,
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))
    connection = baker.make(
        Connection,
        scoping_account=account)
    connection_new = baker.make(
        Connection,
        scoping_account=account)
    service = baker.make(
        ExchangeLanNetworkService,
        nsc_required_contact_roles=[
            contacts_svc.get_default_role("implementation"),
        ])
    vlan_config = baker.make(PortVLanConfig)
    config = baker.make(
        ExchangeLanNetworkServiceConfig,
        connection=connection,
        vlan_config=vlan_config,
        network_service=service,
        scoping_account=account)
    mac_addr = baker.make(
        MacAddress,
        scoping_account=account)

    view = views.NetworkServiceConfigsViewSet.as_view({"put": "update"})
    request = authorized_requests.put(account, {
        "macs": [mac_addr.pk],
        "connection": connection_new.pk,
        "network_service": config.network_service.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "role_assignments": [impl.pk],
        "listed": True,
        "vlan_config": {
            "vlan_type": VLAN_CONFIG_TYPE_DOT1Q,
            "vlan": 1235,
        },
    })
    response = view(request, pk=config.pk)
    assert response.status_code == 202, response.__dict__


@pytest.mark.django_db
def test_network_service_configs_view_set__partial_update():
    """Partially update a network service config"""
    account = baker.make(
        Account)
    billing_account = baker.make(
        Account,
        billing_information=baker.make(BillingInformation),
        scoping_account=account)
    contact = baker.make(
        Contact,
        scoping_account=account,
        consuming_account=account)
    impl = baker.make(
        RoleAssignment,
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))
    service = baker.make(
        ExchangeLanNetworkService,
        nsc_required_contact_roles=[
            contacts_svc.get_default_role("implementation"),
        ])
    config = baker.make(
        ExchangeLanNetworkServiceConfig,
        scoping_account=account,
        network_service=service)

    view = views.NetworkServiceConfigsViewSet.as_view({
        "patch": "partial_update",
    })
    request = authorized_requests.patch(account, {
        "role_assignments": [impl.pk],
        "billing_account": billing_account.pk,
    })
    response = view(request, pk=config.pk)
    assert response.status_code == 202


@pytest.mark.django_db
def test_network_service_configs_view_set__destroy():
    """Delete a network service config"""
    account = baker.make(
        Account)
    config = baker.make(
        ExchangeLanNetworkServiceConfig,
        scoping_account=account)

    view = views.NetworkServiceConfigsViewSet.as_view({"delete": "destroy"})
    request = authorized_requests.delete(account)
    response = view(request, pk=config.pk)
    assert response.status_code == 202


@pytest.mark.django_db
def test_network_service_config_view_set__statistics_read():
    """Read the snapshot statistics for a nsc"""
    account = baker.make(
        Account)
    config = baker.make(
        ExchangeLanNetworkServiceConfig,
        scoping_account=account)
    view = views.NetworkServiceConfigsViewSet.as_view({"get": "statistics_read"})
    request = authorized_requests.get(account)
    response = view(request, pk=config.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_network_service_config_view_set__statistics_timerseries_read():
    """Read the timeseries for a nsc"""
    account = baker.make(Account)
    config = baker.make(
        ExchangeLanNetworkServiceConfig,
        scoping_account=account)
    view = views.NetworkServiceConfigsViewSet.as_view({"get": "statistics_timeseries_read"})
    request = authorized_requests.get(account)
    response = view(request, pk=config.pk)
    assert response.status_code == 200

#
# Network Feature Configs
#
@pytest.mark.django_db
def test_network_feature_configs_view_set__list():
    """Test list in feature config view set"""
    account = baker.make(Account)
    baker.make(
        RouteServerNetworkFeatureConfig,
        scoping_account=account,
        managing_account=account,
        billing_account=account,
        consuming_account=account)
    view = views.NetworkFeatureConfigsViewSet.as_view({"get": "list"})
    request = authorized_requests.get(account)
    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_network_feature_configs_view_set__retrieve():
    """Test getting a feature config"""
    account = baker.make(
        Account)
    config = baker.make(
        RouteServerNetworkFeatureConfig,
        scoping_account=account,
        managing_account=account,
        billing_account=account,
        consuming_account=account)
    view = views.NetworkFeatureConfigsViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(account)
    response = view(request, pk=config.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_network_feature_configs_view_set__create():
    """Test creating a network service feature"""
    account = baker.make(
        Account,
        billing_information=baker.make(BillingInformation))
    network_service = baker.make(
        ExchangeLanNetworkService)
    network_feature = baker.make(
        RouteServerNetworkFeature,
        network_service=network_service,
        address_families=['af_inet'],
        nfc_required_contact_roles=[],
        available_bgp_session_types=[
            config_models.BGPSessionType.TYPE_PASSIVE.value,
        ])
    network_service_config = baker.make(
        ExchangeLanNetworkServiceConfig,
        scoping_account=account,
        network_service=network_service)
    ip_address = baker.make(
        IpAddress,
        scoping_account=account,
        exchange_lan_network_service_config=network_service_config)

    # Create request
    view = views.NetworkFeatureConfigsViewSet.as_view({"post": "create"})
    request = authorized_requests.post(account, {
        "type": "route_server",
        "network_feature": network_feature.pk,
        "network_service_config": network_service_config.pk,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "asn": 23042,
        "as_set_v4": "AS-FOO",
        "ip": ip_address.pk,
        "session_mode": network_feature.session_mode.value,
        "bgp_session_type":
            config_models.BGPSessionType.TYPE_PASSIVE.value,
        "role_assignments": [],
    })
    response = view(request)
    print(response.__dict__)
    assert response.status_code == 201


@pytest.mark.django_db
def test_network_feature_configs_view_set__update():
    """Update a network feature config"""
    account = baker.make(
        Account)
    network_service = baker.make(
        ExchangeLanNetworkService)
    network_feature = baker.make(
        RouteServerNetworkFeature,
        address_families=['af_inet'],
        network_service=network_service)
    network_service_config = baker.make(
        ExchangeLanNetworkServiceConfig,
        scoping_account=account,
        network_service=network_service)
    network_feature_config = baker.make(
        RouteServerNetworkFeatureConfig,
        scoping_account=account,
        network_feature=network_feature,
        network_service_config=network_service_config)
    ip_address = baker.make(
        IpAddress,
        exchange_lan_network_service_config=network_service_config)

    # Create request
    view = views.NetworkFeatureConfigsViewSet.as_view({"put": "update"})
    request = authorized_requests.put(account, {
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "asn": 23042,
        "as_set_v4": "AS-FOO",
        "session_mode": network_feature.session_mode.value,
        "ip": ip_address.pk,
        "bgp_session_type":
            config_models.BGPSessionType.TYPE_ACTIVE.value,
    })
    response = view(request, pk=network_feature_config.pk)

    assert response.status_code == 202, response.__dict__


@pytest.mark.django_db
def test_network_feature_configs_view_set__partial_update():
    """Update a network feature config with partial changes"""
    account = baker.make(
        Account)
    network_service = baker.make(
        ExchangeLanNetworkService)
    network_feature = baker.make(
        RouteServerNetworkFeature,
        network_service=network_service,
        address_families=['af_inet6'])
    network_service_config = baker.make(
        ExchangeLanNetworkServiceConfig,
        scoping_account=account,
        network_service=network_service)
    network_feature_config = baker.make(
        RouteServerNetworkFeatureConfig,
        scoping_account=account,
        as_set_v6="AS-BAR",
        network_feature=network_feature,
        network_service_config=network_service_config)

    # Create request
    view = views.NetworkFeatureConfigsViewSet.as_view({
        "patch": "partial_update",
    })
    request = authorized_requests.patch(account, {
        "asn": 23042,
        "as_set_v6": "AS-FOO",
    })
    response = view(request, pk=network_feature_config.pk)

    assert response.status_code == 202


@pytest.mark.django_db
def test_network_feature_configs_view_set__destroy():
    """Remove a network feature config"""
    account = baker.make(Account)
    network_feature_config = baker.make(
        RouteServerNetworkFeatureConfig,
        scoping_account=account)

    view = views.NetworkFeatureConfigsViewSet.as_view({"delete": "destroy"})
    request = authorized_requests.delete(account)
    response = view(request, pk=network_feature_config.pk)

    assert response.status_code == 202
