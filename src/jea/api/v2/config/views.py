"""
Config :: Views

Implement views for config objects like
network service configs and feature configs.
"""

from datetime import datetime, date

from django.http import HttpResponse, JsonResponse
from rest_framework import exceptions, status
from rest_framework.decorators import action
from rest_framework.parsers import BaseParser, JSONParser
from ixapi_schema.v2.schema import (
    Port,
    PortReservation,
    PortReservationRequest,
    PortReservationUpdate,
    Connection,
    ConnectionRequest,
    ConnectionUpdate,
    NetworkServiceConfig,
    NetworkServiceConfigRequest,
    NetworkFeature,
    NetworkFeatureConfig,
    CancellationPolicy,
)
from ixapi_schema.v2.entities.statistics import (
    PortStatistics,
    Aggregate,
    AggregateTimeseries,
)

from jea.api.v2.viewsets import IxApiViewSet, wrap_response
from jea.api.v2.permissions import require_account
from jea.config.changes import (
    create_network_service_config,
    update_network_service_config,
    decommission_network_service_config,
    create_network_feature_config,
    update_network_feature_config,
    decommission_network_feature_config,
)
from jea.config.services import (
    ports as ports_svc,
    connections as connections_svc,
    configs as configs_svc,
    loas as loas_svc,
)
from jea.service.models import (
    ExchangeLanNetworkService,
)
from jea.statistics.services import (
    port_statistics as port_statistics_svc,
    connection_statistics as connection_statistics_svc,
    network_service_config_statistics as network_service_config_statistics_svc,
)
from jea.stateful import dispatch
from jea.stateful.models import State
from jea.exceptions import CancellationPolicyError


class LoaFileUploadParser(BaseParser):
    """Parse loa file upload"""
    media_type = "application/pdf"

    def parse(self, stream, media_type=None, parser_context=None):
        """Return the request body"""
        return stream.read()


#
# Ports and PortReservations
#
class PortReservationsViewSet(IxApiViewSet):
    """
    A PortReservation is an allocation of a physical
    Port and an Account using it in a Connection.
    """
    parser_classes = [LoaFileUploadParser, JSONParser]

    @require_account
    def list(self, request, account=None):
        """List all PortReservations"""
        filters = request.query_params.copy()
        if not filters.get("state") \
            and not filters.get("state__is_not"):
            filters["state__is_not"] = "decommissioned"

        reservations = connections_svc.get_port_reservations(
            filters=filters,
            scoping_account=account)

        serializer = PortReservation(reservations, many=True)
        return serializer.data


    @require_account
    def retrieve(self, request, account=None, pk=None):
        """Retrieve a single port allocation"""
        reservation = connections_svc.get_port_reservation(
            scoping_account=account,
            port_reservation=pk)

        serializer = PortReservation(reservation)
        return serializer.data

    @require_account
    def create(self, request, account=None):
        """Create a new PortReservation"""
        serializer = PortReservationRequest(data=request.data)
        serializer.is_valid(raise_exception=True)

        reservation = connections_svc.reserve_port(
            scoping_account=account,
            port_reservation_request=serializer.validated_data)

        serializer = PortReservation(reservation)
        return serializer.data, status.HTTP_201_CREATED

    @require_account
    def update(self, request, account=None, pk=None):
        """Full update of a PortReservation"""
        serializer = PortReservationUpdate(data=request.data)
        serializer.is_valid(raise_exception=True)

        reservation = connections_svc.update_port_reservation(
            scoping_account=account,
            port_reservation=pk,
            port_reservation_update=serializer.validated_data)

        serializer = PortReservation(reservation)
        return serializer.data, status.HTTP_202_ACCEPTED

    @require_account
    def partial_update(self, request, account=None, pk=None):
        """Full update of a PortReservation"""
        serializer = PortReservationUpdate(
            data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        reservation = connections_svc.update_port_reservation(
            scoping_account=account,
            port_reservation=pk,
            port_reservation_update=serializer.validated_data)

        serializer = PortReservation(reservation)
        return serializer.data, status.HTTP_202_ACCEPTED

    @require_account
    def destroy(self, request, account=None, pk=None):
        """Remove a port allocation"""
        data = {}
        if request.data is not None:
            data = request.data

        reservation = connections_svc.get_port_reservation(
            scoping_account=account,
            port_reservation=pk)
        product_offering = reservation.connection.product_offering

        # Use decommission_at from request body
        decommission_at = data.get("decommission_at")
        if decommission_at:
            decommission_at = datetime.strptime(
                decommission_at, "%Y-%m-%d").date()

            # Validate date against cancellation policy
            policy = product_offering.cancellation_policy(
                date(2000, 1, 1), decommission_at=decommission_at)
            if policy["decommission_at"] != decommission_at:
                raise CancellationPolicyError(policy=policy)
        else:
            policy = product_offering.cancellation_policy(
                date(2000, 1, 1))

        reservation.decommission_at = policy["decommission_at"]
        reservation.charged_until = policy["charged_until"]
        reservation.state = State.DECOMMISSION_REQUESTED
        reservation.save()

        serializer = PortReservation(reservation)
        return serializer.data, status.HTTP_202_ACCEPTED

    @action(
        detail=True,
        methods=["GET"],
        url_path="cancellation-policy")
    @require_account
    def cancellation_policy(self, request, pk=None, account=None):
        """Query the cancellation policy"""
        reservation = connections_svc.get_port_reservation(
            scoping_account=account,
            port_reservation=pk)
        product_offering = reservation.connection.product_offering

        decommission_at = request.query_params.get("decommission_at")
        if decommission_at:
            decommission_at = datetime.strptime(
                decommission_at, "%Y-%m-%d").date()

        start_at = date(2000, 1, 1) # Maybe use created_at?
        policy = product_offering.cancellation_policy(
            start_at, decommission_at=decommission_at)

        return wrap_response(CancellationPolicy(policy))

    @action(
        detail=True,
        methods=["GET"],
        url_path="loa")
    @require_account
    def loa_download(self, request, pk=None, account=None):
        """
        Download the LOA generated for the port-reservation
        in case of an `subscriber` initiated cross connect.
        """
        reservation = connections_svc.get_port_reservation(
            scoping_account=account,
            port_reservation=pk)

        loa = loas_svc.generate_loa(
            port_reservation=reservation,
            scoping_account=account)

        return HttpResponse(loa, content_type="application/pdf")

    @action(
        detail=True,
        methods=["POST"],
        url_path="loa")
    @require_account
    def loa_upload(self, request, pk=None, account=None):
        """
        Upload a LOA for this port reservation.
        This is only valid for `exchange` initiated cross connects.
        """
        reservation = connections_svc.get_port_reservation(
            scoping_account=account,
            port_reservation=pk)

        loas_svc.process_loa(
            port_reservation=reservation,
            loa=request.data,
            scoping_account=account)

        return HttpResponse() # 200, Empty response body


class PortsViewSet(IxApiViewSet):
    """
    A `Port` (portation point) is the point at which account and
    IXP networks meet, eg a physical port / socket, generally with a
    specified bandwidth.

    Ports are listed on a LoA (Letter of Authorisation).
    Exchange accounts need this information to order a cross connect
    from the datacenter operator to be interconnected to the exchange.

    Due to the reason a `port` is patched to a switch, it comes with
    necessary extra informations like speed and optics `type`.
    A `port` is always associated to one `pop`.
    """
    @require_account
    def list(self, request, account=None):
        """List all `port`s."""
        # Do not include archived ports unless requested
        filters = request.query_params.copy()
        if not filters.get("state") and not filters.get("state__is_not"):
            filters["state__is_not"] = "decommissioned"

        ports = ports_svc.get_ports(
            scoping_account=account,
            filters=filters)

        serializer = Port(ports, many=True)
        return serializer.data

    @require_account
    def retrieve(self, request, account=None, pk=None):
        """Read a `port`."""
        port = ports_svc.get_port(
            scoping_account=account,
            port=pk)

        serializer = Port(port)
        return serializer.data

    @action(
        detail=True,
        methods=["GET"],
        url_path="statistics")
    @require_account
    def statistics_read(self, request, pk=None, account=None):
        """
        Read port statistics
        """
        port = ports_svc.get_port(
            scoping_account=account,
            port=pk)

        statistics = port_statistics_svc.get_statistics(port)
        serializer = PortStatistics(statistics)

        return JsonResponse(serializer.data)

    @action(
        detail=True,
        methods=["GET"],
        url_path="statistics/5m/timeseries")
    @require_account
    def statistics_timeseries_read(self, request, pk=None, account=None):
        """
        Get the timeseries of the port statistics
        """
        port = ports_svc.get_port(
            scoping_account=account,
            port=pk)
        timeseries = port_statistics_svc.get_timeseries(port)

        # For now we can not use the serializer
        # serializer = AggregateTimeseries(timeseries)
        # We need to serialize the date in the correct format.
        def format_date(dt):
            return dt.strftime("%Y-%m-%dT%H:%M:%S.%fZ")

        timeseries["created_at"] = format_date(timeseries["created_at"])
        timeseries["next_update_at"] = format_date(timeseries["next_update_at"])
        timeseries["samples"] = [
            (format_date(s[0]), *s[1:])
            for s in timeseries["samples"]]

        import json
        data = json.dumps(timeseries)

        return HttpResponse(
            data,
            content_type="application/json")


class ConnectionsViewSet(IxApiViewSet):
    """
    A `Connection` is a functional group of physical connections
    collected together into a LAG (aka trunk).

    A `connection` with only one `port` can be also configured as
    standalone which means no LAG configuration on the switch.
    """
    parser_classes = [LoaFileUploadParser, JSONParser]

    @require_account
    def list(self, request, account=None):
        """List all `connection`s."""
        # Filter archived if no state filter is present
        filters = request.query_params.copy()
        if not filters.get("state") and not filters.get("state__is_not"):
            filters["state__is_not"] = "decommissioned"

        connections = connections_svc.get_connections(
            scoping_account=account,
            filters=filters)

        serializer = Connection(connections, many=True)
        return serializer.data

    @require_account
    def retrieve(self, request, account=None, pk=None):
        """
        Read a `connection`.
        """
        connection = connections_svc.get_connection(
            scoping_account=account,
            connection=pk)

        serializer = Connection(connection)
        return serializer.data

    @require_account
    def create(self, request, account=None):
        """
        Create a new `connection`.
        """
        serializer = ConnectionRequest(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Everything good? Let's create the connection
        connection = connections_svc.create_connection(
            scoping_account=account,
            connection_request=serializer.validated_data)

        serializer = Connection(connection)
        return serializer.data, status.HTTP_201_CREATED

    @require_account
    def update(self, request, account=None, pk=None):
        """Update a `connection`."""
        serializer = ConnectionUpdate(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Allright - then let's proceed with the update
        connection = connections_svc.update_connection(
            scoping_account=account,
            connection=pk,
            connection_update=serializer.validated_data)

        serializer = Connection(connection)
        return serializer.data, status.HTTP_202_ACCEPTED

    @require_account
    def partial_update(self, request, account=None, pk=None):
        """Partially update a `connection`."""
        serializer = ConnectionUpdate(
            data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        # Allright - then let's proceed with the update
        connection = connections_svc.update_connection(
            scoping_account=account,
            connection=pk,
            connection_update=serializer.validated_data)

        serializer = Connection(connection)

        return serializer.data, status.HTTP_202_ACCEPTED

    @require_account
    def destroy(self, request, account=None, pk=None):
        """
        Delete a `connection`.
        """
        data = {}
        if request.data is not None:
            data = request.data

        connection = connections_svc.get_connection(
            scoping_account=account,
            connection=pk)
        product_offering = connection.product_offering


        # Use decommission_at from request body
        decommission_at = data.get("decommission_at")
        if decommission_at:
            decommission_at = datetime.strptime(
                decommission_at, "%Y-%m-%d").date()

            # Validate date against cancellation policy
            policy = product_offering.cancellation_policy(
                date(2000, 1, 1), decommission_at=decommission_at)
            if policy["decommission_at"] != decommission_at:
                raise CancellationPolicyError(policy=policy)
        else:
            policy = product_offering.cancellation_policy(
                date(2000, 1, 1))

        connection.state = State.DECOMMISSION_REQUESTED
        connection.decommission_at = policy["decommission_at"]
        connection.charged_until = policy["charged_until"]
        connection.save()

        for reservation in connection.port_reservations.all():
            reservation.state = State.DECOMMISSION_REQUESTED
            reservation.decommission_at = policy["decommission_at"]
            reservation.charged_until = policy["charged_until"]
            reservation.save()

            port = reservation.port
            if port:
                port.state = State.DECOMMISSION_REQUESTED
                port.decommission_at = policy["decommission_at"]
                port.charged_until = policy["charged_until"]
                port.save()

        return Connection(connection).data, status.HTTP_202_ACCEPTED

    @action(
        detail=True,
        methods=["GET"],
        url_path="cancellation-policy")
    @require_account
    def cancellation_policy(self, request, pk=None, account=None):
        """Query the cancellation policy for the connection"""
        connection = connections_svc.get_connection(
            scoping_account=account,
            connection=pk)
        product_offering = connection.product_offering

        decommission_at = request.query_params.get("decommission_at")
        if decommission_at:
            decommission_at = datetime.strptime(
                decommission_at, "%Y-%m-%d").date()

        start_at = date(2000, 1, 1) # Maybe use created_at?
        policy = product_offering.cancellation_policy(
            start_at, decommission_at=decommission_at)

        return wrap_response(CancellationPolicy(policy))

    @action(
        detail=True,
        methods=["GET"],
        url_path="loa")
    @require_account
    def loa_download(self, request, pk=None, account=None):
        """
        Download the LOA generated for the port-reservations of
        this connection in case of an `subscriber` initiated cross connect.
        """
        connection = connections_svc.get_connection(
            scoping_account=account,
            connection=pk)

        loa = loas_svc.generate_loa(
            connection=connection,
            scoping_account=account)

        return HttpResponse(loa, content_type="application/pdf")

    @action(
        detail=True,
        methods=["POST"],
        url_path="loa")
    @require_account
    def loa_upload(self, request, pk=None, account=None):
        """
        Upload a LOA. This is only valid for `exchange` initiated
        cross connects.
        """
        connection = connections_svc.get_connection(
            scoping_account=account,
            connection=pk)

        loas_svc.process_loa(
            loa=request.data,
            connection=connection,
            scoping_account=account)

        return HttpResponse() # 200, Empty response body

    @action(
        detail=True,
        methods=["GET"],
        url_path="statistics")
    @require_account
    def statistics_read(self, request, pk=None, account=None):
        """
        Read connection statistics
        """
        connection = connections_svc.get_connection(
            scoping_account=account,
            connection=pk)

        statistics = connection_statistics_svc.get_statistics(connection)
        serializer = Aggregate(statistics)

        return JsonResponse(serializer.data)

    @action(
        detail=True,
        methods=["GET"],
        url_path="statistics/5m/timeseries")
    @require_account
    def statistics_timeseries_read(self, request, pk=None, account=None):
        """
        Get the timeseries of the port statistics
        """
        connection = connections_svc.get_connection(
            scoping_account=account,
            connection=pk)
        timeseries = connection_statistics_svc.get_timeseries(connection)

        # For now we can not use the serializer
        # serializer = AggregateTimeseries(timeseries)
        # We need to serialize the date in the correct format.
        def format_date(dt):
            return dt.strftime("%Y-%m-%dT%H:%M:%S.%fZ")

        timeseries["created_at"] = format_date(timeseries["created_at"])
        timeseries["next_update_at"] = format_date(timeseries["next_update_at"])
        timeseries["samples"] = [
            (format_date(s[0]), *s[1:])
            for s in timeseries["samples"]]

        import json
        data = json.dumps(timeseries)

        return HttpResponse(
            data,
            content_type="application/json")
#
# Configurations
#
class NetworkServiceConfigsViewSet(IxApiViewSet):
    """
    A `NetworkServiceConfig` is a account's configuration for usage
    of a `NetworkService`, eg the configuration of a (subset of a)
    connection for that account's traffic

    The `type` of the config determines the service you are
    configuring.

    You can find the services available to you on the platform,
    by querying the `/api/v1/network-services` resource.
    """
    @require_account
    def list(self, request, account=None):
        """Get all network-service-configs."""
        service_configs = configs_svc.get_network_service_configs(
            scoping_account=account,
            filters=request.query_params)

        return NetworkServiceConfig(service_configs, many=True).data

    @require_account
    def retrieve(self, request, account=None, pk=None):
        """Get a `network-service-config`"""
        service_config = configs_svc.get_network_service_config(
            scoping_account=account,
            network_service_config=pk)

        return NetworkServiceConfig(service_config).data

    @require_account
    def create(self, request, account=None):
        """Create a `network-service-config`."""
        service_config = dispatch.request(
            account,
            create_network_service_config(request.data))
        serializer = NetworkServiceConfig(service_config)
        return serializer.data, status.HTTP_201_CREATED

    @require_account
    def update(self, request, account=None, pk=None):
        """Update an exisiting `network-service-config`"""
        service_config = configs_svc.get_network_service_config(
            scoping_account=account,
            network_service_config=pk)
        service_config = dispatch.request(
            account,
            update_network_service_config(service_config, request.data))
        # Render result
        return (
            NetworkServiceConfig(service_config).data,
            status.HTTP_202_ACCEPTED)

    @require_account
    def partial_update(self, request, account=None, pk=None):
        """Update parts of an exisiting `network-service-config`"""
        service_config = configs_svc.get_network_service_config(
            scoping_account=account,
            network_service_config=pk)
        service_config = dispatch.request(
            account,
            update_network_service_config(
                service_config,
                request.data,
                partial=True))
        # Render result
        return (
            NetworkServiceConfig(service_config).data,
            status.HTTP_202_ACCEPTED)

    @require_account
    def destroy(self, request, account=None, pk=None):
        """Deconfigure the network service"""
        data = {}
        if request.data is not None:
            data = request.data

        service_config = configs_svc.get_network_service_config(
            scoping_account=account,
            network_service_config=pk)

        product_offering = service_config.network_service.product_offering

        # Use decommission_at from request body
        decommission_at = data.get("decommission_at")
        if decommission_at:
            decommission_at = datetime.strptime(
                decommission_at, "%Y-%m-%d").date()

            # Validate date against cancellation policy
            policy = product_offering.cancellation_policy(
                date(2000, 1, 1), decommission_at=decommission_at)
            if policy["decommission_at"] != decommission_at:
                raise CancellationPolicyError(policy=policy)
        else:
            policy = product_offering.cancellation_policy(date(2000, 1, 1))

        service_config.decommission_at = policy["decommission_at"]
        service_config.charged_until = policy["charged_until"]

        service_config = dispatch.request(
            account,
            decommission_network_service_config(
                service_config,
                decommission_at=decommission_at))

        return (
            NetworkServiceConfig(service_config).data,
            status.HTTP_202_ACCEPTED)

    @action(
        detail=True,
        methods=["GET"],
        url_path="cancellation-policy")
    @require_account
    def cancellation_policy(self, request, pk=None, account=None):
        """Query the cancellation policy"""
        service_config = configs_svc.get_network_service_config(
            scoping_account=account,
            network_service_config=pk)
        network_service = service_config.network_service

        # For anything other than the exchange lan network,
        # the cancellation policy of the service holds.
        if isinstance(network_service, ExchangeLanNetworkService):
            product_offering = service_config.product_offering
        else:
            product_offering = network_service.product_offering

        decommission_at = request.query_params.get("decommission_at")
        if decommission_at:
            decommission_at = datetime.strptime(
                decommission_at, "%Y-%m-%d").date()

        start_at = date(2000, 1, 1) # Maybe use created_at?
        policy = product_offering.cancellation_policy(
            start_at, decommission_at=decommission_at)

        return wrap_response(CancellationPolicy(policy))

    @action(
        detail=True,
        methods=["GET"],
        url_path="statistics")
    @require_account
    def statistics_read(self, request, pk=None, account=None):
        """
        Read network service config statistics
        """
        service_config = configs_svc.get_network_service_config(
            scoping_account=account,
            network_service_config=pk)

        statistics = network_service_config_statistics_svc.get_statistics(
            service_config)
        serializer = Aggregate(statistics)

        return JsonResponse(serializer.data)


    @action(
        detail=True,
        methods=["GET"],
        url_path="statistics/5m/timeseries")
    @require_account
    def statistics_timeseries_read(self, request, pk=None, account=None):
        """
        Get the timeseries of the port statistics
        """
        service_config = configs_svc.get_network_service_config(
            scoping_account=account,
            network_service_config=pk)

        timeseries = network_service_config_statistics_svc.get_timeseries(
            service_config)

        # For now we can not use the serializer
        # serializer = AggregateTimeseries(timeseries)
        # We need to serialize the date in the correct format.
        def format_date(dt):
            return dt.strftime("%Y-%m-%dT%H:%M:%S.%fZ")

        timeseries["created_at"] = format_date(timeseries["created_at"])
        timeseries["next_update_at"] = format_date(timeseries["next_update_at"])
        timeseries["samples"] = [
            (format_date(s[0]), *s[1:])
            for s in timeseries["samples"]]

        import json
        data = json.dumps(timeseries)

        return HttpResponse(
            data,
            content_type="application/json")


class NetworkFeatureConfigsViewSet(IxApiViewSet):
    """
    A `NetworkFeatureConfig` is a account's configuration for usage of
    a `NetworkFeature`
    """
    @require_account
    def list(self, request, account=None):
        """Get all network feature configs."""
        feature_configs = configs_svc.get_network_feature_configs(
            scoping_account=account,
            filters=request.query_params)

        return NetworkFeatureConfig(feature_configs, many=True).data

    @require_account
    def retrieve(self, request, account=None, pk=None):
        """Get a single network feature config"""
        feature_config = configs_svc.get_network_feature_config(
            scoping_account=account,
            network_feature_config=pk)

        return NetworkFeatureConfig(feature_config).data

    @require_account
    def update(self, request, account=None, pk=None):
        """Update a network feature configuration"""
        feature_config = configs_svc.get_network_feature_config(
            scoping_account=account,
            network_feature_config=pk)
        feature_config = dispatch.request(
            account,
            update_network_feature_config(
                feature_config,
                request.data))
        # Respond with result
        return (
            NetworkFeatureConfig(feature_config).data,
            status.HTTP_202_ACCEPTED)

    @require_account
    def partial_update(self, request, account=None, pk=None):
        """Update parts of the network feature config"""
        feature_config = configs_svc.get_network_feature_config(
            scoping_account=account,
            network_feature_config=pk)
        feature_config = dispatch.request(
            account,
            update_network_feature_config(
                feature_config,
                request.data,
                partial=True))

        # Respond with result
        return (
            NetworkFeatureConfig(feature_config).data,
            status.HTTP_202_ACCEPTED)

    @require_account
    def create(self, request, account=None):
        """
        Create a new feature configuration.

        Remeber to provide a feature `type` and the id of the
        `network_feature` you want to configure.
        Additionally you have to provide the `network_service_config`
        where you want to use the network feature.

        You can query the available features from the
        `/api/v1/network-features` resource.
        """
        feature_config = dispatch.request(
            account,
            create_network_feature_config(request.data))
        serializer = NetworkFeatureConfig(feature_config)
        return serializer.data, status.HTTP_201_CREATED

    @require_account
    def destroy(self, request, account=None, pk=None):
        """Remove a network feature config"""
        feature_config = configs_svc.get_network_feature_config(
            scoping_account=account,
            network_feature_config=pk)
        feature_config = dispatch.request(
            account, decommission_network_feature_config(
                feature_config))

        serializer = NetworkFeatureConfig(feature_config)
        return serializer.data, status.HTTP_202_ACCEPTED
