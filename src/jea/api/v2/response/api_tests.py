
from jea.api.v2.response.api import ApiError


def test_status_default():
    """An error status should be assigned"""
    error = ApiError()
    assert error.status_code >= 300, \
       "Some error status should be set."


def test_status_override():
    """A custom status should not be touched"""
    error = ApiError(status=523)
    assert error.status_code == 523, \
        "Status should be overridable"

