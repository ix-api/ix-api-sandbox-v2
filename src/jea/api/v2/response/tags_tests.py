
from rest_framework import status, exceptions

from jea.api.v2.response import tags

def test_tag_for_exceptions():
    """Resolve execptions into error tags"""
    expectations = [
        (exceptions.AuthenticationFailed, tags.AUTHENTICATION_ERROR),
        (exceptions.PermissionDenied, tags.AUTHORIZATION_ERROR),
        (exceptions.NotFound, tags.RESOURCE_ERROR),
        (exceptions.APIException, tags.GENERIC_ERROR),
        (Exception, tags.GENERIC_ERROR),
    ]

    for exc, tag in expectations:
        result = tags.tag_for_exception(exc())
        assert result == tag

