
from jwt import exceptions as jwt_exc
from django.core import exceptions as django_exc
from rest_framework import exceptions as rest_exc

# Success
SUCCESS = "success"

# Error
GENERIC_ERROR = "generic_error"
RUNTIME_ERROR = "runtime_error"
DATA_ERROR = "data_error"
AUTHENTICATION_ERROR = "authentication_error"
AUTHORIZATION_ERROR = "authorization_error"
RESOURCE_ERROR = "resource_error"


def tag_for_exception(exc):
    """Select a fitting tag for a given exception"""
    if isinstance(exc, rest_exc.ValidationError):
        return DATA_ERROR

    # When an internal service breaks with missing
    # or malformed data, we do not respond with a
    # data error tag, because the user can't do anything about it.
    if isinstance(exc, django_exc.ValidationError):
        return RUNTIME_ERROR

    if isinstance(exc, (rest_exc.AuthenticationFailed,
                        rest_exc.NotAuthenticated,
                        jwt_exc.ExpiredSignatureError,
                        jwt_exc.ImmatureSignatureError,
                        jwt_exc.InvalidKeyError)):
        return AUTHENTICATION_ERROR

    if isinstance(exc, rest_exc.PermissionDenied):
        return AUTHORIZATION_ERROR

    if isinstance(exc, (rest_exc.NotFound,
                        django_exc.ObjectDoesNotExist)):
        return RESOURCE_ERROR

    return GENERIC_ERROR

