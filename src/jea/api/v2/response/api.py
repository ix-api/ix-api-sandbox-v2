
"""
JEA API Responses
"""

from datetime import datetime

from rest_framework import exceptions, status
from rest_framework.response import Response

from jea.api.v2.response import tags


class ApiResponse(Response):
    """API Response Base"""
    def __init__(self, *args, tag=tags.SUCCESS, **kwargs):
        super(ApiResponse, self).__init__(*args, **kwargs)
        self.tag = tag


class ApiSuccess(ApiResponse):
    """A successful response"""
    pass


class ApiError(ApiResponse):
    """An unsuccessful response"""
    def __init__(self,
                 *args,
                 tag=tags.GENERIC_ERROR,
                 status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                 **kwargs):
        """Set default error status to 500 (Internal Server Error)"""
        super(ApiError, self).__init__(*args, status=status, tag=tag, **kwargs)
