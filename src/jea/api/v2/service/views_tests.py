
"""
Test NetworkService views
"""

import pytest
from model_bakery import baker
from ixapi_schema.v2.schema import (
    NETWORK_SERVICE_TYPE_EXCHANGE_LAN,
    NETWORK_SERVICE_TYPE_P2P,
    NETWORK_SERVICE_TYPE_P2MP,
    NETWORK_SERVICE_TYPE_MP2MP,
    NETWORK_SERVICE_TYPE_CLOUD,

    MEMBER_JOINING_RULE_TYPE_ALLOW,
    MEMBER_JOINING_RULE_TYPE_DENY,
)

from jea.test import authorized_requests
from jea.api.v2.service import views
from jea.crm.models import (
    Account,
)
from jea.config.models import (
    Port,
    Connection,
    P2PNetworkServiceConfig,
)
from jea.service.models import (
    P2PNetworkService,
)

@pytest.mark.django_db
def test_member_joining_rules_view_set__list():
    """List all member joining rules"""
    account = baker.make("crm.Account")
    network_service = baker.make("service.MP2MPNetworkService")
    rule = baker.make(
        "service.AllowMemberJoiningRule",
        network_service=network_service,
        scoping_account=account)

    view = views.MemberJoiningRulesViewSet.as_view({"get": "list"})
    request = authorized_requests.get(account)
    response = view(request)

    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_member_joining_rules_view_set__retrieve():
    """Get a specific member joining rule"""
    account = baker.make("crm.Account")
    network_service = baker.make("service.MP2MPNetworkService")
    rule = baker.make(
        "service.AllowMemberJoiningRule",
        network_service=network_service,
        scoping_account=account)

    view = views.MemberJoiningRulesViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(account)
    response = view(request, pk=rule.pk)

    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_member_joining_rules_view_set__create_allow():
    """Create a member joining rule"""
    account = baker.make("crm.Account")
    network_service = baker.make(
        "service.MP2MPNetworkService",
        managing_account=account,
        consuming_account=account,
        scoping_account=account)

    view = views.MemberJoiningRulesViewSet.as_view({"post": "create"})
    request = authorized_requests.post(account, {
        "type": MEMBER_JOINING_RULE_TYPE_ALLOW,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "network_service": network_service.pk,
        "capacity_min": 42,
        "capacity_max": 9000,
        "external_ref": "join foo",
    })
    response = view(request)
    assert response.status_code == 201


@pytest.mark.django_db
def test_member_joining_rules_view_set__create_deny():
    """Create a member joining rule"""
    account = baker.make("crm.Account")
    network_service = baker.make(
        "service.MP2MPNetworkService",
        managing_account=account,
        consuming_account=account,
        scoping_account=account)

    view = views.MemberJoiningRulesViewSet.as_view({"post": "create"})
    request = authorized_requests.post(account, {
        "type": MEMBER_JOINING_RULE_TYPE_DENY,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "network_service": network_service.pk,
        "external_ref": "join foo",
    })
    response = view(request)
    assert response.status_code == 201


@pytest.mark.django_db
def test_member_joining_rules_view_set__update():
    """Update a member joining rule"""
    account = baker.make("crm.Account")
    network_service = baker.make("service.MP2MPNetworkService")
    rule = baker.make(
        "service.AllowMemberJoiningRule",
        network_service=network_service,
        managing_account=account,
        consuming_account=account,
        scoping_account=account)

    view = views.MemberJoiningRulesViewSet.as_view({"put": "update"})
    request = authorized_requests.put(account, {
        "type": MEMBER_JOINING_RULE_TYPE_ALLOW,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "capacity_min": 42,
        "capacity_max": 9000,
        "external_ref": "join foo",
    })
    response = view(request, pk=rule.pk)
    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_member_joining_rules_view_set__partial_update():
    """Update some fields of a member joining rule"""
    account = baker.make("crm.Account")
    network_service = baker.make("service.MP2MPNetworkService")
    rule = baker.make(
        "service.AllowMemberJoiningRule",
        network_service=network_service,
        managing_account=account,
        consuming_account=account,
        scoping_account=account)

    view = views.MemberJoiningRulesViewSet.as_view({"patch": "partial_update"})
    request = authorized_requests.patch(account, {
        "capacity_min": 42,
        "capacity_max": 9000,
    })
    response = view(request, pk=rule.pk)
    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_member_joining_rules_view_set__delete():
    """Remove a member joining rule"""
    account = baker.make("crm.Account")
    network_service = baker.make("service.MP2MPNetworkService")
    rule = baker.make(
        "service.DenyMemberJoiningRule",
        network_service=network_service,
        managing_account=account,
        consuming_account=account,
        scoping_account=account)

    view = views.MemberJoiningRulesViewSet.as_view({"delete": "destroy"})
    request = authorized_requests.delete(account)
    response = view(request, pk=rule.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_network_service_view_set__list():
    """List all the network services"""
    account = baker.make("crm.Account")
    network_services = [
        baker.make("service.ExchangeLanNetworkService"),
        baker.make("service.P2PNetworkService"),
        baker.make("service.P2MPNetworkService"),
        baker.make("service.MP2MPNetworkService"),
    ]
    view = views.NetworkServicesViewSet.as_view({"get": "list"})
    request = authorized_requests.get(account)
    response = view(request)

    assert response.status_code == 200


@pytest.mark.django_db
def test_network_service_view_set__retrieve():
    """Get a specific network service"""
    account = baker.make("crm.Account")
    service = baker.make("service.ExchangeLanNetworkService")
    view = views.NetworkServicesViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(account)
    response = view(request, pk=service.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_network_service_view_set__create_p2p():
    """Create a network service"""
    account = baker.make("crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    joining_account = baker.make("crm.Account",
        scoping_account=account)
    offering = baker.make("catalog.P2PNetworkProductOffering")

    view = views.NetworkServicesViewSet.as_view({"post": "create"})
    request = authorized_requests.post(account, {
        "type": NETWORK_SERVICE_TYPE_P2P,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "joining_member_account": joining_account.pk,
        "display_name": "My ELine",
        "capacity": 1000,
        "product_offering": offering.pk,
    })
    response = view(request)
    assert response.status_code == 201, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__update_p2p():
    """Update an eline network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    joining_account = baker.make(
        "crm.Account",
        scoping_account=account)
    new_joining_account = baker.make(
        "crm.Account",
        scoping_account=account)
    offering = baker.make("catalog.P2PNetworkProductOffering")
    new_offering = baker.make("catalog.P2PNetworkProductOffering")
    service = baker.make(
        "service.P2PNetworkService",
        product_offering=offering,
        managing_account=account,
        consuming_account=account,
        joining_member_account=joining_account)

    # Make update
    view = views.NetworkServicesViewSet.as_view({"put": "update"})
    request = authorized_requests.put(account, {
        "type": NETWORK_SERVICE_TYPE_P2P,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "joining_member_account": new_joining_account.pk,
        "display_name": "My New ELine",
        "capacity": 2000,
        "product_offering": new_offering.pk,
    })
    response = view(request, pk=service.pk)
    assert response.status_code == 202, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__partial_update_p2p():
    """Partially update an eline network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    joining_account = baker.make(
        "crm.Account",
        scoping_account=account)
    new_joining_account = baker.make(
        "crm.Account",
        scoping_account=account)
    offering = baker.make("catalog.P2PNetworkProductOffering")
    new_offering = baker.make("catalog.P2PNetworkProductOffering")
    service = baker.make(
        "service.P2PNetworkService",
        product_offering=offering,
        managing_account=account,
        consuming_account=account,
        joining_member_account=joining_account)

    # Make update
    view = views.NetworkServicesViewSet.as_view({"patch": "partial_update"})
    request = authorized_requests.patch(account, {
        "joining_member_account": new_joining_account.pk,
        "product_offering": new_offering.pk,
    })
    response = view(request, pk=service.pk)
    assert response.status_code == 202, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__create_mp2mp():
    """Create a network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    offering = baker.make("catalog.MP2MPNetworkProductOffering")

    view = views.NetworkServicesViewSet.as_view({"post": "create"})
    request = authorized_requests.post(account, {
        "type": NETWORK_SERVICE_TYPE_MP2MP,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "display_name": "My ELan",
        "product_offering": offering.pk,
    })
    response = view(request)
    assert response.status_code == 201, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__update_mp2mp():
    """Update an ELan network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    offering = baker.make("catalog.MP2MPNetworkProductOffering")
    new_offering = baker.make("catalog.MP2MPNetworkProductOffering")

    service = baker.make(
        "service.MP2MPNetworkService",
        managing_account=account,
        consuming_account=account,
        billing_account=account,
        product_offering=offering)

    # Make update
    view = views.NetworkServicesViewSet.as_view({"put": "update"})
    request = authorized_requests.put(account, {
        "type": NETWORK_SERVICE_TYPE_MP2MP,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "display_name": "My New ELan",
        "product_offering": new_offering.pk,
    })
    response = view(request, pk=service.pk)
    assert response.status_code == 202, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__partial_update_mp2mp():
    """Partially update an ELan network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    offering = baker.make("catalog.MP2MPNetworkProductOffering")
    new_offering = baker.make("catalog.MP2MPNetworkProductOffering")

    service = baker.make(
        "service.MP2MPNetworkService",
        managing_account=account,
        consuming_account=account,
        billing_account=account,
        product_offering=offering)

    # Make update
    view = views.NetworkServicesViewSet.as_view({"patch": "partial_update"})
    request = authorized_requests.patch(account, {
        "display_name": "My New ELan",
        "product_offering": new_offering.pk,
    })
    response = view(request, pk=service.pk)
    assert response.status_code == 202, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__create_p2mp():
    """Create a network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    offering = baker.make("catalog.P2MPNetworkProductOffering")

    view = views.NetworkServicesViewSet.as_view({"post": "create"})
    request = authorized_requests.post(account, {
        "type": NETWORK_SERVICE_TYPE_P2MP,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "product_offering": offering.pk,
    })
    response = view(request)
    assert response.status_code == 201, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__update_p2mp():
    """Update an ETree network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    offering = baker.make("catalog.P2MPNetworkProductOffering")

    service = baker.make(
        "service.P2MPNetworkService",
        managing_account=account,
        consuming_account=account,
        billing_account=account,
        product_offering=offering)

    view = views.NetworkServicesViewSet.as_view({"put": "update"})
    request = authorized_requests.put(account, {
        "type": NETWORK_SERVICE_TYPE_P2MP,
        "managing_account": account.pk,
        "consuming_account": account.pk,
        "billing_account": account.pk,
        "name": "My New ETree",
        "product_offering": offering.pk,
    })
    response = view(request, pk=service.pk)
    assert response.status_code == 202, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__partial_update_p2mp():
    """Partially update an ETree network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    offering = baker.make("catalog.P2MPNetworkProductOffering")
    new_offering = baker.make("catalog.P2MPNetworkProductOffering")

    service = baker.make(
        "service.P2MPNetworkService",
        managing_account=account,
        consuming_account=account,
        billing_account=account,
        product_offering=offering)

    view = views.NetworkServicesViewSet.as_view({"patch": "partial_update"})
    request = authorized_requests.patch(account, {
        "consuming_account": account.pk,
        "name": "My New ETree",
        "product_offering": new_offering.pk,
    })
    response = view(request, pk=service.pk)
    assert response.status_code == 202, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__destroy():
    """Destroy a network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    service = baker.make(
        "service.P2PNetworkService",
        managing_account=account,
        billing_account=account,
        consuming_account=account,
        scoping_account=account)

    view = views.NetworkServicesViewSet.as_view({"delete": "destroy"})
    request = authorized_requests.delete(account)

    response = view(request, pk=service.pk)
    assert response.status_code == 202, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__cr_retrieve():
    """Retrieve the change request of a network service"""
    account = baker.make("crm.Account")
    service = baker.make(
        "service.P2PNetworkService",
        managing_account=account,
        billing_account=account,
        consuming_account=account,
        scoping_account=account)

    # Make request, we should not find a change request
    view = views.NetworkServicesViewSet.as_view({
        "get": "change_request"})
    request = authorized_requests.get(account)
    response = view(request, pk=service.pk)
    assert response.status_code == 404, response.__dict__

    # Create a change request
    baker.make(
        "service.NetworkServiceChangeRequest",
        network_service=service)

    # Make request, we now should find a change request
    view = views.NetworkServicesViewSet.as_view({
        "get": "change_request"})
    response = view(request, pk=service.pk)
    assert response.status_code == 200, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__cr_create():
    """Create a change request"""
    account = baker.make("crm.Account")
    service = baker.make(
        "service.P2PNetworkService",
        managing_account=account,
        billing_account=account,
        consuming_account=account,
        scoping_account=account)
    new_offering = baker.make(
        "catalog.P2PNetworkProductOffering")

    # Make request, we should not find a change request
    view = views.NetworkServicesViewSet.as_view({
        "post": "change_request"})
    request = authorized_requests.post(account, {
        "product_offering": new_offering.pk,
    })
    response = view(request, pk=service.pk)
    assert response.status_code == 202, response.__dict__


@pytest.mark.django_db
def test_network_service_view_set__cr_delete():
    """Delete a change request"""
    account = baker.make("crm.Account")
    service = baker.make(
        "service.P2PNetworkService",
        managing_account=account,
        billing_account=account,
        consuming_account=account,
        scoping_account=account)
    baker.make(
        "service.NetworkServiceChangeRequest",
        network_service=service)

    # Make request, we should not find a change request
    view = views.NetworkServicesViewSet.as_view({
        "delete": "change_request"})
    request = authorized_requests.delete(account)
    response = view(request, pk=service.pk)
    assert response.status_code == 202, response.__dict__


@pytest.mark.django_db
def test_network_services_view_set__statistics_read():
    """Read the snapshot statistics for a network service"""
    account = baker.make(Account)
    service = baker.make(
        "service.P2PNetworkService",
        managing_account=account,
        billing_account=account,
        consuming_account=account,
        scoping_account=account)

    connection = baker.make(Connection)
    baker.make(Port, connection=connection, speed=1000, operational_state="up")
    baker.make(P2PNetworkServiceConfig, connection=connection)

    view = views.NetworkServicesViewSet.as_view({"get": "statistics_read"})
    request = authorized_requests.get(account)
    response = view(request, pk=service.pk)
    print(response.__dict__)
    assert response.status_code == 200


@pytest.mark.django_db
def test_network_services_view_set__statistics_timerseries_read():
    """Read the timeseries for a port"""
    account = baker.make(Account)
    service = baker.make(
        "service.P2PNetworkService",
        managing_account=account,
        billing_account=account,
        consuming_account=account,
        scoping_account=account)

    connection = baker.make(Connection)
    baker.make(Port, connection=connection, speed=1000, operational_state="up")
    baker.make(P2PNetworkServiceConfig, connection=connection)

    view = views.NetworkServicesViewSet.as_view({"get": "statistics_timeseries_read"})
    request = authorized_requests.get(account)
    response = view(request, pk=service.pk)

    print(response.__dict__)
    assert response.status_code == 200


@pytest.mark.django_db
def test_network_features_view_set__list():
    """List all the network features"""
    account = baker.make("crm.Account")
    view = views.NetworkFeaturesViewSet.as_view({"get": "list"})
    request = authorized_requests.get(account)
    response = view(request)

    assert response.status_code == 200


@pytest.mark.django_db
def test_network_features_view_set__retrieve():
    """A a specific of all the network features"""
    account = baker.make("crm.Account")
    feature = baker.make("service.RouteServerNetworkFeature")
    view = views.NetworkFeaturesViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(account)
    response = view(request, pk=feature.pk)

    assert response.status_code == 200
