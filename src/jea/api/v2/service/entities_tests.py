
"""
Test Service Serializers
"""

import ipaddress

import pytest
from model_bakery import baker
from ixapi_schema.v2 import schema

from jea.crm.services import contacts as contacts_svc

#
# Helper: Create a service with product
#
def _make_service(service_model, product_offering_model):
    """Create a service with a product"""
    offering = baker.make(product_offering_model)
    network_service = baker.make(
        service_model,
        product_offering=offering,
        nsc_required_contact_roles=[
            contacts_svc.get_default_role("noc"),
        ])

    return network_service


@pytest.mark.django_db
def test_exchange_lan_network_serializer():
    """
    Test serialization of exchange lan network services
    """
    network_service = _make_service(
        "service.ExchangeLanNetworkService",
        "catalog.ExchangeLanNetworkProductOffering",
    )
    baker.make(
        "ipam.IpAddress",
        version=4,
        prefix_length=16,
        exchange_lan_network_service=network_service,
        address="192.168.0.0")
    baker.make(
        "ipam.IpAddress",
        version=6,
        prefix_length=48,
        exchange_lan_network_service=network_service,
        address="fd23:42::")
    serializer = schema.ExchangeLanNetworkService(network_service)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"

    # Check ip representation
    assert "192.168.0.0/16" == result["subnet_v4"]
    assert "fd23:42::/48" == result["subnet_v6"]


@pytest.mark.django_db
def test_p2p_serializer():
    """
    Test serialization of eline network services
    """
    network_service = _make_service(
        "service.P2PNetworkService",
        "catalog.P2PNetworkProductOffering",
    )
    serializer = schema.P2PNetworkService(network_service)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"


@pytest.mark.django_db
def test_mp2mp_serializer():
    """
    Test serialization of mp2mp / elan network services
    """
    network_service = _make_service(
        "service.MP2MPNetworkService",
        "catalog.MP2MPNetworkProductOffering",
    )
    serializer = schema.MP2MPNetworkService(network_service)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"


@pytest.mark.django_db
def test_p2mp_serializer():
    """
    Test serialization of eline network services
    """
    network_service = _make_service(
        "service.P2MPNetworkService",
        "catalog.P2MPNetworkProductOffering",
    )
    serializer = schema.P2MPNetworkService(network_service)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"


@pytest.mark.django_db
def test_cloud_serializer():
    """
    Test serialization of a cloud network services
    """
    network_service = _make_service(
        "service.CloudNetworkService",
        "catalog.CloudNetworkProductOffering",
    )
    serializer = schema.CloudNetworkService(network_service)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"


@pytest.mark.django_db
def test_polymorphic_network_service_serializer_serialize():
    """
    Test the polymorphic network service serializer
    serialization method.
    """
    exchange_lan = _make_service(
        "service.ExchangeLanNetworkService",
        "catalog.ExchangeLanNetworkProductOffering",
    )
    p2p = _make_service(
        "service.P2PNetworkService",
        "catalog.P2PNetworkProductOffering",
    )
    mp2mp = _make_service(
        "service.MP2MPNetworkService",
        "catalog.MP2MPNetworkProductOffering",
    )
    p2mp = _make_service(
        "service.P2MPNetworkService",
        "catalog.P2MPNetworkProductOffering",
    )
    cloud = _make_service(
        "service.CloudNetworkService",
        "catalog.CloudNetworkProductOffering",
    )

    services = [exchange_lan, p2p, p2mp, mp2mp]
    serializer = schema.NetworkService(services, many=True)
    result = serializer.data

    assert result, "Serializer should serialize without crashing."

#
# Features
#
@pytest.mark.django_db
def test_ixp_specific_feature_flag_serializer():
    """Test feature flag serialization"""
    feature_flag = baker.make("service.IXPSpecificFeatureFlag")
    serializer = schema.IXPSpecificFeatureFlag(feature_flag)
    assert serializer.data, "Serializer should serialize without crashing"


@pytest.mark.django_db
def test_feature_base_serializer():
    """Test base serializer for features"""
    feature = baker.make("service.NetworkFeature")
    serializer = schema.NetworkFeatureBase(feature)
    result = serializer.data
    assert result, "Serializer should serialize without crashing"

    # Check embeddings
    assert isinstance(result["flags"], list)



@pytest.mark.django_db
def test_route_server_feature_serializer():
    """Test route server feature serialization"""
    feature = baker.make("service.RouteServerNetworkFeature")
    baker.make(
        "ipam.IpAddress",
        version=4,
        prefix_length=32,
        address="123.42.23.1",
        route_server_network_feature=feature)
    baker.make(
        "ipam.IpAddress",
        version=6,
        prefix_length=128,
        address="fd42:13fa:1111::1",
        route_server_network_feature=feature)
    serializer = schema.RouteServerNetworkFeature(feature)
    result = serializer.data

    assert result, "Serializer should produce data"

    # Check embeddings
    assert result["ip_v4"] == "123.42.23.1"
    assert result["ip_v6"] == "fd42:13fa:1111::1"


@pytest.mark.django_db
def test_polymorphic_feature_serializer():
    """Test polymorphic feature serialization"""
    features = [
        baker.make("service.RouteServerNetworkFeature"),
    ]

    serializer = schema.NetworkFeature(features, many=True)
    result = serializer.data

    assert result, "Serializer should serialize without crashing"
    assert len(result) == len(features), \
        "Serializer should serialize all features"


@pytest.mark.django_db
def test_deny_member_joining_rule():
    """Test member joining rule 'deny' serializer"""
    rule = baker.make("service.DenyMemberJoiningRule")
    serializer = schema.DenyMemberJoiningRule(rule)

    assert serializer.data


@pytest.mark.django_db
def test_allow_member_joining_rule():
    """Test member joining rule 'allow' serializer"""
    rule = baker.make("service.AllowMemberJoiningRule")
    serializer = schema.AllowMemberJoiningRule(rule)

    assert serializer.data


@pytest.mark.django_db
def test_member_joining_rule():
    """Test polymorphic member joining rule"""
    rules = [
        baker.make("service.AllowMemberJoiningRule"),
        baker.make("service.DenyMemberJoiningRule"),
    ]
    serializer = schema.MemberJoiningRule(rules, many=True)
    assert serializer.data

