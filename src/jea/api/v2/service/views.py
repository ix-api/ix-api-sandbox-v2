
"""
Service :: Views

Implements service related endpoints.
In our case services are all network services.
"""

from datetime import datetime, date

from django.http import HttpResponse, JsonResponse
from rest_framework import exceptions, status
from rest_framework.decorators import action
from rest_framework.exceptions import MethodNotAllowed, NotFound
from ixapi_schema.v2 import schema
from ixapi_schema.v2.entities.statistics import (
    Aggregate,
    AggregateTimeseries,
)

from jea.exceptions import CancellationPolicyError
from jea.api.v2.permissions import require_account
from jea.api.v2.viewsets import (
    IxApiViewSet,
    wrap_response,
)
from jea.service.changes import (
    create_network_service,
    update_network_service,
    decommission_network_service,
)
from jea.service.services import (
    network as network_svc,
    membership as membership_svc,
)
from jea.service.models import (
    ExchangeLanNetworkService,
)
from jea.statistics.services import (
    network_service_statistics as network_service_statistics_svc,
)
from jea.stateful import dispatch


class MemberJoiningRulesViewSet(IxApiViewSet):
    """
    Manage member joining rules for Network Services
    """
    @require_account
    def list(self, request, account=None):
        """List all member joining rules"""
        rules = membership_svc.get_member_joining_rules(
            scoping_account=account)

        return schema.MemberJoiningRule(rules, many=True).data

    @require_account
    def retrieve(self, request, account=None, pk=None):
        """Get a single member joining rule"""
        rule = membership_svc.get_member_joining_rule(
            member_joining_rule=pk,
            scoping_account=account)
        return schema.MemberJoiningRule(rule).data

    @require_account
    def create(self, request, account=None):
        """Create a new member joining rule"""
        rule_request = schema.MemberJoiningRuleRequest(data=request.data)
        rule_request.is_valid(raise_exception=True)

        rule = membership_svc.create_member_joining_rule(
            member_joining_rule_request=rule_request.validated_data,
            scoping_account=account)

        return schema.MemberJoiningRule(rule).data, status.HTTP_201_CREATED

    @require_account
    def update(self, request, account=None, pk=None):
        """Update a member joining rule"""
        rule_update = schema.MemberJoiningRuleUpdate(data=request.data)
        rule_update.is_valid(raise_exception=True)

        rule = membership_svc.update_member_joining_rule(
            member_joining_rule=pk,
            member_joining_rule_update=rule_update.validated_data,
            scoping_account=account)

        return schema.MemberJoiningRule(rule).data

    @require_account
    def partial_update(self, request, account=None, pk=None):
        """Update a member joining rule"""
        # Override type
        rule = membership_svc.get_member_joining_rule(
            member_joining_rule=pk,
            scoping_account=account)
        data = request.data
        data["type"] = rule.__polymorphic_type__

        # Deserialize request
        rule_update = schema.MemberJoiningRuleUpdate(
            data=data,
            partial=True)
        rule_update.is_valid(raise_exception=True)

        rule = membership_svc.update_member_joining_rule(
            member_joining_rule=pk,
            member_joining_rule_update=rule_update.validated_data,
            scoping_account=account)

        return schema.MemberJoiningRule(rule).data

    @require_account
    def destroy(self, request, account=None, pk=None):
        """Delete a member joining rule"""
        rule = membership_svc.delete_member_joining_rule(
            member_joining_rule=pk,
            scoping_account=account)

        return schema.MemberJoiningRule(rule).data


class NetworkServicesViewSet(IxApiViewSet):
    """
    A `NetworkService` is an instances of a `Product` accessible
    by one or multiple users, depending on the type of product

    For example, each Exchange Network LAN is considered as a shared
    instance of the related LAN's `Product`.
    """
    @require_account
    def list(self, request, account=None):
        """List available `network-services`."""
        network_services = network_svc.get_network_services(
            scoping_account=account,
            filters=request.query_params)

        return schema.NetworkService(network_services, many=True).data

    @require_account
    def retrieve(self, request, account=None, pk=None):
        """Get a specific `network-service` by id"""
        network_service = network_svc.get_network_service(
            network_service=pk)

        return schema.NetworkService(network_service).data

    @require_account
    def create(self, request, account=None):
        """
        Create a new NetworkService.

        Right now only elines, etrees and elans are supported.
        """
        service_config = dispatch.request(account, create_network_service(
            request.data))
        serializer = schema.NetworkService(service_config)
        return serializer.data, status.HTTP_201_CREATED

    @require_account
    def update(self, request, account=None, pk=None):
        """Update a NetworkService"""
        network_service = network_svc.get_network_service(
            network_service=pk,
            scoping_account=account)

        # Override type to prevent updating this attribute
        data = request.data
        data["type"] = network_service.__polymorphic_type__

        network_service = dispatch.request(
            account, update_network_service(network_service, data))

        return schema.NetworkService(network_service), status.HTTP_202_ACCEPTED

    @require_account
    def partial_update(self, request, account=None, pk=None):
        """Update some fields of a NetworkService"""
        network_service = network_svc.get_network_service(
            network_service=pk,
            scoping_account=account)

        # Override type
        data = request.data
        data["type"] = network_service.__polymorphic_type__

        network_service = dispatch.request(
            account,
            update_network_service(network_service, data, partial=True))

        return schema.NetworkService(network_service), status.HTTP_202_ACCEPTED

    @require_account
    def destroy(self, request, account=None, pk=None):
        """Destroy a network service"""
        # Request decommissioning
        network_service = network_svc.get_network_service(
            network_service=pk,
            scoping_account=account)

        product_offering = network_service.product_offering

        # Use decommission_at from request body if present
        decommission_at = request.data.get("decommission_at")
        if decommission_at:
            decommission_at = datetime.strptime(
                decommission_at, "%Y-%m-%d").date()

            # Validate date against cancellation policy
            policy = product_offering.cancellation_policy(
                date(2000, 1, 1), decommission_at=decommission_at)
            if policy["decommission_at"] != decommission_at:
                raise CancellationPolicyError(policy=policy)
        else:
            # Get next cancellation
            policy = product_offering.cancellation_policy(date(2000, 1, 1))

        network_service.decommission_at = policy["decommission_at"]
        network_service.charged_until = policy["charged_until"]

        network_service = dispatch.request(
            account, decommission_network_service(
                network_service,
                decommission_at=decommission_at))

        return schema.NetworkService(network_service), status.HTTP_202_ACCEPTED

    @action(
        detail=True,
        methods=["GET"],
        url_path="cancellation-policy")
    @require_account
    def cancellation_policy(self, request, pk=None, account=None):
        """Query the cancellation policy"""
        # Get the network service
        network_service = network_svc.get_network_service(
            network_service=pk,
            scoping_account=account)

        # ExchangeLans do not have a cancellation policy, but an
        # individual policy on the config.
        if isinstance(network_service, ExchangeLanNetworkService):
            raise NotFound

        product_offering = network_service.product_offering
        decommission_at = request.query_params.get("decommission_at")
        if decommission_at:
            decommission_at = datetime.strptime(
                decommission_at, "%Y-%m-%d").date()

        start_at = date(2000, 1, 1) # Maybe use created_at?
        policy = product_offering.cancellation_policy(
            start_at, decommission_at=decommission_at)

        return wrap_response(schema.CancellationPolicy(policy))

    @action(
        detail=True,
        methods=["GET", "POST", "DELETE"],
        url_path="change-request")
    @require_account
    def change_request(self, request, pk=None, account=None):
        """Handle network service change request"""
        if request.method == "GET":
            response = retrieve_network_service_change_request(
                network_service=pk,
                account=account)
        elif request.method == "POST":
            response = create_network_service_change_request(
                request=request,
                network_service=pk,
                account=account)
        elif request.method == "DELETE":
            response = destroy_network_service_change_request(
                network_service=pk,
                account=account)
        else:
            raise MethodNotAllowed(request.method)

        return wrap_response(response)

    @action(
        detail=True,
        methods=["GET"],
        url_path="statistics")
    @require_account
    def statistics_read(self, request, pk=None, account=None):
        """
        Read network service config statistics
        """
        # Get the network service
        network_service = network_svc.get_network_service(
            network_service=pk,
            scoping_account=account)

        statistics = network_service_statistics_svc.get_statistics(
            network_service)
        serializer = Aggregate(statistics)

        return JsonResponse(serializer.data)

    @action(
        detail=True,
        methods=["GET"],
        url_path="statistics/5m/timeseries")
    @require_account
    def statistics_timeseries_read(self, request, pk=None, account=None):
        """
        Get the timeseries of the port statistics
        """
        network_service = network_svc.get_network_service(
            network_service=pk,
            scoping_account=account)

        timeseries = network_service_statistics_svc.get_timeseries(
            network_service)

        # For now we can not use the serializer
        # serializer = AggregateTimeseries(timeseries)
        # We need to serialize the date in the correct format.
        def format_date(dt):
            return dt.strftime("%Y-%m-%dT%H:%M:%S.%fZ")

        timeseries["created_at"] = format_date(timeseries["created_at"])
        timeseries["next_update_at"] = format_date(timeseries["next_update_at"])
        timeseries["samples"] = [
            (format_date(s[0]), *s[1:])
            for s in timeseries["samples"]]

        import json
        data = json.dumps(timeseries)

        return HttpResponse(
            data,
            content_type="application/json")

#
# Network service change requests
#
def retrieve_network_service_change_request(
        network_service=None,
        account=None):
    """Get a network service change request or 404"""
    change_req = network_svc.get_network_service_change_request(
        network_service=network_service,
        scoping_account=account)
    return schema.NetworkServiceChangeRequest(change_req)


def create_network_service_change_request(
        request=None,
        network_service=None,
        account=None):
    """Create a change request"""
    change_req = schema.NetworkServiceChangeRequest(
        data=request.data)
    change_req.is_valid(raise_exception=True)
    change_req = network_svc.create_network_service_change_request(
        network_service_change_request=change_req.validated_data,
        network_service=network_service,
        scoping_account=account)

    return (
        schema.NetworkServiceChangeRequest(change_req),
        status.HTTP_202_ACCEPTED)


def destroy_network_service_change_request(
        network_service=None,
        account=None):
    """Destroy a network service change request if present"""
    change_request = network_svc.delete_network_service_change_request(
        network_service=network_service,
        scoping_account=account)

    return (
        schema.NetworkServiceChangeRequest(change_request),
        status.HTTP_202_ACCEPTED)


class NetworkFeaturesViewSet(IxApiViewSet):
    """
    `NetworkFeatures` are functionality made available to accounts
    within a `NetworkService`.
    Certain features may need to be configured by a account to use that
    service.

    This can be for example a `route server` on an `exchange lan`.

    Some of these features are mandatory to configure if you
    want to access the platform. Which of these features you have to
    configure you can query using: `/api/v1/network-features?required=true`
    """
    @require_account
    def list(self, request, account=None):
        """List available network features."""
        features = network_svc.get_network_features(
            scoping_account=account,
            filters=request.query_params)

        return schema.NetworkFeature(features, many=True).data

    @require_account
    def retrieve(self, request, account=None, pk=None):
        """Get a single network feature by it's id"""
        feature = network_svc.get_network_feature(
            scoping_account=account,
            network_feature=pk)

        return schema.NetworkFeature(feature).data

