
from rest_framework import (
    serializers,
    exceptions as rf_exceptions,
)
from ixapi_schema.v2 import schema

from jea.config import exceptions as access_exceptions
from jea.api.v2.errors import problem_factory


def test__make_validation_problem():
    """Test making a validation problem out of an error"""
    err = access_exceptions.PortUnavailable()
    problem = problem_factory._make_validation_problem(err)

    assert problem.extra["properties"]
    assert problem.extra["properties"][0]["name"]

    print(problem.__dict__)

def test__make_validation_problem_serializer_error():
    """Test making a problem out of a serializer error"""
    problem = None # Uh oh. We don't have a problem!

    # Create simple serializer
    class Foo(serializers.Serializer):
        field = serializers.CharField()

    serializer = Foo(data={})
    try:
        serializer.is_valid(raise_exception=True)
    except Exception as e:
        problem = problem_factory._make_validation_problem(e)

    assert problem
    assert problem.extra["properties"]
    assert problem.extra["properties"][0]["name"]

    print(problem.__dict__)


def test__make_problem_from_api_exception():
    """
    Test rest framework exceptions
    """
    expected = [
        (rf_exceptions.ValidationError(),
         schema.ValidationErrorProblem),
        (rf_exceptions.ParseError(),
         schema.ParseErrorProblem),
        (rf_exceptions.AuthenticationFailed(),
         schema.AuthenticationProblem),
        (rf_exceptions.NotAuthenticated(),
         schema.NotAuthenticatedProblem),
        (rf_exceptions.PermissionDenied(),
         schema.PermissionDeniedProblem),
        (rf_exceptions.NotFound(),
         schema.NotFoundProblem),
        (rf_exceptions.MethodNotAllowed("FNRD"),
         schema.MethodNotAllowedProblem),
        (rf_exceptions.NotAcceptable(),
         schema.NotAcceptableProblem),
        (rf_exceptions.Throttled(),
         schema.ServerErrorProblem),
    ]

    for exc, problem_class in expected:
        problem = problem_factory._make_problem_from_api_exception(exc)
        assert isinstance(problem, problem_class)

