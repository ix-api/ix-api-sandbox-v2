

from ixapi_schema.v2 import schema


def test_problems____init__():
    """Initialize problem, check if defaults are overridden"""
    problem = schema.Problem(title="title")

    assert problem.ptype == "about:blank" # default value
    assert problem.title == "title" # as above
    assert problem.status == None # default none


#
# Problem classes: Rest Framework
#
def test_generic_server_problem():
    """Test the generic error problem"""
    assert schema.ServerErrorProblem()


def test_validation_error_problem():
    """Test validation error problem"""
    problem = schema.ValidationErrorProblem(extra={
        "properties": ["foo"],
    })

    assert problem
    assert problem.extra["properties"][0] == "foo"


def test_parse_error_problem():
    """Test parse error problem"""
    assert schema.ParseErrorProblem()


def test_authentication_problem():
    """Test authentication problem"""
    assert schema.AuthenticationProblem()


def test_not_authenticated_problem():
    """Test authentication missing"""
    assert schema.NotAuthenticatedProblem()


def test_permission_denied_problem():
    """Test permission denied"""
    assert schema.PermissionDeniedProblem()


def test_not_found_problem():
    """Test 404 not found"""
    assert schema.NotFoundProblem()


def test_method_not_allowed_problem():
    """Test method not allowed"""
    assert schema.MethodNotAllowedProblem()


def test_not_acceptable_problem():
    """Test not acceptable errors"""
    assert schema.NotAcceptableProblem()


def test_unsupported_media_type_problem():
    """Test creeating an unsupported media type problem"""
    assert schema.UnsupportedMediaTypeProblem()


def test_too_many_requests_problem():
    """Test a throttled problem"""
    # I guess the problem is not trottled.
    assert schema.TooManyRequestsProblem()



