
"""
This factory is making problems. Nothing but problems.
"""

import inspect

from rest_framework import exceptions as rest_exceptions
from django.core import exceptions as django_exceptions
from jwt import exceptions as jwt_exceptions
from ixapi_schema.v2.entities import problems

from jea import exceptions


DJANGO_EXCEPTIONS = tuple(cls for (_, cls)
                          in inspect.getmembers(
                              django_exceptions,
                              inspect.isclass))



def make_problem(obj):
    """
    This function's primay objective is to make a problem
    out of everything.

    Put in any object and you will definitly get a problem.
    """
    validation_errors = (
        django_exceptions.ValidationError,
        rest_exceptions.ValidationError,
    )
    if isinstance(obj, exceptions.CancellationPolicyError):
        return _make_cancellation_problem(obj)
    if isinstance(obj, validation_errors):
        return _make_validation_problem(obj)
    if isinstance(obj, rest_exceptions.APIException):
        return _make_problem_from_api_exception(obj)
    if isinstance(obj, DJANGO_EXCEPTIONS):
        return _make_problem_from_django_exception(obj)
    if isinstance(obj, Exception):
        return _make_problem_from_exception(obj)

    return problems.ServerErrorProblem()


def _make_cancellation_problem(obj) -> problems.CancellationPolicyErrorProblem:
    """Make cancellation policy error problem"""
    prob = problems.CancellationPolicyErrorProblem(extra={
        "decommission_at": obj.decommission_at,
        "charged_until": obj.charged_until,
    })
    return prob


def _make_validation_problem(obj) -> problems.ValidationErrorProblem:
    """
    Make validation problem.
    First of all from a validation error.
    """
    properties: list = []

    # We have a django validation error. We extract the details from
    # the exception's `detail` attribute.
    if isinstance(obj, django_exceptions.ValidationError):
        properties = [
            {
                "name": getattr(obj, "field", "unknown"),
                "reason": obj.message,
            },
        ]
        return problems.ValidationErrorProblem(extra={
            "properties": properties,
        })

    # An ixapi specific validation error
    if isinstance(obj, exceptions.ValidationError):
        properties = [
            {
                "name": getattr(obj, "field", "unknown"),
                "reason": obj.detail[0],
            },
        ]
        return problems.ValidationErrorProblem(extra={
            "properties": properties,
        })


    # We are dealing with a rest framework
    # validation error.
    exc_details = getattr(obj, "detail")
    if isinstance(exc_details, list):
        field = getattr(obj, "field", None)
        if field:
            properties = [{"name": field, "reason": str(exc_details)}]
        else:
            properties = [{"name": "unknown", "reason": str(detail)}
                          for detail in exc_details]

    elif isinstance(exc_details, dict):
        # Unpack validation errors
        properties = []
        properties_ = [{"name": name, "reason": details}
                       for name, details in exc_details.items()]
        # Unpack reasons if list
        for prop in properties_:
            if isinstance(prop["reason"], list):
                for reason in prop["reason"]:
                    properties.append({
                        "name": prop["name"],
                        "reason": reason,
                    })
            else:
                properties.append(prop)
    else:
        properties = []

    # Create problem
    return problems.ValidationErrorProblem(extra={
        "properties": properties,
    })


def _get_problem_class(exception):
    """
    Get the problem class for a given exception.
    If no such class could be found, fall back to
    an empty problem.

    :param exception: The exception object
    """
    # First, try a static lookup
    exc_problems = {
        jwt_exceptions.ExpiredSignatureError:
            problems.SignatureExpiredProblem,
        rest_exceptions.AuthenticationFailed:
            problems.AuthenticationProblem

    }
    for exc_class, problem_class in exc_problems.items():
        if isinstance(exception, exc_class):
            return problem_class


    # Try to derive problem class from name
    if inspect.isclass(exception):
        cls = exception
    else:
        cls = exception.__class__

    if cls == object: # We are all the way down
        return problems.ServerErrorProblem

    name = cls.__name__
    problem_name = f"{name}Problem"

    # Try to get the corresponding problem class
    problem_class = getattr(problems, problem_name, None)
    if not problem_class:
        problem_class = _get_problem_class(cls.__base__)

    return problem_class


def _make_problem_from_api_exception(api_exception):
    """
    We have a rest framework api exception.
    Let's make a problem out of it.
    """
    problem_class = _get_problem_class(api_exception)

    if api_exception.detail and isinstance(api_exception.detail, list):
        detail = str(api_exception.detail[0])
    elif api_exception.detail:
        detail = str(api_exception.detail)
    else:
        detail = None

    # Fill in the details from the original object
    problem = problem_class(
        detail=detail,
        response_status=api_exception.status_code)

    return problem


def _make_problem_from_django_exception(exception):
    """
    Make problem from django exception
    """
    problem_class = _get_problem_class(exception)
    if isinstance(exception, django_exceptions.ObjectDoesNotExist):
        problem_class = problems.NotFoundProblem

    # Get the detail, if there is any. Otherwise make explicit none.
    detail = str(exception)
    if not detail:
        detail = None

    problem = problem_class(detail=detail)
    return problem


def _make_problem_from_exception(obj):
    """
    Try to make an exeption from any object
    """
    problem_class = _get_problem_class(obj)
    try:
        title = problem_class.default_title
        if problem_class == problems.Problem:
            title = obj.__class__.__name__

        detail = str(obj)
        if not detail:
            detail = None
        return problem_class(title=title, detail=detail)
    except:
        return problems.ServerErrorProblem()
