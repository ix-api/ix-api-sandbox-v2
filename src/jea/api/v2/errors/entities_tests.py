

"""
Test Problem serializers
"""

from ixapi_schema.v2 import schema

from jea.config import exceptions as access_exceptions
from jea.api.v2.errors import problem_factory


def test_problem_serializer():
    """Test serializing a problem"""
    problem = schema.MethodNotAllowedProblem()
    serializer = schema.ProblemResponse(problem)

    assert serializer.data


def test_problem_serializer__field_validation_error():
    """Test more complex error"""
    exc = access_exceptions.PortInUse()
    problem = problem_factory.make_problem(exc)
    assert isinstance(problem, schema.ValidationErrorProblem)

    serializer = schema.ProblemResponse(problem)

    assert serializer.data
    print(serializer.data)


def test_problem_serializer__validation_error():
    """Test more complex error"""
    serializer = schema.ConnectionRequest(data={})
    problem = None
    try:
        serializer.is_valid(raise_exception=True)
    except Exception as e:
        problem = problem_factory.make_problem(e)

    assert isinstance(problem, schema.ValidationErrorProblem)

    serializer = schema.ProblemResponse(problem)
    assert serializer.data

