

"""
Error Handler
"""

import traceback

from django.http import response as http_response
from ixapi_schema.v2 import schema

from jea.api.v2.errors import problem_factory


def handle_exception(exception, _context):
    """
    Handle the exception: Make a problem out of it,
    then serialize it, then make a JsonResponse with
    the right error code.

    :param exception: The exception to handle
    :param context: The context of the exception
    """
    problem = problem_factory.make_problem(exception)
    serializer = schema.ProblemResponse(problem)
    if problem.response_status >= 500:
        print(traceback.format_exc())

    return http_response.JsonResponse(
        serializer.data, status=problem.response_status)
