
"""
Test Catalog Views
"""

import pytest
from model_bakery import baker
from rest_framework import test

from jea.test import authorized_requests
from jea.api.v2.catalog import views


@pytest.mark.django_db
def test_metro_areas_view_set__list():
    """List all metro areas"""
    baker.make("catalog.MetroArea")
    account = baker.make("crm.Account")

    # Make request
    view = views.MetroAreasViewSet.as_view({"get": "list"})
    request = authorized_requests.get(account)
    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_metro_areas_view_set__retrieve():
    """Get a specific metro areas"""
    area = baker.make("catalog.MetroArea")
    account = baker.make("crm.Account")

    # Make request
    view = views.MetroAreasViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(account)
    response = view(request, pk=str(area.pk))
    assert response.status_code == 200


@pytest.mark.django_db
def test_metro_area_networks_view_set__list():
    """List all metro area networks"""
    baker.make("catalog.MetroAreaNetwork")
    account = baker.make("crm.Account")

    # Make request
    view = views.MetroAreaNetworksViewSet.as_view({"get": "list"})
    request = authorized_requests.get(account)
    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_metro_area_network_view_set__retrieve():
    """Get a metro are network by id"""
    man = baker.make("catalog.MetroAreaNetwork")
    account = baker.make("crm.Account")

    # Make request
    view = views.MetroAreaNetworksViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(account)
    response = view(request, pk=str(man.pk))
    assert response.status_code == 200


@pytest.mark.django_db
def test_facilities_view_set__list():
    """Test list all facilities"""
    account = baker.make("crm.Account")
    baker.make("catalog.Facility")
    view = views.FacilitiesViewSet.as_view({"get": "list"})

    # Make request
    request = authorized_requests.get(account)
    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_facilities_view_set__retrieve():
    """Test retrieving a facility"""
    account = baker.make("crm.Account")
    facility = baker.make("catalog.Facility")

    # Make request
    view = views.FacilitiesViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(account)
    response = view(request, pk=facility.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_devices_view_set__list():
    """Test listing devices"""
    account = baker.make("crm.Account")
    baker.make("catalog.Device")
    view = views.DevicesViewSet.as_view({"get": "list"})
    request = authorized_requests.get(account)

    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_devices_view_set__retrieve():
    """Test retrieving a device"""
    account = baker.make("crm.Account")
    device = baker.make("catalog.Device")

    view = views.DevicesViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(account)

    response = view(request, pk=device.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_pops_view_set__list():
    """Test getting all pops"""
    account = baker.make("crm.Account")
    baker.make("catalog.PointOfPresence")
    request = authorized_requests.get(account)
    view = views.PointsOfPresenceViewSet.as_view({
        "get": "list",
    })

    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_pops_view_set__retrieve():
    """Test getting a specific device"""
    account = baker.make("crm.Account")
    pop = baker.make("catalog.PointOfPresence")
    request = authorized_requests.get(account)
    view = views.PointsOfPresenceViewSet.as_view({
        "get": "retrieve",
    })

    response = view(request, pk=pop.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_product_offerings_view_set__list():
    """Test getting all the products"""
    account = baker.make("crm.Account")
    baker.make("catalog.ExchangeLanNetworkProductOffering")
    baker.make("catalog.P2PNetworkProductOffering")
    baker.make("catalog.MP2MPNetworkProductOffering")
    baker.make("catalog.P2MPNetworkProductOffering")
    baker.make("catalog.CloudNetworkProductOffering")
    baker.make("catalog.ConnectionProductOffering")
    request = authorized_requests.get(account)
    view = views.ProductOfferingsViewSet.as_view({
        "get": "list",
    })

    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_product_offerings_view_set__retrieve():
    """Test getting a specific product"""
    account = baker.make("crm.Account")
    product = baker.make("catalog.ExchangeLanNetworkProductOffering")
    request = authorized_requests.get(account)
    view = views.ProductOfferingsViewSet.as_view({"get": "retrieve"})

    response = view(request, pk=product.pk)
    assert response.status_code == 200

