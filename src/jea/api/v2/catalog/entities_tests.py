
"""
Test Catalog Serializers
"""

import pytest
from model_bakery import baker

from ixapi_schema.v2 import schema
from ixapi_schema.v2.schema import (
    PRODUCT_TYPE_EXCHANGE_LAN,
    PRODUCT_TYPE_P2P,
    PRODUCT_TYPE_P2MP,
    PRODUCT_TYPE_MP2MP,
    PRODUCT_TYPE_CLOUD,
)

from jea.catalog.models import (
    PointOfPresence,
    Device,
    DeviceCapability,
    Facility,
    FacilityOperator,
    MetroArea,
    MetroAreaNetwork,
    ExchangeLanNetworkProductOffering,
    P2PNetworkProductOffering,
    P2MPNetworkProductOffering,
    MP2MPNetworkProductOffering,
    CloudNetworkProductOffering,
)


@pytest.mark.django_db
def test_metro_area_serializer():
    """Test Metro Area Serializer"""
    metro_area = baker.make("catalog.MetroArea")
    baker.make(
        "catalog.Facility",
        metro_area=metro_area)
    baker.make(
        "catalog.MetroAreaNetwork",
        metro_area=metro_area)
    serializer = schema.MetroArea(metro_area)

    result = serializer.data
    assert result
    assert result["facilities"]
    assert result["metro_area_networks"]


@pytest.mark.django_db
def test_metro_area_network_serializer():
    """Test metro area network serializer"""
    man = baker.make("catalog.MetroAreaNetwork")
    pop = baker.make(
        "catalog.PointOfPresence",
        metro_area_network=man)
    serializer = schema.MetroAreaNetwork(man)
    result = serializer.data
    assert result
    assert result["pops"]


def test_facility_serializer():
    """Test facility serialization"""
    operator = baker.prepare(FacilityOperator)
    facility = baker.prepare(Facility, operator=operator)

    serializer = schema.Facility(facility)
    result = serializer.data
    assert result, "Serializer should have generated a result"
    assert result["organisation_name"] == operator.name


@pytest.mark.django_db
def test_devices_serializer():
    """Test device serialization"""
    device = baker.make(Device)
    serializer = schema.Device(device)

    result = serializer.data
    assert result, "The serializer should produce data"


@pytest.mark.django_db
def test_devices_serializer_capabilities():
    """Test embedded capabilities"""
    capa_1 = baker.make(DeviceCapability)
    capa_2 = baker.make(DeviceCapability)
    capa_3 = baker.make(DeviceCapability)

    device = baker.make(Device, capabilities=[capa_1, capa_2, capa_3])

    serializer = schema.Device(device)

    result = serializer.data
    assert result, "The serializer should produce data"
    assert result["capabilities"]
    assert len(result["capabilities"]) == 3


def test_device_capability_serializer():
    """Test the serialization of a device capability"""
    capa = baker.prepare(DeviceCapability)
    serializer = schema.DeviceCapability(capa)

    assert serializer.data, "Serializer should produce data"


@pytest.mark.django_db
def test_pop_serializer():
    """Test serialization of a pop"""
    device_a = baker.make(Device, name="device_a")
    device_b = baker.make(Device, name="device_b")

    # Add capability
    baker.make(DeviceCapability, device=device_a)

    facility = baker.make(Facility)
    pop_a = baker.make(PointOfPresence,
                       devices=[device_a, device_b],
                       facility=facility)

    serializer = schema.PointOfPresence(pop_a)
    result = serializer.data
    assert result, "Serializer should produce data without crashing."

#
# Polymorphic Product Serialization
#
@pytest.mark.django_db
def test_polymorphic_product_serialization():
    """Test polymorphic product serializer"""
    exchange_lan = baker.make(ExchangeLanNetworkProductOffering)
    p2p = baker.make(P2PNetworkProductOffering)
    p2mp = baker.make(P2MPNetworkProductOffering)
    mp2mp = baker.make(MP2MPNetworkProductOffering)
    cloud = baker.make(CloudNetworkProductOffering)

    # Serialize products
    result = schema.ProductOffering(exchange_lan).data
    assert result["type"] == PRODUCT_TYPE_EXCHANGE_LAN

    result = schema.ProductOffering(p2p).data
    assert result["type"] == PRODUCT_TYPE_P2P

    result = schema.ProductOffering(p2mp).data
    assert result["type"] == PRODUCT_TYPE_P2MP

    result = schema.ProductOffering(mp2mp).data
    assert result["type"] == PRODUCT_TYPE_MP2MP

    result = schema.ProductOffering(cloud).data
    assert result["type"] == PRODUCT_TYPE_CLOUD

