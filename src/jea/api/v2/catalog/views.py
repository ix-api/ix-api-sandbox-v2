
"""
Catalog :: Views

Implements catalog related endpoints. For example
for listing facilities and points of presence.
"""

from rest_framework import exceptions, status
from ixapi_schema.v2 import schema

from utils.datastructures import must_list
from jea.api.v2.permissions import require_account
from jea.api.v2.response import tags
from jea.api.v2.viewsets import IxApiViewSet
from jea.api.v2.response import ApiSuccess, ApiError
from jea.catalog.services import (
    devices as devices_svc,
    pops as pops_svc,
    products as products_svc,
    facilities as facilities_svc,
    locations as locations_svc,
)


class MetroAreasViewSet(IxApiViewSet):
    """MetroArea Endpoints"""
    def list(self, request):
        """Get all metro areas"""
        metro_areas = locations_svc.get_metro_areas(
            filters=request.query_params)
        return schema.MetroArea(metro_areas, many=True)

    def retrieve(self, _request, pk=None):
        """Get a metro area by id"""
        metro_area = locations_svc.get_metro_area(
            metro_area=pk)
        return schema.MetroArea(metro_area)


class MetroAreaNetworksViewSet(IxApiViewSet):
    """All metro area network views"""
    def list(self, request):
        """Get all metro area networks"""
        networks = locations_svc.get_metro_area_networks(
            filters=request.query_params)
        return schema.MetroAreaNetwork(networks, many=True)

    def retrieve(self, request, pk=None):
        """"Get a metro area network by id"""
        network = locations_svc.get_metro_area_network(
            metro_area_network=pk)
        return schema.MetroAreaNetwork(network)


class FacilitiesViewSet(IxApiViewSet):
    """
    A `Facility` is a data centre, with a determined
    physical address.
    """
    def list(self, request):
        """Get a (filtered) list of `facilities`."""
        facilities = facilities_svc.get_facilities(
            filters=request.query_params)
        return schema.Facility(facilities, many=True)

    def retrieve(self, request, pk=None):
        """Retrieve a facility by id"""
        facility = facilities_svc.get_facility(facility=pk)
        return schema.Facility(facility)


class DevicesViewSet(IxApiViewSet):
    """
    A `Device` is a network hardware device, typically a Switch, which
    is located at a specified facility and connected to one or more
    PoPs.

    They may be physically located at their related PoPs or remotely
    available.
    """
    def list(self, request):
        """List available devices"""
        devices = devices_svc.get_devices(
            filters=request.query_params)
        return schema.Device(devices, many=True).data

    def retrieve(self, request, pk=None):
        """Get a specific device identified by id"""
        device = devices_svc.get_device(device=pk)
        return schema.Device(device)


class PointsOfPresenceViewSet(IxApiViewSet):
    """
    A `PoP` is a location within a Facility which is connected to a
    single Network Infrastructure and has defined reachability of other
    facilities.

    A single room may contain multiple PoPs, each linking to a different
    infrastructure.
    """
    def list(self, request):
        """List all PoPs"""
        pops = pops_svc.get_pops(filters=request.query_params)
        return schema.PointOfPresence(pops, many=True)

    def retrieve(self, request, pk=None):
        """Get a pop"""
        pop = pops_svc.get_pop(pop=pk)
        return schema.PointOfPresence(pop)


class ProductOfferingsViewSet(IxApiViewSet):
    """
    A `ProductOffering` is a network or peering-related product instance
    of a defined type sold by an IXP to its `Accounts`.
    """
    def list(self, request):
        """List all (filtered) products available on the platform"""
        products = products_svc.get_product_offerings(
            filters=request.query_params)
        results = schema.ProductOffering(products, many=True).data

        # Build custom representation based on provided fields
        fields_filter = must_list(
            request.query_params.get("fields"))
        if not fields_filter:
            return results

        fields_filter = ["type"] + fields_filter
        filtered = [
            {key: prod.get(key)
             for key in fields_filter}
            for prod in results]

        return filtered

    def retrieve(self, request, pk=None):
        """Get a specific product by id"""
        offering = products_svc.get_product_offering(
            product_offering=pk)
        return schema.ProductOffering(offering)
