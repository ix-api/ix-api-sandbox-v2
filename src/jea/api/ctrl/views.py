"""
Controll Endpoints

Right now the primary endpoint is the reset trigger endpoint
"""

from django.http import response
from django.views.decorators.csrf import csrf_exempt

from jea.ctrl.services import (
    secrets as secrets_svc,
    snapshot as snapshot_svc,
)


@csrf_exempt
def reset_trigger(request, token):
    """Reset the sandbox to the last snapshot state"""
    # Allow only post
    if request.method != "POST":
        return response.HttpResponseNotAllowed(
            permitted_methods=["GET"])

    # Check token validity
    try:
        secrets_svc.validate(token)
    except secrets_svc.InvalidTokenException:
        return response.HttpResponseForbidden()

    # Everything's fine? Reset to snapshot
    snapshot_svc.restore()

    return response.HttpResponse("ok.")
