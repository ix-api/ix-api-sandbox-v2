"""
Sandbox Control URLs
"""

from django.urls import path

from jea.api.ctrl import views as ctrl_views


urlpatterns = [
    path("reset-<token>", ctrl_views.reset_trigger),
]
