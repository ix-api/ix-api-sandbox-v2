
"""
Token Authentication Service Tests
"""

from datetime import datetime, timedelta

import pytest
from model_bakery import baker
from freezegun import freeze_time
from jwt.exceptions import InvalidTokenError

from jea.auth import roles, models as auth_models
from jea.auth.services import token_authentication as token_authentication_svc
from jea.crm import models as crm_models


def test_issue_token():
    """Test JWT creation"""
    account = baker.prepare(crm_models.Account)
    token = token_authentication_svc.issue_token(
        account.pk, lifetime=timedelta(minutes=5))

    assert token


def test_issue_tokens():
    """Test token pair creation"""
    account = baker.prepare(crm_models.Account)
    access, refresh = token_authentication_svc.issue_tokens(
        account.pk)

    # Decode tokens and inspect payload
    access_payload = token_authentication_svc.decode(access)
    refresh_payload = token_authentication_svc.decode(refresh)

    assert roles.ACCESS in access_payload["roles"]
    assert roles.TOKEN_REFRESH in refresh_payload["roles"]


def test_decode_validation():
    """Test token validation"""
    account = baker.prepare(crm_models.Account)

    with freeze_time() as t:
        # T0: Create fresh token with a lifetime of 30 minutes
        token = token_authentication_svc.issue_token(
            account.id, lifetime=timedelta(minutes=30))

        # T0+10m:
        # This should be valid and decodable
        t.tick(delta=timedelta(minutes=10))
        assert not token_authentication_svc.decode(token) is None

        # T0+31m:
        # The token should have expired
        t.tick(delta=timedelta(minutes=21))
        with pytest.raises(InvalidTokenError) as exc:
            token_authentication_svc.decode(token)


@pytest.mark.django_db
def test_authenticate_empty_accounts():
    """Authenticate with valid credentials"""
    account = baker.make("crm.Account")
    user = baker.make(auth_models.User,
                      account=account)

    access_token, refresh_token = token_authentication_svc.authenticate_api_key(
        user.api_key, user.api_secret)

    # Assertations
    assert access_token # Not empty
    assert refresh_token # Not empty

    # Check content
    payload = token_authentication_svc.decode(access_token)
    assert payload["sub"], \
        "Account should be set"
    assert str(payload["sub"]) == str(user.account.pk), \
        "JWT token subject should be the assigned account"

