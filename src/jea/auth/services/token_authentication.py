
"""
Token Authentication Services
"""

from typing import Tuple, Optional, Mapping, Any, List, Union
from datetime import datetime, timedelta

import jwt
from jwt.exceptions import DecodeError
from django.conf import settings

from jea.auth import roles
from jea.auth import models as auth_models
from jea.auth.exceptions import CredentialsError
from jea.crm import models as crm_models


def encode(payload: dict) -> str:
    """
    Encode a json web token with settings suited for
    our authentication

    :param payload: the JWT payload
    """
    # We use the application secret as the HMAC secret
    secret = settings.SECRET_KEY
    token = jwt.encode(payload, secret, algorithm="HS256")

    return token


def issue_token(
        account_id: Union[int, str],
        lifetime: timedelta,
        roles: List[str] = [],
        extra = {},
    ) -> str:
    """
    Create a JWT with a lifetime and include account
    and subaccount.

    :param root_account: The entry account to the accounts tree
    :param sub_account: The subaccount
    :param lifetime: The ttl of the token
    """
    # Expiration and lifetime. Integer encoding is done by the
    # jwt library.
    issued_at = datetime.utcnow()
    expires_at = issued_at + lifetime
    account_id = str(account_id)

    payload = {
        "iss": "auth.ixapi",
        "sub": account_id,
        "iat": issued_at,
        "exp": expires_at,
        "roles": roles,
        **extra,
    }

    return encode(payload)


def issue_auth_code(app_user, user):
    """Issue an auth code token"""
    issued_at = datetime.utcnow()
    expires_at = issued_at + timedelta(minutes=5)

    payload = {
        "iss": "auth.ixapi.oauth2.code",
        "sub": str(user.pk),
        "act": {
            "sub": str(app_user.pk),
        },
        "exp": expires_at,
    }

    return encode(payload)


def issue_tokens(account_id: Union[int, str]) -> Tuple[str, str]:
    """
    Issue both access and refresh token.
    Configuration of the token's lifetimes is possible within
    the django settings module.

    JEA_ACCESS_TOKEN_LIFETIME and JEA_REFRESH_TOKEN_LIFETIME should
    both be of a datetime.timedelta type.

    :param account_id: The id of the acting account
    """
    access_token_lifetime = getattr(settings,
                                    "JEA_ACCESS_TOKEN_LIFETIME",
                                    timedelta(minutes=30))

    refresh_token_lifetime = getattr(settings,
                                     "JEA_REFRESH_TOKEN_LIFETIME",
                                     timedelta(hours=24))

    access_token = issue_token(
        account_id,
        lifetime=access_token_lifetime,
        roles=[
            roles.ACCESS,
        ])
    refresh_token = issue_token(
        account_id,
        lifetime=refresh_token_lifetime,
        roles=[
            roles.TOKEN_REFRESH,
        ])

    return (access_token, refresh_token)


def refresh(token_payload) -> Tuple[str, str]:
    """
    Generate a fresh access and refresh token.
    It is importent, that the token_payload was verified.

    :param token_payload: The payload coming from a refresh token
    """
    account_id = token_payload["sub"]

    return issue_tokens(account_id)


def decode(token: str, raise_exception=True) -> Optional[Mapping[str, Any]]:
    """
    Decode a given token and check validity.
    Will raise an error if the verification fails exception.

    :param token: The token to check

    :raises: jwt.exceptions.DecodeError
    """
    secret = settings.SECRET_KEY
    payload = None
    if raise_exception:
        payload = jwt.decode(token, secret, algorithms=["HS256"])
    else:
        try:
            payload = jwt.decode(token, secret, algorithms=["HS256"])
        except DecodeError:
            pass

    return payload


def authenticate_api_key(api_key: str, api_secret: str) -> Tuple[str, str]:
    """
    Authenticate an api key and secret pair.

    If an additional account_id is supplied, bind the
    token to the given account.

    :param api_key: The api key
    :param api_secret: The secret associated with the api key
    :param account_id: The id of the acting account id

    :raises: AuthenticationError

    :returns: An access and refresh token pair on success
    """
    # Get user by api_key and api_secret
    try:
        user = auth_models.User.objects \
            .prefetch_related("account") \
            .get(api_key=api_key, api_secret=api_secret)

    except auth_models.User.DoesNotExist:
        raise CredentialsError("invalid api_key or api_secret")

    # If we got until here, we can issue the access and refresh token
    return issue_tokens(user.account.pk)

