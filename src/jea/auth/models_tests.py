import pytest

from jea.auth import models


def test_user_api_key_generation():
    """Users should have an api key and secret"""
    user = models.User()

    assert len(user.api_key) > 1, \
        "An API KEY should be present"

    assert len(user.api_secret) > 1, \
        "An API SECRET should be present"

