from django.shortcuts import render
from django.contrib.auth.decorators import login_required


@login_required
def account_view(request):
    """Show the current user account"""
    return render(request, "auth/account.html", {
        "user": request.user,
        "account": request.user.account,
    })
