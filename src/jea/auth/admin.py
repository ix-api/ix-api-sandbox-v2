
from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.utils.translation import gettext_lazy as _

from jea.admin import site as admin_site
from jea.auth.models import User


class UserAdmin(BaseUserAdmin):
    """
    A customized version of the UserAdmin to accomodate
    for api_key and api_secret
    """
    # Add account to list and to edit forms
    list_display = (
        "username",
        "first_name",
        "last_name",
        "email",
        "account",
        "is_staff",
    )

    fieldsets = (
        (None,
            {"fields": ("username", "password")}),
        (_("Personal info"),
            {"fields": ("first_name", "last_name", "email")}),
        (_("Account"), {"fields": ("account", )}),
        (_("Api Access"),
            {"fields": ("api_key", "api_secret")}),
        (_("Permissions"),
            {"fields": ("is_active", "is_staff", "is_superuser",
                        "groups", "user_permissions")}),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )
    add_fieldsets = (
        (None, {
            "classes": ("wide",),
            "fields": ("username", "email",
                        "password1", "password2"),
        }),
        (_("Personal info"),
            {"fields": ("first_name", "last_name", "email")}),
        (_("Account"), {"fields": ("account", )}),
    )

    search_fields = ("username",)
    ordering = ("username",)

    filter_horizontal = ()


# Register UserAdmin
admin_site.register(User, UserAdmin)

