
"""
JEA :: CRM :: Models
--------------------

"""


import secrets

from django.db import models
from django.contrib.auth.models import AbstractUser

from jea.auth.roles import ACCESS, TOKEN_REFRESH


def _make_api_key():
    """Create an api key"""
    return secrets.token_hex(8)


def _make_api_secret():
    """Create an api secret"""
    return secrets.token_urlsafe(64)


class User(AbstractUser):
    """JEA User Model"""
    account = models.OneToOneField(
        "crm.Account",
        null=True,
        default=None,
        on_delete=models.CASCADE)

    api_key = models.CharField(max_length=16,
                               default=_make_api_key)

    api_secret = models.CharField(max_length=86,
                                  default=_make_api_secret)

    # Auth permissions
    access_roles = [ ACCESS, TOKEN_REFRESH ]


