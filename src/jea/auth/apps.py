
import logging

from django.apps import AppConfig

logger = logging.getLogger(__name__)


class AuthConfig(AppConfig):
    name = "jea.auth"
    label = "jea_auth"

    verbose_name = "JEA :: Auth"

    def ready(self):
        """Application is ready"""
        logger.info("Initializing app: {}".format(self.name))


