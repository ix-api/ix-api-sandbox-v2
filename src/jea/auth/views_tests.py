
import pytest

from model_bakery import baker

from jea.test import authorized_requests
from jea.crm.models import Account
from jea.auth.views import account_view


@pytest.mark.django_db
def test_account_view():
    """Test account rendering"""
    account = baker.make(Account)
    request = authorized_requests.get(account)

    response = account_view(request)
    assert response.status_code == 200, \
        "Response should be OK"


