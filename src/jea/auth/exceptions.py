
"""
Authentication Exceptions
"""

from rest_framework import exceptions, status


class AuthenticationError(exceptions.AuthenticationFailed):
    """Error Base Class"""
    status_code = status.HTTP_403_FORBIDDEN


class TokenError(AuthenticationError):
    """Token Error"""
    default_detail = "The provided token was insufficient for the request."


class AuthorizationError(AuthenticationError):
    """Access Denied."""
    default_detail = ("The provided token had insufficient rights "
                      "to authorize the request.")


class AuthorizationIncompleteError(AuthorizationError):
    """Access denied, no account provided"""
    default_detail = ("The provided authorization token does not "
                      "include a account_id to act upon. Please create fully "
                      "authorized token by providing a account_id.")


class CredentialsError(AuthenticationError):
    """Raised when invalid credentials were provided"""

