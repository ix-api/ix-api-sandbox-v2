
from django.urls import include, path
from django.contrib.auth.views import LoginView, LogoutView

from jea.auth.oauth2 import urls as oauth2_urls
from jea.auth.views import account_view


login_view = LoginView.as_view(
    template_name="auth/login.html",
    next_page="account",
    redirect_authenticated_user=True)

logout_view = LogoutView.as_view(
    next_page="login")

urlpatterns = [
    path(r"oauth2/", include(oauth2_urls)),
    path(r"login", login_view, name="login"),
    path(r"logout", logout_view, name="logout"),
    path(r"account", account_view, name="account")
]
