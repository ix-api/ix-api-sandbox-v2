
"""
JEA Auth Roles
"""

# ACCESS is a minimal base permission.
# This is used for limiting the refresh token's permissions
ACCESS = "access"

# Ability to generate a new access token from
# a refresh token.
TOKEN_REFRESH = "token_refresh"

