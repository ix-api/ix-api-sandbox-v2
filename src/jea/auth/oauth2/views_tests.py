import json

import pytest
from model_bakery import baker
from django.http import HttpRequest

from jea.crm.models import Account
from jea.auth.models import User
from jea.auth.services import token_authentication
from jea.auth.oauth2 import views
from jea.auth.oauth2.views import (
    FLOW_CLIENT_CREDENTIALS,
    FLOW_REFRESH_TOKEN,
)


def test_oauth_token_success():
    """Test oauth token success"""
    req = HttpRequest()
    tokens = ["access", "refresh"]

    res = views.oauth_token_success(req, tokens)
    assert res.status_code == 200

    req.POST["state"] = "state123"
    res = views.oauth_token_success(req, tokens)
    assert res.status_code == 200

    data = res.serialize()
    assert b"state123" in data


def test_decode_basic_auth():
    """Test Basic Auth Payload Decoding"""
    auth = "Basic dXNlcm5hbWU6cGFzc3dvcmQ="
    user, password = views.decode_basic_auth(auth)
    assert user == 'username'
    assert password == 'password'


def test_decode_basic_auth__invalid():
    """Test Invalid Basic Auth Payload Decoding"""
    with pytest.raises(UnicodeDecodeError):
        views.decode_basic_auth("Basic d$XNlcmGFzc3dvc?m$Q=")

    with pytest.raises(ValueError):
        _user , _pass = views.decode_basic_auth("Basic YXNkZg==")

    with pytest.raises(ValueError):
        views.decode_basic_auth("YXNkZg==")


@pytest.mark.django_db
def test_oauth_token_flow_credentials():
    """Test credentials flow"""
    # Prepare user
    account = baker.make(Account)
    baker.make(
        User,
        api_key="apikey",
        api_secret="secret",
        account=account)

    # Make authorized request with `apikey:secret` as
    # Basic Auth credentials.
    req = HttpRequest()
    req.POST["grant_type"] = FLOW_CLIENT_CREDENTIALS
    req.META["HTTP_AUTHORIZATION"] = "Basic YXBpa2V5OnNlY3JldA=="

    res = views.oauth_token(req)
    assert res.status_code == 200, res.__dict__

    data = json.loads(res.content)
    assert data.get("access_token")
    assert data.get("refresh_token")
    assert data.get("token_type")


@pytest.mark.django_db
def test_oauth_token_flow_refresh_token():
    """Test refresh token flow"""
    # Prepare user
    account = baker.make(Account)
    user = baker.make(
        User,
        api_key="apikey",
        api_secret="secret",
        account=account)

    _, refresh_token = token_authentication.\
        authenticate_api_key(user.api_key, user.api_secret)

    # Make authorized request with `apikey:secret` as
    # Basic Auth credentials.
    req = HttpRequest()
    req.POST["grant_type"] = FLOW_REFRESH_TOKEN
    req.POST["refresh_token"] = refresh_token

    res = views.oauth_token(req)
    assert res.status_code == 200, res.__dict__

    data = json.loads(res.content)
    assert data.get("access_token")
    assert data.get("refresh_token")
    assert data.get("token_type")

