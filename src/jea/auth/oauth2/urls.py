"""
OAuth2 endpoints
"""

from django.urls import path

from jea.auth.oauth2.views import (
    oauth_token as oauth_token_view,
    oauth_authorize_view,
    oauth_authorize_callback_view,
)

urlpatterns = [
    path(r"token", oauth_token_view),
    path(r"authorize", oauth_authorize_view),
    path(r"callback", oauth_authorize_callback_view),
]
