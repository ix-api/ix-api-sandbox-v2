
from django.http import JsonResponse

class OAuthError(Exception):
    """OAuth2 Error"""
    error = ""
    description = None

    def __init__(self, description=None, error=None):
        """Initialize error"""
        if error:
            self.error = error
        if description:
            self.description = description

    def into_response(self):
        """Make JsonResponse from exception"""
        res = {
            "error": self.error,
        }
        if self.description:
            res["error_description"] = self.description

        return JsonResponse(res, status=400)


class InvalidRequestError(OAuthError):
    error = "invalid_request"

class InvalidClientError(OAuthError):
    error = "invalid_client"

class InvalidGrantError(OAuthError):
    error = "invalid_grant"

class UnsupportedGrantTypeError(OAuthError):
    error = "unsupported_grant_type"
