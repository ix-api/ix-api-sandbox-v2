from base64 import b64decode

from django.http import JsonResponse, HttpResponseRedirect
from django.utils.http import urlencode
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from jea.auth.services import token_authentication
from jea.auth.models import User
from jea.auth.roles import (
    TOKEN_REFRESH as ROLE_TOKEN_REFRESH,
)
from jea.auth.oauth2.exceptions import (
    OAuthError,
    InvalidRequestError,
    InvalidGrantError,
    UnsupportedGrantTypeError,
)

FLOW_CLIENT_CREDENTIALS = "client_credentials"
FLOW_REFRESH_TOKEN = "refresh_token"
FLOW_AUTH_CODE = "authorization_code"

ERROR_GRANT_TYPE_MISSING = (
    "the `grant_type` was not provided in the request"
)
ERROR_AUTHORIZATION_HEADER_MISSING = (
    "the `Authorization` header is missing from the request"
)
ERROR_BASIC_AUTH_INVALID = (
    "the basic auth information was not provided "
    "in the format: `<apikey>:<secret>`"
)
ERROR_API_CREDENTIALS = (
    "the provided api key and secret are not associated "
    "with a user account"
)


def decode_basic_auth(authorization):
    """Decode basic auth"""
    _, credentials = authorization.split(" ", 1)
    credentials = str(b64decode(credentials), encoding="utf-8")

    return credentials.split(":", 1)


def grant_type_from_request(request):
    """Get the grant type"""
    grant_type = request.POST.get("grant_type")
    if not grant_type:
        raise InvalidRequestError(ERROR_GRANT_TYPE_MISSING)

    return grant_type


def credentials_from_request(request):
    """Get api key and secret"""
    # Request body
    api_key = request.POST.get("client_id")
    secret = request.POST.get("client_secret")
    if api_key and secret:
        return api_key, secret

    # Basic Auth Header
    authorization = request.headers.get("authorization")
    if not authorization:
        raise InvalidRequestError(ERROR_AUTHORIZATION_HEADER_MISSING)

    try:
        return decode_basic_auth(authorization)
    except:
        raise InvalidRequestError(ERROR_BASIC_AUTH_INVALID)


def oauth_token_error(error, description=None):
    """Make an error"""
    res = {
        "error": error,
    }
    if description:
        res["error_description"] = description

    return JsonResponse(res, status=400)


def oauth_token_success(request, tokens):
    """Token Success Response"""
    access_token, refresh_token = tokens

    # OAuth2 Success Response
    res = {
        "access_token": access_token,
        "refresh_token": refresh_token,
        "expires_in": 30 * 60, # minutes
        "token_type": "bearer",
    }

    state = request.POST.get("state")
    if state:
        res["state"] = state

    return JsonResponse(res)


@csrf_exempt
def oauth_token(request):
    """
    Try to grant an access token.
    """
    try:
        tokens = oauth_token_flow(request)
        return oauth_token_success(request, tokens)

    except OAuthError as err:
        return err.into_response()


def oauth_token_flow(request):
    """Implement oauth flows"""
    grant_type = grant_type_from_request(request)
    flow = {
        FLOW_CLIENT_CREDENTIALS: oauth_token_flow_client_credentials,
        FLOW_REFRESH_TOKEN: oauth_token_flow_refresh_token,
        FLOW_AUTH_CODE: oauth_token_flow_auth_code,
    }.get(grant_type)
    if not flow:
        raise UnsupportedGrantTypeError()

    return flow(request)


def oauth_token_flow_client_credentials(request):
    """
    Authorize with client credentials using basic auth:
    ApiKey and Secret are used as 'username' and 'password'
    and encoded as Authorization: Basic BASE64(username:password)
    """
    api_key, secret = credentials_from_request(request)

    # We have a key and secret, let's check the auth user model
    try:
        return token_authentication.authenticate_api_key(api_key, secret)
    except:
        raise InvalidGrantError(ERROR_API_CREDENTIALS)


def oauth_token_flow_refresh_token(request):
    """
    Authorize a new access token using a refresh token
    """
    refresh_token = request.POST.get("refresh_token")
    if not refresh_token:
        raise InvalidRequestError()

    # Decode refresh token. This will fail if the token is invalid.
    try:
        refresh_token = token_authentication.decode(refresh_token)
    except:
        raise InvalidGrantError()

    # Check if the token has refresh capabilities
    if not ROLE_TOKEN_REFRESH in refresh_token["roles"]:
        raise InvalidGrantError()

    # Issue a new set of tokens
    return token_authentication.refresh(refresh_token)


def oauth_token_flow_auth_code(request):
    """
    Authorize a new access token using an auth code
    """
    token = request.POST["code"]
    claims = token_authentication.decode(token)

    # Create new auth token for delegate account
    user = User.objects.get(pk=claims["act"]["sub"])
    return token_authentication.issue_tokens(user.account.pk)


@login_required
def oauth_authorize_view(request):
    """Authorize app view"""
    client_id = request.GET.get("client_id")
    response_type = request.GET.get("response_type", "code")
    accept = request.POST.get("accept") is not None
    implicit = response_type == "token"

    if not client_id:
        return render(request, "auth/oauth2/error.html", {
            "error": "client_id is missing",
        })

    # Identify application requesting access
    app = User.objects.filter(api_key=client_id).first()
    if not app:
        return render(request, "auth/oauth2/error.html", {
            "error": "client_id is unknown",
        })

    # Handle action
    if request.method == "POST" and accept and implicit:
        # This is not secure. In a real world application this
        # may never be done.
        access_token, _ = token_authentication.issue_tokens(app.account.pk)
        return oauth_invoke_callback(request, access_token=access_token)

    if request.method == "POST" and accept:
        auth_code = token_authentication.issue_auth_code(
            app, request.user)
        return oauth_invoke_callback(request, code=auth_code)

    if request.method == "POST":
        return oauth_invoke_callback(request, error="access_denied")

    return render(request, "auth/oauth2/authorize.html", {
        "user": request.user,
        "app": app,
        "app_account": app.account,
    })


def oauth_invoke_callback(request, **params):
    """Invoke callback URL"""
    state = request.GET.get("state")
    redirect_uri = request.GET.get(
        "redirect_uri",
        "/auth/oauth2/callback")

    if state:
        params["state"] = state

    target = redirect_uri + "?" + urlencode(params)

    return HttpResponseRedirect(target)


def oauth_authorize_callback_view(request):
    """Render a dummy callback page"""
    error = request.GET.get("error")
    code = request.GET.get("code")

    return render(request, "auth/oauth2/callback.html", {
        "error": error,
        "code": code,
    })

