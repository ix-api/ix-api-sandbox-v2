

from datetime import timedelta, datetime

from jea.statistics.generators.timeseries import (
    parse_delta,
    timeseries,
    eval_at,
)
from jea.statistics.generators.models import (
    port_traffic_octets,
    periodic_waveform,
)


def test_parse_delta():
    delta = parse_delta('30s')
    assert delta == timedelta(seconds=30)

    delta = parse_delta('5m')
    assert delta == timedelta(minutes=5)

    delta = parse_delta('6h')
    assert delta == timedelta(hours=6)

    delta = parse_delta('2d')
    assert delta == timedelta(days=2)


def test_timeseries():
    start = datetime(2023, 1, 1)
    resolution = parse_delta('5m')
    period = parse_delta('24h')

    model = port_traffic_octets(period, 10000)
    gen = timeseries(start, resolution, model)

    series = [next(gen) for _ in range(100)]
    print(series)


def test_finite_timeseries():
    """Test the timeseries with an end"""
    start = datetime(2023, 1, 1)
    resolution = parse_delta('5m')
    period = parse_delta('24h')
    end = start + period

    model = port_traffic_octets(period, 10000)
    gen = timeseries(start, resolution, model, end=end)

    series = list(gen) 
    print(series)


def test_eval_at():
    start = datetime(2023, 1, 1)
    period = parse_delta('24h')

    model = periodic_waveform(period, 0, 100)
    t = datetime(2023, 1, 10, 3)
    r0 = eval_at(start, t, model)

    model = periodic_waveform(period, 0, 100)
    t = datetime(2023, 1, 1, 12)
    r1 = eval_at(start, t, model)

    assert r0 < r1
