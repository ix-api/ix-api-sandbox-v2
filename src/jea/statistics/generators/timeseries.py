
from datetime import datetime, timedelta


def parse_delta(resolution):
    """
    Resolution is represented in as <number><unit>.
    Allowed units are s, m, h, d
    """
    unit = resolution[-1]
    value = int(resolution[:-1])
    if unit == 's':
        return timedelta(seconds=value)
    if unit == 'm':
        return timedelta(minutes=value)
    if unit == 'h':
        return timedelta(hours=value)
    if unit == 'd':
        return timedelta(days=value)

    raise ValueError("Invalid timedelta: {}".format(resolution))


def timeseries(start, delta, generator, end=None):
    """
    Generate a timeseries using a generator function
    for values. The generator function should scale
    the values to match the period.
    """
    t = start
    series = generator(delta)
    while True:
        yield (t, next(series))
        t += delta
        if end and t > end:
            break


def eval_at(t0, t, generator):
    """
    Evaluate a generator function at a specific time.
    """
    delta = t - t0
    series = generator(delta)
    next(series)
    return next(series)

