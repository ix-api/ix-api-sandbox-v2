"""
Higher order function for generating values.
"""
import random
from math import sin, pi
from datetime import timedelta


def combine(aggregate, *generators):
    """
    Combine multiple generators into a single generator
    that sums the values of all the generators.
    """
    def gen(delta):
        series = [g(delta) for g in generators]

        while True:
            yield aggregate(next(s) for s in series)

    return gen


def cap(minimum, maximum, generator):
    """Cap the values of a generator."""
    def gen(delta):
        series = generator(delta)
        while True:
            yield max(minimum, min(maximum, next(series)))

    return gen


def cast(target, generator):
    """Cast to a specific type, e.g. int"""
    def gen(delta):
        series = generator(delta)
        while True:
            yield target(next(series))

    return gen


def static_jitter(value, jitter):
    """Generate a series of values with static jitter."""
    def gen(_delta):
        while True:
            yield value + random.uniform(-jitter, jitter)

    return gen


def periodic_waveform(period: timedelta, minimum, maximum):
    """Generate a periodic waveform."""
    def gen(delta: timedelta):
        step = 0.0
        period_steps = 1.0
        if delta > timedelta(0):
            period_steps = period / delta

        while True:
            x = step / period_steps
            yield sin(x * pi) * (maximum - minimum) + minimum
            step += 1.0
            if step >= period_steps:
                step = 0.0

    return gen


def port_traffic_octets(period: timedelta, bandwidth: float):
    """
    Port Traffic is a periodic waveform, with a jitter
    """
    buffer = bandwidth * 0.005
    jitter_value = bandwidth * 0.001

    jitter = static_jitter(jitter_value, jitter_value) # > 0
    traffic = periodic_waveform(period, 100, bandwidth - buffer)

    return cap(0, bandwidth, combine(sum, traffic, jitter))

