
from datetime import timedelta

from jea.statistics.generators.models import (
    static_jitter,
    periodic_waveform,
)

def test_static_jitter():
    gen = static_jitter(3, 0.75)(None) # Resolution is irrelevant

    # Generate series
    series = [next(gen) for _ in range(100)]

    # Check if something in the series is 2.25 <= x <= 3.75
    assert any(2.25 <= x <= 3.75 for x in series)


def test_periodic_waveform():
    delta = timedelta(seconds=5)
    period = timedelta(seconds=100)
    gen = periodic_waveform(period, 0, 10000)(delta)

    # Generate series
    series = [next(gen) for _ in range(100)]
    print(series)
