import pytest
from model_bakery import baker

from jea.config.models import (
    Port,
    PortState,
    Connection,
)
from jea.statistics.services import (
    connection_statistics as connection_statistics_svc,
)


@pytest.mark.django_db
def test_connection_statistics():
    """test statistics for a LAG"""
    # Connection
    connection = baker.make(Connection)
    # Add ports
    baker.make(
        Port,
        speed=1000,
        operational_state=PortState.UP,
        connection=connection)
    baker.make(
        Port,
        speed=1000,
        operational_state=PortState.UP,
        connection=connection)
    baker.make(
        Port,
        speed=100000,
        operational_state=PortState.DOWN)


    # Get statistics
    stats = connection_statistics_svc.get_statistics(connection)
    assert stats["aggregates"].get("5m")
    assert (stats["aggregates"]["5m"]["average_ops_in"] / 1024 / 1024) <= 2000
    print(stats)

@pytest.mark.django_db
def test_connection_timeseries():
    """Test timeseries for a LAG"""
    # Connection
    connection = baker.make(Connection)
    # Add ports
    baker.make(
        Port,
        speed=1000,
        operational_state=PortState.UP,
        connection=connection)
    baker.make(
        Port,
        speed=1000,
        operational_state=PortState.UP,
        connection=connection)
    baker.make(
        Port,
        speed=100000,
        operational_state=PortState.DOWN)

    timeseries = connection_statistics_svc.get_timeseries(connection)
    print(timeseries)



