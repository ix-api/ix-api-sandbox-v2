from jea.config.models import (
    PortState,
)

from jea.statistics.services import (
    traffic as traffic_svc,
    port_statistics as port_statistics_svc,
)


def get_statistics(network_service):
    """Get statistics for a port"""
    name = getattr(network_service, "display_name", "") 
    title = "NetworkService {} 5m".format(name)

    # Get network service configs and see what bandwidth
    # was allocated
    configs = network_service.configs.all()
    speed = sum(config.capacity_allocated for config in configs)

    stats = traffic_svc.aggregate_statistics(
        title,
        resolution="5m",
        period="24h",
        speed=speed)

    return {
        "aggregates": {
            "5m": stats,
        }
    }


def get_timeseries(network_service):
    """Get the timeseries for a port"""
    name = getattr(network_service, "display_name", "") 
    title = "NetworkService {} 5m".format(name)

    # Get network service configs and see what bandwidth
    # was allocated
    configs = network_service.configs.all()
    speed = sum(config.capacity_allocated for config in configs)

    return traffic_svc.aggregate_timeseries(
        title,
        resolution="5m",
        period="24h",
        speed=speed,
        state=PortState.UP)



