
from datetime import datetime

from jea.statistics.generators.timeseries import (
    parse_delta,
    eval_at,
    timeseries,
)
from jea.statistics.generators.models import (
    port_traffic_octets,
    static_jitter,
)
from jea.config.models import (
    PortState,
)


def aggregate_statistics(
        title,
        resolution="5m",
        period="24h",
        speed=10000,
        state=PortState.UP,
    ):
    """
    Show the aggregate of a port that is up,
    which should include traffic.
    """
    title = "{} {}".format(title, resolution)
    resolution = parse_delta(resolution)

    period = parse_delta(period)

    speed = speed * 1024 * 1024

    model_in = port_traffic_octets(period, speed)
    model_out = port_traffic_octets(period, speed * 0.75)

    t0 = datetime.now().replace(
        hour=0, minute=0, second=0, microsecond=0)

    t = datetime.now()
    start = t - resolution

    ops_in = eval_at(t0, t, model_in)
    ops_out = eval_at(t0, t, model_out)

    pps_in = int(ops_in / 1000) # MTU 1500, avg??
    pps_out = int(ops_out / 1000)

    aggregate = {
        "title": title,
        "precision": int(resolution.total_seconds()),
        "accuracy": 1.0,
        "start": start,
        "end": t,
        "created_at": t,
        "next_update_at": t + resolution,
        "average_pps_in": pps_in,
        "average_pps_out": pps_out,
        "average_ops_in": ops_in,
        "average_ops_out": ops_out,
    }

    return aggregate



def aggregate_timeseries(
        title,
        resolution="5m",
        period="24h",
        speed=10000,
        state=PortState.UP,
    ):
    """Get the timeseries for a port"""
    title = "{} {} {}".format(title, resolution, period)

    resolution = parse_delta(resolution)
    period = parse_delta(period)

    now = datetime.now()
    start = now - period

    speed = speed * 1024 * 1024

    model_in = port_traffic_octets(period, speed)
    model_out = port_traffic_octets(period, speed * 0.75)

    samples_in = list(timeseries(start, resolution, model_in, end=now))
    samples_out = timeseries(start, resolution, model_out, end=now)

    ts = [o[0] for o in samples_in]

    if state == PortState.DOWN:
        octets_in = [None for o in samples_in]
        octets_out = [None for o in samples_out]

        packets_in = [None for o in samples_in]
        packets_out = [None for o in samples_out]
    else:
        octets_in = [int(o[1]) for o in samples_in]
        octets_out = [int(o[1]) for o in samples_out]

        packets_in = [int(o / 1200) for o in octets_in]
        packets_out = [int(o / 1200) for o in octets_out]

    samples = zip(ts, packets_in, packets_out, octets_in, octets_out)

    return {
        "title": title,
        "origin_timezone": "Europe/Amsterdam",
        "precision": int(resolution.total_seconds()),
        "created_at": now,
        "next_update_at": now + resolution,
        "samples": list(samples),
    }

