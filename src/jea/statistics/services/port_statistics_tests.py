
import pytest
from model_bakery import baker

from jea.config.models import (
    Port,
    PORT_STATE_UP,
    PORT_STATE_DOWN,
)
from jea.statistics.services import (
    port_statistics as port_statistics_svc,
)


@pytest.mark.django_db
def test_get_statistics():
    """Test get port statistics"""
    port = baker.make(
        Port,
        speed=10000,
        operational_state=PORT_STATE_UP)
    stats = port_statistics_svc.get_statistics(port)

    assert stats["aggregates"]["5m"]
    assert len(stats["light_levels_tx"]) > 0
    assert len(stats["light_levels_rx"]) > 0
    print(stats)


@pytest.mark.django_db
def test_get_statistics_port_down():
    """Test get port statistics for a down port"""
    port = baker.make(
        Port,
        speed=10000,
        operational_state=PORT_STATE_DOWN)
    stats = port_statistics_svc.get_statistics(port)

    assert stats["aggregates"].get("5m") == None


@pytest.mark.django_db
def test_get_timeseries():
    """Get timeseries for a port"""
    port = baker.make(
        Port,
        speed=10000,
        operational_state=PORT_STATE_UP)

    timeseries = port_statistics_svc.get_timeseries(port)
    print(timeseries)
    assert len(timeseries["samples"]) > 0
