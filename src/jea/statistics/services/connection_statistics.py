
from jea.config.models import (
    PortState,
)

from jea.statistics.services import (
    traffic as traffic_svc,
    port_statistics as port_statistics_svc,
)


def get_statistics(connection):
    """Get statistics for a port"""
    title = "Connection {} 5m".format(connection.name)
    port_stats = [
        port_statistics_svc.get_statistics(port)
        for port in connection.ports.all()
        if port.operational_state == PortState.UP]

    if not port_stats:
        return {
            "aggregates": {},
        }

    # Sum aggregates
    aggregates_5m = [s["aggregates"]["5m"] for s in port_stats]
    agg_5m = {
        **aggregates_5m[0],
        "title": title, 
        "average_pps_in": int(
            sum([a["average_pps_in"] for a in aggregates_5m])),
        "average_pps_out": int(
            sum([a["average_pps_out"] for a in aggregates_5m])),
        "average_ops_in": int(
            sum([a["average_ops_in"] for a in aggregates_5m])),
        "average_ops_out": int(
            sum([a["average_ops_out"] for a in aggregates_5m])),
    }
    return {
        "aggregates": {
            "5m": agg_5m,
        }
    }


def get_timeseries(connection):
    """Get the timeseries for a port"""
    ports = connection.ports.all()
    speed = sum(port.speed for port in ports)
    up = any(port.operational_state == PortState.UP
             for port in ports) 
    state = PortState.UP if up else PortState.DOWN

    title = "Connection {}".format(connection.name)
    return traffic_svc.aggregate_timeseries(
        title,
        resolution="5m",
        period="24h",
        speed=speed,
        state=state)


