from jea.config.models import (
    PortState,
)

from jea.statistics.services import (
    traffic as traffic_svc,
    port_statistics as port_statistics_svc,
)


def get_statistics(network_service_config):
    """Get statistics for a port"""
    network_service = network_service_config.network_service
    name = getattr(network_service, "display_name", "") 
    title = "NetworkServiceConfig {} 5m".format(name)
    speed = network_service_config.capacity_allocated

    stats = traffic_svc.aggregate_statistics(
        title,
        resolution="5m",
        period="24h",
        speed=speed)

    return {
        "aggregates": {
            "5m": stats,
        }
    }


def get_timeseries(network_service_config):
    """Get the timeseries for a port"""
    name = getattr(network_service_config.network_service, "display_name", "") 
    title = "NetworkServiceConfig {} 5m".format(name)

    # Get network service configs and see what bandwidth
    # was allocated
    speed = network_service_config.capacity_allocated

    return traffic_svc.aggregate_timeseries(
        title,
        resolution="5m",
        period="24h",
        speed=speed,
        state=PortState.UP)



