import pytest
from model_bakery import baker

from jea.service.models import (
    P2PNetworkService,
)
from jea.config.models import (
    Port,
    PortState,
    Connection,
    P2PNetworkServiceConfig,
)
from jea.statistics.services import (
    network_service_statistics as network_service_statistics_svc,
)


@pytest.mark.django_db
def test_network_service_get_statistics():
    """test statistics for a NetworkService"""
    # Connection
    connection = baker.make(Connection)
    baker.make(
        Port,
        speed=1000,
        operational_state=PortState.UP,
        connection=connection)

    service = baker.make(P2PNetworkService)

    # Get statistics - no configs present
    stats = network_service_statistics_svc.get_statistics(service)
    assert stats["aggregates"].get("5m")
    assert (stats["aggregates"]["5m"]["average_ops_in"]) == 0
    assert (stats["aggregates"]["5m"]["average_ops_out"]) == 0

    # Create config
    baker.make(
        P2PNetworkServiceConfig,
        network_service=service,
        connection=connection)

    # Get statistics
    stats = network_service_statistics_svc.get_statistics(service)
    assert stats["aggregates"].get("5m")
    assert (stats["aggregates"]["5m"]["average_ops_in"] / 1024 / 1024) <= 1000


@pytest.mark.django_db
def test_connection_get_timeseries():
    """Network Service Statistics Timeseries"""
    # Connection, Service and Config
    connection = baker.make(Connection)
    baker.make(
        Port,
        speed=1000,
        operational_state=PortState.UP,
        connection=connection)

    service = baker.make(P2PNetworkService)
    baker.make(
        P2PNetworkServiceConfig,
        network_service=service,
        connection=connection)

    timeseries = network_service_statistics_svc.get_timeseries(service)
    assert len(timeseries["samples"]) > 0


