
from datetime import datetime

from jea.statistics.generators.timeseries import (
    parse_delta,
    eval_at,
    timeseries,
)
from jea.statistics.generators.models import (
    port_traffic_octets,
    static_jitter,
)
from jea.statistics.services import (
    traffic as traffic_svc,
)
from jea.config.models import (
    PortState,
)


def port_aggregate(port):
    """Get the current aggregate for the port"""
    title = "Port {}".format(port.name)
    return traffic_svc.aggregate_statistics(
        title,
        resolution="5m",
        period="24h",
        speed=port.speed)


def light_levels(port):
    """Get current port light levels"""
    model_rx = static_jitter(-3.5, 1)(None)
    model_tx = static_jitter(-5.5, 1)(None)

    tx = next(model_tx)
    rx = next(model_rx)

    levels = {
        "light_levels_tx": [tx],
        "light_levels_rx": [rx],
    }

    return levels


def get_statistics(port):
    """Get statistics for a port"""
    resolution = "5m"
    if port.operational_state == PortState.DOWN:
        return {
            "aggregates": {},
            "light_levels_tx": [],
            "light_levels_rx": [],
        }

    return {
        "aggregates": {
            resolution: port_aggregate(port),
        },
        **light_levels(port),
    }


def get_timeseries(port):
    """Get the timeseries for a port"""
    title = "Port {}".format(port.name)
    return traffic_svc.aggregate_timeseries(
        title,
        resolution="5m",
        period="24h",
        speed=port.speed,
        state=port.operational_state)


