import logging

from django.apps import AppConfig

logger = logging.getLogger(__name__)

class ServiceConfig(AppConfig):
    name = 'jea.statistics'

    verbose_name = "JEA :: Statistics"

    def ready(self):
        """Application is ready"""
        logger.info("Initializing app: {}".format(self.name))

