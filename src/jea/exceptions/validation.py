
from rest_framework.exceptions import (
    APIException,
    ValidationError as RFValidationError,
)
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_409_CONFLICT,
)

from jea.exceptions.base import IxApiException


class ConditionAssertionError(IxApiException, APIException):
    """
    We can not fulfill the request, because the
    current state is in conflict with the desired next state.
    """
    status_code = HTTP_409_CONFLICT
    default_code = "assertion_error"
    default_detail = "The request could not be fulfilled."


class UnableToFulfillError(IxApiException, APIException):
    """
    We can not fulfill the request.
    """
    status_code = HTTP_400_BAD_REQUEST
    default_code = "unable_to_fulfill"
    default_detail = "The server cannot perform the requested task."


class ValidationError(IxApiException, RFValidationError):
    """
    An extended validation error including the field
    which caused the exception.
    """
    default_field = None

    def __init__(self, detail=None, code=None, field=None):
        """Initialize the exception"""
        super().__init__(detail=detail, code=code)
        if not field:
            field = self.default_field
        self.field = field

