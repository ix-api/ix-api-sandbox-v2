
from rest_framework.exceptions import APIException

class IxApiException(APIException):
    """IX-API Exception"""
    default_code = 'ixapi_error'
    default_detail = 'An error occured while processing your request.'
