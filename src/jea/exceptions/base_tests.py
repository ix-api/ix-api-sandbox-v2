
from jea.exceptions.base import IxApiException


def test_ixapi_exception__str():
    """Test the ixapi base exception."""
    exc = IxApiException("Test")
    assert str(exc) == "Test"
    assert exc.status_code == 500
    assert exc.detail == "Test"
