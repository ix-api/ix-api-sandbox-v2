
from jea.exceptions.validation import (
    ValidationError,
    ConditionAssertionError,
    UnableToFulfillError,
)

def test_validation_error_default_field():
    """Test default field behaviour of exception"""
    class Exc(ValidationError):
        default_field = "some field"

    exc = Exc()
    assert exc.field == "some field"


def test_condition_assertion_error():
    """Test assertion error"""
    exc = ConditionAssertionError()
    assert str(exc)
    print(str(exc))


def test_unable_to_fulfill_error():
    """Test assertion error"""
    exc = UnableToFulfillError()
    assert str(exc)
    print(str(exc))

