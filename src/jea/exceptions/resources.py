
import inspect

from django.db.models import Model
from rest_framework.exceptions import (
    PermissionDenied,
    NotFound,
)

from jea.exceptions.base import IxApiException


class ResourceNotFound(IxApiException, NotFound):
    """Resource Not Found"""


class ResourceAccessDenied(IxApiException, PermissionDenied):
    """
    Custom permission denied error, including the resource
    where the error occured.
    """
    def __init__(self, resource, pk=None):
        """Permission denied for resource"""
        if isinstance(resource, Model):
            resource_name = type(resource).__name__
            pk = resource.pk
        elif inspect.isclass(resource) and \
            issubclass(resource, Model):
            resource_name = resource.__name__
        else:
            resource_name = resource

        detail = (
            f"You do not have permission to access "
            f"'{resource_name}' with id={pk}"
        )

        super().__init__(detail)


