
import pytest
from datetime import datetime
from dateutil.relativedelta import relativedelta

from jea.exceptions.policy import CancellationPolicyError


def test_cancellation_policy():
    """Test cancellation policy."""
    period = relativedelta(months=1)
    date = datetime(2000, 5, 6)

    policy = {
        "decommission_at": date + period,
        "charged_until": date + period,
    }
    exc = CancellationPolicyError(policy=policy)

    assert(str(exc))
    print(str(exc))

def test_cancellation_policy__blank():
    """Test blank cancellation policy"""
    exc = CancellationPolicyError()
    assert(str(exc))


