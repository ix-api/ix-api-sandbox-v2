
from datetime import date

from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
)

from jea.exceptions.base import IxApiException


class CancellationPolicyError(IxApiException):
    """Cancellation policy failed"""
    status_code = HTTP_400_BAD_REQUEST
    default_code = "cancellation_policy_error"

    def __init__(self, detail=None, policy=None):
        """Initialize the exception"""
        if policy:
            self.decommission_at = policy["decommission_at"]
            self.charged_until = policy["charged_until"]
        else:
            self.decommission_at = date.min
            self.charged_until = date.min

        if not detail:
            detail = (
              f"The cancellation policy forbids cancellation before "
              f"{self.decommission_at}, it will be charged until "
              f"{self.charged_until}."
            )

        super().__init__(detail)
