
import pytest
from model_bakery import baker

from jea.crm.models import Account
from jea.config.models import NetworkServiceConfig
from jea.exceptions.resources import (
    ResourceNotFound,
    ResourceAccessDenied,
)


def test_resource_not_found():
    """Test ResourceNotFound"""
    exc = ResourceNotFound()
    assert str(exc)
    print(str(exc))
    assert(exc.status_code == 404)


def test_resource_access_denied__resource_name():
    """Test ResourceAccessDenied"""
    exc = ResourceAccessDenied("foo")
    assert str(exc)
    print(str(exc))
    assert(exc.status_code == 403)


def test_resource_access_denied__model_resource():
    """Test ResourceAccessDenied with model"""
    exc = ResourceAccessDenied(NetworkServiceConfig, 23)
    assert str(exc)
    print(str(exc))
    assert(exc.status_code == 403)


@pytest.mark.django_db
def test_resource_access_denied__model_instance():
    """Test access denied with model object"""
    obj = baker.make(Account)
    exc = ResourceAccessDenied(obj)
    assert str(exc)
    print(str(exc))
    assert(exc.status_code == 403)

