

"""
Filters for Access Models
"""

import django_filters
from django.db import models

from utils import filters
from utils.datastructures import must_list
from jea.crm import filters as crm_filters
from jea.stateful.filters import StatefulMixin
from jea.config.models import (
    NETWORK_FEATURE_CONFIG_MODELS,
    NETWORK_SERVICE_CONFIG_MODELS,
    Port,
    PortReservation,
    Connection,
    NetworkServiceConfig,
    NetworkFeatureConfig,
)


class PortReservationFilter(
        StatefulMixin,
        django_filters.FilterSet,
    ):
    """Filters for port-reservations"""
    id = filters.BulkIdFilter()

    class Meta:
        model = PortReservation

        fields = [
            "external_ref",
            "connection",
            "port",
        ]


class PortFilter(
        crm_filters.OwnableFilterMixin,
        StatefulMixin,
        django_filters.FilterSet,
    ):
    """Filters for Ports"""
    id = filters.BulkIdFilter()


    media_type = django_filters.CharFilter(
        field_name="media_type",
        lookup_expr="iexact",
        label="""
            Filter by media type (e.g. 10GBASE-LR")

            Example: 10GBASE-LR
        """)

    pop = django_filters.CharFilter(
        field_name="pop",
        label="""
            Filter by point of presence (PoP).

            Example: 23
        """)


    class Meta:
        model = Port

        fields = [
            "name",
            "external_ref",
            "speed",
            "connection",
        ]

        filter_overrides = {
            models.CharField: {
                'filter_class':
                    django_filters.CharFilter,
                'extra': lambda f: {
                    'lookup_expr': 'iexact',
                },
            },
        }


class ConnectionFilter(
        crm_filters.OwnableFilterMixin,
        StatefulMixin,
        django_filters.FilterSet,
    ):
    """Filters for Connections"""
    id = filters.BulkIdFilter()

    mode = django_filters.CharFilter(
        field_name="mode", lookup_expr="iexact")

    mode__is_not = django_filters.CharFilter(
        field_name="mode", lookup_expr="iexact", exclude=True)

    metro_area_network = django_filters.CharFilter(
        field_name="ports__device__pop__metro_area_network")

    port = django_filters.CharFilter(
        method="filter_ports")
    def filter_ports(self, queryset, _name, value):
        """Filter by ports"""
        port_ids = must_list(value)

        return queryset.filter(ports__in=port_ids)


    class Meta:
        model = Connection

        fields = [
            "name",
            "external_ref",
        ]

        filter_overrides = {
            models.CharField: {
                'filter_class':
                    django_filters.CharFilter,
                'extra': lambda f: {
                    'lookup_expr': 'iexact',
                },
            },
        }


class NetworkServiceConfigFilter(
        crm_filters.OwnableFilterMixin,
        StatefulMixin,
        django_filters.FilterSet,
    ):
    """Filter polymorphic network service configs"""
    id = filters.BulkIdFilter()

    service = django_filters.CharFilter(
        field_name="network_service")

    type = filters.PolymorphicModelTypeFilter(NETWORK_SERVICE_CONFIG_MODELS)

    outer_vlan = django_filters.CharFilter(
        method="filter_outer_vlan")
    def filter_outer_vlan(self, queryset, _name, value):
        """Filter by outer vlan number"""
        return queryset

    inner_vlan = django_filters.CharFilter(
        method="filter_inner_vlan")
    def filter_inner_vlan(self, queryset, _name, value):
        """Filter by inner vlan number"""
        return queryset


    class Meta:
        model = NetworkServiceConfig

        fields = [
            "capacity",
            "external_ref",
            "connection",
        ]

        filter_overrides = {
            models.CharField: {
                'filter_class':
                    django_filters.CharFilter,
                'extra': lambda f: {
                    'lookup_expr': 'iexact',
                },
            },
        }


class NetworkFeatureConfigFilter(
        crm_filters.OwnableFilterMixin,
        StatefulMixin,
        django_filters.FilterSet,
    ):
    """Filter polymorphic network service feature configs"""
    id = filters.BulkIdFilter()

    service_config = django_filters.CharFilter(
        field_name="network_service_config")

    type = filters.PolymorphicModelTypeFilter(NETWORK_FEATURE_CONFIG_MODELS)

    class Meta:
        model = NetworkFeatureConfig

        fields = [
            "network_feature",
            "purchase_order",
            "contract_ref",
            "external_ref",
            "managing_account",
            "consuming_account",
        ]

        filter_overrides = {
            models.CharField: {
                'filter_class':
                    django_filters.CharFilter,
                'extra': lambda f: {
                    'lookup_expr': 'iexact',
                },
            },
        }
