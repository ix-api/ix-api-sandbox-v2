"""
Change Creators
"""

from jea.stateful.models import (
    Change,
    Create,
    Update,
    StateUpdate,
    Decommission,
)


def create_network_service_config(data):
    """Create network service config request"""
    return Create(
        resource=Change.NETWORK_SERVICE_CONFIG,
        data=data)


def update_network_service_config(nsc, data, partial=True) -> Change:
    """Update the network service config request"""
    return Update(
        resource=Change.NETWORK_SERVICE_CONFIG,
        ref=nsc,
        partial=partial,
        data=data)


def decommission_network_service_config(nsc, decommission_at=None) -> Change:
    """Create decommission change"""
    return Decommission(
        resource=Change.NETWORK_SERVICE_CONFIG,
        decommission_at=decommission_at,
        ref=nsc)


def create_network_feature_config(data):
    """Create network feature config request"""
    return Create(
        resource=Change.NETWORK_FEATURE_CONFIG,
        data=data)


def update_network_feature_config(nfc, data, partial=False) -> Change:
    """Update the network feature config request"""
    return Update(
        resource=Change.NETWORK_FEATURE_CONFIG,
        ref=nfc,
        partial=partial,
        data=data)


def decommission_network_feature_config(nfc) -> Change:
    """Create decommission change"""
    return Decommission(
        resource=Change.NETWORK_FEATURE_CONFIG,
        ref=nfc)

