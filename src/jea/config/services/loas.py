
"""
LOA Service
"""


import pdfkit
from rest_framework.exceptions import NotFound
from django.utils import timezone
from django.db import transaction
from django.template import (
    loader as template_loader,
)

from jea.config.services import (
    connections as connections_svc,
)
from jea.catalog.models import (
    CrossConnectInitiator,
)
from jea.exceptions import (
    ResourceAccessDenied,
    ValidationError,
)

def generate_loa(
        port_reservation=None,
        connection=None,
        scoping_account=None,
    ):
    """
    Generate a LOA for a port reservation or connection.
    This only applies to subscriber initiated cross-connects.

    :param port_reservation: Use this port reservation
    :param connection: Use all port reservations from this connection
    :param scoping_account: scope limiting account
    """
    if connection:
        product_offering = connection.product_offering
    if port_reservation:
        product_offering = port_reservation.connection.product_offering
    if not product_offering:
        raise ValidationError("a port_reservation or connection is required")

    if product_offering.cross_connect_initiator == \
            CrossConnectInitiator.EXCHANGE:
        raise NotFound(
            "downloading LOAs only applies to subscriber"
            "initiated cross connects")

    if port_reservation:
        port_reservations = [port_reservation]
    else:
        port_reservations = connection.port_reservations.all()

    # Generate PDF
    tmpl = template_loader.get_template("loas/loa.html")
    html = tmpl.render({
        "port_reservations": port_reservations,
    })

    pdf = pdfkit.from_string(html, False)
    return pdf


@transaction.atomic
def process_loa(
        loa=None,
        port_reservation=None,
        connection=None,
        scoping_account=None,
    ):
    """
    Handle an incoming LoA.
    """
    # This does not do much for now.
    # But at least check we are dealing with a PDF file
    if not loa:
        raise ValidationError("a letter of authorization is missing")

    # Check magic header
    if loa[:4] != b"%PDF":
        raise ValidationError("uploaded document is not a PDF")

    if connection:
        return _process_loa_connection(connection)

    if port_reservation:
        return _process_loa_port_reservation(port_reservation)

    raise ValidationError("a port reservation or connection is required")


def _process_loa_port_reservation(reservation):
    """
    Assign a LOA to a single port reservation
    """
    reservation.loa_received_at = timezone.now()
    reservation.save()


def _process_loa_connection(connection):
    """
    Assign a LOA to all port reservations in a connection.
    """
    for reservation in connection.port_reservations.all():
        _process_loa_port_reservation(reservation)

