"""
Configs Service
---------------

Configure service accesses and features like
route servers and blackholing.
"""

import secrets
import random
from typing import List

from django.db import transaction
from ixapi_schema.v2.constants.config import (
    VLanEthertype,

    NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
    NETWORK_SERVICE_CONFIG_TYPE_P2P,
    NETWORK_SERVICE_CONFIG_TYPE_P2MP,
    NETWORK_SERVICE_CONFIG_TYPE_MP2MP,
    NETWORK_SERVICE_CONFIG_TYPE_CLOUD,

    VLAN_CONFIG_TYPE_PORT,
    VLAN_CONFIG_TYPE_DOT1Q,
    VLAN_CONFIG_TYPE_QINQ,
)
from ixapi_schema.v2.constants.catalog import (
    ProviderVlanTypes,
)

from jea.exceptions import (
    ResourceAccessDenied,
    ValidationError,
)
from jea.config.exceptions import (
    SessionModeInvalid,
)
from jea.ipam import models as ipam_models
from jea.ipam.models import AddressFamilies
from jea.stateful.models import State
from jea.service import models as service_models
from jea.service.models import (
    DenyMemberJoiningRule,
)
from jea.crm.services import (
    accounts as accounts_svc,
    contacts as contacts_svc,
)
from jea.ipam.services import (
    ip_addresses as ip_addresses_svc,
    mac_addresses as mac_addresses_svc,
)
from jea.service.services import (
    network as network_svc,
    membership as membership_svc,
)
from jea.config.services import (
    connections as connections_svc,
)
from jea.config import (
    models as config_models,
    filters as access_filters,
)
from jea.catalog.services import products as products_svc
from jea.config.models import (
    RouteServerSessionMode,
    VLanConfig,
    PortVLanConfig,
    QinQVLanConfig,
    Dot1QVLanConfig,
)
from utils.datastructures import filter_fields


def assert_network_service_membership(
        account=None,
        network_service=None):
    """
    Check if the account can join a private vlan.

    :param account: A account
    :param network_service: A network service

    :raises ResourceAccessDenied: when account has no joining rule
    """
    if account == network_service.consuming_account:
        return  # We are allowed to join our own service

    # In all other cases, we have to check if there is a rule
    # in place.
    joining_rules = membership_svc.get_member_joining_rules(
        network_service=network_service)
    membership = joining_rules.filter(
        consuming_account=account).first()
    if not membership:
        raise ResourceAccessDenied(
            network_service,
            details=(
                "The consuming account requires a "
                "`member-joining-rule` to access "
                "the network-service."))

    # Check if the account is on a reject list
    if isinstance(membership, DenyMemberJoiningRule):
        raise ResourceAccessDenied(
            network_service,
            details=(
                "The consuming account was rejected by a "
                "`member-joining-rule`."))


def assert_cloud_vlan_constraints(
        scoping_account=None,
        network_service=None,
        consuming_account=None,
        handover=None,
        cloud_vlan=None):
    """
    The handover, cloud_vlan tuple must be unique.
    Also the cloud_vlan has to be null, when the corresponding
    network service product offering offsers 'single' provider_vlans.

    :raises ValidationError: when the constraints are not fulfilled.

    :param network_service: A cloud network service
    :param scoping_account: A scope limiting account
    :param consuming_account: The account consuming the network
        service.
    :param cloud_vlan: A cloud vlan id (might be null)
    :param handover: The handover index
    """
    network_service = network_svc.get_network_service(
        scoping_account=scoping_account,
        network_service=network_service)

    # Product offering provider vlan constraint
    provider_vlans = network_service.product_offering.provider_vlans

    if provider_vlans == ProviderVlanTypes.SINGLE \
            and cloud_vlan is not None:
        raise ValidationError(
            ("The cloud_vlan for provider_vlans 'SINGLE' "
             "must be null."),
            field="cloud_vlan")

    # Uniqueness of cloud vlan and handover within the
    # scope of configs, used by the consuming account.
    configs = network_service.configs.filter(
        consuming_account=consuming_account)
    configured_handover_cloud_vlans = (
        (c["handover"], c["cloud_vlan"]) for c in configs)
    if (handover, cloud_vlan) in configured_handover_cloud_vlans:
        raise ValidationError(
            "cloud vlan for handover already in use",
            field="cloud_vlan")


def assert_cloud_handover_constraints(
        scoping_account=None,
        network_service=None,
        consuming_account=None,
        handover=1):
    """
    Assert uniqueness constraint on handovers cloud network service.
    Also the handover must be > 0.

    Only one network service configuration for each handover and
    cloud_vlan can be created.

    :raises ValidationError: when a constraint is not fulfilled.

    :param network_service: A cloud network service
    :param consuming_account: The account consuming the
        cloud network service
    :param scoping_account: A scope limiting account
    :param handover: The handover index
    """
    if handover < 1:
        raise ValidationError(
            "Invalid handover index. Must be >= 1.",
            field="handover")

    if handover > network_service.diversity:
        raise ValidationError(
            "Handover index is unexpected by network "
            "service diversity",
            field="handover")

    # Uniqueness check:
    #
    # Get related configs on this network service configured
    # for the account using the connection.
    configs = network_service.configs.filter(
        consuming_account=consuming_account)

    if handover in (c.handover for c in configs):
        raise ValidationError(
            "Handover already configured.",
            field="handover")


def assert_cloud_connection_uniqueness(
        scoping_account=None,
        consuming_account=None,
        network_service=None,
        connection=None):
    """
    Assert the connection is not already present in a
    configuration for the cloud network service.

    :param network_service: A cloud network service
    :param consuming_account: The account consuming the
        cloud network service
    :param connection: The connection to be used
    :param scoping_account: A scope limiting account
    """
    # Get related configs on this network service configured
    # for the account.
    configs = network_service.configs.filter(
        consuming_account=consuming_account)
    connection = connections_svc.get_connection(
        scoping_account=scoping_account,
        connection=connection)

    # The number of unique connections should be the same
    # as the required diversity.
    configured_connections = (c.connection_id for c in configs)
    if connection.pk in configured_connections:
        raise ValidationError(
            "The connection is already in use",
            field="connection")


def _create_vlan_config(
        scoping_account=None,
        connection=None,
        vlan_config_request=None) -> VLanConfig:
    """
    Create a vlan configuration for a network service config

    :param scoping_account: A scoping account
    :param vlan_config_request: A vlan configuration request
    :param connection: The connection the network service
        configuration is using.
    """
    vlan_type = vlan_config_request["vlan_type"]
    if vlan_type == VLAN_CONFIG_TYPE_PORT:
        return _create_vlan_config_port(
            scoping_account=scoping_account,
            vlan_config_request=vlan_config_request,
            connection=connection)

    if vlan_type == VLAN_CONFIG_TYPE_DOT1Q:
        return _create_vlan_config_dot1q(
            scoping_account=scoping_account,
            vlan_config_request=vlan_config_request,
            connection=connection)

    if vlan_type == VLAN_CONFIG_TYPE_QINQ:
        return _create_vlan_config_qinq(
            scoping_account=scoping_account,
            vlan_config_request=vlan_config_request,
            connection=connection)

    raise ValidationError(
        "Invalid or unknown vlan type",
        field="vlan_config.vlan_type")


def _create_vlan_config_port(
        scoping_account=None,
        connection=None,
        vlan_config_request=None) -> PortVLanConfig:
    """
    Create a new port vlan config

    :param scoping_account: A scoping account
    :param connection: The connection the network service
        configuration is using.
    :param vlan_config_request: VLanConfig request
    """
    # Check if there is another network service config using
    # a port config or if there are other vlan based bindings
    # configured on the connection.
    vlan_configs = connections_svc.get_vlan_configs(
        scoping_account=scoping_account,
        connection=connection)
    port_vlan_configs = [c for c in vlan_configs
                         if isinstance(c, PortVLanConfig)]
    if port_vlan_configs:
        raise ValidationError(
            "A port based vlan config is already present",
            field="connection")

    # Create new port based connection binding
    config = PortVLanConfig()
    config.save()

    return config


def _create_vlan_config_dot1q(
        scoping_account=None,
        connection=None,
        vlan_config_request=None) -> Dot1QVLanConfig:
    """
    Create a new Dot1Q vlan configuration.

    :param scoping_account: A scoping account
    :param connection: The connection the network service
        configuration is using.
    :param vlan_config_request: VLanConfig data
    """
    vlan = vlan_config_request.get("vlan", random.randint(300, 600))

    # Check constraints
    connections_svc.assert_outer_vlan_uniqueness(
        outer_vlan=vlan,
        connection=connection,
        scoping_account=scoping_account)

    # Create vlan configuration
    config = Dot1QVLanConfig(vlan=vlan)
    config.save()

    return config


def _create_vlan_config_qinq(
        scoping_account=None,
        connection=None,
        vlan_config_request=None) -> QinQVLanConfig:
    """
    Create a new QinQ vlan config

    :param scoping_account: A scoping account
    :param connection: The connection the network service
        configuration is using.
    :param vlan_config_request: VLanConfig data
    """
    outer_vlan_ethertype = vlan_config_request.get(
        "outer_vlan_ethertype", "0x8100")
    outer_vlan_ethertype = VLanEthertype(outer_vlan_ethertype)
    outer_vlan = vlan_config_request.get(
        "outer_vlan", random.randint(300, 600))
    inner_vlan = vlan_config_request["inner_vlan"]

    # Check constraints
    connections_svc.assert_outer_vlan_uniqueness(
        outer_vlan=outer_vlan,
        connection=connection,
        scoping_account=scoping_account)

    # Create new vlan configuration
    config = QinQVLanConfig(
        outer_vlan_ethertype=outer_vlan_ethertype,
        outer_vlan=outer_vlan,
        inner_vlan=inner_vlan,
    )
    config.save()

    return config


@transaction.atomic
def _update_vlan_config(
        scoping_account=None,
        network_service_config=None,
        vlan_config_update=None):
    """
    Update a network service config's vlan configuration.

    :param scoping_account: A scoping account
    :param network_service_config: A network service configuration
    :param vlan_config_update: A vlan config update.
    """
    connection = network_service_config.connection
    vlan_config = network_service_config.vlan_config
    vlan_type = vlan_config_update["vlan_type"]

    update_fields = vlan_config_update.keys()

    # The network service was acidentally created without
    # a vlan config. In practice this should never happen,
    # however the model tests produce these because
    # the field is null.
    if not vlan_config:
        return _create_vlan_config(
            scoping_account=scoping_account,
            connection=connection,
            vlan_config_request=vlan_config_update)

    # Type Change
    if "vlan_type" in update_fields and \
            vlan_config.__polymorphic_type__ != vlan_type:

        # We just replace the vlan config with a fresh one.
        # Remove vlan config first, so we can check our constraints
        # when creating a new one.
        network_service_config.vlan_config = None
        network_service_config.save()
        vlan_config.delete()

        vlan_config = _create_vlan_config(
            scoping_account=scoping_account,
            connection=connection,
            vlan_config_request=vlan_config_update)

        return vlan_config

    # Otherwise we update the fields - if present
    if "vlan" in update_fields and \
            isinstance(vlan_config, Dot1QVLanConfig):
        vlan_config.vlan = vlan_config_update["vlan"]
    if "inner_vlan" in update_fields and \
            isinstance(vlan_config, QinQVLanConfig):
        vlan_config.inner_vlan = vlan_config_update["inner_vlan"]
    if "outer_vlan" in update_fields and \
            isinstance(vlan_config, QinQVLanConfig):
        vlan_config.outer_vlan = vlan_config_update["outer_vlan"]
    if "outer_vlan_ethertype" in update_fields and \
            isinstance(vlan_config, QinQVLanConfig):
        vlan_config.outer_vlan_ethertype = \
            vlan_config_update["outer_vlan_ethertype"]

    vlan_config.save()
    return vlan_config


@transaction.atomic
def assign_network_feature_role_assignments(
        scoping_account=None,
        network_feature_config=None,
        role_assignments=None):
    """
    Assign contact role assignments to a config object.
    Providing a list of required contacts, will assert the presence
    of at least one of the required contact types.

    :param network_feature_config: A network feature configuration,
        where the contacts can be assigned to.
    :param scoping_account: Override config managing account
        property when checking the ownership of the contact.
    :param role_assignments: A list of contact role assignments.

    :raises RequiredContactTypesMissing:
    :raises ValidationError: In case a config object is missing
    """
    # Apply permission check and resovlve object if required.
    config = get_network_feature_config(
        scoping_account=scoping_account,
        network_feature_config=network_feature_config)

    # Now, do we have our config?
    if not config:
        raise ValidationError("A config object is required")

    if not scoping_account:
        scoping_account = config.scoping_account

    if not config.network_feature:
        raise ValidationError(
            "The config requires a network_feature.",
            field="network_service")

    # Load all contacts
    role_assignments = [
        contacts_svc.get_role_assignment(
            role_assignment=assignment,
            scoping_account=scoping_account)
        for assignment in role_assignments or []]
    # Check the required contacts are included
    required_roles = config.network_feature \
        .nfc_required_contact_roles \
        .all()
    contacts_svc.assert_presence_of_contact_roles(
        required_contact_roles=required_roles,
        role_assignments=role_assignments)

    # Set contact role assignments
    config.role_assignments.set(role_assignments)

    return config


@transaction.atomic
def assign_network_service_role_assignments(
        scoping_account=None,
        network_service_config=None,
        role_assignments=None):
    """
    Assign contact role assignments to a config object.
    Providing a list of required contacts, will assert the presence
    of at least one of the required contact types.

    :param network_service_config: A network service configuration,
        the implementation contacts can be assigned to.
    :param scoping_account: Override config managing account
        property when checking the ownership of the contact.
    :param contacts: A list of contact ids or contact objects.

    :raises RequiredContactTypesMissing:
    :raises ValidationError: In case a config object is missing
    """
    # Apply permission check and resovlve object if required.
    config = get_network_service_config(
        scoping_account=scoping_account,
        network_service_config=network_service_config)

    # Now, do we have our config?
    if not config:
        raise ValidationError("A config object is required")

    if not scoping_account:
        scoping_account = config.scoping_account

    if not config.network_service:
        raise ValidationError(
            "The config requires a network_service.",
            field="network_service")

    # Load all role assignments
    role_assignments = role_assignments or []
    role_assignments = [
        contacts_svc.get_role_assignment(
            role_assignment=ra,
            scoping_account=scoping_account)
        for ra in role_assignments]

    # Check the required contacts are included
    required_roles = config.network_service \
        .nsc_required_contact_roles \
        .all()
    contacts_svc.assert_presence_of_contact_roles(
        required_contact_roles=required_roles,
        role_assignments=role_assignments,
        scoping_account=scoping_account)

    # Set contact role assignments
    config.role_assignments.set(role_assignments)

    return config


@transaction.atomic
def assign_mac_addresses(
        scoping_account=None,
        network_service_config=None,
        mac_addresses=None):
    """
    Assign mac address to supported service configurations.
    Currently the only supported config types are:

      - ExchangeLanNetworkServiceConfig
      - MP2MPNetworkServiceConfig

    :param network_service_config: The network service config to
        assign macs to
    :param mac_addresses: A list of mac address objects.
    """
    if not hasattr(network_service_config, "mac_addresses"):
        raise TypeError("Config object does not have mac_addresses")

    if not scoping_account:
        scoping_account = network_service_config.scoping_account

    # Load with permission check
    mac_addresses = [
        mac_addresses_svc.get_mac_address(
            scoping_account=scoping_account,
            mac_address=mac)
        for mac in mac_addresses or []]

    # Assign mac addresses
    network_service_config.mac_addresses.set(mac_addresses)

    return network_service_config


def get_network_service_configs(
        scoping_account=None,
        filters=None):
    """
    Get all service configurations within a account context.

    :param scoping_account: Limit the scope to the managing account.
    :param filters: A dict of filter params
    """
    configs = access_filters.NetworkServiceConfigFilter(filters).qs

    # Apply ownership filtering
    if scoping_account:
        configs = configs.filter(scoping_account=scoping_account)

    return configs


def get_network_service_config(
        scoping_account=None,
        network_service_config=None):
    """
    Get a single network service configuration.
    If billing or owning account are present, perform an
    ownership test.

    :param config: The config to load. Might be an ID.
    :param managing_account: The billing account
    :param consuming_account: The owning account of the config.
    """
    if isinstance(
            network_service_config,
            config_models.NetworkServiceConfig):
        # Just perform an ownership test
        config_scoping_account = network_service_config.scoping_account
        if scoping_account and not \
                config_scoping_account == scoping_account:
            raise ResourceAccessDenied(network_service_config)

        return network_service_config

    # Otherwise get configs queryset:
    configs = get_network_service_configs(
        scoping_account=scoping_account)

    return configs.get(pk=network_service_config)


@transaction.atomic
def create_network_service_config(
        scoping_account=None,
        network_service_config_request=None,
        ) -> config_models.NetworkServiceConfig:
    """
    Create a new network service config.

    :param scoping_account: The managing account
    :param network_service_config_request:
        The configuration request
    """
    # Load networkservice and connection and check access permissions.
    network_service = network_svc.get_network_service(
        scoping_account=scoping_account,
        network_service=network_service_config_request["network_service"])
    connection = connections_svc.get_connection(
        scoping_account=scoping_account,
        connection=network_service_config_request["connection"])

    # Managing, consuming and billing account
    managing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_service_config_request["managing_account"])
    consuming_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_service_config_request["consuming_account"])
    billing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_service_config_request["billing_account"])

    # Make sure, that the billing account can be used for billing
    accounts_svc.assert_billing_information(billing_account)

    # Dispatch network service configuration creation:
    config_type = network_service_config_request["type"]

    # Prepare vlan config
    vlan_config = _create_vlan_config(
        scoping_account=scoping_account,
        connection=connection,
        vlan_config_request=network_service_config_request["vlan_config"])

    # ExchangeLan
    if config_type == NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN:
        config = _create_exchange_lan_network_service_config(
            scoping_account=scoping_account,
            managing_account=managing_account,
            consuming_account=consuming_account,
            billing_account=billing_account,
            network_service=network_service,
            connection=connection,
            vlan_config=vlan_config,
            network_service_config_request=network_service_config_request)
    # ELine
    elif config_type == NETWORK_SERVICE_CONFIG_TYPE_P2P:
        config = _create_p2p_network_service_config(
            scoping_account=scoping_account,
            managing_account=managing_account,
            consuming_account=consuming_account,
            billing_account=billing_account,
            network_service=network_service,
            connection=connection,
            vlan_config=vlan_config,
            network_service_config_request=network_service_config_request)
    # ELan
    elif config_type == NETWORK_SERVICE_CONFIG_TYPE_MP2MP:
        config = _create_mp2mp_network_service_config(
            scoping_account=scoping_account,
            managing_account=managing_account,
            consuming_account=consuming_account,
            billing_account=billing_account,
            network_service=network_service,
            connection=connection,
            vlan_config=vlan_config,
            network_service_config_request=network_service_config_request)
    # ETree
    elif config_type == NETWORK_SERVICE_CONFIG_TYPE_P2MP:
        config = _create_p2mp_network_service_config(
            scoping_account=scoping_account,
            managing_account=managing_account,
            consuming_account=consuming_account,
            billing_account=billing_account,
            network_service=network_service,
            connection=connection,
            vlan_config=vlan_config,
            network_service_config_request=network_service_config_request)
    # Cloud
    elif config_type == NETWORK_SERVICE_CONFIG_TYPE_CLOUD:
        config = _create_cloud_network_service_config(
            scoping_account=scoping_account,
            managing_account=managing_account,
            consuming_account=consuming_account,
            billing_account=billing_account,
            network_service=network_service,
            connection=connection,
            vlan_config=vlan_config,
            network_service_config_request=network_service_config_request)
    else:
        raise ValidationError("Unknown network service: {}".format(
            network_service_config_request["type"]))

    # Assign many to many contact roles
    role_assignments = network_service_config_request.get(
        "role_assignments", [])
    assign_network_service_role_assignments(
        scoping_account=scoping_account,
        network_service_config=config,
        role_assignments=role_assignments)

    return config


def _create_exchange_lan_network_service_config(
        scoping_account=None,
        managing_account=None,
        consuming_account=None,
        billing_account=None,
        network_service=None,
        connection=None,
        vlan_config=None,
        network_service_config_request=None):
    """
    Create a new config for the exchange lan network service.

    :param network_service: The network service to configure
    :param connection: The connection to configure for the network service
    :param vlan_config: A vlan configuration
    :param scoping_account: The owning account of the config.
    :param billing_account: The account paying for the config
    :param managing_account: A validated managing account
    :param consuming_account: A validated consuming account
    :param network_service_config_request: The configuration data
    """
    # Check if the service type is correct
    if not isinstance(network_service,
                      service_models.ExchangeLanNetworkService):
        raise ValidationError(
            ("Service type must match config type. "
             "{} given, expected {}").format(
                 network_service.__class__, "ExchangeLanNetworkService"))

    product = network_service_config_request["product_offering"]
    if product.exchange_lan_network_service != network_service:
        raise ValidationError(
            ("Product must match exchange lan to configure. "
             "{} given, expected {}").format(
                 network_service.__class__, "ExchangeLanNetworkService"))

    # Assert constraints
    capacity = network_service_config_request["capacity"]
    products_svc.assert_capacity_constraints(
        scoping_account=scoping_account,
        product_offering=product,
        capacity=capacity)

    # Create the Exchange Lan Network Access Configuration
    config = config_models.ExchangeLanNetworkServiceConfig(
        state=State.REQUESTED,
        asns=network_service_config_request["asns"],
        vlan_config=vlan_config,
        capacity=capacity,
        purchase_order=network_service_config_request["purchase_order"],
        external_ref=network_service_config_request["external_ref"],
        contract_ref=network_service_config_request["contract_ref"],
        network_service=network_service,
        scoping_account=scoping_account,
        consuming_account=consuming_account,
        managing_account=managing_account,
        billing_account=billing_account,
        connection=connection,
        product_offering=product)
    config.save()

    # Assign mac address(es)
    mac_addresses = network_service_config_request.get(
        "mac_addresses", [])
    if mac_addresses:
        assign_mac_addresses(
            scoping_account=scoping_account,
            network_service_config=config,
            mac_addresses=mac_addresses)

    # Allocate ip addresses
    ip_addresses_svc.allocate_ip_address(
        scoping_account=scoping_account,
        network_service_config=config,
        version=4)
    if network_service.ip_addresses.filter(version=6):
        ip_addresses_svc.allocate_ip_address(
            scoping_account=scoping_account,
            network_service_config=config,
            version=6)

    return config


def _create_p2p_network_service_config(
        scoping_account=None,
        managing_account=None,
        consuming_account=None,
        billing_account=None,
        network_service=None,
        connection=None,
        vlan_config=None,
        network_service_config_request=None):
    """
    Create a new config for an eline / p2p network service.

    :param managing_account: The billing account
    :param consuming_account: The owning account of the config.
    :param network_servie: The network service to configure
    :param billing_account: The account paying for the eline config
    :param connection: The connection to configure for the network service
    :param vlan_config: The vlan configuration
    :param network_service_config_request: The configuration data
    """
    # Check if the service type is correct
    if not isinstance(network_service, service_models.P2PNetworkService):
        raise ValidationError(
            ("Service type must match config type. "
             "{} given, expected {}").format(
                 network_service.__class__.__name__,
                 "P2PNetworkService"))

    # Membership check
    if consuming_account not in (
            network_service.joining_member_account,
            network_service.consuming_account):
        raise ResourceAccessDenied(
            network_service,
            details=("the consuming account is not allowed "
                     "to join this service"))

    # Only allow for one membership per account
    if network_service.configs.filter(
            consuming_account=consuming_account).exists():
        raise ValidationError(
            "Only one eline network service config per account",
            field="network_service")

    # Only allow for two network service configs on an eline
    if network_service.configs.count() > 2:
        raise ValidationError(
            "There can only be two configs on an eline",
            field="network_service")

    # Assert constraints
    capacity = network_service_config_request.get("capacity")
    products_svc.assert_capacity_constraints(
        scoping_account=scoping_account,
        product_offering=network_service.product_offering,
        capacity=capacity)

    # Create the eline config
    config = config_models.P2PNetworkServiceConfig(
        state=State.REQUESTED,
        capacity=capacity,
        purchase_order=network_service_config_request.get("purchase_order"),
        external_ref=network_service_config_request.get("external_ref"),
        contract_ref=network_service_config_request.get("contract_ref"),
        network_service=network_service,
        scoping_account=scoping_account,
        consuming_account=consuming_account,
        managing_account=managing_account,
        billing_account=billing_account,
        vlan_config=vlan_config,
        connection=connection)
    config.save()

    return config


def _create_mp2mp_network_service_config(
        scoping_account=None,
        managing_account=None,
        consuming_account=None,
        billing_account=None,
        network_service=None,
        connection=None,
        vlan_config=None,
        network_service_config_request=None):
    """
    Create a new config for an ELan service.
    This is pretty much the same as with ELines, however we support
    the assignment of mac addresses.

    :param managing_account: The billing account
    :param consuming_account: The owning account of the config.
    :param billing_account: The account paying the bills
    :param network_servie: The network service to configure
    :param connection: The connection to configure for the network service
    :param vlan_config: The vlan configuration
    :param network_service_config_request: The configuration data
    """
    # Check if the service type is correct
    if not isinstance(network_service, service_models.MP2MPNetworkService):
        raise ValidationError(
            "Service type must match config type. {} given, expected {}" \
                .format(network_service.__class__, "MP2MPNetworkService"))

    # Membership check
    assert_network_service_membership(
        account=consuming_account,
        network_service=network_service)

    # Assert constraints
    capacity = network_service_config_request.get("capacity")
    products_svc.assert_capacity_constraints(
        scoping_account=scoping_account,
        product_offering=network_service.product_offering,
        capacity=capacity,
    )

    # Create the eline config
    config = config_models.MP2MPNetworkServiceConfig(
        state=State.REQUESTED,
        capacity=capacity,
        purchase_order=network_service_config_request.get("purchase_order"),
        external_ref=network_service_config_request.get("external_ref"),
        contract_ref=network_service_config_request.get("contract_ref"),
        network_service=network_service,
        scoping_account=scoping_account,
        consuming_account=consuming_account,
        managing_account=managing_account,
        billing_account=billing_account,
        vlan_config=vlan_config,
        connection=connection)
    config.save()

    # Assign mac address(es)
    mac_addresses = network_service_config_request.get("mac_addresses", [])
    if mac_addresses:
        assign_mac_addresses(
            scoping_account=scoping_account,
            network_service_config=config,
            mac_addresses=mac_addresses)

    return config


def _create_p2mp_network_service_config(
        scoping_account=None,
        managing_account=None,
        consuming_account=None,
        billing_account=None,
        network_service=None,
        connection=None,
        vlan_config=None,
        network_service_config_request=None):
    """
    Create a new config for an ETree service.

    :param managing_account: The billing account
    :param consuming_account: The owning account of the config.
    :param billing_account: The billing account
    :param network_servie: The network service to configure
    :param connection: The connection to configure for the network service
    :param vlan_config: The prepared vlan configuration for the service config
    :param network_service_config_request: The configuration data
    """
    # Check if the service type is correct
    if not isinstance(network_service, service_models.P2MPNetworkService):
        raise ValidationError(
            "Service type must match config type. {} given, expected {}" \
                .format(network_service.__class__, "MP2MPNetworkService"))

    # Assert constraints
    capacity = network_service_config_request.get("capacity")
    products_svc.assert_capacity_constraints(
        scoping_account=scoping_account,
        product_offering=network_service.product_offering,
        capacity=capacity,
    )

    assert_network_service_membership(
        account=consuming_account,
        network_service=network_service)

    # Create the P2MP / ETree config
    config = config_models.P2MPNetworkServiceConfig(
        state=State.REQUESTED,
        capacity=capacity,
        purchase_order=network_service_config_request.get("purchase_order"),
        external_ref=network_service_config_request.get("external_ref"),
        contract_ref=network_service_config_request.get("contract_ref"),
        role=network_service_config_request["role"],
        network_service=network_service,
        scoping_account=scoping_account,
        consuming_account=consuming_account,
        managing_account=managing_account,
        billing_account=billing_account,
        vlan_config=vlan_config,
        connection=connection)
    config.save()

    # Assign mac address(es)
    mac_addresses = network_service_config_request.get("mac_addresses", [])
    if mac_addresses:
        assign_mac_addresses(
            scoping_account=scoping_account,
            network_service_config=config,
            mac_addresses=mac_addresses)

    return config


def _create_cloud_network_service_config(
        scoping_account=None,
        managing_account=None,
        consuming_account=None,
        billing_account=None,
        network_service=None,
        connection=None,
        vlan_config=None,
        network_service_config_request=None):
    """
    Create a new config for a cloud network service.

    :param managing_account: The billing account
    :param consuming_account: The owning account of the config.
    :param billing_account: The billing account
    :param network_service: The network service to configure
    :param connection: The connection to configure for the network service
    :param vlan_config: The prepared vlan configuration for the service config
    :param network_service_config_request: The configuration data
    """
    # Check if the service type is correct
    if not isinstance(network_service, service_models.CloudNetworkService):
        raise ValidationError(
            "Service type must match config type. {} given, expected {}" \
                .format(network_service.__class__, "CloudNetworkService"))

    # Cloud configuration
    capacity = network_service_config_request.get("capacity")
    cloud_vlan = network_service_config_request["cloud_vlan"]
    handover = network_service_config_request["handover"]

    # Assert constraints
    products_svc.assert_capacity_constraints(
        scoping_account=scoping_account,
        product_offering=network_service.product_offering,
        capacity=capacity)

    assert_cloud_handover_constraints(
        scoping_account=scoping_account,
        consuming_account=consuming_account,
        network_service=network_service,
        handover=handover)

    # Create cloud network configuration
    config = config_models.CloudNetworkServiceConfig(
        state=State.REQUESTED,
        purchase_order=network_service_config_request.get("purchase_order"),
        external_ref=network_service_config_request.get("external_ref"),
        contract_ref=network_service_config_request.get("contract_ref"),
        network_service=network_service,
        scoping_account=scoping_account,
        consuming_account=consuming_account,
        managing_account=managing_account,
        billing_account=billing_account,
        capacity=capacity,
        vlan_config=vlan_config,
        connection=connection,
        cloud_vlan=cloud_vlan,
        handover=handover)
    config.save()

    return config


@transaction.atomic
def update_network_service_config(
        scoping_account=None,
        network_service_config=None,
        network_service_config_update=None):
    """
    We allow for updates on the network service config
    objects. However, how much is updated depends on the
    type of network service configuration.

    :param scoping_account: A account managing the
        network service config.
    :param network_service_config: The network service
        configuration to update.
    :param network_service_config_update: A dict with config updates.
    """
    update_fields = network_service_config_update.keys()
    network_service_config = get_network_service_config(
        scoping_account=scoping_account,
        network_service_config=network_service_config)

    # Update common fields and properties
    # Accounts
    if "managing_account" in update_fields:
        managing_account = accounts_svc.get_account(
            account=network_service_config_update["managing_account"],
            scoping_account=scoping_account)
        network_service_config.managing_account = managing_account

    if "consuming_account" in update_fields:
        consuming_account = accounts_svc.get_account(
            account=network_service_config_update["consuming_account"],
            scoping_account=scoping_account)
        network_service_config.consuming_account = consuming_account

    if "billing_account" in update_fields:
        billing_account = accounts_svc.get_account(
            account=network_service_config_update["billing_account"],
            scoping_account=scoping_account)
        # Make sure account can be used for billing
        accounts_svc.assert_billing_information(billing_account)
        network_service_config.billing_account = billing_account

    # Vlan Configuration
    if "vlan_config" in update_fields:
        vlan_config = _update_vlan_config(
            scoping_account=scoping_account,
            network_service_config=network_service_config,
            vlan_config_update=network_service_config_update["vlan_config"])
        network_service_config.vlan_config = vlan_config

    # Connection
    if "connection" in network_service_config_update.keys():
        connection = connections_svc.get_connection(
            scoping_account=scoping_account,
            connection=network_service_config_update["connection"])
        network_service_config.connection = connection
        # TODO: Enforce vlan config constraints

    # Use update method dependent on config type.
    # Ownership is tested in the individual update methods
    if isinstance(
            network_service_config,
            config_models.ExchangeLanNetworkServiceConfig):
        network_service_config = _update_exchange_lan_network_service_config(
            scoping_account=scoping_account,
            network_service_config=network_service_config,
            network_service_config_update=network_service_config_update)
    elif isinstance(
            network_service_config,
            config_models.P2PNetworkServiceConfig):
        network_service_config = _update_p2p_network_service_config(
            scoping_account=scoping_account,
            network_service_config=network_service_config,
            network_service_config_update=network_service_config_update)
    elif isinstance(
            network_service_config,
            config_models.MP2MPNetworkServiceConfig):
        network_service_config = _update_mp2mp_network_service_config(
            scoping_account=scoping_account,
            network_service_config=network_service_config,
            network_service_config_update=network_service_config_update)
    elif isinstance(
            network_service_config,
            config_models.P2MPNetworkServiceConfig):
        network_service_config = _update_p2mp_network_service_config(
            scoping_account=scoping_account,
            network_service_config=network_service_config,
            network_service_config_update=network_service_config_update)
    elif isinstance(
            network_service_config,
            config_models.CloudNetworkServiceConfig):
        network_service_config = _update_cloud_network_service_config(
            scoping_account=scoping_account,
            network_service_config=network_service_config,
            network_service_config_update=network_service_config_update)
    else:
        raise NotImplementedError(
            ("Update method missing for "
             "network service config type: {}").format(
                 network_service_config.__class__.__name__))

    # Assign contact roles
    if "role_assignments" in update_fields:
        role_assignments = network_service_config_update["role_assignments"]
        assign_network_service_role_assignments(
            scoping_account=scoping_account,
            network_service_config=network_service_config,
            role_assignments=role_assignments)

    network_service_config.save()

    return network_service_config


def _update_exchange_lan_network_service_config(
        scoping_account=None,
        network_service_config=None,
        network_service_config_update=None):
    """
    Update an existing Exchange Lan Network Service configuration.

    :param scoping_account: The account managing the config
    :param config: An exchange lan network service config
    :param config_update: Update data
    """
    update_fields = network_service_config_update.keys()

    # Clean update data
    update = filter_fields(network_service_config_update, [
        "purchase_order",
        "external_ref",
        "asns",
    ])

    # Update the configuration with other fields
    for attr, value in update.items():
        setattr(network_service_config, attr, value)

    if "capacity" in update_fields:
        # Assert constraints
        network_service = network_service_config.network_service
        capacity = network_service_config_update["capacity"]
        products_svc.assert_capacity_constraints(
            scoping_account=scoping_account,
            product_offering=network_service.product_offering,
            capacity=capacity,
        )
        network_service_config.capacity = capacity

    # Assign mac addresses if present in update
    if "mac_addresses" in update_fields:
        assign_mac_addresses(
            scoping_account=scoping_account,
            network_service_config=network_service_config,
            mac_addresses=network_service_config_update["mac_addresses"])

    return network_service_config


def _update_p2p_network_service_config(
        scoping_account=None,
        network_service_config=None,
        network_service_config_update=None):
    """
    Update an existing ELine network service configuration.

    :param scoping_account: A account managing the config.
    :param config: An eline network service config
    :param config_update: Update data
    """
    # Clean update data
    update_fields = network_service_config_update.keys()
    update = filter_fields(network_service_config_update, [
        "purchase_order",
        "external_ref",
    ])

    if "capacity" in update_fields:
        # Assert constraints
        network_service = network_service_config.network_service
        capacity = network_service_config_update["capacity"]
        products_svc.assert_capacity_constraints(
            scoping_account=scoping_account,
            product_offering=network_service.product_offering,
            capacity=capacity,
        )
        network_service_config.capacity = capacity

    # Update the configuration with other fields
    for attr, value in update.items():
        setattr(network_service_config, attr, value)

    return network_service_config


def _update_mp2mp_network_service_config(
        scoping_account=None,
        network_service_config=None,
        network_service_config_update=None):
    """
    Update an existing ELine network service configuration.

    :param scoping_account: A account managing the config.
    :param config: An eline network service config
    :param config_update: Update data
    """
    # Clean update data
    update_fields = network_service_config_update.keys()
    update = filter_fields(network_service_config_update, [
        "purchase_order",
        "external_ref",
    ])

    if "capacity" in update_fields:
        # Assert constraints
        network_service = network_service_config.network_service
        capacity = network_service_config_update["capacity"]
        products_svc.assert_capacity_constraints(
            scoping_account=scoping_account,
            product_offering=network_service.product_offering,
            capacity=capacity,
        )
        network_service_config.capacity = capacity

    # Update the configuration with other fields
    for attr, value in update.items():
        setattr(network_service_config, attr, value)

    # Assign mac addresses if present in update
    if "mac_addresses" in update_fields:
        assign_mac_addresses(
            scoping_account=scoping_account,
            network_service_config=network_service_config,
            mac_addresses=network_service_config_update["mac_addresses"])

    return network_service_config


def _update_p2mp_network_service_config(
        scoping_account=None,
        network_service_config=None,
        network_service_config_update=None):
    """
    Update an existing ETree network service configuration.

    :param scoping_account: A account managing the config.
    :param config: An eline network service config
    :param config_update: Update data
    """
    # Clean update data
    update_fields = network_service_config_update.keys()
    update = filter_fields(network_service_config_update, [
        "purchase_order",
        "external_ref",
    ])

    if "capacity" in update_fields:
        # Assert constraints
        network_service = network_service_config.network_service
        capacity = network_service_config_update["capacity"]
        products_svc.assert_capacity_constraints(
            scoping_account=scoping_account,
            product_offering=network_service.product_offering,
            capacity=capacity,
        )
        network_service_config.capacity = capacity

    # Update the configuration with other fields
    for attr, value in update.items():
        setattr(network_service_config, attr, value)

    # Assign mac addresses if present in update
    if "mac_addresses" in update_fields:
        assign_mac_addresses(
            scoping_account=scoping_account,
            network_service_config=network_service_config,
            mac_addresses=network_service_config_update["mac_addresses"])

    return network_service_config


def _update_cloud_network_service_config(
        scoping_account=None,
        network_service_config=None,
        network_service_config_update=None):
    """
    Update a cloud network service config

    :param scoping_account: A account managing the config.
    :param config: A cloud network service config
    :param config_update: Update data
    """
    update_fields = network_service_config_update.keys()

    # Cloud configuration
    if "cloud_vlan" in update_fields:
        cloud_vlan = network_service_config_update["cloud_vlan"]
        network_service_config.cloud_vlan = cloud_vlan

    if "handover" in update_fields:
        handover = network_service_config_update["handover"]
        # Assert constraints
        if handover != network_service_config.handover:
            assert_cloud_handover_constraints(
                scoping_account=scoping_account,
                consuming_account=network_service_config.consuming_account,
                network_service=network_service_config.network_service,
                handover=handover)
            network_service_config.handover = handover

    if "capacity" in update_fields:
        # Assert capacity constraints
        network_service = network_service_config.network_service
        capacity = network_service_config_update["capacity"]
        products_svc.assert_capacity_constraints(
            scoping_account=scoping_account,
            product_offering=network_service.product_offering,
            capacity=capacity,
        )
        network_service_config.capacity = capacity

    return network_service_config


@transaction.atomic
def destroy_network_service_config(
        scoping_account=None,
        network_service_config=None):
    """
    Destroy a service config.

    :param config: The config to destroy. Might be an ID.
    :param managing: The owning account of the config.
    """
    network_service_config = get_network_service_config(
        scoping_account=scoping_account,
        network_service_config=network_service_config)

    # Cascade this to all network feature configs
    feature_configs = network_service_config.network_feature_configs
    for feature_config in feature_configs.all():
        destroy_network_feature_config(
            network_feature_config=feature_config,
            scoping_account=scoping_account)

    # Destroy destroy destroy
    network_service_config.delete()

    return network_service_config

#
# Feature Configurations
#

def get_network_feature_configs(
        scoping_account=None,
        filters=None):
    """
    Get all feature configurations within a account context.

    :param managing_account: Scope configs to those of this account
    :param consuming_account: Scope configs to those of this account
    :param filters: A dict of filter params
    """
    configs = access_filters.NetworkFeatureConfigFilter(filters).qs

    # Apply ownership filtering
    if scoping_account:
        configs = configs.filter(scoping_account=scoping_account)

    return configs


def get_network_feature_config(
        scoping_account=None,
        network_feature_config=None):
    """
    Get a single feature configuration.
    If billing or owning account are present, perform
    an ownership test.

    :param config: The config to load. Might be an ID.
    :param managing_account: The billing account
    :param consuming_account: The owning account of the config.
    """
    if isinstance(
            network_feature_config,
            config_models.NetworkFeatureConfig):
        config_scoping_account = \
            network_feature_config.scoping_account
        # Just perform an ownership test
        if scoping_account and not \
                config_scoping_account == scoping_account:
            raise ResourceAccessDenied(network_feature_config)

        return network_feature_config

    configs = get_network_feature_configs(
        scoping_account=scoping_account)

    return configs.get(pk=network_feature_config)


def _assign_network_feature_flags(
        network_feature_config=None,
        flags=None
    ):
    """
    Update network feature flags

    :param network_feature_config: The configuration to update.
    :param flags: A list of flags with name and enabled property.
    """
    feature = network_feature_config.network_feature
    feature_flags = feature.ixp_specific_flags.all()
    feature_flag_names = [f.name for f in feature_flags]
    mandatory_flag_names = [f.name for f in feature_flags if f.mandatory]

    # Assert all mandatory flags are enabled
    for name in mandatory_flag_names:
        network_feature_config.flags.get_or_create(
            name=name,
            defaults={
                "enabled": True,
            })

    if not flags:
        return network_feature_config

    for flag in flags:
        name = flag["name"]
        if name in mandatory_flag_names:
            continue

        if not name in feature_flag_names:
            continue

        config_flag, created = network_feature_config.flags.get_or_create(
            name=name,
            defaults={
                "enabled": flag["enabled"],
            })
        if not created:
            config_flag.enabled = flag["enabled"]
            config_flag.save()


    return network_feature_config


@transaction.atomic
def create_network_feature_config(
        scoping_account=None,
        network_feature_config_input=None):
    """
    Create a new network feature config.

    :param scoping_account: The account managing the config.
    :param config_input: The configuration data
    """
    # Load network feature and service config
    network_feature = network_svc.get_network_feature(
        scoping_account=scoping_account,
        network_feature=network_feature_config_input["network_feature"])
    service_config = get_network_service_config(
        scoping_account=scoping_account,
        network_service_config=network_feature_config_input["network_service_config"])

    # Check permissions on related objects
    managing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_feature_config_input.get("managing_account"))
    consuming_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_feature_config_input.get("consuming_account"))

    billing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_feature_config_input["billing_account"])

    # Make sure, that the billing account can be used for billing
    accounts_svc.assert_billing_information(billing_account)

    # Check that the feature is available for
    # the service in question.
    network_svc.assert_network_feature_available(
        network_feature, service_config.network_service)

    # Dispatch configuration creation
    if network_feature_config_input["type"] == \
        config_models.NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER:
        config = _create_route_server_network_feature_config(
            network_feature, service_config,
            scoping_account, managing_account, consuming_account,
            billing_account,
            network_feature_config_input)
    else:
        raise ValidationError("Unknown network feature: {}".format(
            config["type"]))

    # Feature flags
    _assign_network_feature_flags(
        network_feature_config=config,
        flags=network_feature_config_input.get("flags"))

    return config


def _create_route_server_network_feature_config(
        network_feature,
        network_service_config,
        scoping_account,
        managing_account,
        consuming_account,
        billing_account,
        config_input):
    """
    Create a route server feature configuration.

    :param network_feature: The network feature to configure
    :param network_service_config: A network service configuration
    :param scoping_account: The account managing the config.
    :param managing_account: The billing account.
    :param consuming_account: The owning account of the config.
    :param config_input: Validated configuration data.
    """
    if not isinstance(network_feature,
                      service_models.RouteServerNetworkFeature):
        raise ValidationError(
            ("The selected feature ({}) does not match the "
             "feature config for: route_server").format(
                 network_feature.__class__.__name__))

    # Create password if not exists
    password = config_input["password"]
    if not password:
        password = secrets.token_hex(23)

    # Check if the session mode matches
    session_mode = config_input["session_mode"]
    if network_feature.session_mode != session_mode:
        raise SessionModeInvalid(
            ("The session_mode of the config must match the "
             "mode of the feature to configue."))

    # Ip address. The IP address must be from within the networkkk
    ip_address = config_input["ip"]
    ip_address_nsc = ip_address.exchange_lan_network_service_config
    # For now: one to one
    if not ip_address_nsc or not ip_address_nsc == network_service_config:
        raise ValidationError(
            "IP-Address must be assigned to the network service config",
            field="ip")

    # AS sets:
    as_set_v4 = config_input.get("as_set_v4")
    as_set_v6 = config_input.get("as_set_v6")

    # An AS-SET is required if the rs network service
    # only supports one address family.
    if AddressFamilies.AF_INET in network_feature.address_families and \
       AddressFamilies.AF_INET6 in network_feature.address_families:
        # Check if either as_set_v4 or v6 is present
        if not as_set_v4 and not as_set_v6:
            raise ValidationError(
                ("The route-server supports both af_inet and af_inet6, "
                 "at least one AS-SET is required."),
                field="as_set_v6")

    else:
        if AddressFamilies.AF_INET in network_feature.address_families \
           and not as_set_v4:
            raise ValidationError(
                ("The route-serve requires an IPv4 AS-SET."),
                field="as_set_v4")
        if AddressFamilies.AF_INET6 in network_feature.address_families \
           and not as_set_v6:
            raise ValidationError(
                ("The route-serve requires an IPv4 AS-SET."),
                field="as_set_v6")

    # Check if session type is valid
    bgp_session_type = config_input["bgp_session_type"]
    if not bgp_session_type in network_feature.available_bgp_session_types:
        raise ValidationError(
            ("The bgp_session_type `{}` is not available on the "
             "route server.").format(bgp_session_type.value),
            field="bgp_session_type")

    # Create the Route Server access configuration:
    config = config_models.RouteServerNetworkFeatureConfig(
        state=State.REQUESTED,
        network_feature=network_feature,
        network_service_config=network_service_config,
        scoping_account=scoping_account,
        managing_account=managing_account,
        consuming_account=consuming_account,
        billing_account=billing_account,
        asn=config_input["asn"],
        purchase_order=config_input["purchase_order"],
        external_ref=config_input["external_ref"],
        contract_ref=config_input["contract_ref"],
        ip=ip_address,
        password=password,
        as_set_v4=as_set_v4,
        as_set_v6=as_set_v6,
        session_mode=session_mode,
        bgp_session_type=config_input["bgp_session_type"],
        max_prefix_v4=config_input.get("max_prefix_v4"),
        max_prefix_v6=config_input.get("max_prefix_v6"),
        insert_ixp_asn=config_input["insert_ixp_asn"])
    config.save()

    # Get contact role assignments and check if the
    # required roles are present.
    assign_network_feature_role_assignments(
        network_feature_config=config,
        role_assignments=config_input["role_assignments"],
        scoping_account=scoping_account)

    return config


@transaction.atomic
def update_network_feature_config(
        scoping_account=None,
        network_feature_config=None,
        network_feature_config_update=None):
    """
    Update a network feature configuration.
    The correct update method is determined by the type of
    config passed. Pattern matching would be fun.

    :param scoping_account: The account managing the feature config
    :param consuming_account: The account owning the configuration
    :param config: The network feature config
    """
    if not network_feature_config_update:
        raise ValidationError("Missing config update")

    # Load the network feature configuration (and test ownership)
    config = get_network_feature_config(
        scoping_account=scoping_account,
        network_feature_config=network_feature_config)

    # Check permissions on related objects
    managing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_feature_config_update.get("managing_account"))
    consuming_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_feature_config_update.get("consuming_account"))

    billing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_feature_config_update.get("billing_account"))

    if billing_account:
        # Assign account for billing, if billing information is present
        accounts_svc.assert_billing_information(billing_account)


    # Choose update method based on feature config type
    if isinstance(config, config_models.RouteServerNetworkFeatureConfig):
        config = _update_route_server_network_feature_config(
            config, network_feature_config_update,
            scoping_account=scoping_account)
    else:
        raise NotImplementedError(
            ("The provided network feature config: '{}' "
             "has no update capabilities.").format(
                 config.__class__.__name__))

    # Update network feature flags config
    if "flags" in network_feature_config_update.keys():
        _assign_network_feature_flags(
            network_feature_config=config,
            flags=network_feature_config_update["flags"])

    return config


@transaction.atomic
def _update_route_server_network_feature_config(
        config,
        config_update,
        scoping_account=None):
    """
    Update a route server network feature configuration.

    :param config: The route server config
    :param config_update: The update to apply. Might be partial.
    """
    update_keys = config_update.keys()

    # Clean update data
    update = filter_fields(config_update, [
        "purchase_order",
        "external_ref",
        "password",
        "max_prefix_v4",
        "max_prefix_v6",
        "insert_ixp_asn",
        "bgp_session_type",
        "managing_account",
        "consuming_account",
        "billing_account",
    ])

    # Get network service feature
    feature = config.network_feature

    # Set allowed attributes
    for attr, value in update.items():
        setattr(config, attr, value)

    # Set session mode
    if "session_mode" in update_keys:
        session_mode = config_update["session_mode"]

        # Check if the session mode matches
        if feature.session_mode != session_mode:
            raise SessionModeInvalid(
                ("The session_mode of the config must match the "
                 "mode of the feature to configue."))

    # Handle dependent as set
    as_set_v4 = config_update.get("as_set_v4", config.as_set_v4)
    as_set_v6 = config_update.get("as_set_v6", config.as_set_v6)

    if config.session_mode == RouteServerSessionMode.MODE_PUBLIC:
        families = feature.address_families
        af_inet = ipam_models.AddressFamilies.AF_INET
        af_inet6 = ipam_models.AddressFamilies.AF_INET6

        if af_inet in families and not as_set_v4:
            raise SessionModeInvalid(
                ("The session mode requires the `as_set_v4`"),
                field="as_set_v4")

        if af_inet6 in families and not as_set_v6:
            raise SessionModeInvalid(
                ("The session mode requires the `as_set_v6`"),
                field="as_set_v6")

    config.as_set_v4 = as_set_v4
    config.as_set_v6 = as_set_v6

    # Update contacts
    if "role_assignments" in update_keys:
        assign_network_feature_role_assignments(
            network_feature_config=config,
            role_assignments=config_update["role_assignments"],
            scoping_account=scoping_account)

    # Persist changes
    config.save()

    # Update IP address
    if "ip" in update_keys:
        ip_address = config_update["ip"]
        ip_nsc_id = ip_address.exchange_lan_network_service_config_id
        if not ip_nsc_id or not ip_nsc_id == config.network_service_config_id:
            raise ValidationError(
                ("The IP address must be assigned to the "
                 "network service config"),
                field="ip")
        config.ip = ip_address

    return config


def destroy_network_feature_config(
        scoping_account=None,
        network_feature_config=None):
    """
    Destroy a feature config.

    :param network_feature_config: The config to destroy. Might be an ID.
    :param scoping_account: The account managing the config.
    """
    config = get_network_feature_config(
        scoping_account=scoping_account,
        network_feature_config=network_feature_config)

    config.delete()
    return config
