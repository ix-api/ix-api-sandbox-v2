"""
Test config service
"""

import pytest
from model_bakery import baker
from ixapi_schema.v2.schema import (
    NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
    NETWORK_SERVICE_CONFIG_TYPE_P2P,
    NETWORK_SERVICE_CONFIG_TYPE_P2MP,
    NETWORK_SERVICE_CONFIG_TYPE_MP2MP,
    NETWORK_SERVICE_CONFIG_TYPE_CLOUD,

    NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER,

    VLAN_CONFIG_TYPE_PORT,
    VLAN_CONFIG_TYPE_QINQ,
    VLAN_CONFIG_TYPE_DOT1Q,
)
from ixapi_schema.v2.constants.catalog import (
    ProviderVlanTypes,
)

from jea.exceptions import ResourceAccessDenied
from jea.crm.exceptions import (
    RequiredContactRolesInvalid,
    ValidationError,
)
from jea.crm.services import contacts as contacts_svc
from jea.config.services import (
    configs as configs_svc,
)
from jea.config.models import (
    Dot1QVLanConfig,
    QinQVLanConfig,
    PortVLanConfig,
    NetworkServiceConfig,
    BGPSessionType,
    RouteServerSessionMode,
)
from jea.config.exceptions import (
    DiversityError,
)
from jea.service.exceptions import NetworkFeatureNotAvailable


@pytest.mark.django_db
def test_assign_network_service_role_assignments():
    """Test (implementation) contact assignment"""
    impl_role = baker.make("crm.Role", name="impl")
    service = baker.make(
        "service.ExchangeLanNetworkService",
        nsc_required_contact_roles=[impl_role])
    config = baker.make(
        "config.ExchangeLanNetworkServiceConfig",
        network_service=service)
    contact = baker.make(
        "crm.Contact",
        scoping_account=config.scoping_account)
    impl_assignment = baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=impl_role)

    assert impl_assignment not in config.role_assignments.all()

    # Assign network service config
    configs_svc.assign_network_service_role_assignments(
        network_service_config=config,
        role_assignments=[impl_assignment])

    assert impl_assignment in config.role_assignments.all()


@pytest.mark.django_db
def test_assign_network_service_role_assignments__ownership_fails():
    """
    Test (implementation) contact assignment
    with a failing ownership test.
    """
    # Create a config and an implementation contact.
    # Both have different scoping_accounts.
    c1 = baker.make("crm.Account")
    c2 = baker.make("crm.Account")
    service = baker.make(
        "service.ExchangeLanNetworkService",
        nsc_required_contact_roles=[
            contacts_svc.get_default_role("implementation"),
        ])
    config = baker.make(
        "config.ExchangeLanNetworkServiceConfig",
        network_service=service,
        scoping_account=c1)
    contact = baker.make(
        "crm.Contact",
        scoping_account=c2)
    impl_assignment = baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))

    assert config.scoping_account != contact.scoping_account

    # Assign config
    with pytest.raises(ResourceAccessDenied):
        configs_svc.assign_network_service_role_assignments(
            scoping_account=config.scoping_account,
            network_service_config=config,
            role_assignments=[impl_assignment])


@pytest.mark.django_db
def test_assign_network_service_contacts__ownership_override():
    """
    Test (implementation) contact assignment with
    ownership override.
    """
    role = contacts_svc.get_default_role("noc")
    service = baker.make(
        "service.ExchangeLanNetworkService",
        nsc_required_contact_roles=[role])
    config = baker.make(
        "config.ExchangeLanNetworkServiceConfig",
        network_service=service)
    contact = baker.make(
        "crm.Contact",
        scoping_account=config.scoping_account)
    noc_assignment = baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=role)

    assert noc_assignment not in config.role_assignments.all()

    # Assign config
    configs_svc.assign_network_service_role_assignments(
        scoping_account=config.scoping_account,
        network_service_config=config,
        role_assignments=[noc_assignment])

    assert noc_assignment in config.role_assignments.all()


@pytest.mark.django_db
def test_assign_network_service_contacts__clear():
    """Test removal of all (implementation) contacts from object"""
    role = baker.make("crm.Role")
    contact = baker.make("crm.Contact")
    role_assignment = baker.make(
        "crm.RoleAssignment",
        role=role,
        contact=contact)
    service = baker.make(
        "service.ExchangeLanNetworkService",
        nsc_required_contact_roles=[])
    config = baker.make(
        "config.ExchangeLanNetworkServiceConfig",
        network_service=service,
        scoping_account=contact.scoping_account,
        role_assignments=[role_assignment])

    assert role_assignment in config.role_assignments.all()

    # Assign config
    configs_svc.assign_network_service_role_assignments(
        scoping_account=config.scoping_account,
        network_service_config=config,
        role_assignments=[])

    assert role_assignment not in config.role_assignments.all()
    assert config.role_assignments.count() == 0


@pytest.mark.django_db
def test_assign_mac_addresses():
    """Test assigning mac addresses to a service config"""
    config = baker.make(
        "config.ExchangeLanNetworkServiceConfig")
    mac = baker.make(
        "ipam.MacAddress",
        scoping_account=config.scoping_account,
        managing_account=config.managing_account,
        consuming_account=config.consuming_account)

    assert not mac in config.mac_addresses.all()

    # Assign mac
    configs_svc.assign_mac_addresses(
        network_service_config=config,
        mac_addresses=[mac])

    assert mac in config.mac_addresses.all()


@pytest.mark.django_db
def test_assign_mac_addresses__invalid_config_type():
    """Test assigning a mac address to an unsupport network service config"""
    config = baker.make("config.CloudNetworkServiceConfig")
    mac = baker.make("ipam.MacAddress")

    with pytest.raises(TypeError):
        configs_svc.assign_mac_addresses(
            network_service_config=config,
            mac_addresses=[mac])


@pytest.mark.django_db
def test_assign_mac_addresses__ownership():
    """Test mac addrss assignment with ownership fail and override"""
    c1 = baker.make("crm.Account")
    c2 = baker.make("crm.Account")
    config = baker.make(
        "config.ExchangeLanNetworkServiceConfig",
        scoping_account=c1)
    mac = baker.make(
        "ipam.MacAddress",
        scoping_account=c2)

    # Preflight check
    assert mac not in config.mac_addresses.all()

    with pytest.raises(ResourceAccessDenied):
        configs_svc.assign_mac_addresses(
            network_service_config=config,
            mac_addresses=[mac])

    # Override config ownership
    configs_svc.assign_mac_addresses(
        scoping_account=mac.scoping_account,
        network_service_config=config,
        mac_addresses=[mac])

    assert mac in config.mac_addresses.all()


@pytest.mark.django_db
def test_assign_mac_addresses__clear():
    """Test removal of mac addresses"""
    mac = baker.make("ipam.MacAddress")
    config = baker.make(
        "config.ExchangeLanNetworkServiceConfig",
        mac_addresses=[mac])

    # Clear
    configs_svc.assign_mac_addresses(
        network_service_config=config)

    assert not mac in config.mac_addresses.all()
    assert not config.mac_addresses.count()


@pytest.mark.django_db
def test_get_network_service_configs():
    """Test getting network service configs"""
    c1 = baker.make("crm.Account")
    c2 = baker.make("crm.Account")

    s1 = baker.make(
        "config.ExchangeLanNetworkServiceConfig",
        scoping_account=c1)
    s2 = baker.make(
        "config.MP2MPNetworkServiceConfig",
        scoping_account=c2)

    configs = configs_svc.get_network_service_configs(
        scoping_account=c1)

    assert s1 in configs
    assert not s2 in configs


@pytest.mark.django_db
def test_get_network_service_configs__filters__type():
    """Test getting network service configs with filtering"""
    c1 = baker.make("crm.Account")

    # Make some configs
    s1 = baker.make(
        "config.ExchangeLanNetworkServiceConfig",
        scoping_account=c1)
    s2 = baker.make(
        "config.MP2MPNetworkServiceConfig",
        scoping_account=c1)

    # query configs
    configs = configs_svc.get_network_service_configs(
        scoping_account=c1,
        filters={
            "type": NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
        })

    assert s1 in configs
    assert not s2 in configs


@pytest.mark.django_db
def test_get_network_service_config__lookup():
    """Test getting a single network service config"""
    c1 = baker.make("crm.Account")
    c2 = baker.make("crm.Account")

    # Make some configs
    s1 = baker.make("config.ExchangeLanNetworkServiceConfig",
        scoping_account=c2)

    # Check lookup
    config = configs_svc.get_network_service_config(
        network_service_config=str(s1.pk),
        scoping_account=c2)

    assert config.pk == s1.pk

    # Check invalid pk
    with pytest.raises(NetworkServiceConfig.DoesNotExist):
        configs_svc.get_network_service_config(
            network_service_config="123459012345690",
            scoping_account=c1)


@pytest.mark.django_db
def test_assign_network_feature_flag():
    """Test assigning network feature flags"""
    nf = baker.make("service.NetworkFeature")
    f1 = baker.make("service.IXPSpecificFeatureFlag", **{
        "name": "mandatory-filter1",
        "mandatory": True,
        "network_feature": nf,
    })
    f2 = baker.make("service.IXPSpecificFeatureFlag", **{
        "name": "filter2",
        "network_feature": nf,
    })


    config = baker.make("config.RouteServerNetworkFeatureConfig", **{
        "network_feature": nf,
    })

    configs_svc._assign_network_feature_flags(
        network_feature_config=config,
        flags=[
            { "name": "unknown", "enabled": True },
            { "name": "filter2", "enabled": False },
        ])

    assert not config.flags.filter(name="unknown").first()

    f1c = config.flags.get(name="mandatory-filter1")
    assert f1c
    assert f1c.enabled

    f2c = config.flags.get(name="filter2")
    assert f2c
    assert not f2c.enabled

    # Update: Enable f2
    configs_svc._assign_network_feature_flags(
        network_feature_config=config,
        flags=[
            { "name": "mandatory-filter1", "enabled": False },
            { "name": "filter2", "enabled": True },
        ])


    f1c = config.flags.get(name="mandatory-filter1")
    assert f1c
    assert f1c.enabled, "mandatory must be enabled"

    f2c = config.flags.get(name="filter2")
    assert f2c
    assert f2c.enabled, "optional should be enabled now"


@pytest.mark.django_db
def test_get_network_service_config_ownership():
    """Test ownership test of single network service config"""
    c1 = baker.make("crm.Account")
    c2 = baker.make("crm.Account")

    # Make some configs
    s1 = baker.make(
        "config.ExchangeLanNetworkServiceConfig",
        scoping_account=c1)
    s2 = baker.make(
        "config.ExchangeLanNetworkServiceConfig",
        scoping_account=c2)

    # Check lookup
    config = configs_svc.get_network_service_config(
        network_service_config=s1,
        scoping_account=c1)

    assert config.pk == s1.pk
    assert id(config) == id(s1)

    # Check invalid ownership
    with pytest.raises(ResourceAccessDenied):
        configs_svc.get_network_service_config(
            network_service_config=s2,
            scoping_account=c1)


@pytest.mark.django_db
def test__create_vlan_qinq_config():
    """Test creating a vlan configuration"""
    conn = baker.make("config.Connection")
    vlan_config_request = {
        "vlan_type": VLAN_CONFIG_TYPE_QINQ,
        "outer_vlan_ethertype": "0x8100",
        "outer_vlan": 42,
        "inner_vlan": 23,
    }
    vlan_config = configs_svc._create_vlan_config(
        vlan_config_request=vlan_config_request,
        connection=conn,
    )
    assert vlan_config

    # With defaults
    conn = baker.make("config.Connection")
    vlan_config_request = {
        "vlan_type": VLAN_CONFIG_TYPE_QINQ,
        "outer_vlan": 42,
        "inner_vlan": 23,
    }
    vlan_config = configs_svc._create_vlan_config(
        vlan_config_request=vlan_config_request,
        connection=conn,
    )
    assert vlan_config



@pytest.mark.django_db
def test__create_vlan_dot1q_config():
    """Test creating a dot1q vlan configuration"""
    conn = baker.make("config.Connection")
    vlan_config_request = {
        "vlan_type": VLAN_CONFIG_TYPE_DOT1Q,
        "vlan": 23,
    }
    vlan_config = configs_svc._create_vlan_config(
        vlan_config_request=vlan_config_request,
        connection=conn,
    )
    assert vlan_config


@pytest.mark.django_db
def test__create_vlan_port_config():
    """Test creating a vlan configuration"""
    conn = baker.make("config.Connection")
    request = {
        "vlan_type": VLAN_CONFIG_TYPE_PORT,
    }
    vlan_config = configs_svc._create_vlan_config(
        connection=conn,
        vlan_config_request=request)

    assert vlan_config


@pytest.mark.django_db
def test__create_vlan_port_config__unique_constraint():
    """Test creating a vlan configuration"""
    conn = baker.make("config.Connection")
    vc = baker.make("config.PortVLanConfig")
    baker.make("config.ExchangeLanNetworkServiceConfig",
        connection=conn,
        vlan_config=vc)

    request = {
        "vlan_type": VLAN_CONFIG_TYPE_PORT,
    }

    with pytest.raises(ValidationError):
        configs_svc._create_vlan_config(
            connection=conn,
            vlan_config_request=request)

    # Creating another vlan based binding should work
    vlan_config_request = {
        "vlan_type": VLAN_CONFIG_TYPE_DOT1Q,
        "vlan": 23,
    }

    assert configs_svc._create_vlan_config(
        vlan_config_request=vlan_config_request,
        connection=conn,
    )


@pytest.mark.django_db
def test_create_p2p_network_service_config():
    """Test creating an eline network service config"""
    c1 = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    c2 = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))

    # We need a connection
    connection = baker.make("config.Connection",
        scoping_account=c1)

    # Create a network service
    service = baker.make(
        "service.P2PNetworkService",
        consuming_account=c1,
        joining_member_account=c2,
        nsc_required_contact_roles=[])

    config_input = {
        "type": NETWORK_SERVICE_CONFIG_TYPE_P2P,
        "capacity": None,
        "purchase_order": "PO2342-1111",
        "external_ref": "23199",
        "contract_ref": None,
        "network_service": service,
        "connection": connection,
        "managing_account": c1,
        "consuming_account": c1,
        "billing_account": c1,
        "vlan_config": {
            "vlan_type": VLAN_CONFIG_TYPE_DOT1Q,
            "vlan": 42,
        }
    }

    # Create the config
    config = configs_svc.create_network_service_config(
        network_service_config_request=config_input,
        scoping_account=c1)

    assert config
    assert config.pk
    assert config.vlan_config
    assert config.vlan_config.vlan == 42


@pytest.mark.django_db
def test_create_mp2mp_network_service_config():
    """Test creating an elan network service config"""
    c1 = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))

    # We need a connection
    connection = baker.make("config.Connection",
        scoping_account=c1)

    # Create a network service
    service = baker.make(
        "service.MP2MPNetworkService",
        nsc_required_contact_roles=[])

    # There has to be a member joining rule for the account
    baker.make(
        "service.MemberJoiningRule",
        consuming_account=c1,
        network_service=service)

    vlan_config = {
        "vlan_type": VLAN_CONFIG_TYPE_QINQ,
        "inner_vlan": 42,
        "outer_vlan": 23,
        # Default ethertype.
    }

    mac = baker.make("ipam.MacAddress", scoping_account=c1)

    config_input = {
        "type": NETWORK_SERVICE_CONFIG_TYPE_MP2MP,
        "capacity": None,
        "purchase_order": "PO2342-1111",
        "external_ref": "23199",
        "contract_ref": None,
        "network_service": service,
        "connection": connection,
        "managing_account": c1,
        "consuming_account": c1,
        "billing_account": c1,
        "vlan_config": vlan_config,
        "mac_addresses": [mac],
    }

    # Create the config
    config = configs_svc.create_network_service_config(
        network_service_config_request=config_input,
        scoping_account=c1)

    assert config
    assert config.pk
    assert config.vlan_config
    assert config.vlan_config.outer_vlan == 23
    assert config.vlan_config.inner_vlan == 42
    assert mac in config.mac_addresses.all()


@pytest.mark.django_db
def test_create_p2mp_network_service_config():
    """Test creating an etree network service config"""
    c1 = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))

    # We need a connection
    connection = baker.make(
        "config.Connection",
        scoping_account=c1)

    # Create a network service
    service = baker.make(
        "service.P2MPNetworkService",
        nsc_required_contact_roles=[])

    baker.make(
        "service.MemberJoiningRule",
        consuming_account=c1,
        network_service=service)

    vlan_config = {
        "vlan_type": VLAN_CONFIG_TYPE_PORT,
    }

    mac = baker.make("ipam.MacAddress", scoping_account=c1)

    config_input = {
        "type": NETWORK_SERVICE_CONFIG_TYPE_P2MP,
        "capacity": None,
        "purchase_order": "PO2342-1111",
        "external_ref": "23199",
        "contract_ref": None,
        "network_service": service,
        "connection": connection,
        "managing_account": c1,
        "consuming_account": c1,
        "billing_account": c1,
        "vlan_config": vlan_config,
        "role": "root",
        "mac_addresses": [mac],
    }

    # Create the config
    config = configs_svc.create_network_service_config(
        network_service_config_request=config_input,
        scoping_account=c1)

    assert config
    assert config.pk
    assert config.vlan_config
    assert mac in config.mac_addresses.all()


@pytest.mark.django_db
def test_create_exchange_lan_network_service_config():
    """Test creating an exchange lan network service config"""
    c1 = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))

    # We need a connection
    connection = baker.make(
        "config.Connection",
        scoping_account=c1)

    contact = baker.make(
        "crm.Contact",
        scoping_account=c1)
    # Implementation contact
    impl_role = baker.make("crm.Role")
    impl = baker.make(
        "crm.RoleAssignment",
        role=impl_role,
        contact=contact)

    # A mac address
    mac = baker.make(
        "ipam.MacAddress",
        scoping_account=c1)

    product = baker.make(
        "catalog.ExchangeLanNetworkProductOffering")

    # Create a network service
    service = baker.make(
        "service.ExchangeLanNetworkService",
        nsc_required_contact_roles=[impl_role],
        product_offering=product,
        scoping_account=c1)

    net4 = baker.make(
        "ipam.IpAddress",
        version=4,
        address="123.42.42.0",
        prefix_length=24,
        exchange_lan_network_service=service)

    net6 = baker.make(
        "ipam.IpAddress",
        version=6,
        address="1000:23:42::0",
        prefix_length=64,
        exchange_lan_network_service=service)

    config_input = {
        "type": NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
        "asns": [1234],
        "mac_addresses": [mac],
        "capacity": None,
        "purchase_order": "PO2342-1111",
        "external_ref": "23199",
        "contract_ref": None,
        "network_service": service,
        "role_assignments": [impl],
        "connection": connection,
        "managing_account": c1,
        "consuming_account": c1,
        "billing_account": c1,
        "product_offering": product,
        "vlan_config": {
            "vlan_type": VLAN_CONFIG_TYPE_QINQ,
            "inner_vlan": 42,
            "outer_vlan": 23,
            "outer_vlan_ethertype": "0x8100",
        }
    }

    # Create the config
    config = configs_svc.create_network_service_config(
        network_service_config_request=config_input,
        scoping_account=c1)

    assert config
    assert config.pk
    assert config.vlan_config
    assert isinstance(config.vlan_config, QinQVLanConfig)
    assert impl in config.role_assignments.all()
    assert mac in config.mac_addresses.all()


@pytest.mark.django_db
def test_create_cloud_network_service_config():
    """Test creating a cloud network service config"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))

    # We need a connection
    connection = baker.make(
        "config.Connection",
        consuming_account=account,
        scoping_account=account)

    # Create a network service
    service = baker.make(
        "service.CloudNetworkService",
        diversity=1,
        nsc_required_contact_roles=[])

    config_input = {
        "type": NETWORK_SERVICE_CONFIG_TYPE_CLOUD,
        "capacity": 1000,
        "purchase_order": "PO2342-1111",
        "external_ref": "23199",
        "contract_ref": None,
        "network_service": service,
        "connection": connection,
        "managing_account": account,
        "consuming_account": account,
        "billing_account": account,
        "handover": 1,
        "cloud_vlan": 100,
        "vlan_config": {
            "vlan_type": VLAN_CONFIG_TYPE_DOT1Q,
            "vlan": 42,
        }
    }

    # Create the config
    config = configs_svc.create_network_service_config(
        network_service_config_request=config_input,
        scoping_account=account)

    assert config
    assert config.pk
    assert config.vlan_config
    assert config.handover == config_input["handover"]
    assert config.cloud_vlan == config_input["cloud_vlan"]


@pytest.mark.django_db
def test_update_p2p_network_service_config():
    """Update an existing eline config"""
    impl_role = baker.make("crm.Role")
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    network_service = baker.make(
        "service.P2PNetworkService",
        nsc_required_contact_roles=[impl_role])
    config = baker.make(
        "config.P2PNetworkServiceConfig",
        network_service=network_service,
        vlan_config=baker.make("config.QinQVLanConfig"),
        scoping_account=account)
    contact = baker.make(
        "crm.Contact",
        scoping_account=account)
    impl_assignment = baker.make(
        "crm.RoleAssignment",
        role=impl_role,
        contact=contact)
    mac = baker.make(
        "ipam.MacAddress",
        scoping_account=config.scoping_account,
        managing_account=config.managing_account,
        consuming_account=config.consuming_account)

    # Prepare update
    po_num = "FOO-23-2000-01"
    inner_vlan = 23
    managing_account = "FAIL"

    update = {
        "purchase_order": po_num,
        "vlan_config": {
            "vlan_type": VLAN_CONFIG_TYPE_QINQ,
            "inner_vlan": inner_vlan,
            "outer_vlan": 42,
        },
        "managing_account": account,
        "consuming_account": account,
        "billing_account": account,
        "role_assignments": [impl_assignment.pk],
    }

    # Update exchange lan network service config
    config = configs_svc.update_network_service_config(
        scoping_account=account,
        network_service_config=config,
        network_service_config_update=update)

    print(config.vlan_config)

    assert impl_assignment in config.role_assignments.all()
    assert config.purchase_order == po_num
    assert config.vlan_config.inner_vlan == inner_vlan
    assert config.managing_account == account
    assert config.consuming_account == account


@pytest.mark.django_db
def test_update_cloud_network_service_config():
    """Update an existing cloud nsc"""
    account = baker.make("crm.Account")
    ns = baker.make(
        "service.CloudNetworkService",
        diversity=4,
        consuming_account=account,
        managing_account=account,
        scoping_account=account)
    config = baker.make(
        "config.CloudNetworkServiceConfig",
        network_service=ns,
        consuming_account=account,
        managing_account=account,
        scoping_account=account)

    update = {
        "cloud_vlan": 42,
        "handover": 4,
    }

    # Update exchange lan network service config
    config = configs_svc.update_network_service_config(
        scoping_account=account,
        network_service_config=config,
        network_service_config_update=update)

    assert config.cloud_vlan == update["cloud_vlan"]
    assert config.handover == update["handover"]


@pytest.mark.django_db
def test_update_exchange_lan_network_service_config():
    """Update an exchange lan network service config"""
    impl_role = baker.make("crm.Role")
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    service = baker.make(
        "service.ExchangeLanNetworkService",
        nsc_required_contact_roles=[impl_role])
    conn = baker.make(
        "config.Connection",
        scoping_account=account)
    config = baker.make(
        "config.ExchangeLanNetworkServiceConfig",
        connection=conn,
        network_service=service,
        scoping_account=account,
        vlan_config=baker.make("config.PortVLanConfig"))
    contact = baker.make(
        "crm.Contact",
        scoping_account=account)
    impl_assignment = baker.make(
        "crm.RoleAssignment",
        role=impl_role,
        contact=contact)
    mac = baker.make(
        "ipam.MacAddress",
        scoping_account=account,
        managing_account=config.managing_account,
        consuming_account=config.consuming_account)

    # Prepare update
    po_num = "FOO-23-2000-01"
    inner_vlan = 23
    managing_account = baker.make(
        "crm.Account", scoping_account=account)
    consuming_account = baker.make(
        "crm.Account", scoping_account=account)

    update = {
        "purchase_order": po_num,
        "vlan_config": {
            "vlan_type": VLAN_CONFIG_TYPE_QINQ,
            "inner_vlan": inner_vlan,
            "outer_vlan": 42,
        },
        "managing_account": managing_account,
        "consuming_account": consuming_account,
        "billing_account": account,
        "role_assignments": [impl_assignment],
        "mac_addresses": [mac],
        "asns": [42],
    }

    # Update exchange lan network service config
    config = configs_svc.update_network_service_config(
        scoping_account=account,
        network_service_config=config,
        network_service_config_update=update)

    assert mac in config.mac_addresses.all()
    assert impl_assignment in config.role_assignments.all()
    assert config.purchase_order == po_num
    assert config.vlan_config.inner_vlan == inner_vlan
    assert config.managing_account == managing_account
    assert config.consuming_account == consuming_account


@pytest.mark.django_db
def test_destroy_network_service_config():
    """Test destruction of a network service config"""
    config = baker.make(
        "config.ExchangeLanNetworkServiceConfig")
    feature_config = baker.make(
        "config.RouteServerNetworkFeatureConfig",
        network_service_config=config)

    # Destroy this config
    configs_svc.destroy_network_service_config(
        scoping_account=config.scoping_account,
        network_service_config=config)


@pytest.mark.django_db
def test_create_invalid_network_feature_config():
    """Test creating a network feature config for an incompatible service"""
    account = baker.make(
        "crm.Account")
    managing_account = baker.make(
        "crm.Account",
        scoping_account=account)
    consuming_account = baker.make(
        "crm.Account",
        scoping_account=account)
    billing_account = baker.make(
        "crm.Account",
        scoping_account=account,
        billing_information=baker.make("crm.BillingInformation"))
    service = baker.make(
        "service.ExchangeLanNetworkService")
    feature = baker.make(
        "service.NetworkFeature")
    service_config = baker.make(
        "config.ExchangeLanNetworkServiceConfig",
        network_service=service,
        scoping_account=account,
        managing_account=managing_account,
        consuming_account=consuming_account)

    with pytest.raises(NetworkFeatureNotAvailable):
        configs_svc.create_network_feature_config(
            scoping_account=account,
            network_feature_config_input={
                "network_feature": feature,
                "network_service_config": service_config,
                "managing_account": managing_account,
                "consuming_account": consuming_account,
                "billing_account": billing_account,
            })


@pytest.mark.django_db
def test_create_route_server_network_feature_config():
    """Test feature configuration creation: RouteServer"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    managing_account = baker.make(
        "crm.Account",
        scoping_account=account)
    consuming_account = baker.make(
        "crm.Account",
        scoping_account=account)

    service = baker.make("service.ExchangeLanNetworkService")
    feature = baker.make("service.RouteServerNetworkFeature",
                         available_bgp_session_types=[
                             BGPSessionType.TYPE_ACTIVE,
                             BGPSessionType.TYPE_PASSIVE,
                         ],
                         nfc_required_contact_roles=[],
                         network_service=service,
                         address_families=['af_inet'])

    service_config = baker.make(
        "config.ExchangeLanNetworkServiceConfig",
        network_service=service,
        scoping_account=account,
        managing_account=managing_account,
        consuming_account=consuming_account,
        billing_account=account)
    ip_addr = baker.make(
        "ipam.IpAddress",
        scoping_account=account,
        exchange_lan_network_service_config=service_config)

    # AS-SET is dependent on the mode
    as_set_v4 = None
    if feature.session_mode == RouteServerSessionMode.MODE_PUBLIC:
        as_set_v4 = "AS-FOO@RIPE"

    config_input = {
        "type": NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER,
        "network_service_config": service_config,
        "network_feature": feature,
        "purchase_order": "PO2222-1111",
        "external_ref": "CX000000420",
        "contract_ref": None,
        "password": None,
        "asn": 2342,
        "as_set_v4": as_set_v4,
        "bgp_session_type": BGPSessionType.TYPE_ACTIVE,
        "session_mode": feature.session_mode,
        "insert_ixp_asn": False,
        "max_prefix_v4": 5000,
        "managing_account": managing_account,
        "consuming_account": consuming_account,
        "billing_account": account,
        "role_assignments": [],
        "ip": ip_addr,
    }

    config = configs_svc.create_network_feature_config(
        scoping_account=account,
        network_feature_config_input=config_input)

    # Assertions
    assert config
    assert config.pk
    assert config.password
    assert isinstance(config.password, str)


@pytest.mark.django_db
def test_update_route_server_network_feature_config():
    """Test updating a routeserver network feature configuration"""
    feature = baker.make("service.RouteServerNetworkFeature",
        address_families=["af_inet6"])
    config = baker.make("config.RouteServerNetworkFeatureConfig",
        network_feature=feature)

    # Prepare update
    purchase_order = "FOO-23-42-1"
    external_ref = "11111-2"
    insert_ixp_asn = True
    password = "password"

    update = {
        "purchase_order": purchase_order,
        "external_ref": external_ref,
        "insert_ixp_asn": insert_ixp_asn,
        "password": password,
        "as_set_v6": "AS-FOO@BAR",
    }

    # Perform update
    config = configs_svc.update_network_feature_config(
        network_feature_config=config,
        network_feature_config_update=update)

    # Assertions
    assert config.purchase_order == purchase_order
    assert config.external_ref == external_ref
    assert config.insert_ixp_asn == insert_ixp_asn
    assert config.password == password


@pytest.mark.django_db
def test_assert_cloud_vlan_constraints():
    """Test cloud vlan constraint assertion"""
    account = baker.make("crm.Account")
    offering_single = baker.make(
        "catalog.CloudNetworkProductOffering",
        provider_vlans=ProviderVlanTypes.SINGLE)
    single_ns = baker.make(
        "service.CloudNetworkService",
        product_offering=offering_single,
        diversity=2)

    conn1 = baker.make("config.Connection", consuming_account=account)


@pytest.mark.django_db
def test_assert_cloud_handover_constraints():
    """Test cloud handover constraint enforcing"""
    account = baker.make("crm.Account")
    ns = baker.make(
        "service.CloudNetworkService",
        diversity=2)

    # Garbage data
    with pytest.raises(ValidationError):
        configs_svc.assert_cloud_handover_constraints(
            consuming_account=account,
            network_service=ns,
            handover=-1)

    # Diversity of 2 enables handovers [1, 2]
    with pytest.raises(ValidationError):
        configs_svc.assert_cloud_handover_constraints(
            consuming_account=account,
            network_service=ns,
            handover=3)

    # This should work, until we have created the first config
    configs_svc.assert_cloud_handover_constraints(
        consuming_account=account,
        network_service=ns,
        handover=1)
    baker.make(
        "config.CloudNetworkServiceConfig",
        network_service=ns,
        consuming_account=account,
        handover=1)

    with pytest.raises(ValidationError):
        # Handover 1 is now configured
        configs_svc.assert_cloud_handover_constraints(
            consuming_account=account,
            network_service=ns,
            handover=1)


@pytest.mark.django_db
def test_assert_cloud_connection_uniqueness():
    """Check cloud connection uniqueness"""
    account = baker.make("crm.Account")
    ns = baker.make(
        "service.CloudNetworkService",
        diversity=2)

    conn1 = baker.make(
        "config.Connection",
        consuming_account=account)
    conn2 = baker.make(
        "config.Connection",
        consuming_account=account)

    # Add config for account
    baker.make(
        "config.CloudNetworkServiceConfig",
        connection=conn1,
        consuming_account=account,
        network_service=ns)

    configs_svc.assert_cloud_connection_uniqueness(
        consuming_account=account,
        network_service=ns,
        connection=conn2)

    with pytest.raises(ValidationError):
        configs_svc.assert_cloud_connection_uniqueness(
            consuming_account=account,
            network_service=ns,
            connection=conn1)

