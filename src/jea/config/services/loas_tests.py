import pytest
from model_bakery import baker

from jea.exceptions import ValidationError
from jea.crm.models import Account
from jea.config.models import (
    Connection,
    PortReservation,
)
from jea.config.services import loas as loas_svc
from jea.catalog.models import (
    ConnectionProductOffering,
    CrossConnectInitiator,
)

@pytest.mark.django_db
def test_generate_loa():
    """Test generating a loa"""
    offering = baker.make(
            ConnectionProductOffering,
            cross_connect_initiator=CrossConnectInitiator.SUBSCRIBER)
    connection = baker.make(
            Connection,
            product_offering=offering)

    # Add some port reservations
    def add_port(connection, port):
        baker.make(
            PortReservation,
            connection=connection,
            exchange_side_demarc=port,
            connecting_party="demo ix")

    add_port(connection, "PC-293-19-1")
    add_port(connection, "PC-293-19-2")
    add_port(connection, "PC-293-19-3")

    # Generate LOA
    pdf = loas_svc.generate_loa(connection=connection)

    assert len(pdf) > 0
    # Further ideas for testing welcome.

    # with open("loatest.pdf", "wb") as f:
    #    f.write(pdf)


@pytest.mark.django_db
def test_process_loa__connection():
    """Test LOA processing"""
    with open("config/testdata/loa.pdf", "rb") as file:
        loa = file.read()

    account = baker.make(Account)
    offering = baker.make(
            ConnectionProductOffering,
            cross_connect_initiator=CrossConnectInitiator.SUBSCRIBER)
    connection = baker.make(
            Connection,
            product_offering=offering)
    reservation = baker.make(
        PortReservation,
        connection=connection,
        exchange_side_demarc="Port-23-42",
        connecting_party="demo party",
        scoping_account=account)
    assert not reservation.loa_received_at

    loas_svc.process_loa(loa=loa, connection=connection)

    reservation.refresh_from_db()
    assert reservation.loa_received_at


@pytest.mark.django_db
def test_process_loa__reservation():
    """Test LOA processing"""
    with open("config/testdata/loa.pdf", "rb") as file:
        loa = file.read()

    account = baker.make(Account)
    offering = baker.make(
            ConnectionProductOffering,
            cross_connect_initiator=CrossConnectInitiator.SUBSCRIBER)
    connection = baker.make(
            Connection,
            product_offering=offering)
    reservation = baker.make(
        PortReservation,
        connection=connection,
        exchange_side_demarc="Port-23-42",
        connecting_party="demo party",
        scoping_account=account)
    assert not reservation.loa_received_at

    loas_svc.process_loa(loa=loa, port_reservation=reservation)

    reservation.refresh_from_db()
    assert reservation.loa_received_at


@pytest.mark.django_db
def test_process_invalid_loa():
    """Test LOA with an non PDF"""
    with open(__file__, "rb") as file:
        loa = file.read()

    with pytest.raises(ValidationError):
        loas_svc.process_loa(loa=loa)
