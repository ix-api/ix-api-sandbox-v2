
"""
Tests for PortService
"""

import pytest
from model_bakery import baker

from jea.crm.services import contacts as contacts_svc
from jea.exceptions import ResourceAccessDenied
from jea.stateful.models import State
from jea.catalog.models import MediaType
from jea.config.services import ports as ports_svc
from jea.config.exceptions import (
    PortUnavailable,
    PortInUse,
    PortNotReady,
)


def test_max_speed_for_media_type():
    """Test get max connection speed"""
    # Check properties
    for c_type in MediaType:
        speed = ports_svc.max_speed_for_media_type(c_type.value)
        assert speed > 0

    # Unknown type
    with pytest.raises(ports_svc.UnknownMediaTypeError):
        ports_svc.max_speed_for_media_type("FOO")


@pytest.mark.django_db
def test_allocate_port():
    """Test port allocation"""
    account = baker.make(
        "crm.Account")
    managing_account = baker.make(
        "crm.Account",
        scoping_account=account)
    consuming_account = baker.make(
        "crm.Account",
        scoping_account=account)
    billing_account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"),
        scoping_account=account)
    pop = baker.make(
        "catalog.PointOfPresence")
    device = baker.make(
        "catalog.Device",
        pop=pop)
    baker.make(
        "catalog.DeviceCapability",
        device=device,
        media_type=MediaType.TYPE_10GBASE_LR.value,
        availability_count=100)
    contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=consuming_account)
    impl_assign = baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))

    product_offering = baker.make(
        "catalog.ConnectionProductOffering",
        physical_media_type=MediaType.TYPE_10GBASE_LR,
        physical_port_speed=10000,
        handover_pop=pop,
        cross_connect_initiator="subscriber")
    connection = baker.make(
        "config.Connection",
        product_offering=product_offering,
        scoping_account=account,
        billing_account=billing_account,
        managing_account=managing_account,
        consuming_account=consuming_account)


    port = ports_svc.allocate_port(
        scoping_account=account,
        connection=connection)

    assert port
    assert port.pk



@pytest.mark.django_db
def test_allocate_port_no_capacity():
    """Test port allocation"""
    account = baker.make(
        "crm.Account")
    managing_account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"),
        scoping_account=account)
    consuming_account = baker.make(
        "crm.Account",
        scoping_account=account)
    pop = baker.make("catalog.PointOfPresence")
    device = baker.make(
        "catalog.Device",
        pop=pop)
    baker.make(
        "catalog.DeviceCapability",
        device=device,
        media_type=MediaType.TYPE_10GBASE_LR.value,
        availability_count=0)
    product_offering = baker.make(
        "catalog.ConnectionProductOffering",
        handover_pop=pop,
        physical_media_type=MediaType.TYPE_10GBASE_LR,
        physical_port_speed=10000,
        cross_connect_initiator="subscriber")
    connection = baker.make(
        "config.Connection",
        product_offering=product_offering,
        scoping_account=account,
        managing_account=managing_account,
        billing_account=managing_account,
        consuming_account=consuming_account)

    with pytest.raises(ports_svc.PortUnavailable):
        ports_svc.allocate_port(connection=connection)


@pytest.mark.django_db
def test_release_port():
    """Release a port"""
    port = baker.make("config.Port")
    ports_svc.release_port(
        port=port)

    assert port.state == State.DECOMMISSIONED


@pytest.mark.django_db
def test_release_port_in_use():
    """Release a port while in use"""
    connection = baker.make(
        "config.Connection")
    port = baker.make(
        "config.Port",
        connection=connection)

    with pytest.raises(ports_svc.PortInUse):
        ports_svc.release_port(
            port=port)


@pytest.mark.django_db
def test_get_ports():
    """Test get all ports for accounts"""
    account_a = baker.make(
        "crm.Account")
    account_b = baker.make(
        "crm.Account")

    port_a = baker.make(
        "config.Port",
        scoping_account=account_a)
    port_b = baker.make(
        "config.Port",
        scoping_account=account_b,
        _fill_optional=True)

    ports = ports_svc.get_ports(
        scoping_account=account_a)

    assert port_a in ports
    assert not port_b in ports


@pytest.mark.django_db
def test_get_ports_filtered():
    """Test get all ports for accounts"""
    account = baker.make(
        "crm.Account")
    port_a = baker.make(
        "config.Port",
        scoping_account=account,
        external_ref="port-a:customer:ff06b5",
        _fill_optional=True)

    port_b = baker.make(
        "config.Port",
        scoping_account=account,
        _fill_optional=True)


    # Filter by connection id:
    ports = ports_svc.get_ports(
        scoping_account=account,
        filters={"connection": port_b.connection.id})

    assert port_b in ports
    assert port_a not in ports

    # Or external ref...
    ports = ports_svc.get_ports(
        scoping_account=account,
        filters={"external_ref": port_a.external_ref})

    assert port_a in ports
    assert port_b not in ports


@pytest.mark.django_db
def test_get_ports_exclude_archived():
    """Test get all ports for accounts"""
    account = baker.make(
        "crm.Account")
    port_a = baker.make(
        "config.Port",
        state=State.ARCHIVED,
        scoping_account=account,
        _fill_optional=True)

    port_b = baker.make(
        "config.Port",
        state=State.PRODUCTION,
        scoping_account=account)

    # Filter by connection id:
    ports = ports_svc.get_ports(
        scoping_account=account,
        filters={"state__is_not": "archived"})

    assert not port_a in ports
    assert port_b in ports


@pytest.mark.django_db
def test_get_dmearcation_point():
    """Test getting a signle port"""
    account_a = baker.make("crm.Account")
    account_b = baker.make("crm.Account")

    port_a = baker.make(
        "config.Port",
        scoping_account=account_a)
    port_b = baker.make(
        "config.Port",
        scoping_account=account_b)

    # Resolve ports
    port = ports_svc.get_port(
        scoping_account=account_a,
        port=str(port_a.id))
    assert port == port_a

    with pytest.raises(ResourceAccessDenied):
        ports_svc.get_port(
            scoping_account=account_a,
            port=port_b.id)


@pytest.mark.django_db
def test_get_dmearcation_point_scope_check():
    account_a = baker.make("crm.Account")
    account_b = baker.make("crm.Account")

    port_a = baker.make(
        "config.Port",
        scoping_account=account_a)

    port_b = baker.make(
        "config.Port",
        scoping_account=account_b)

    # We pass a port object to skip lookup:
    # - Managing accounts matches.
    port = ports_svc.get_port(
        port=port_a,
        scoping_account=account_a)
    assert port == port_a

    # - Managing accounts missmatch
    with pytest.raises(ResourceAccessDenied):
        ports_svc.get_port(
            scoping_account=account_a,
            port=port_b)



@pytest.mark.django_db
def test_port_can_join_connection():
    """Test connect port preflight check"""
    connection = baker.make("config.Connection")
    port = baker.make(
        "config.Port",
        state=State.ALLOCATED)

    # This should work
    ports_svc.port_can_join_connection(
        port=port,
        connection=connection)


@pytest.mark.django_db
def test_port_can_join_connection__in_use():
    """Test connect port preflight check"""
    connection = baker.make("config.Connection")
    port = baker.make(
        "config.Port",
        state=State.ALLOCATED,
        connection=baker.make("config.Connection"))

    # We should get port in use
    with pytest.raises(PortInUse):
        ports_svc.port_can_join_connection(
            port=port,
            connection=connection)


@pytest.mark.django_db
def test_port_can_join_connection__not_ready():
    """Test connect port preflight check: state error"""
    connection = baker.make("config.Connection")
    port = baker.make("config.Port",
        state=State.PRODUCTION)

    with pytest.raises(PortNotReady):
        ports_svc.port_can_join_connection(
            port=port,
            connection=connection)


@pytest.mark.django_db
def test_port_can_join_connection__incompatible():
    """Test connect port preflight check: incompatible with connection"""
    connection = baker.make("config.Connection")
    port = baker.make("config.Port",
        state=State.ALLOCATED)

    # Create connected port with different pop
    baker.make("config.Port",
        connection=connection)

    with pytest.raises(PortUnavailable):
        ports_svc.port_can_join_connection(
            port=port,
            connection=connection)

