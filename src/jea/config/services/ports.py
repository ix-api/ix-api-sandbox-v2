
"""
Ports Service
---------------

Handles allocation and releasing of ports.
"""

import re
from typing import (
    Optional,
    Iterable,
)

from rest_framework.exceptions import APIException
from django.db import transaction
from django.utils import text as text_utils

from jea.exceptions import (
    ResourceAccessDenied,
    ValidationError,
)
from jea.stateful.models import State
from jea.catalog.services import pops as pops_svc
from jea.catalog.models import (
    CrossConnectInitiator,
)
from jea.crm.services import (
    contacts as contacts_svc,
    accounts as accounts_svc,
)
from jea.config.filters import PortFilter
from jea.config.models import (
    Port,
    Connection,
)
from jea.config.services import connections as connections_svc
from jea.config.exceptions import (
    PortUnavailable,
    PortInUse,
    PortNotReady,
)

RE_MATCH_SPEED = re.compile(r"(\d+)(\w)?BASE")


class UnknownMediaTypeError(ValueError):
    """The provided media type is invalid"""


def max_speed_for_media_type(media_type: str) -> int:
    """
    Get max speed from media type

    :raises UnknownMediaTypeError: when media type could
        not be parsed
    """
    match = RE_MATCH_SPEED.match(media_type.upper())
    if not match:
        raise UnknownMediaTypeError

    matched_speed, prefix = match.groups()
    try:
        speed = int(matched_speed)
    except ValueError:
        raise UnknownMediaTypeError

    multipliers = {
        None: 1,
        "G": 1000,
        "T": 1000000,
    }
    multiplier = multipliers.get(prefix)
    if multiplier is None:
        raise UnknownMediaTypeError

    return speed * multiplier


def make_port_name(account=None) -> str:
    """Make a port name"""
    # When a account is present, limit to account owned ports and
    # include account in the naming schema: port-account-name-32
    if account:
        counter = account.consumed_ports.count() + 1
        tag = text_utils.slugify(account.name)[:42]
        return f"port-{tag}-{counter}"

    # Fallback
    counter = Connection.objects.count() + 1
    return f"port-{counter}"


#
# Service Implementation
#

@transaction.atomic
def allocate_port(
        connection=None,
        scoping_account=None) -> Port:
    """
    Allocate a port for a given connection.

    :scoping_account: The scope account
    :param connection: The port will be allocated for a given
        connection and its associated product_offering.
        The latter determines the device and speed.

    :returns: A freshly allocated port on a device.
    """
    billing_account = connection.billing_account
    consuming_account = connection.consuming_account

    # Make sure we can use this account for billing
    accounts_svc.assert_billing_information(billing_account)

    product_offering = connection.product_offering
    media_type = product_offering.physical_media_type.value

    # Determine port speed if only the type is given
    port_name = make_port_name(account=consuming_account)

    # Determine PointOfPresence from service metro area network
    # We just use the first pop for now.
    pop = product_offering.handover_pop

    # Check if the requested port is in priciple available
    available = pops_svc.get_media_type_availability(
        pop=pop,
        media_type=media_type)
    if not available:
        raise PortUnavailable(
            ("There is no free slot available at the given pop."),
            field="pop")


    # TODO for now get the first available device
    # present at pop. This will most likely change in the
    # future when we discuss ordering ports and so on.
    device = pop.devices.first()

    # Okay - Looking good. Let's create a new port!
    port = Port(
        state=State.REQUESTED,
        connection=connection,
        name=port_name,
        media_type=media_type,
        speed=product_offering.physical_port_speed,
        scoping_account=scoping_account,
        managing_account=connection.managing_account,
        consuming_account=connection.consuming_account,
        billing_account=connection.billing_account,
        device=device,
        purchase_order=connection.purchase_order,
        external_ref=connection.external_ref,
        contract_ref=connection.contract_ref)
    port.save()

    # Add role assignments
    port.role_assignments.set(connection.role_assignments.all())

    return port


def release_port(
        port=None,
        scoping_account=None):
    """
    Deallocate a port. This will fail if the port
    is still used in a connection.

    :param port: The port to release
    """
    port = get_port(
        scoping_account=scoping_account,
        port=port)

    if port.connection:
        raise PortInUse

    # Remove port
    prev_state = port.state
    port.state = State.DECOMMISSIONED
    port.save()

    return port


def archive_port(
        scoping_account=None,
        port=None):
    """
    Archive port. This is only okay if the
    state is decommissioned.
    Archived ports are not included in the default set.

    :param port: The port to archive.
    """
    port = get_port(
        scoping_account=scoping_account,
        port=port)

    if not port.state == State.DECOMMISSIONED:
        raise PortInUse

    prev_state = port.state
    port.state = State.ARCHIVED
    port.save()

    return port


def get_ports(
        scoping_account=None,
        filters=None) -> Iterable[Port]:
    """
    Get all port for a given managing account.

    :param scoping_account: A account managing ports
    :param filters: A dict with filters from query
    """
    # Get filtered ports query set
    if filters:
        ports = PortFilter(filters).qs
    else:
        ports = Port.objects.all()

    # Apply additional account filtering
    if scoping_account:
        ports = ports.filter(scoping_account=scoping_account)

    return ports


def get_port(
        scoping_account=None,
        port=None) -> Optional[Port]:
    """
    Get a single port. Limit set of valid ports to
    managing account's scope.

    :raises: ResourceAccessDenied

    :param scoping_account: The managing account of the port
    :param port: The port identifier.
    """
    if not port:
        raise ValidationError(
            "A port is required")

    # We have a given port, so no lookup is required
    # however, we perform a check on the scoping:
    if not isinstance(port, Port):
        # Resolve port by id
        port = Port.objects.get(pk=port)

    # Check ownership
    if scoping_account and not \
            port.scoping_account == scoping_account:
        raise ResourceAccessDenied(port)

    return port  # We are done here.


def port_can_join_connection(
        scoping_account=None,
        port=None,
        connection=None):
    """
    Check if a port can join a connection.
      - A port needs to be at the same pop as the others.
      - The state should be just allocated or requested.
        Not in production or decomissioned.

    This will raise a validation error.

    :raises PortInUse: In case the port is already
        in a connection.
    :raises PortUnavailable: In case the port can not
        join the connection.
    """
    port = get_port(
        port=port,
        scoping_account=scoping_account)
    connection = connections_svc.get_connection(
        connection=connection,
        scoping_account=scoping_account)

    # Check if we are in use
    if port.connection and port.connection != connection:
        raise PortInUse(field="connection")

    # Check State
    if port.state not in [State.ALLOCATED, State.REQUESTED]:
        raise PortNotReady(field="connection")

    # Check related ports
    connected_port = connection.ports.first()
    if connected_port:
        # Check if we are at the same pop
        if port.device.pop_id != \
                connected_port.device.pop_id:
            raise PortUnavailable(
                ("The port is located a different pop and can not "
                 "join the connection."),
                field="connection")


