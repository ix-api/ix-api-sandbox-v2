
"""
Test Connections Service
"""

import pytest
from model_bakery import baker

from jea.exceptions import ResourceAccessDenied
from jea.catalog.models import (
    MediaType,
)
from jea.config.exceptions import (
    VLanInUse,
)
from jea.stateful.models import State
from jea.config.exceptions import (
    ConnectionModeUnavailable,
    PortInUse,
)
from jea.config.models import (
    ConnectionMode,
)
from jea.crm.services import contacts as contacts_svc
from jea.config.services import (
    connections as connections_svc,
)

@pytest.mark.django_db
def test_get_connections():
    """Get a list of connections"""
    account_a = baker.make("crm.Account")
    account_b = baker.make("crm.Account")
    connection_a = baker.make(
        "config.Connection",
        scoping_account=account_a)
    connection_b = baker.make(
        "config.Connection",
        scoping_account=account_b)

    connections = connections_svc.get_connections(
        scoping_account=account_a)
    assert connection_a in connections
    assert not connection_b in connections

    connections = connections_svc.get_connections(
        scoping_account=account_b)
    assert connection_b in connections
    assert not connection_a in connections


@pytest.mark.django_db
def test_get_connection():
    """Get a single connection and check ownership"""
    account_a = baker.make("crm.Account")
    account_b = baker.make("crm.Account")
    connection_a = baker.make(
        "config.Connection",
        scoping_account=account_a)
    connection_b = baker.make(
        "config.Connection",
        scoping_account=account_b)

    connection = connections_svc.get_connection(
        connection=str(connection_a.id),
        scoping_account=account_a)
    assert connection == connection_a

    # Lookup with accounts filtering
    # with pytest.raises(Connection.DoesNotExist):
    with pytest.raises(ResourceAccessDenied):
        connections_svc.get_connection(
            connection=str(connection_b.id),
            scoping_account=account_a)

    # No lookup but with ownership check
    with pytest.raises(ResourceAccessDenied):
        connections_svc.get_connection(
            connection=connection_b,
            scoping_account=account_a)


@pytest.mark.django_db
def test_decommission_port_reservation():
    """Remove a PortReservation"""
    connection = baker.make(
        "config.Connection")
    port = baker.make(
        "config.Port",
        connection=connection)
    port_reservation = baker.make(
        "config.PortReservation",
        connection=connection,
        port=port)

    connections_svc.decommission_port_reservation(
        port_reservation=port_reservation)


@pytest.mark.django_db
def test_update_port_reservation():
    """Update a PortReservation"""
    port_reservation = baker.make("config.PortReservation")

    port_reservation = connections_svc.update_port_reservation(
        port_reservation=port_reservation,
        port_reservation_update={
            "purchase_order": "new-PO",
            "external_ref": "ref123",
            "subscriber_side_demarc": "demarc1",
        })

    assert port_reservation.purchase_order == "new-PO"
    assert port_reservation.external_ref == "ref123"
    assert port_reservation.subscriber_side_demarc == "demarc1"


@pytest.mark.django_db
def test_disconnect_port():
    """Disconnect a port."""
    connection = baker.make(
        "config.Connection")
    port = baker.make(
        "config.Port",
        connection=connection)

    connections_svc.disconnect_port(
        port=port)

    assert not port.connection


@pytest.mark.django_db
def test_create_connection_subscriber_initiated():
    """
    Create a connection with a subscriber initiated
    cross connect.
    """
    pop = baker.make(
        "catalog.PointOfPresence")
    device = baker.make(
        "catalog.Device",
        pop=pop)
    baker.make(
        "catalog.DeviceCapability",
        device=device,
        media_type=MediaType.TYPE_10GBASE_LR.value,
        availability_count=100)
    product = baker.make(
        "catalog.ConnectionProductOffering",
        handover_pop=pop,
        physical_port_speed=1000,
        cross_connect_initiator="subscriber")
    account = baker.make(
        "crm.Account")
    managing_account = baker.make(
        "crm.Account",
        scoping_account=account)
    consuming_account = baker.make(
        "crm.Account",
        scoping_account=account)
    billing_account = baker.make(
        "crm.Account",
        scoping_account=account,
        billing_information=baker.make("crm.BillingInformation"))
    contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=consuming_account)
    impl_assignment = baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))

    # Create connections
    connection = connections_svc.create_connection(
        scoping_account=account,
        connection_request={
            "mode": ConnectionMode.MODE_STANDALONE,
            "managing_account": managing_account,
            "consuming_account": consuming_account,
            "billing_account": billing_account,
            "purchase_order": "PO-23",
            "product_offering": product,
            "port_quantity": 2,
            "role_assignments": [impl_assignment],
        })

    assert connection


@pytest.mark.django_db
def test_assert_conneciton_can_assume_mode__blank():
    """Check extended mode validation: blank connection"""
    connection = baker.make("config.Connection")
    # This connection should be able to assume
    # any mode, as no ports are assigned.
    for mode in ConnectionMode:
        connections_svc.assert_connection_can_assume_mode(
            connection, mode)

@pytest.mark.django_db
def test_assert_conneciton_can_assume_mode__ports():
    """Check extended mode validation: with ports"""
    connection = baker.make("config.Connection")

    # Assign two ports
    baker.make(
        "config.Port",
        connection=connection)
    baker.make(
        "config.Port",
        connection=connection)

    # This connection should not be able to assume
    # the standalone mode.
    allowed_modes = set(ConnectionMode) - {ConnectionMode.MODE_STANDALONE}
    for mode in allowed_modes:
        connections_svc.assert_connection_can_assume_mode(
            connection, mode)

    # The standalone configuration should fail
    with pytest.raises(ConnectionModeUnavailable):
        connections_svc.assert_connection_can_assume_mode(
            connection, ConnectionMode.MODE_STANDALONE)


@pytest.mark.django_db
def test_update_connection():
    """Update the settings and properties of a connection"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    contact = baker.make(
        "crm.Contact",
        consuming_account=account,
        managing_account=account,
        scoping_account=account)
    impl_assignment = baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=contacts_svc.get_default_role("implementation"))
    connection = baker.make(
        "config.Connection",
        scoping_account=account)

    # Create connections
    connection = connections_svc.update_connection(
        scoping_account=account,
        connection=connection,
        connection_update={
            "mode": ConnectionMode.MODE_STANDALONE,
            "managing_account": account.pk,
            "consuming_account": account.pk,
            "billing_account": account.pk,
            "purchase_order": "PO-23",
            "role_assignments": [impl_assignment.pk],
        })

    assert connection


@pytest.mark.django_db
def test_reserve_port():
    """Test creating a port reservation"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))

    pop = baker.make("catalog.PointOfPresence")
    device = baker.make(
        "catalog.Device",
        pop=pop)
    baker.make(
        "catalog.DeviceCapability",
        device=device,
        media_type=MediaType.TYPE_10GBASE_LR.value,
        availability_count=100)
    product_offering = baker.make(
        "catalog.ConnectionProductOffering",
        handover_pop=pop,
        physical_port_speed=10000,
        physical_media_type=MediaType.TYPE_10GBASE_LR,
        cross_connect_initiator="subscriber")
    connection = baker.make(
        "config.Connection",
        product_offering=product_offering,
        billing_account=account,
        managing_account=account,
        consuming_account=account)

    reservation = connections_svc.reserve_port(
        port_reservation_request={
            "connection": connection,
        })

    assert reservation

    # Now demarc information should be required
    product_offering.cross_connect_initiator = \
        connections_svc.CrossConnectInitiator.EXCHANGE
    product_offering.save()
    with pytest.raises(connections_svc.ValidationError):
        connections_svc.reserve_port(
            port_reservation_request={
                "connection": connection,
            })

    reservation = connections_svc.reserve_port(
        port_reservation_request={
            "connection": connection,
            "subscriber_side_demarc": "foo",
        })

    assert reservation

@pytest.mark.django_db
def test_decommission_connection():
    """Destroy a connection and release all ports"""
    connection = baker.make(
        "config.Connection")
    port = baker.make(
        "config.Port",
        connection=connection)

    connections_svc.decommission_connection(connection=connection)

    port.refresh_from_db()
    assert not port.connection

    connection.refresh_from_db()
    assert connection.state == State.DECOMMISSIONED


@pytest.mark.django_db
def test_get_vlan_configs():
    """Test getting all vlan configs on a connection"""
    conn = baker.make("config.Connection")
    vc1 = baker.make("config.PortVLanConfig")
    vc2 = baker.make("config.QinQVLanConfig")
    vc3 = baker.make("config.Dot1QVLanConfig")
    vc4 = baker.make("config.VLanConfig") # Base class

    # The following thing is only possible through
    # direct model manipulation. Constraint checks are not enforced.
    baker.make(
        "config.NetworkServiceConfig",
        connection=conn,
        vlan_config=vc1)
    baker.make(
        "config.NetworkServiceConfig",
        connection=conn,
        vlan_config=vc2)
    baker.make(
        "config.NetworkServiceConfig",
        connection=conn,
        vlan_config=vc3)
    baker.make(
        "config.NetworkServiceConfig",
        connection=conn,
        vlan_config=vc4)

    configs = connections_svc.get_vlan_configs(
        connection=conn)

    assert vc1 in configs
    assert vc2 in configs
    assert vc3 in configs
    assert not vc4 in configs


@pytest.mark.django_db
def test_assert_outer_vlan_uniqueness():
    """Test assert uniqueness constraint"""
    conn = baker.make("config.Connection")
    vconf = baker.make("config.Dot1QVLanConfig", vlan=23)
    baker.make(
        "config.P2MPNetworkServiceConfig",
        connection=conn,
        vlan_config=vconf,
    )
    vconf = baker.make("config.QinQVLanConfig", outer_vlan=42)
    baker.make(
        "config.P2MPNetworkServiceConfig",
        connection=conn,
        vlan_config=vconf,
    )

    with pytest.raises(VLanInUse):
        connections_svc.assert_outer_vlan_uniqueness(
            outer_vlan=23,
            connection=conn,
        )

    with pytest.raises(VLanInUse):
        connections_svc.assert_outer_vlan_uniqueness(
            outer_vlan=42,
            connection=conn,
        )

