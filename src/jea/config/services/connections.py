
"""
Connections Service
-------------------

Create new connections with multiple ports
or disconnect ports.

In general: Configure connections.
"""

from typing import Iterable, Optional

from django.utils import text as text_utils
from django.db import transaction

from jea.exceptions import (
    ResourceAccessDenied,
    ValidationError,
)
from jea.stateful.models import State
from jea.crm.services import (
    accounts as accounts_svc,
    contacts as contacts_svc,
)
from jea.catalog.models import (
    ConnectionProductOffering,
    CrossConnectInitiator,
)
from jea.config.models import (
    Connection,
    ConnectionMode,
    Port,
    PortState,
    PortReservation,
    QinQVLanConfig,
    Dot1QVLanConfig,
    LACPTimeout,
)
from jea.config.services import ports as ports_svc
from jea.config.filters import (
    ConnectionFilter,
    PortReservationFilter,
)
from jea.config.exceptions import (
    ConnectionModeUnavailable,
    VLanInUse,
)


def make_connection_name(account=None) -> str:
    """Make connection name"""
    # When a account is present, limit to account connections and
    # include account in the naming schema: conn-account-name-32
    if account:
        counter = account.consumed_connections.count() + 1
        tag = text_utils.slugify(account.name)[:42]
        return f"conn-{tag}-{counter}"

    # Fallback
    counter = Connection.objects.count() + 1
    return f"conn-{counter}"


def get_connections(
        scoping_account=None,
        filters=None) -> Iterable[Connection]:
    """
    Get connections scoped to managing account.
    Either or both might be None, in which case the filter is
    not applied and all connection objects are considered.

    :param scoping_account: Manages connections
    :param filters: A mapping of string filter params
    """
    if filters:
        connections = ConnectionFilter(filters).qs
    else:
        connections = Connection.objects.all()

    # Apply ownership filteres
    if scoping_account:
        connections = connections.filter(
            scoping_account=scoping_account)

    return connections


def get_connection(
        connection=None,
        scoping_account=None) -> Connection:
    """
    Get a connection and perform ownership check on it,
    in case billing- or owning account are provided.

    :param connection: The connection to load
    :param scoping_account: The account managing the connection

    :raises ResourceAccessDenied: When the managing account
        lacks access rights.
    """
    if not connection:
        raise ValidationError("A connection needs to be provided")

    if not isinstance(connection, Connection):
        connection = Connection.objects.get(pk=connection)

    # We don't need to fetch the connection from the
    # database, however we need to perform a ownership test
    if scoping_account and not \
            connection.scoping_account == scoping_account:
        raise ResourceAccessDenied(connection)

    # Everythings fine, just pass back the connection
    return connection


def get_port_reservations(
        scoping_account=None,
        filters=None) -> Iterable[PortReservation]:
    """
    Get PortReservations.

    :param scoping_account: Manages all port reservations 
    :param filters: A mapping of string filter params
    """
    if filters:
        port_reservations = PortReservationFilter(filters).qs
    else:
        port_reservations = PortReservation.objects.all()

    # Apply ownership filteres
    if scoping_account:
        port_reservations = port_reservations.filter(
            scoping_account=scoping_account)

    return port_reservations 


def get_port_reservation(
        port_reservation=None,
        scoping_account=None) -> PortReservation:
    """
    Get a port reservation

    :param port_reservation: The port reservation to load
    :param scoping_account: The scope giving account
    """
    if not port_reservation:
        raise ValidationError("a port reservation is required")
    if not isinstance(port_reservation, PortReservation):
        port_reservation = PortReservation.objects.get(
            pk=port_reservation)

    if scoping_account and not \
            port_reservation.scoping_account == scoping_account:
        raise ResourceAccessDenied(port_reservation)

    return port_reservation


@transaction.atomic
def reserve_port(
        port_reservation_request=None,
        scoping_account=None,
    ):
    """
    Create a PortReservation for a given connection and
    try to allocate a port.

    :param port_reservation_request: A port reservation request, including
        a connection and a subscriber side demarc on demand.
    """
    connection = get_connection(
        connection=port_reservation_request["connection"],
        scoping_account=scoping_account)
    product_offering = connection.product_offering
    xc_initiator = product_offering.cross_connect_initiator
    subscriber_side_demarc = port_reservation_request.get("subscriber_side_demarc")
    if not subscriber_side_demarc:
        subscriber_side_demarc = ""
    if subscriber_side_demarc:
        subscriber_side_demarc = subscriber_side_demarc.strip()

    # In case the cross connect initiator is the exchange,
    # the subscriber must provide demarc information.
    if xc_initiator == CrossConnectInitiator.EXCHANGE and \
        subscriber_side_demarc == "":
        raise ValidationError(
            "a subscriber_side_demarc is required for this "
            "product-offering", field="subscriber_side_demarc")

    state = State.REQUESTED

    # Allocate a port for the connection
    try:
        port = ports_svc.allocate_port(
            connection=connection,
            scoping_account=scoping_account)
    except ports_svc.PortUnavailable:
        port = None

    if port:
        port.state = State.PRODUCTION
        port.operational_state = PortState.UP
        port.save()

    reservation = PortReservation(
        scoping_account=scoping_account,
        state=state,
        purchase_order=port_reservation_request.get(
            "purchase_order", ""),
        contract_ref=port_reservation_request.get("contract_ref"),
        subscriber_side_demarc=subscriber_side_demarc,
        connecting_party=port_reservation_request.get("connecting_party"),
        connection=connection,
        port=port)

    if port:
        reservation.exchange_side_demarc = port.name

    reservation.save()

    return reservation


def update_port_reservation(
        port_reservation=None,
        port_reservation_update=None,
        scoping_account=None,
    ):
    """Update a port reservation"""
    port_reservation = get_port_reservation(
        port_reservation=port_reservation,
        scoping_account=scoping_account)

    # Update optional properties
    update_keys = port_reservation_update.keys()
    if "purchase_order" in update_keys:
        port_reservation.purchase_order = port_reservation_update[
            "purchase_order"]
    if "external_ref" in update_keys:
        port_reservation.external_ref = port_reservation_update[
            "external_ref"]

    if "subscriber_side_demarc" in update_keys:
        demarc = port_reservation_update["subscriber_side_demarc"]
        if port_reservation.port \
            and demarc != port_reservation.subscriber_side_demarc:
            raise ValidationError(
                "demarc can not be changed after port allocation",
                field="subscriber_side_demarc")
        port_reservation.subscriber_side_demarc = demarc

    port_reservation.save()

    return port_reservation


def decommission_port_reservation(
        port_reservation=None,
        scoping_account=None):
    """
    Remove a port reservation.
    """
    port_reservation = get_port_reservation(
        port_reservation=port_reservation,
        scoping_account=scoping_account)

    if port_reservation.port:
        disconnect_port(
            port=port_reservation.port,
            scoping_account=scoping_account)
    else:
        port_reservation.delete()

    return port_reservation


def disconnect_port(
        scoping_account=None,
        port=None) -> Port:
    """
    Remove a port from a connection.

    :param scoping_account: The account used for scope checking
    :param port: The port to disconnect
    """
    # Get port
    port = ports_svc.get_port(
        port=port,
        scoping_account=scoping_account)

    # Check if port is connected
    connection = port.connection
    if not connection:
        return port  # Do nothing

    # Delete PortReservation
    connection.port_reservations.filter(port=port).delete()

    # Disconnect
    port.connection = None
    port.state = State.DECOMMISSIONED

    port.save()

    return port


def create_connection(
        connection_request=None,
        scoping_account=None) -> Connection:
    """
    Create a new connection and allocate ports, according
    to port quantity. Changing the property will result in
    port allocations and decommissioning.

    Reseller connections might be used from a subaccount.

    :param connection_request: The connection request
    :param scoping_account: The account managing the connection

    :returns: A new connection.
    """
    # Check permissions on related objects
    managing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=connection_request["managing_account"])
    consuming_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=connection_request["consuming_account"])
    billing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=connection_request["billing_account"])

    # Assert the product-offering is correct
    product_offering = connection_request["product_offering"]
    if not isinstance(product_offering, ConnectionProductOffering):
        raise ValidationError(
            "The product_offering must be a connection offering",
            field="product_offering")

    port_quantity = connection_request["port_quantity"]
    if not port_quantity:
        raise ValidationError(
            "The port_quantity has to be >= 1",
            field="port_quantity")

    # Make sure billing account can be used for billing
    accounts_svc.assert_billing_information(billing_account)

    # Load contact role assignments and assert presence
    # of required contact roles
    role_assignments = connection_request["role_assignments"]
    role_assignments = [
        contacts_svc.get_role_assignment(
            role_assignment=role_assignment,
            scoping_account=scoping_account)
        for role_assignment in role_assignments]
    contacts_svc.assert_presence_of_contact_roles(
        required_contact_roles=[
            contacts_svc.get_default_role("implementation"),
        ],
        role_assignments=role_assignments,
        scoping_account=scoping_account)

    # Prepare mode and timeout
    connection_mode = connection_request["mode"]
    lacp_timeout = None
    if connection_mode == ConnectionMode.MODE_LACP:
        lacp_timeout = connection_request.get(
            "lacp_timeout",
            LACPTimeout.TIMEOUT_SLOW)

    # Connections are named by us
    connection_name = make_connection_name(
        account=consuming_account)

    # Commercial attributes
    external_ref = connection_request.get("external_ref")
    contract_ref = connection_request.get("contract_ref")
    purchase_order = connection_request.get("purchase_order", "")

    connecting_party = connection_request.get("connecting_party")
    subscriber_demarcs = connection_request.get("subscriber_side_demarcs")

    # We not have everything required to create a new connection,
    # but in case anything goes wrong with assigning the port
    # to a connection, we want to be able to rollback.
    connection = Connection(
        name=connection_name,
        state=State.REQUESTED,
        mode=connection_mode,
        lacp_timeout=lacp_timeout,
        product_offering=product_offering,
        scoping_account=scoping_account,
        managing_account=managing_account,
        consuming_account=consuming_account,
        billing_account=billing_account,
        pop=product_offering.handover_pop,
        external_ref=external_ref,
        contract_ref=contract_ref,
        purchase_order=purchase_order)

    if subscriber_demarcs:
        connection.subscriber_side_demarcs = subscriber_demarcs
    else:
        subscriber_demarcs = []

    connection.save()

    # Assign contact roles to connection
    connection.role_assignments.set(role_assignments)


    # Create port reservations
    for n in range(port_quantity):
        demarc = None
        if subscriber_demarcs:
            demarc = subscriber_demarcs[n % len(subscriber_demarcs)]
        reserve_port(
            port_reservation_request={
                "connection": connection,
                "purchase_order": purchase_order,
                "contract_ref": contract_ref,
                "connecting_party": connecting_party,
                "subscriber_side_demarc": demarc,
            },
            scoping_account=scoping_account)

    if connection.ports.count() == port_quantity:
        connection.state = State.PRODUCTION

    connection.save()

    return connection


def assert_connection_can_assume_mode(
        connection: Connection,
        mode: ConnectionMode):
    """
    Check that a connection is able to assume a mode:
    E.g. a lag can only be change to standalone, if there
    is only one port connected.

    :raises ConnectionModeUnavailable:
    """
    # Check modes:
    if mode == ConnectionMode.MODE_STANDALONE:
        if connection.ports.count() > 1:
            raise ConnectionModeUnavailable(
                "There is more than one port attached to the connection")

    # All other should be ok


@transaction.atomic
def update_connection(
        scoping_account=None,
        connection=None,
        connection_update=None) -> Connection:
    """Update a given connection"""
    if not connection_update:
        raise ValidationError("update data for connection is missing")

    update_fields = connection_update.keys()

    # Load connection and check permissions
    connection = get_connection(
        connection=connection,
        scoping_account=scoping_account)

    # Allright. Let's see what we can update
    # Ownership: Billing and owning account
    if "managing_account" in update_fields:
        managing_account = accounts_svc.get_account(
            account=connection_update["managing_account"],
            scoping_account=scoping_account)
        connection.managing_account = managing_account

    if "consuming_account" in update_fields:
        consuming_account = accounts_svc.get_account(
            account=connection_update["consuming_account"],
            scoping_account=scoping_account)
        connection.consuming_account = consuming_account

    if "billing_account" in update_fields:
        billing_account = accounts_svc.get_account(
            account=connection_update["billing_account"],
            scoping_account=scoping_account)
        accounts_svc.assert_billing_information(billing_account)
        connection.billing_account = billing_account

    # Connection Mode and LACP timeout
    mode = connection_update.get("mode")
    lacp_timeout = connection_update.get(
        "lacp_timeout", connection.lacp_timeout)

    if "mode" in update_fields:
        assert_connection_can_assume_mode(connection, mode)
        if mode == ConnectionMode.MODE_LACP:
            if not lacp_timeout:
                raise ValidationError(
                    "A valid lacp timeout (slow, fast) is required",
                    field="lacp_timeout")
        else:
            lacp_timeout = None

        connection.mode = mode
        connection.lacp_timeout = lacp_timeout

    if "lacp_timeout" in update_fields:
        connection.lacp_timeout = lacp_timeout

    # Implementation contacts:
    # Check ownership on implementation contacts
    if "role_assignments" in update_fields:
        assignments = connection_update["role_assignments"]
        assignments = [
            contacts_svc.get_role_assignment(
                role_assignment=assignment,
                scoping_account=scoping_account)
            for assignment in assignments]

        # Check for presence of required accounts
        contacts_svc.assert_presence_of_contact_roles(
            required_contact_roles=[
                contacts_svc.get_default_role("implementation"),
            ],
            role_assignments=assignments,
            scoping_account=scoping_account)

        connection.role_assignments.set(assignments)

    # Purchase Order / External Ref / Contract Ref
    if "purchase_order" in update_fields:
        connection.purchase_order = connection_update["purchase_order"]
    if "external_ref" in update_fields:
        connection.external_ref = connection_update["external_ref"]
    if "contract_ref" in update_fields:
        connection.contract_ref = connection_update["contract_ref"]

    # Persist changes and trigger state changes
    connection.save()

    return connection


@transaction.atomic
def decommission_connection(
        scoping_account=None,
        connection=None):
    """
    Decommission a connection.

    Release all connected ports and then archive
    this connection.

    When accounts are provided, they will be used for scope checking.

    :param scoping_account: The account managing the connection
    :param connection: The connection to dissolve.
    """
    # Perform scope checking. In case the accounts do not match,
    # the object does not exist in the requested scope.
    connection = get_connection(
        scoping_account=scoping_account,
        connection=connection)

    # Disconnect all ports
    for port in connection.ports.all():
        disconnect_port(
            scoping_account=scoping_account,
            port=port)

    # Archive connection
    prev_state = connection.state
    connection.state = State.DECOMMISSIONED
    connection.save()

    return connection


def get_vlan_configs(
        scoping_account=None,
        connection=None):
    """
    Get all vlan configurations active on the connection

    :param scoping_account: The account context
    :param connection: A connection
    """
    # Perform scope checking. In case the accounts do not match,
    # the object does not exist in the requested scope.
    connection = get_connection(
        scoping_account=scoping_account,
        connection=connection)

    # Get all derived configs, that are not the base class
    vlan_configs = [
        c.vlan_config
        for c in connection.network_service_configs.exclude(
            state=State.DECOMMISSIONED)
        if getattr(c.vlan_config, "__polymorphic_type__", False)]

    return vlan_configs


def assert_outer_vlan_uniqueness(
        outer_vlan=None,
        connection=None,
        scoping_account=None):
    """
    Check vlan uniqueness.

    :param scoping_account: A scoping account
    :param vlan: The (outer) vlan number
    :param connection: The connection used for the
        network service configuration
    """
    vlan_configs = get_vlan_configs(
        scoping_account=scoping_account,
        connection=connection)

    # Check if the vlan is in use.
    dot1q_vlan_configs = (c for c in vlan_configs
                          if isinstance(c, Dot1QVLanConfig))
    qinq_vlan_configs = (c for c in vlan_configs
                         if isinstance(c, QinQVLanConfig))

    # Check if the vlan is configured on a different dot1q binding
    if outer_vlan in (c.vlan for c in dot1q_vlan_configs):
        raise VLanInUse(field="vlan_config.vlan")

    # Check if the vlan is configured as an outer tag of a
    # qinq vlan config
    if outer_vlan in (c.outer_vlan for c in qinq_vlan_configs):
        raise VLanInUse(field="vlan_config.outer_vlan")

