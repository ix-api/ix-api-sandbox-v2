
"""
Service Access Model Admins
---------------------------

Provide a basic admin interfaces for access models.
"""

from django.contrib import admin

from jea.admin import site
from jea.config.models import (
    Connection,
    Port,
    ExchangeLanNetworkServiceConfig,
    MP2MPNetworkServiceConfig,
    P2MPNetworkServiceConfig,
    P2PNetworkServiceConfig,
    CloudNetworkServiceConfig,
    RouteServerNetworkFeatureConfig,
)

#
# Connections and Ports
#
class ConnectionAdmin(admin.ModelAdmin):
    pass

class PortAdmin(admin.ModelAdmin):
    pass

#
# Inline Feature Config
#
class RouteServerNetworkFeatureConfigInline(admin.StackedInline):
    model = RouteServerNetworkFeatureConfig
    extra = 0

#
# Service Config
#
class ExchangeLanNetworkServiceConfigAdmin(admin.ModelAdmin):
    inlines = (
        RouteServerNetworkFeatureConfigInline,
    )

class MP2MPNetworkServiceConfigAdmin(admin.ModelAdmin):
    inlines = (
    )

class P2PNetworkServiceConfigAdmin(admin.ModelAdmin):
    pass

class P2MPNetworkServiceConfigAdmin(admin.ModelAdmin):
    pass

class CloudNetworkServiceConfigAdmin(admin.ModelAdmin):
    pass


# Register models
site.register(Connection, ConnectionAdmin)
site.register(Port, PortAdmin)

site.register(ExchangeLanNetworkServiceConfig,
              ExchangeLanNetworkServiceConfigAdmin)
site.register(P2MPNetworkServiceConfig,
              P2MPNetworkServiceConfigAdmin)
site.register(MP2MPNetworkServiceConfig,
              MP2MPNetworkServiceConfigAdmin)
site.register(P2PNetworkServiceConfig,
              ExchangeLanNetworkServiceConfigAdmin)
site.register(CloudNetworkServiceConfig,
              CloudNetworkServiceConfigAdmin)

