
"""
Status Message Creators
"""

from jea.stateful.models import StatusMessage, Severity


MAC_ADDRESS_MISSING = "mac_address_missing"
REQUIRED_NETWORK_FEATURE_MISSING = \
    "required_network_feature_missing"
NETWORK_FEATURE_CONFIG_IN_USE = \
    "network_feature_config_in_use"

PORTS_NOT_PRESENT = "ports_not_present"
PORTS_NOT_PRODUCTION = "ports_not_production"

CONNECTION_NOT_PRODUCTION = "connection_state_not_production"

def mac_address_missing(config):
    """Create mac address missing status"""
    return StatusMessage(
        ref=config,
        severity=Severity.CRITICAL, # Blocker,
        message="At least one active mac address is required",
        tag=MAC_ADDRESS_MISSING)


def network_feature_config_missing(config, feature):
    """Create route server network feature missing status"""
    attrs = {
        "network_feature_type": feature.polymorphic_ctype.name,
        "network_feature_name": feature.name,
        "network_feature_id": feature.pk,
    }
    message = ("network-feature-config missing for required feature: "
               "{network_feature_type} {network_feature_name} "
               "id={network_feature_id}").format(**attrs)

    return StatusMessage(
        ref=config,
        severity=Severity.CRITICAL, # Blocker
        tag=REQUIRED_NETWORK_FEATURE_MISSING,
        attrs=attrs,
        message=message)


def network_feature_config_in_use(config):
    """Network feature config is in use status"""
    attrs = {
    }
    message = ("network-feature-config is still in use: ",
               "create a new feature config to replace the "
               "config for decommissioning.")

    return StatusMessage(
        ref=config,
        severity=Severity.CRITICAL, # Blocker
        tag=NETWORK_FEATURE_CONFIG_IN_USE,
        attrs=attrs,
        message=message)


def connection_has_nonproduction_ports(connection, ports):
    """Some ports in the connection are not in state production"""
    attrs = {
        "ports": [d.pk for d in ports],
    }
    message = ("Not all ports in the connection are "
               "in the PRODUCTION state.")
    return StatusMessage(
        ref=connection,
        severity=Severity.ERROR,
        tag=PORTS_NOT_PRODUCTION,
        attrs=attrs,
        message=message)


def connection_has_no_ports(connection):
    """The connection just does not have any ports (yet)"""
    message = "The connection does not have any ports, yet."

    return StatusMessage(
        ref=connection,
        severity=Severity.WARNING,
        tag=PORTS_NOT_PRESENT,
        attrs={},
        message=message)


def config_connection_not_production(config, connection=None):
    """The referenced connection is not in production"""
    if not connection:
        connection = config.connection

    message = ("Waiting for the connection state to become `production`")
    return StatusMessage(
        ref=config,
        severity=Severity.ERROR,
        tag=CONNECTION_NOT_PRODUCTION,
        attrs={
            "connection_id": connection.pk,
            "network_service_config_id": config.pk,
        },
        message=message)


