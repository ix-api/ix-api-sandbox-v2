
"""
Test status message creation
"""

import pytest
from model_bakery import baker

from jea.config.status import messages

@pytest.mark.django_db
def test_mac_address_missing():
    """Test mac address missing feature"""
    config = baker.make("config.ExchangeLanNetworkServiceConfig")
    status_message = messages.mac_address_missing(config)

    assert status_message


@pytest.mark.django_db
def test_network_feature_config_missing():
    """Test network feature missing message creator"""
    config = baker.make("config.ExchangeLanNetworkServiceConfig")
    feature = baker.make("service.RouteServerNetworkFeature")

    status_message = messages.network_feature_config_missing(
        config, feature)

    assert status_message


