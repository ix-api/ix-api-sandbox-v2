
"""
Status Handler For Network Service Configs
"""

from django.db import transaction

from jea.config.models import (
    NetworkServiceConfig,
    ExchangeLanNetworkServiceConfig,
    P2PNetworkServiceConfig,
    P2MPNetworkServiceConfig,
    MP2MPNetworkServiceConfig,
    NetworkFeatureConfig,
)
from jea.config.status.messages import (
    network_feature_config_missing,
)

@transaction.atomic
def update_network_service_config_status(
        nsc: NetworkServiceConfig) -> NetworkServiceConfig:
    """Update the network service config status"""
    # Clear all status
    nsc.status.clear()

    # Handle general status for all network services
    # configs, like checking for required feature configs
    related_features = nsc.network_service.network_features
    for feature in related_features.filter(required=True):
        # Check presence of network feature config
        feature_config = feature.configs.active.filter(network_service_config=nsc)
        if feature_config.exists():
            continue

        # Add status messages to inform about missing
        # required features.
        network_feature_config_missing(nsc, feature).save()

    return nsc
