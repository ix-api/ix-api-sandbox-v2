"""
StatusReducers tests
"""

import pytest
from model_bakery import baker

from jea.config.status.reducers import (
    update_network_service_config_status,
)

@pytest.mark.django_db
def test_update_network_service_config_status():
    """Test updatating the NFC status"""
    nsc = baker.make(
        "config.ExchangeLanNetworkServiceConfig")
    feature = baker.make(
        "service.RouteServerNetworkFeature",
        network_service=nsc.network_service,
        required=True)

    nsc.status.clear()
    update_network_service_config_status(nsc)

    assert nsc.status.count() > 0

    # Create nfc
    baker.make(
        "config.RouteServerNetworkFeatureConfig",
        network_feature=feature,
        network_service_config=nsc)

    nsc.status.clear()
    update_network_service_config_status(nsc)

    assert nsc.status.count() == 0


