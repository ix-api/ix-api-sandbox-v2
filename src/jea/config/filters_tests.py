
"""
Test access model filters
"""

import pytest
from model_bakery import baker

from jea.config import filters as access_filters


#
# Port Reservations
#
@pytest.mark.django_db
def test_port_reservation_filter__connection():
    """Test filtering for a connection"""
    conn1 = baker.make("config.Connection")
    conn2 = baker.make("config.Connection")

    res1 = baker.make(
        "config.PortReservation",
        connection=conn1)
    res2 = baker.make(
        "config.PortReservation",
        connection=conn2)

    qs = access_filters.PortReservationFilter({
        "connection": conn1.pk,
    }).qs

    assert res1 in qs
    assert not res2 in qs


@pytest.mark.django_db
def test_port_reservation_filter__port():
    """Test filtering for a port"""
    port1 = baker.make("config.Port")
    port2 = baker.make("config.Port")

    res1 = baker.make(
        "config.PortReservation",
        port=port1)
    res2 = baker.make(
        "config.PortReservation",
        port=port2)

    qs = access_filters.PortReservationFilter({
        "port": port1.pk,
    }).qs

    assert res1 in qs
    assert not res2 in qs
#
# Ports
#
@pytest.mark.django_db
def test_port_filter():
    """Just test the filter instanciation"""
    port = baker.make("config.Port")
    ports = access_filters.PortFilter(None).qs
    assert port in ports


@pytest.mark.django_db
def test_port_filter__state():
    """Test port filtering: state"""
    port1 = baker.make(
        "config.Port",
        state="error")

    port2 = baker.make(
        "config.Port",
        state="production")

    # Do some filtering:
    ports = access_filters.PortFilter({
        "state": "error",
    }).qs

    assert port1 in ports
    assert not port2 in ports

    ports = access_filters.PortFilter({
        "state__is_not": "error",
    }).qs

    assert not port1 in ports
    assert port2 in ports


@pytest.mark.django_db
def test_port_filter__relations():
    """Test relations filtering"""
    conn = baker.make("config.Connection")
    port1 = baker.make("config.Port", connection=conn)
    port2 = baker.make("config.Port")

    # Filter
    ports = access_filters.PortFilter({
        "connection": str(conn.pk)
    }).qs

    assert port1 in ports
    assert not port2 in ports

#
# Connections
#
@pytest.mark.django_db
def test_connection_filter():
    """Test filter instanciation"""
    conn = baker.make("config.Connection")
    connections = access_filters.ConnectionFilter().qs

    assert conn in connections


@pytest.mark.django_db
def test_connection_filter__state():
    """Test filtering connection by state"""
    conn1 = baker.make("config.Connection", state="error")
    conn2 = baker.make("config.Connection", state="production")

    connections = access_filters.ConnectionFilter({
        "state": "error",
    }).qs

    assert conn1 in connections
    assert not conn2 in connections

    connections = access_filters.ConnectionFilter({
        "state__is_not": "error",
    }).qs

    assert not conn1 in connections
    assert conn2 in connections


@pytest.mark.django_db
def test_connection_filter__port():
    """Test filtering connections by port"""
    port1 = baker.make("config.Port")
    port2 = baker.make("config.Port")

    conn1 = baker.make(
        "config.Connection",
        ports=[port1])

    conn2 = baker.make(
        "config.Connection",
        ports=[port2])

    # Filter one
    connections = access_filters.ConnectionFilter({
        "port": f"{port1.pk}",
    }).qs

    assert conn1 in connections
    assert not conn2 in connections

    # Filter many
    connections = access_filters.ConnectionFilter({
        "port": f"{port1.pk},{port2.pk}",
    }).qs

    assert conn1 in connections
    assert conn2 in connections


#
# Service Configs
#
@pytest.mark.django_db
def test_network_service_config_filter():
    """Test filter instance"""
    conf = baker.make("config.NetworkServiceConfig")
    configs = access_filters.NetworkServiceConfigFilter().qs

    assert conf in configs


@pytest.mark.django_db
def test_network_service_config_filter__state():
    """Test filtering by state"""
    conf1 = baker.make("config.NetworkServiceConfig", state="error")
    conf2 = baker.make("config.NetworkServiceConfig", state="production")

    configs = access_filters.NetworkServiceConfigFilter({
        "state": "error",
    }).qs

    assert conf1 in configs
    assert not conf2 in configs

    configs = access_filters.NetworkServiceConfigFilter({
        "state__is_not": "error",
    }).qs

    assert not conf1 in configs
    assert conf2 in configs


@pytest.mark.django_db
def test_network_service_config_filter__relations():
    """Test filtering on relations"""
    svc1 = baker.make("service.ExchangeLanNetworkService")
    conn1 = baker.make("config.Connection")

    conf1 = baker.make(
        "config.NetworkServiceConfig",
        network_service=svc1)
    conf2 = baker.make(
        "config.NetworkServiceConfig",
        connection=conn1)

    # Filter by service relation
    configs = access_filters.NetworkServiceConfigFilter({
        "service": svc1.pk
    }).qs

    assert conf1 in configs
    assert not conf2 in configs

    # Filter by connection relation
    configs = access_filters.NetworkServiceConfigFilter({
        "connection": str(conn1.pk),
    }).qs

    assert not conf1 in configs
    assert conf2 in configs

#
# Feature Configs
#
@pytest.mark.django_db
def test_feature_config_filter():
    """Test filter instanciation"""
    conf = baker.make("config.RouteServerNetworkFeatureConfig")
    configs = access_filters.NetworkFeatureConfigFilter().qs
    assert conf in configs


@pytest.mark.django_db
def test_feature_config_filter__state():
    """Test filtering by state"""
    conf1 = baker.make(
        "config.RouteServerNetworkFeatureConfig",
        state="error")
    conf2 = baker.make(
        "config.RouteServerNetworkFeatureConfig",
        state="production")

    configs = access_filters.NetworkFeatureConfigFilter({
        "state": "production",
    }).qs

    assert not conf1 in configs
    assert conf2 in configs

    configs = access_filters.NetworkFeatureConfigFilter({
        "state__is_not": "production",
    }).qs

    assert conf1 in configs
    assert not conf2 in configs


@pytest.mark.django_db
def test_feature_config_filter__type():
    """Test filtering by type"""
    conf1 = baker.make("config.RouteServerNetworkFeatureConfig")
    conf2 = baker.make("config.NetworkFeatureConfig")

    # RS
    configs = access_filters.NetworkFeatureConfigFilter({
        "type": "route_server",
    }).qs
    assert conf1 in configs
    assert not conf2 in configs


@pytest.mark.django_db
def test_feature_config_filter__relations():
    """Test filter by related keys"""
    service_config = baker.make("config.ExchangeLanNetworkServiceConfig")
    feature = baker.make("service.RouteServerNetworkFeature")

    conf1 = baker.make(
        "config.RouteServerNetworkFeatureConfig",
        network_feature=feature)
    conf2 = baker.make(
        "config.RouteServerNetworkFeatureConfig",
        network_service_config=service_config)

    # Filter feature relation
    configs = access_filters.NetworkFeatureConfigFilter({
        "network_feature": str(feature.pk),
    }).qs

    assert conf1 in configs
    assert not conf2 in configs

    # Filter by service configuration
    configs = access_filters.NetworkFeatureConfigFilter({
        "service_config": service_config.pk,
    }).qs

    assert not conf1 in configs
    assert conf2 in configs
