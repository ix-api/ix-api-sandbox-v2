
"""
JEA Access Models Tests
"""

import pytest
from django.db.models import ProtectedError, ObjectDoesNotExist
from model_bakery import baker

from jea.config.models import (
    Connection,
    Port,
    NetworkServiceConfig,
    ExchangeLanNetworkServiceConfig,
    P2PNetworkServiceConfig,
    MP2MPNetworkServiceConfig,
    CloudNetworkServiceConfig,
    NetworkFeatureConfig,
    RouteServerNetworkFeatureConfig,
    QinQVLanConfig,
    Dot1QVLanConfig,
    PortVLanConfig,
)


@pytest.mark.django_db
def test_connection():
    """Test connection model"""
    account = baker.make("crm.Account")

    implementation_role = baker.make("crm.Role", name="implementation")

    contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        managing_account=account,
        consuming_account=account)

    impl_assignment = baker.make(
        "crm.RoleAssignment",
        role=implementation_role,
        contact=contact)

    connection = baker.make(
        Connection,
        scoping_account=account,
        managing_account=account,
        consuming_account=account,
        billing_account=account,
        role_assignments=[impl_assignment])

    # We now have a connection, let's check our relations
    # - Contacts
    assert connection in Connection.objects \
        .filter(role_assignments__contact=contact) \
        .all()

    # - Accounts
    scoping = connection.scoping_account
    assert scoping

    managing = connection.managing_account
    assert managing
    assert connection in managing.managed_connections.all()

    owning = connection.consuming_account
    assert owning
    assert connection in owning.consumed_connections.all()

    billing = connection.billing_account
    assert billing
    assert connection in billing.billed_connections.all()


def test_connection_representations():
    """Test __str__ and __repr__"""
    connection = baker.prepare(Connection)
    assert isinstance(str(connection), str), "Should return a string"
    assert isinstance(repr(connection), str), "Should return a string"


@pytest.mark.django_db
def test_connection_speed():
    """Test the calculated connection speed property"""
    connection = baker.make(Connection)
    # Add ports
    baker.make(Port, connection=connection, speed=5000)
    baker.make(Port, connection=connection, speed=2000)
    baker.make(Port, connection=connection, speed=3000)

    assert connection.speed == 10000, \
        "expected speed to be 10.000"


@pytest.mark.django_db
def test_port_quantity():
    """Test port quantity property"""
    # Create connection and ports
    connection = baker.make(Connection)
    baker.make(Port, connection=connection, speed=5000)
    baker.make(Port, connection=connection, speed=3000)

    assert connection.port_quantity == 2, \
        "expected 2 ports in the connection"


@pytest.mark.django_db
def test_qinq_vlan_config():
    """Test vlan relations"""
    vlan_config = baker.make(
        QinQVLanConfig,
        outer_vlan=42,
        inner_vlan=23)

    assert vlan_config


@pytest.mark.django_db
def test_dot1q_vlan_config():
    """Test vlan relations"""
    vlan_config = baker.make(Dot1QVLanConfig)
    assert vlan_config


@pytest.mark.django_db
def test_port_vlan_config():
    """Test vlan relations"""
    vlan_config = baker.make(PortVLanConfig)
    assert vlan_config



@pytest.mark.django_db
def test_port():
    """Test the portation point model"""
    pop = baker.make("catalog.PointOfPresence")
    device = baker.make("catalog.Device", pop=pop)
    account = baker.make("crm.Account")
    contact = baker.make("crm.Contact")
    role = baker.make("crm.Role")
    assignment = baker.make(
        "crm.RoleAssignment",
        contact=contact,
        role=role)
    connection = baker.make("config.Connection")
    port = baker.make(
        Port,
        connection=connection,
        device=device,
        managing_account=account,
        consuming_account=account,
        scoping_account=account,
        billing_account=account,
        role_assignments=[assignment])

    # Test relations
    assert port in Port.objects.filter(
        device__pop=pop).all()
    assert port in account.managed_ports.all()
    assert port in account.consumed_ports.all()
    assert port in account.billed_ports.all()
    assert port in assignment.ports.all()

    # A portation point needs to be released before the
    # connection is removed.
    with pytest.raises(ProtectedError):
        connection.delete()


def test_port_representation():
    """Test to string and repr methods"""
    port = baker.prepare(Port)
    assert str(port), "Should return a string"
    assert repr(port), "Should return a string"


#
# Test Access Configuration Objects
#

@pytest.mark.django_db
def test_network_service_config():
    """Test polymorphic network service config model"""
    config = baker.make(
        NetworkServiceConfig,
        vlan_config=baker.make("config.PortVLanConfig"))
    managing = config.managing_account
    owning = config.consuming_account
    connection = config.connection

    role = baker.make("crm.Role")
    contact = baker.make("crm.Contact")
    assignment = baker.make(
        "crm.RoleAssignment",
        role=role)
    config.role_assignments.add(assignment)

    # Check references
    assert config in set(managing.managed_networkserviceconfigs.all())
    assert config in set(owning.consumed_networkserviceconfigs.all())
    assert config in set(connection.network_service_configs.all())
    assert config in set(assignment.networkserviceconfigs.all())

    # Check presence of required vlan configuration
    assert config.vlan_config


@pytest.mark.django_db
def test_exchange_lan_network_service_config():
    """Test exchange lan network service config model"""
    config = baker.make(ExchangeLanNetworkServiceConfig)
    ip_address = baker.make("ipam.IpAddress")
    config.ip_addresses.add(ip_address)

    # Check ip address relation
    assert ip_address in set(config.ip_addresses.all())
    assert config == ip_address.exchange_lan_network_service_config

    # Check mac address list
    mac_address = baker.make("ipam.MacAddress",
        managing_account=config.managing_account,
        consuming_account=config.consuming_account)
    config.mac_addresses.add(mac_address)

    assert mac_address in config.mac_addresses.all()


def test_exchange_lan_network_service_config_representation():
    """Test exchange lan network service config representation"""
    config = baker.prepare(ExchangeLanNetworkServiceConfig)
    assert str(config), "Should return a string"
    assert repr(config), "Should return a string"


@pytest.mark.django_db
def test_mp2mp_network_service_config():
    """Test closed usergroup config model"""
    config = baker.make(MP2MPNetworkServiceConfig)

    # Closed usere groups have a mac address list
    mac_address = baker.make("ipam.MacAddress",
        managing_account=config.managing_account,
        consuming_account=config.consuming_account)
    config.mac_addresses.add(mac_address)

    assert mac_address in set(config.mac_addresses.all())


def test_mp2mp_network_service_config_representation():
    """Test mp2mp network service config representation"""
    config = baker.prepare(MP2MPNetworkServiceConfig)
    assert str(config), "Should return a string"
    assert repr(config), "Should return a string"


@pytest.mark.django_db
def test_p2p_network_service_config():
    """Test eline / p2p network service config model"""
    # Nothing special with this model, but it should be
    # at least creatable.
    config = baker.make(P2PNetworkServiceConfig)
    assert config.pk, "Config Object should have been saved"


def test_p2p_network_serivce_config_representation():
    """Test P2P / Eline network service config representation"""
    config = baker.prepare(P2PNetworkServiceConfig)
    assert str(config), "Should return a (non empty) string"
    assert repr(config), "Should return a (non empty) string"


@pytest.mark.django_db
def test_cloud_network_service_config():
    """Test eline network service config model"""
    # Nothing special with this model, but it should be
    # at least creatable.
    config = baker.make(CloudNetworkServiceConfig)
    assert config.pk, "Config Object should have been saved"


def test_cloud_network_service_config_representation():
    """Test representation of config object"""
    config = baker.prepare(CloudNetworkServiceConfig)
    assert str(config), "Should return a (non empty) string"
    assert repr(config), "Should return a (non empty) string"


#
# Test Feature Configuration
#

@pytest.mark.django_db
def test_network_feature_config():
    """Test polymorphic feature config base"""
    account = baker.make("crm.Account")
    service_config = baker.make(ExchangeLanNetworkServiceConfig)
    config = baker.make(NetworkFeatureConfig,
                        network_service_config=service_config,
                        managing_account=account,
                        consuming_account=account)

    # Check relations and reverse relations
    assert config in service_config.network_feature_configs.all()
    assert config in account.managed_networkfeatureconfigs.all()
    assert config in account.consumed_networkfeatureconfigs.all()


@pytest.mark.django_db
def test_route_server_network_feature_config():
    """Test route server feature config model"""
    config = baker.make(RouteServerNetworkFeatureConfig)

    # All relations have been checked in the base model,
    # so we should make sure, that this just saves
    assert config.pk, "A primary key should have been assigned."


def test_route_server_network_feature_config_representation():
    """Test make representation of feature config"""
    config = baker.prepare(RouteServerNetworkFeatureConfig)
    assert str(config), "Should return a (non empty) string"
    assert repr(config), "Should return a (non empty) string"




