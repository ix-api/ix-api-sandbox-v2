
"""
Service Access Models
---------------------

Access to services is granted to accounts through
these models.
"""
from typing import List

import enumfields
from django.db import models
from django.contrib.postgres import fields as postgres_fields
from polymorphic.models import PolymorphicModel
from ixapi_schema.v2.constants.config import (
    BGPSessionType,
    RouteServerSessionMode,
    ConnectionMode,
    LACPTimeout,
    VLanType,
    VLanEthertype,
    P2MPRole,


    NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
    NETWORK_SERVICE_CONFIG_TYPE_P2P,
    NETWORK_SERVICE_CONFIG_TYPE_P2MP,
    NETWORK_SERVICE_CONFIG_TYPE_MP2MP,
    NETWORK_SERVICE_CONFIG_TYPE_CLOUD,

    NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER,

    VLAN_CONFIG_TYPE_DOT1Q,
    VLAN_CONFIG_TYPE_QINQ,
    VLAN_CONFIG_TYPE_PORT,

    P2MP_ROLE_LEAF,
)

from schema.stub import (
    PortState,
    PORT_STATE_DOWN,
    PORT_STATE_UP,
)
from jea.crm.models import (
    OwnableMixin,
    ReferencableMixin,
    ManagedMixin,
    BillableMixin,
    InvoiceableMixin,
    ContactableMixin,
    AccountScopedMixin,
)
from jea.stateful.models import (
    State,
    StatefulMixin,
    StatefulManager,
    StatefulPolymorphicManager,
)
from jea.catalog.models import (
    Device,
)

def default_connection_vlantypes() -> List[VLanType]:
    """Default values for vlan types array in connection"""
    return [VLanType.DOT1Q, VLanType.PORT]


def default_connection_outer_ethertypes() -> List[VLanEthertype]:
    """Default values for outer ethertype array in connection"""
    return [VLanEthertype.E_0x8100, VLanEthertype.E_0x88a8]


class Connection(
        StatefulMixin,
        OwnableMixin,
        ManagedMixin,
        BillableMixin,
        InvoiceableMixin,
        ContactableMixin,
        AccountScopedMixin,
        models.Model):
    """
    A model for connections.

    Connections describe the combination of several ports
    into a, well, connection. This connection can for example
    be a LAG.

    Remember to require a `billing` and `implementation` contact
    in the contacts array when creating.
    """
    name = models.CharField(max_length=255)

    lacp_timeout = enumfields.EnumField(
        LACPTimeout,
        max_length=4,
        null=True)

    mode = enumfields.EnumField(ConnectionMode, max_length=16)

    vlan_types = postgres_fields.ArrayField(
        enumfields.EnumField(VLanType, max_length=32),
        default=default_connection_vlantypes)
    outer_vlan_ethertypes = postgres_fields.ArrayField(
        enumfields.EnumField(VLanEthertype, max_length=16),
        default=default_connection_outer_ethertypes)

    product_offering = models.ForeignKey(
        "catalog.ProductOffering",
        related_name="connections",
        on_delete=models.CASCADE)

    loa = models.FileField(
        null=True,
        upload_to="loas/")

    subscriber_side_demarcs = postgres_fields.ArrayField(
        models.CharField(max_length=255),
        null=True)

    pop = models.ForeignKey(
        "catalog.PointOfPresence",
        on_delete=models.CASCADE,
        related_name="connections")

    objects = StatefulManager()

    @property
    def capacity_allocated(self) -> int:
        """Calculated total bandwidth of all network services"""
        services = NetworkServiceConfig.objects.filter(connection=self)
        return sum(s.capacity_allocated for s in services)

    @property
    def speed(self) -> int:
        """Caclulcated total bandwidth of the connection"""
        return sum(p.speed for p in self.ports.all())

    @property
    def port_quantity(self) -> int:
        """Return the number of ports allocated"""
        return self.ports.exclude(state=State.DECOMMISSIONED).count()

    def __str__(self) -> str:
        """Connection to String"""
        return (f"Connection: {self.name}, "
                f"Mode: {self.mode} "
                f"LACP: {self.lacp_timeout}")

    def __repr__(self) -> str:
        """Connection representation"""
        return (f"<Connection id='{self.pk}' name='{self.name}' "
                f"mode='{self.mode}' lacp='{self.lacp_timeout}'>")


#
# VLan Configuration
#
class VLanConfig(PolymorphicModel):
    """
    A VLanConfig is polymorphic on type, which can be
    of [dot1q, qinq, port].
    """

class PortVLanConfig(VLanConfig):
    """Port based vlan binding"""
    __polymorphic_type__ = VLAN_CONFIG_TYPE_PORT


class Dot1QVLanConfig(VLanConfig):
    """Dot1Q vlan"""
    vlan = models.PositiveSmallIntegerField()
    vlan_ethertype = enumfields.EnumField(
        VLanEthertype,
        default="0x8100",
        max_length=32)

    __polymorphic_type__ = VLAN_CONFIG_TYPE_DOT1Q


class QinQVLanConfig(VLanConfig):
    """QinQ Vlan Configuration"""
    outer_vlan = models.PositiveSmallIntegerField()
    outer_vlan_ethertype = enumfields.EnumField(
        VLanEthertype,
        default="0x8100",
        max_length=32)
    inner_vlan = models.PositiveSmallIntegerField()

    __polymorphic_type__ = VLAN_CONFIG_TYPE_QINQ

#
# Ports
#
class Port(
        StatefulMixin,
        OwnableMixin,
        ManagedMixin,
        BillableMixin,
        InvoiceableMixin,
        ContactableMixin,
        AccountScopedMixin,
        models.Model,
    ):
    """
    A model for the (port) portation point.
    This represents the port assigned to a account.
    """
    name = models.CharField(max_length=255)

    media_type = models.CharField(max_length=20)
    speed = models.PositiveIntegerField()

    connection = models.ForeignKey(
        Connection,
        on_delete=models.PROTECT,
        null=True, blank=True,
        related_name="ports")

    device = models.ForeignKey(
        Device,
        on_delete=models.PROTECT,
        related_name="ports")

    operational_state = enumfields.EnumField(
        PortState,
        default=PORT_STATE_DOWN)

    objects = StatefulManager()

    @property
    def pop(self):
        """Get PoP through device"""
        if self.device:
            return self.device.pop
        return None

    def __str__(self) -> str:
        """Port as String"""
        return f"PortPort: {self.name}"

    def __repr__(self) -> str:
        """Port representation"""
        return (f"<Port id='{self.pk}' "
                f"name='{self.name}' "
                f"consumed_by='{self.consuming_account_id}' "
                f"managed_by='{self.managing_account_id}'>")

    class Meta:
        verbose_name = "Port"


class PortReservation(
        StatefulMixin,
        AccountScopedMixin,
        InvoiceableMixin,
        ContactableMixin,
        ReferencableMixin,
        models.Model,
    ):
    """A PortReservation for a Port in a Connection"""
    connection = models.ForeignKey(
        Connection,
        related_name="port_reservations",
        on_delete=models.CASCADE)
    port = models.ForeignKey(
        Port,
        null=True,
        on_delete=models.CASCADE)

    # State
    loa_received_at = models.DateTimeField(null=True)

    # Demarcs
    exchange_side_demarc = models.CharField(max_length=128, null=True)
    subscriber_side_demarc = models.CharField(max_length=128, null=True)

    connecting_party = models.CharField(max_length=128, null=True)

    # Cancellation
    decommission_at = models.DateField(null=True)
    charged_until = models.DateField(null=True)

    @property
    def product_offering(self):
        """This is a proxy for connection"""
        return self.connection.product_offering

#
# Service Access Models
#

class NetworkServiceConfig(
        StatefulMixin,
        OwnableMixin,
        ManagedMixin,
        BillableMixin,
        InvoiceableMixin,
        ContactableMixin,
        AccountScopedMixin,
        PolymorphicModel,
    ):
    """
    Polymorphic NetworkService access model.

    Network access is granted by creating an access object
    associated with a billing and owning account.

    The model references `service.NetworkService`.
    """
    capacity = models.PositiveIntegerField(null=True, blank=True)

    # Cancellation
    decommission_at = models.DateField(null=True)
    charged_until = models.DateField(null=True)

    # Rate limiting
    capacity = models.PositiveIntegerField(null=True)

    # Relations
    connection = models.ForeignKey(
        Connection,
        related_name="network_service_configs",
        on_delete=models.PROTECT)

    network_service = models.ForeignKey(
        "service.NetworkService",
        related_name="configs",
        on_delete=models.CASCADE)

    vlan_config = models.ForeignKey(
        VLanConfig,
        null=True,
        related_name="network_service_configs",
        on_delete=models.PROTECT)

    objects = StatefulPolymorphicManager()

    @property
    def capacity_allocated(self):
        """
        The calculated allocated capacity
        """
        connection_speed = self.connection.speed
        capacity = self.capacity
        if capacity is None:
            capacity = self.connection.speed

        return capacity


class ExchangeLanNetworkServiceConfig(NetworkServiceConfig):
    """
    An ExchangeLAN network service config model.

    Grant access to a an exchange lan and configure features.
    """
    asns = postgres_fields.ArrayField(
        models.PositiveIntegerField(),
        default=list,
        size=20)
    listed = models.BooleanField(default=True)

    product_offering = models.ForeignKey(
        "catalog.ProductOffering",
        related_name="exchange_lan_network_service_configs",
        on_delete=models.CASCADE)

    availability_zone = models.ForeignKey(
        "catalog.AvailabilityZone",
        null=True,
        related_name="exchange_lan_network_service_configs",
        on_delete=models.CASCADE)


    def __str__(self) -> str:
        """String from exchange lan network service access"""
        return f"ExchangeLAN NetworkService Configuration for ASNs: {self.asns}"

    def __repr__(self) -> str:
        """ExchangeLanNetworkServiceAccess representation"""
        return (f"<ExchangeLanNetworkServiceConfig id='{self.pk}' "
                f"asns='{self.asns}'>")

    class Meta:
        verbose_name = "ExchangeLAN NetworkService Config"


class P2PNetworkServiceConfig(NetworkServiceConfig):
    """
    A config model for a P2P (e-line) network service.
    """
    def __str__(self) -> str:
        """String from p2p service config"""
        return f"P2P NetworkService Configuration (Id: {self.pk})"

    def __repr__(self) -> str:
        """P2P Access representation"""
        return f"<P2PNetworkServiceConfig id='{self.pk}'>"

    class Meta:
        verbose_name = "P2P NetworkService Config"


class MP2MPNetworkServiceConfig(NetworkServiceConfig):
    """
    A config model for a MP2MP (e-lan) network service.
    This might be a closed user group.
    """
    def __str__(self) -> str:
        """String from mp2mp e-lan service config"""
        return f"MP2MP NetworkService Configuration (Id: {self.pk})"

    def __repr__(self) -> str:
        """MP2MP Access representation"""
        return f"<MP2MPNetworkServiceConfig id='{self.pk}'>"

    class Meta:
        verbose_name = "MP2MP NetworkService Config"


class P2MPNetworkServiceConfig(NetworkServiceConfig):
    """
    A config model for a P2MP e-tree network service.
    """
    role = enumfields.EnumField(
        P2MPRole,
        default=P2MP_ROLE_LEAF,
        max_length=32)

    def __str__(self) -> str:
        """String from P2MP service config"""
        return f"P2MP NetworkService Configuration (Id: {self.pk})"

    def __repr__(self) -> str:
        """P2PMP Access representation"""
        return f"<P2MPNetworkServiceConfig id='{self.pk}'>"

    class Meta:
        verbose_name = "P2MP NetworkService Config"


class CloudNetworkServiceConfig(NetworkServiceConfig):
    """
    A cloud network service config model.
    """
    cloud_vlan = models.PositiveSmallIntegerField(null=True)
    handover = models.PositiveSmallIntegerField(default=1)

    availability_zone = models.ForeignKey(
        "catalog.AvailabilityZone",
        null=True,
        related_name="cloud_network_service_configs",
        on_delete=models.CASCADE)

    def __str__(self):
        """String from cloud service config"""
        return f"Cloud NetworkService Configuration (Id: {self.pk})"

    def __repr__(self):
        """Cloud Access representation"""
        return f"<CloudNetworkServiceConfig id='{self.pk}'>"

    class Meta:
        verbose_name = "Cloud NetworkService Config"


NETWORK_SERVICE_CONFIG_MODELS = {
    NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN:
        ExchangeLanNetworkServiceConfig,
    NETWORK_SERVICE_CONFIG_TYPE_P2P:
        P2PNetworkServiceConfig,
    NETWORK_SERVICE_CONFIG_TYPE_P2MP:
        P2MPNetworkServiceConfig,
    NETWORK_SERVICE_CONFIG_TYPE_MP2MP:
        MP2MPNetworkServiceConfig,
    NETWORK_SERVICE_CONFIG_TYPE_CLOUD:
        CloudNetworkServiceConfig,
}

#
# Cloud Provider Side
#
class CloudProviderNetworkServiceConfig(
        StatefulMixin,
        models.Model):
    """
    A CloudProviderNetworkServiceConfig is a dummy / placeholder
    configuration for the provider side of the cloud network
    service.

    The cloud provider network service config is
    created by the backend during cloud provider configuration
    """
    network_service = models.ForeignKey(
        "service.NetworkService",
        related_name="cloud_provider_configs",
        on_delete=models.CASCADE)

    def __str__(self):
        """String from cloud provider config"""
        return f"Cloud Provider NetworkService Configuration (Id: {self.pk})"

    def __repr__(self):
        """Cloud Access representation"""
        return f"<CloudProviderNetworkServiceConfig id='{self.pk}'>"

    class Meta:
        verbose_name = "Cloud Provider NetworkService Config"


class IXPSpecificFlagsConfig(models.Model):
    """Configure flags"""
    network_feature_config = models.ForeignKey(
        "config.NetworkFeatureConfig",
        on_delete=models.CASCADE,
        related_name="flags")

    name = models.CharField(max_length=40)
    enabled = models.BooleanField()


#
# Feature Access Configuration
#
class NetworkFeatureConfig(
        StatefulMixin,
        OwnableMixin,
        ManagedMixin,
        BillableMixin,
        InvoiceableMixin,
        ContactableMixin,
        AccountScopedMixin,
        PolymorphicModel):
    """
    The polymorphic feature access base model.

    Access to features like route servers are granted here.
    The configuration is stored in the specific feature
    access object.
    """
    # Relations
    network_feature = models.ForeignKey(
        "service.NetworkFeature",
        on_delete=models.PROTECT,
        related_name="configs")

    network_service_config = models.ForeignKey(
        NetworkServiceConfig,
        on_delete=models.CASCADE,
        related_name="network_feature_configs")

    objects = StatefulPolymorphicManager()


class RouteServerNetworkFeatureConfig(NetworkFeatureConfig):
    """
    Configure RouteServer Access

    This model contains everything required for granting
    accounts access to a RS.
    """
    asn = models.PositiveIntegerField()
    password = models.CharField(max_length=128, blank=True)
    as_set_v4 = models.CharField(max_length=100, null=True)
    as_set_v6 = models.CharField(max_length=100, null=True)
    max_prefix_v4 = models.PositiveIntegerField(
        null=True,
        default=None)
    max_prefix_v6 = models.PositiveIntegerField(
        null=True,
        default=None)
    insert_ixp_asn = models.BooleanField()

    session_mode = enumfields.EnumField(
        RouteServerSessionMode, max_length=20)

    bgp_session_type = enumfields.EnumField(
        BGPSessionType, max_length=10)

    ip = models.ForeignKey(
        "ipam.IpAddress",
        related_name="route_server_network_feature_config",
        on_delete=models.PROTECT)

    def __str__(self) -> str:
        """Routeserver Feature Access Configuration as String"""
        return f"RouteServer FeatureConfiguration for AS{self.asn}"

    def __repr__(self) -> str:
        """Representation of feature config"""
        return (f"<RouteServerFeatureConfig id='{self.pk}' "
                f"consuming_account_id='{self.consuming_account_id}'>")

    class Meta:
        verbose_name = "RouteServer FeatureAccess"
        verbose_name_plural = "RouteServer FeatureAccesses"


NETWORK_FEATURE_CONFIG_MODELS = {
    NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER: RouteServerNetworkFeatureConfig,
}
