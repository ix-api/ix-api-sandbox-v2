"""
Network Feature Config Change Handlers
"""

from ixapi_schema.v2 import schema

from jea.api.v2.config.serializers import get_config_update_serializer
from jea.config.services import configs as configs_svc
from jea.config.status.reducers import update_network_service_config_status
from jea.stateful.models import (
    State,
    Change,
)
from jea.stateful.changes import (
    state_update,
)


def create(change: Change):
    """Create a new network feature config"""
    serializer = schema.NetworkFeatureConfigRequest(data=change.data)
    serializer.is_valid(raise_exception=True)
    feature_config = configs_svc.create_network_feature_config(
        scoping_account=change.scoping_account,
        network_feature_config_input=serializer.validated_data)

    # We can directly go to production, as the data is valid
    # and the create successful.
    feature_config.state = State.PRODUCTION
    feature_config.save()

    # Check related network service config, and update
    # state if required.
    service_config = feature_config.network_service_config
    service_config = update_network_service_config_status(service_config)
    if service_config.has_error_status:
        # Nothing left for us to do here. Decommission
        # pending when everything is OK.
        return feature_config, []

    # Remove all pending decommissions
    decommissioned = service_config.network_feature_configs.filter(
        state=State.DECOMMISSION_REQUESTED)
    decommissions = [
        state_update(cfg, State.DECOMMISSIONED)
        for cfg in decommissioned.all()]

    return feature_config, [
        state_update(service_config, State.PRODUCTION),
    ] + decommissions


def update(change: Change):
    """Update a network feature config"""
    feature_config = change.ref

    # Deserialize request
    serializer_class = get_config_update_serializer(feature_config)
    serializer = serializer_class(data=change.data, partial=change.partial)
    serializer.is_valid(raise_exception=True)

    # Update feature config using service
    feature_config = configs_svc.update_network_feature_config(
        scoping_account=change.scoping_account,
        network_feature_config=feature_config,
        network_feature_config_update=serializer.validated_data)

    return feature_config, []


def decommission(change: Change):
    """Decommission a network feature config"""
    feature_config = change.ref
    feature = feature_config.network_feature

    # Was this feature required?
    # Then we defer deletion until a replacement was
    # created.
    if feature.required:
        feature_config.state = State.DECOMMISSION_REQUESTED
        feature_config.save()
        return feature_config, []

    # Proceed with decommissioning
    return feature_config, [
        state_update(feature_config, State.DECOMMISSIONED)]
