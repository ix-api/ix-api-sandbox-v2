"""
Network Service Config Change Handlers
"""

from datetime import timedelta

from ixapi_schema.v2 import schema
from ixapi_schema.v2.constants.catalog import ServiceProviderWorkflow

from jea.api.v2.config.serializers import get_config_update_serializer
from jea.service.status.reducers import update_network_service_status
from jea.service.models import (
    NetworkService,
)
from jea.config.status.reducers import update_network_service_config_status
from jea.config.changes import create_network_service_config
from jea.config.models import (
    P2MPRole,
    P2PNetworkServiceConfig,
    P2MPNetworkServiceConfig,
    MP2MPNetworkServiceConfig,
    CloudNetworkServiceConfig,
    CloudProviderNetworkServiceConfig,
)
from jea.config.services import configs as configs_svc
from jea.stateful.models import (
    State,
    Change,
)
from jea.stateful.changes import (
    schedule_in,
    state_update,
    side_effect,
)


def create(change: Change):
    """Create a new network service config"""
    # Handle shadow configs like cloud provider network service configs
    requested_type = change.data.get("type")
    if requested_type == "provider_config":
        return _create_cloud_provider_config(change)

    # Normal workflow for network service configs
    serializer = schema.NetworkServiceConfigRequest(data=change.data)
    serializer.is_valid(raise_exception=True)

    # We now have validated configuration input,
    # now let the configs service create a new configuration
    service_config = configs_svc.create_network_service_config(
        scoping_account=change.scoping_account,
        network_service_config_request=serializer.validated_data)
    change.ref = service_config
    change.save()

    network_service = service_config.network_service

    # Initial state will be in requested until
    # the required network feature configs are created.
    service_config.state = State.REQUESTED

    # Update the config status
    service_config = update_network_service_config_status(service_config)
    if service_config.has_error_status:
        service_config.state = State.ERROR
        service_config.save()

    # Update the status of the related network service
    network_service = update_network_service_status(network_service)
    if network_service.has_error_status:
        service.state = State.ERROR
        service.save()

    # Run network service type specific create handlers
    if isinstance(service_config, P2PNetworkServiceConfig):
        return _create_p2p(change)
    if isinstance(service_config, P2MPNetworkServiceConfig):
        return _create_p2mp(change)
    if isinstance(service_config, MP2MPNetworkServiceConfig):
        return _create_mp2mp(change)
    if isinstance(service_config, CloudNetworkServiceConfig):
        return _create_cloud(change)

    return service_config, []


def _create_p2p(change: Change):
    """On create of a p2p vc NSC"""
    # Check if A and B side are connected
    # If both are present bring entire service into production
    nsc = change.ref
    service = nsc.network_service

    a_side_nsc = service.configs.active.filter(
        consuming_account=service.consuming_account)
    b_side_nsc = service.configs.active.filter(
        consuming_account=service.joining_member_account)

    if a_side_nsc.exists() and b_side_nsc.exists():
        # Apply all pending decommissions
        decommissions = [
            state_update(dcom, State.DECOMMISSIONED)
            for dcom in service.configs.decommission_requested]

        # Next step: go into production
        return nsc, [
            state_update(a_side_nsc.first(), State.PRODUCTION),
            state_update(b_side_nsc.first(), State.PRODUCTION),
            schedule_in(
                state_update(service, State.PRODUCTION),
                timedelta(seconds=5)),
        ] + decommissions

    return nsc, []


def _create_p2mp(change: Change):
    """On create p2mp service config"""
    config = change.ref
    service = config.network_service

    # We will put this config into production next
    deferred = [state_update(config, State.PRODUCTION)]

    if config.role == P2MPRole.ROOT:
        # Decommission pending, create new config
        deferred += [
            state_update(dcom, State.DECOMMISSIONED)
            for dcom in service.configs.decommission_requested]

    if service.state != State.PRODUCTION:
        # Can we put the service into production?
        # Check if there is at least one root present
        if service.configs.filter(
                p2mpnetworkserviceconfig__role=P2MPRole.ROOT).exists():
            deferred += [
                schedule_in(
                    state_update(service, State.PRODUCTION),
                    timedelta(seconds=5)),
            ]

    return config, deferred


def _create_mp2mp(change: Change):
    """On create mp2mp service configs"""
    # This is pretty much boring. Bring everything
    # into production
    config = change.ref
    service = config.network_service

    if service.state != State.PRODUCTION:
        return config, [
            state_update(service, State.PRODUCTION),
            state_update(config, State.PRODUCTION),
        ]

    return config, [
        state_update(config, State.PRODUCTION),
    ]


def _create_cloud(change: Change):
    """On create cloud network serivce config"""
    config = change.ref
    network_service = config.network_service
    offering = network_service.product_offering
    workflow = offering.service_provider_workflow

    effects = []

    # Request creating the provider cloud network service
    # config if not exists
    if not network_service.cloud_provider_configs.exists():
        effects += [
            side_effect(network_service, "add_status_info_text", [
                "configuring cloud provider virtual circuit",
            ]),
            schedule_in(
                create_network_service_config({
                    "type": "provider_config",
                    "network_service": network_service.pk,
                    "customer_network_service_config": config.pk,
                }),
                timedelta(seconds=1)),
        ]


    if workflow == ServiceProviderWorkflow.EXCHANGE_FIRST:
        effects += _create_cloud_workflow_exchange_first(config, network_service)
    elif workflow == ServiceProviderWorkflow.PROVIDER_FIRST:
        effects += _create_cloud_workflow_provider_first(config, network_service)
    else:
        raise NotImplementedError("unknown service provider workflow")

    if not network_service.has_status:
        effects += [
            schedule_in(
                state_update(config, State.PRODUCTION),
                timedelta(seconds=8)),
            schedule_in(
                state_update(network_service, State.PRODUCTION),
                timedelta(seconds=8)),
        ]

    return config, effects


def _create_cloud_provider_config(change: Change):
    """Create cloud provider config from internal change"""
    effects = []

    network_service = NetworkService.objects.get(
        pk=change.data["network_service"])

    # Create cloud provider network service config
    config = CloudProviderNetworkServiceConfig(
        network_service=network_service)
    config.save()

    # Update the service status
    network_service = update_network_service_status(network_service)
    if network_service.has_error_status:
        network_service.state = State.ERROR
        network_service.save()

    if not network_service.has_status:
        effects += [
            schedule_in(
                state_update(network_service, State.PRODUCTION),
                timedelta(seconds=1)),
        ]
        for config in network_service.configs.all():
            effects += [
                schedule_in(
                    state_update(config, State.PRODUCTION),
                    timedelta(seconds=1)),
            ]

    return config, effects


def _create_cloud_workflow_exchange_first(config, service):
    """On create an exchange first cloud service nsc"""
    effects = [
        schedule_in(
            side_effect(service, "assign_provider_ref"),
            timedelta(seconds=0)),
    ]

    return effects


def _create_cloud_workflow_provider_first(config, service):
    """On create an exchange first cloud service nsc"""
    effects = []

    return effects


def update(change: Change):
    """Update a network service config"""
    service_config = configs_svc.get_network_service_config(
        scoping_account=change.scoping_account,
        network_service_config=change.ref)

    serializer_class = get_config_update_serializer(service_config)
    serializer = serializer_class(data=change.data, partial=change.partial)
    serializer.is_valid(raise_exception=True)

    # Perform update
    service_config = configs_svc.update_network_service_config(
        scoping_account=change.scoping_account,
        network_service_config=service_config,
        network_service_config_update=serializer.validated_data)

    # Update the configuration and related network service status
    service_config = update_network_service_config_status(service_config)
    if service_config.has_error_status:
        service_config.state = State.ERROR
        service_config.save()

    update_network_service_status(service_config.network_service)

    return service_config, []


def decommission(change: Change):
    """Decommission a network service config"""
    service_config = configs_svc.get_network_service_config(
        scoping_account=change.scoping_account,
        network_service_config=change.ref)

    if isinstance(service_config, P2PNetworkServiceConfig):
        return _decommission_p2p(change)
    if isinstance(service_config, P2MPNetworkServiceConfig):
        return _decommission_p2mp(change)

    # First time we do this, we just set the decommission
    # requested state. However, if it is already set,
    # proceed with the decommissioning. 2nd pass:
    if service_config.state == State.DECOMMISSION_REQUESTED:
        service_config.state = State.DECOMMISSIONED
        service_config.save()
        # This cascades to all feature configs
        for feature_config in service_config.network_feature_configs.all():
            feature_config.state = State.DECOMMISSIONED
            feature_config.save()

        return service_config, [] # We are done here

    # Mark as decommission requested. 1st pass:
    service_config.state = State.DECOMMISSION_REQUESTED
    service_config.save()

    # This cascades to all feature configs
    for feature_config in service_config.network_feature_configs.all():
        feature_config.state = State.DECOMMISSION_REQUESTED
        feature_config.save()

    return service_config, [
        # Try again - and decommission next time
        schedule_in(change, timedelta(seconds=5))
    ]


def _decommission_p2p(change: Change):
    """
    When decommissioning a p2p nsc, the process
    will be deferred until a replacement is available.

    If the deletion is requested again we force quitting
    the network service. However, this puts the network
    service into an error state.
    """
    # Defer decommissioning
    nsc = change.ref
    nns = nsc.network_service

    # This might affect the network service
    update_network_service_status(nns)

    # 2nd pass:
    if nsc.state == State.DECOMMISSION_REQUESTED:
        # Force decommissioning and put the network
        # service into an error state
        return nsc, [
            state_update(nsc, State.DECOMMISSIONED),
            state_update(nns, State.ERROR),
        ]

    # 1st pass:
    nsc.state = State.DECOMMISSION_REQUESTED
    nsc.save()

    return nsc, []


def _decommission_p2mp(change: Change):
    """Decommission p2mp service configuration"""
    config = change.ref
    service = config.network_service

    # We can just decommission leafs
    if config.role == P2MPRole.LEAF:
        return config, [
            state_update(config, State.DECOMMISSIONED)
        ]

    # When decommissioning a root peer, we have
    # to check, that there is another root available,
    # otherwise we defer the decommissioning.
    roots = service.configs.active.filter(
        p2mpnetworkserviceconfig__role=P2MPRole.ROOT)
    if roots.exists():
        # We can proceed with the decommissioning
        return config, [
            state_update(config, State.DECOMMISSIONED)
        ]

    # Otherwise, we keep the decommission requested
    # state, and try decommissioning when a new
    # network service config was created.
    return config, []
