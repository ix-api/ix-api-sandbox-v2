
import logging

from django.apps import AppConfig

logger = logging.getLogger(__name__)

class ConfigAppConfig(AppConfig):
    """Configuration Application"""
    name = 'jea.config'
    verbose_name = "JEA :: Access"

    def ready(self):
        """Application is ready"""
        logger.info("Initializing app: {}".format(self.name))
