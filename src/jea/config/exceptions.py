
"""
Access Exception
----------------

All exceptions that describe anything that could go
wrong when dealing with access objects.
"""

from jea.exceptions import ValidationError

#
# Ports
#
class PortNotReady(ValidationError):
    default_detail = ("The portation point's state is not ALLOCATED "
                      "or REQUESTED.")
    default_code = "port_not_ready"
    default_field = "connection"


class PortUnavailable(ValidationError):
    default_detail = ("The portation point can not be "
                      "used in this configuration at this location.")
    default_code = "port_unavailable"
    default_field = "connection"


class PortInUse(ValidationError):
    default_detail = ("The portation point is still in use by "
                      "a connection and can not be released.")
    default_code = "port_in_use"
    default_field = "connection"


class PortNotConnected(ValidationError):
    default_detail = "The portation point is not connected"
    default_code = "port_not_connected"
    default_field = "connection"


class ConnectionModeUnavailable(ValidationError):
    default_detail = "The connection can not assume the requested mode."
    default_code = "connection_mode_unavailable"
    default_field = "mode"


class SessionModeInvalid(ValidationError):
    default_detail = ("The session_mode of the config is not valid.")
    default_code = "session_mode_invalid"
    default_field = "session_mode"


class VLanInUse(ValidationError):
    default_detail = ("The (outer) vlan id is already in use "
                      "on the connection.")
    default_code = "vlan_in_use"
    default_field = "vlan_config.vlan"

class DiversityError(ValidationError):
    default_detail = "The diversity requirement is not fulfilled."
    default_code = "diversity_unfulfilled"
    default_field = "handover"
