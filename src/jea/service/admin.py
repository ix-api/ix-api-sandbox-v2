from django.contrib import admin

from jea.admin import site
from jea.service.models import (
    ExchangeLanNetworkService,
    P2PNetworkService,
    MP2MPNetworkService,
    P2MPNetworkService,

    RouteServerNetworkFeature,
)
from jea.ipam.models import (
    IpAddress,
)

#
# ExchangeLan Extra
#
class ExchangeLanIpAddressInline(admin.TabularInline):
    model = IpAddress
    extra = 1

    fields = ("version", "address", "prefix_length", "fqdn",
              "valid_not_before", "valid_not_after")

#
# Inline Model Admins: Features
#
class RouteServerNetworkFeatureInline(admin.TabularInline):
    model = RouteServerNetworkFeature
    extra = 0
#
# Model Admins: Services
#
class ExchangeLanNetworkServiceAdmin(admin.ModelAdmin):
    inlines = (ExchangeLanIpAddressInline,
               RouteServerNetworkFeatureInline)


class P2PNetworkServiceAdmin(admin.ModelAdmin):
    pass

class MP2MPNetworkServiceAdmin(admin.ModelAdmin):
    pass

class P2MPNetworkServiceAdmin(admin.ModelAdmin):
    pass


class CloudNetworkServiceAdmin(admin.ModelAdmin):
    pass

# Register model admins
site.register(ExchangeLanNetworkService,
              ExchangeLanNetworkServiceAdmin)
site.register(MP2MPNetworkService,
              MP2MPNetworkServiceAdmin)
site.register(P2PNetworkService,
              P2PNetworkServiceAdmin)
site.register(P2MPNetworkService,
              P2MPNetworkServiceAdmin)
