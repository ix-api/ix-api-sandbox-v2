"""
Server Status Reducers
"""

from django.db import transaction

from jea.service.models import (
    NetworkService,
    P2PNetworkService,
    P2MPNetworkService,
    MP2MPNetworkService,
    CloudNetworkService,
)
from jea.config.models import (
    P2MPRole,
)
from jea.service.status.messages import (
    awaiting_network_service_config,
)


@transaction.atomic
def update_network_service_status(
        service: NetworkService) -> NetworkService:
    """Update the status of a network service"""
    service.status.clear()

    if isinstance(service, P2PNetworkService):
        return _update_p2p_network_service_status(service)
    if isinstance(service, P2MPNetworkService):
        return _update_p2mp_network_service_status(service)
    if isinstance(service, MP2MPNetworkService):
        return _update_mp2mp_network_service_status(service)
    if isinstance(service, CloudNetworkService):
        return _update_cloud_network_service_status(service)
    return service


def _update_p2p_network_service_status(
        service: NetworkService) -> NetworkService:
    """Check p2p network service config status"""
    a_side_nsc = service.configs \
        .active \
        .filter(consuming_account=service.consuming_account)
    b_side_nsc = service.configs \
        .active \
        .filter(consuming_account=service.joining_member_account)
    # awaiting configs
    if not a_side_nsc.exists():
        awaiting_network_service_config(service, "A-side").save()
    if not b_side_nsc.exists():
        awaiting_network_service_config(service, "B-side").save()
    return service


def _update_p2mp_network_service_status(
        service: NetworkService) -> NetworkService:
    """Check p2mp network service status"""
    # Check if there is at least one root peer
    # configuration present
    if not service.configs.active.filter(
            p2mpnetworkserviceconfig__role=P2MPRole.ROOT).exists():
        awaiting_network_service_config(service, "root").save()
    return service


def _update_mp2mp_network_service_status(
        service: NetworkService) -> NetworkService:
    """Check mp2mp network service status"""
    if not service.configs.active.exists():
        awaiting_network_service_config(service).save()
    return service


def _update_cloud_network_service_status(
        service: NetworkService) -> NetworkService:
    """
    Update the status of a cloud network service.
    Assert diversity constraints and configuration
    presence.
    """
    # If our provider side is not configured 
    if not service.cloud_provider_configs.exists():
        awaiting_network_service_config(service, "cloud_provider").save()

    # Check the diversity constaint
    diversity = service.product_offering.diversity
    # Handover index starting at 1
    for handover_idx in range(1, diversity+1):
        if service.configs.filter(
                cloudnetworkserviceconfig__handover=handover_idx).exists():
            continue

        # Check presence of handovers
        handover = "handover-{}".format(handover_idx)
        awaiting_network_service_config(service, {
            "dependency": "customer",
            "handover": handover
        }).save()

    return service


