"""
Network Service Status Creators
"""

from jea.stateful.models import StatusMessage, Severity


AWAITING_NETWORK_SERVICE_CONFIG = "awaiting_network_service_config"

def awaiting_network_service_config(service, details=None):
    """
    Create a awaiting config status message.
    The dependency is a free field to supply
    additional info
    """
    attrs = {}
    message = "waiting for network-service-config"

    if isinstance(details, dict):
        attrs = details
        message += ": {}".format(", ".join(details.values()))
    else:
        attrs = {"dependency": details}
        message = ("waiting for network-service-config: {}") \
            .format(details)

    return StatusMessage(
        ref=service,
        severity=Severity.NOTICE,
        attrs=attrs,
        message=message)
