"""
Change Creators for NetworkServices
"""

from jea.stateful.models import (
    Change,
    State,
    Create,
    Update,
    Decommission,
)


def create_network_service(data) -> Change:
    """Create network service request"""
    return Create(
        resource=Change.NETWORK_SERVICE,
        data=data)


def update_network_service(service, data, partial=True) -> Change:
    """Update the network service request"""
    return Update(
        resource=Change.NETWORK_SERVICE,
        ref=service,
        partial=partial,
        data=data)


def decommission_network_service(
        service,
        decommission_at=None) -> Change:
    """Create decommission change"""
    return Decommission(
        resource=Change.NETWORK_SERVICE,
        decommission_at=decommission_at,
        ref=service)
