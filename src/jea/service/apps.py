
import logging

from django.apps import AppConfig

logger = logging.getLogger(__name__)

class ServiceConfig(AppConfig):
    name = 'jea.service'

    verbose_name = "JEA :: Services"

    def ready(self):
        """Application is ready"""
        logger.info("Initializing app: {}".format(self.name))

