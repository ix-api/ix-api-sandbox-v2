
"""
Service Exceptions
"""

from jea.exceptions import ValidationError


class NetworkServiceNotAvailable(ValidationError):
    """A network service is not available"""
    default_field = "network_service"

    def __init__(self, service):
        msg = "Network service '{}' is not available.".format(
            service.__class__.__name__)
        super(NetworkServiceNotAvailable, self).__init__(msg)


class NetworkFeatureNotAvailable(ValidationError):
    """A network feature is not available"""
    default_field = "network_feature"

    def __init__(self, network_feature, network_service=None):
        """Initialize exception"""
        if network_service:
            msg = "Network service '{}' is not available for '{}'.".format(
                network_feature.__class__.__name__,
                network_service.__class__.__name__)
        else:
            msg = "Network feature '{}' is not available.".format(
                network_feature.__class__.__name__)

        super(NetworkFeatureNotAvailable, self).__init__(msg)


class MemberJoiningRuleInUse(ValidationError):
    """A member joining rule is currently in use."""
    default_field="consuming_account"
    default_detail = (
        "The member joining rule is in use. "
        "This can be fixed by removing the `consuming_account`s "
        " network service config.")


class ProductTypeMismatch(ValidationError):
    """The given product does not match the requested service"""
    default_field="product"
    default_detail = \
        "The product does not match the requested network service."
