
"""
Private VLan Membership Service

This service manages vlan member joining rules
"""

from jea.exceptions import ValidationError, ResourceAccessDenied
from jea.service.exceptions import MemberJoiningRuleInUse
from jea.service.models import (
    P2PNetworkService,
    P2MPNetworkService,
    MP2MPNetworkService,
    MemberJoiningRule,
    AllowMemberJoiningRule,
    DenyMemberJoiningRule,
)
from jea.crm.models import Account
from jea.crm.services import accounts as accounts_svc
from jea.service.services import network as network_svc


def is_member_joining_rule_in_use(
        member_joining_rule=None,
        scoping_account=None
    ):
    """
    Check if the member joining rule is in use:
    This is the case, when a network service config
    is present for the associated network service and
    consuming account.

    :param member_joining_rule: The member joining rule to check
    :param scoping_account: A scope limiting account
    """
    configs = get_network_service_configs(
        member_joining_rule=member_joining_rule,
        scoping_account=scoping_account)

    return configs.exists()


def get_network_service_configs(
        member_joining_rule=None,
        scoping_account=None,
    ):
    """
    Get network service cofigurations associated with
    the member joining rule.

    The association is through the consuming account
    and network service.

    :param member_joining_rule: The member joining rule
    :param scoping_account: A scope limiting account
    """
    member_joining_rule = get_member_joining_rule(
        member_joining_rule=member_joining_rule,
        scoping_account=scoping_account)

    configs = member_joining_rule.network_service.configs.filter(
        consuming_account=member_joining_rule.consuming_account)

    return configs


def get_member_joining_rules(
        network_service=None,
        scoping_account=None,
    ):
    """
    Get all member joining rules within a account context.

    TODO: Add filtering

    :param network_service: A network service
    :param scoping_account: A account limiting the result set
    """
    queryset = MemberJoiningRule.objects.all()
    if network_service:
        queryset = MemberJoiningRule.objects.filter(
            network_service=network_service)

    # Filter scoped accounts
    if scoping_account:
        scoped_rules = queryset.filter(
            scoping_account=scoping_account)

        # Related rules, where the account is invited as
        # consuming account
        related_rules = queryset.filter(
            consuming_account=scoping_account)

        # Combine queryset
        queryset = scoped_rules | related_rules

    return queryset.distinct()


def get_member_joining_rule(
        member_joining_rule=None,
        scoping_account=None,
    ):
    """
    Retrieve a member joining rule within a account
    scope.

    :param member_joining_rule: A member joining rule identifier
    :param scoping_account: A scope limiting account
    """
    if not isinstance(member_joining_rule, MemberJoiningRule):
        member_joining_rule = MemberJoiningRule.objects.get(
            pk=member_joining_rule)

    # Check access
    if scoping_account:
        accessible_rules = get_member_joining_rules(
            scoping_account=scoping_account)
        if not member_joining_rule in accessible_rules:
            raise ResourceAccessDenied(member_joining_rule)

    return member_joining_rule


def assert_valid_capacity(
        network_service,
        capacity_min,
        capacity_max,
    ):
    """
    Apply validation rules for minimum and maximum capacity

    :param network_service: The affected networkservice
    :param capacity_min: The minimal capacity
    :param capacity_max: The maximum available capacity.
    """
    # There might be no capacity cap
    if not capacity_max:
        return

    if capacity_min > capacity_max:
        raise ValidationError(
            "Minimum capacity is greater than maximum capacity.",
            field="capacity_min")

    # Check if there are network service configs
    # with a capacity set.
    #
    # For configs with a set capacity, we reduce the
    # configuration's capacity.
    #
    for config in network_service.configs.all():
        if config.capacity is not None:
            if config.capacity > capacity_max:
                config.capacity = capacity_max
                config.save()


def assert_one_rule_per_account(
        network_service=None,
        consuming_account=None,
    ):
    """
    There is only one rule per account and
    network service allowed.

    :param network_service: The network service to check
    :param account: A consuming account
    """
    rules = network_service.member_joining_rules.filter(
        consuming_account=consuming_account)
    if rules.exists():
        raise ValidationError(
            "Only one member joining rule per account and network "
            "service is allowed.",
            field="consuming_account")


def assert_network_service_allows_member_joining_rules(
        network_service=None,
    ):
    """
    Raise an exception, when the network service does
    not allow for custom membership rules
    """
    allowed_network_service = (
        MP2MPNetworkService,
        P2MPNetworkService,
    )
    if isinstance(network_service, allowed_network_service):
        return # Ok

    raise ValidationError(
        ("The NetworkService {} does not accept custom "
         "member joining rules.").format(type(network_service).__name__),
        field="network_service")


def create_member_joining_rule(
        member_joining_rule_request=None,
        scoping_account=None,
    ) -> MemberJoiningRule:
    """
    Create a member joining rule.

    This is only allowed for ELine, ETree and ELan network services.
    For ELines only one member joining rule is allowed.

    :param member_joining_rule_request: A request for a new joining rule
    :param scoping_account: A scope limiting account.
    """
    network_service = network_svc.get_network_service(
        network_service=member_joining_rule_request["network_service"],
        scoping_account=scoping_account)

    # Check that the network service supports member joining rules
    assert_network_service_allows_member_joining_rules(
        network_service=network_service)

    # Permission check: Only the scoping account of the
    # network service is allowed to create joining rules.
    if scoping_account and \
        network_service.scoping_account != scoping_account:
        raise ResourceAccessDenied(network_service)

    # Load related accounts: As the consuming account is
    # the 'invited' account, we allow for discoverable accounts
    consuming_account = accounts_svc.get_account(
        allow_discoverable=True,
        scoping_account=scoping_account,
        account=member_joining_rule_request["consuming_account"])
    managing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=member_joining_rule_request["managing_account"])

    # Make sure there is only one rule per account and network service.
    assert_one_rule_per_account(
        network_service=network_service,
        consuming_account=consuming_account)

    # Make specific member joining rules
    rule_type = member_joining_rule_request["type"]
    if rule_type == "deny":
        member_joining_rule = _create_deny_member_joining_rule(
            scoping_account=scoping_account,
            managing_account=managing_account,
            consuming_account=consuming_account,
            network_service=network_service,
            member_joining_rule_request=member_joining_rule_request)
    elif rule_type == "allow":
        member_joining_rule = _create_allow_member_joining_rule(
            scoping_account=scoping_account,
            managing_account=managing_account,
            consuming_account=consuming_account,
            network_service=network_service,
            member_joining_rule_request=member_joining_rule_request)
    else:
        raise ValidationError(
            "Invalid rule type. Choose either allow or deny.",
            field="type")

    # Finalize
    member_joining_rule.external_ref = member_joining_rule_request.get(
        "external_ref")
    member_joining_rule.save()

    return member_joining_rule


def _create_deny_member_joining_rule(
        member_joining_rule_request=None,
        scoping_account: Account = None,
        managing_account: Account = None,
        consuming_account: Account = None,
        network_service: P2PNetworkService = None,
    ) -> MemberJoiningRule:
    """
    Create a deny network service member joining rule.
    A consuming account is prevented from joining the network service.

    :param member_joining_rule_request: A member joining rule request
    :param scoping_account: A scoping account
    :param managing_account: An account managing this member joining rule
    :param consuming_account: The account permitted to join the eline
    :param network_service: An ELine network service
    """
    return DenyMemberJoiningRule(
        scoping_account=scoping_account,
        managing_account=managing_account,
        consuming_account=consuming_account,
        network_service=network_service)


def _create_allow_member_joining_rule(
        member_joining_rule_request=None,
        scoping_account: Account = None,
        managing_account: Account = None,
        consuming_account: Account = None,
        network_service: MP2MPNetworkService = None) -> MemberJoiningRule:
    """
    Create allow network service member joining rule.
    A customized capacity can be provided.

    :param member_joining_rule_request: A member joining rule request
    :param scoping_account: A scoping account
    :param managing_account: An account managing this member joining rule
    :param consuming_account: The account permitted to join the eline
    :param network_service: An ELan network service
    """
    # Capacity
    capacity_min = member_joining_rule_request.get("capacity_min")
    capacity_max = member_joining_rule_request.get("capacity_max")

    assert_valid_capacity(network_service, capacity_min, capacity_max)

    return AllowMemberJoiningRule(
        scoping_account=scoping_account,
        managing_account=managing_account,
        consuming_account=consuming_account,
        capacity_min=capacity_min,
        capacity_max=capacity_max,
        network_service=network_service)


def update_member_joining_rule(
        member_joining_rule=None,
        member_joining_rule_update=None,
        scoping_account=None,
    ):
    """
    Update a given member joining rule.
    However, we do not allow the network service to change, once
    it is assigned to a rule.

    The consuming account of a member joining rule can only
    be changed, if the rule is not used in aconfiguration.

    :param member_joining_rule: The member joining rule to update
    :param member_joining_rule_update: A request for a new joining rule
    :param scoping_account: A scope limiting account.
    """
    member_joining_rule = get_member_joining_rule(
        member_joining_rule=member_joining_rule,
        scoping_account=scoping_account)

    update_fields = member_joining_rule_update.keys()

    # Update the managing account
    if "managing_account" in update_fields:
        managing_account = accounts_svc.get_account(
            account=member_joining_rule_update["managing_account"],
            scoping_account=scoping_account)
        member_joining_rule.managing_account = managing_account

    if "consuming_account" in update_fields:
        # Assert the rule is currently not in use by the
        # consuming account.
        if is_member_joining_rule_in_use(
                member_joining_rule=member_joining_rule,
                scoping_account=scoping_account):
            raise MemberJoiningRuleInUse()

        consuming_account = accounts_svc.get_account(
            allow_discoverable=True,
            account=member_joining_rule_update["consuming_account"],
            scoping_account=scoping_account)
        member_joining_rule.consuming_account = consuming_account

    # Update external ref
    if "external_ref" in update_fields:
        member_joining_rule.external_ref = \
            member_joining_rule_update["external_ref"]

    # Type specific capacity update
    if isinstance(member_joining_rule, AllowMemberJoiningRule):
        # Update allow capacity override
        member_joining_rule = _update_allow_member_joining_rule(
            member_joining_rule=member_joining_rule,
            member_joining_rule_update=member_joining_rule_update)

    return member_joining_rule


def _update_allow_member_joining_rule(
        member_joining_rule=None,
        member_joining_rule_update=None,
    ) -> MemberJoiningRule:
    """
    Update the allow member joining rule, which for now
    means updating capacity customization.

    Note, that changing the capacity might affect the associated
    network service config.

    :param member_joining_rule: A member joining rule to update
    :param member_joining_rule_update: The update data
    """
    update_fields = member_joining_rule_update.keys()

    capacity_min = member_joining_rule_update.get(
        "capacity_min", member_joining_rule.capacity_min)
    capacity_max = member_joining_rule_update.get(
        "capacity_max", member_joining_rule.capacity_max)

    assert_valid_capacity(
        member_joining_rule.network_service, capacity_min, capacity_max)

    if "capacity_min" in update_fields:
        member_joining_rule.capacity_min = capacity_min
    if "capacity_max" in update_fields:
        member_joining_rule.capacity_max = capacity_max

    return member_joining_rule


def delete_member_joining_rule(
        member_joining_rule=None,
        scoping_account=None,
    ):
    """
    Remove a member joining rule.

    This is only possible, if the rule is not in use: e.g. there
    is no network service config associated with the
    consuming account and the network service.

    :param member_joining_rule: The rule to remove
    :param scoping_account: A account managing the rules.
    """
    member_joining_rule = get_member_joining_rule(
        member_joining_rule=member_joining_rule,
        scoping_account=scoping_account)

    if is_member_joining_rule_in_use(
            member_joining_rule=member_joining_rule,
            scoping_account=scoping_account):
        raise MemberJoiningRuleInUse()

    # Everything's fine. Just delete the rule
    member_joining_rule.delete()

    return member_joining_rule

