
"""
Test network service
"""

import pytest
from model_bakery import baker
from ixapi_schema.v2.schema import (
    NETWORK_SERVICE_TYPE_EXCHANGE_LAN,
    NETWORK_SERVICE_TYPE_P2P,
    NETWORK_SERVICE_TYPE_P2MP,
    NETWORK_SERVICE_TYPE_MP2MP,
    NETWORK_SERVICE_TYPE_CLOUD,
)

from jea.exceptions import (
    ValidationError,
    ResourceNotFound,
)
from jea.service.services import network as network_svc
from jea.service import exceptions


@pytest.mark.django_db
def test_get_network_services():
    """Test listing network services"""
    exchange_lan = baker.make("service.ExchangeLanNetworkService")
    mp2mp = baker.make("service.MP2MPNetworkService")
    p2p = baker.make("service.P2PNetworkService")
    cloud = baker.make("service.CloudNetworkService")

    services = network_svc.get_network_services()

    assert exchange_lan in services
    assert mp2mp in services
    assert p2p in services
    assert cloud in services


@pytest.mark.django_db
def test_get_network_services_filter_type():
    """Test listing network services filtered by type"""
    exchange_lan = baker.make("service.ExchangeLanNetworkService")
    eline = baker.make("service.P2PNetworkService")
    elan = baker.make("service.MP2MPNetworkService")
    etree = baker.make("service.P2MPNetworkService")

    # Exchange Lan
    services = network_svc.get_network_services(filters={
        "type": NETWORK_SERVICE_TYPE_EXCHANGE_LAN,
    })
    assert exchange_lan in services
    assert not etree in services
    assert not eline in services
    assert not elan in services

    # ELine / P2P
    services = network_svc.get_network_services(filters={
        "type": NETWORK_SERVICE_TYPE_P2P,
    })
    assert not exchange_lan in services
    assert not elan in services
    assert eline in services
    assert not etree in services

    # ELan / MP2MP
    services = network_svc.get_network_services(filters={
        "type": NETWORK_SERVICE_TYPE_MP2MP,
    })
    assert not exchange_lan in services
    assert not etree in services
    assert not eline in services
    assert elan in services

    # Etree
    services = network_svc.get_network_services(filters={
        "type": NETWORK_SERVICE_TYPE_P2MP,
    })
    assert not exchange_lan in services
    assert etree in services
    assert not eline in services
    assert not elan in services


@pytest.mark.django_db
def test_create_network_service__p2p():
    """Test creating an p2p (eline) network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    joining_account = baker.make(
        "crm.Account",
        scoping_account=account)
    offering = baker.make("catalog.P2PNetworkProductOffering")

    # Create network service
    network_service = network_svc.create_network_service(
        network_service_request={
            "type": NETWORK_SERVICE_TYPE_P2P,
            "managing_account": account,
            "consuming_account": account,
            "billing_account": account,
            "joining_member_account": joining_account,
            "product_offering": offering,
            "display_name": "Eline23",
        })

    assert network_service


@pytest.mark.django_db
def test_create_network_service__mp2mp():
    """Test creating an elan network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    offering = baker.make("catalog.MP2MPNetworkProductOffering")

    # Create network service
    network_service = network_svc.create_network_service(
        network_service_request={
            "type": NETWORK_SERVICE_TYPE_MP2MP,
            "managing_account": account,
            "consuming_account": account,
            "billing_account": account,
            "product_offering": offering,
            "display_name": "Elan2000",
            "ip_addresses": None,
        })

    assert network_service


@pytest.mark.django_db
def test_create_network_service__p2mp():
    """Test creating an elan network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    offering = baker.make("catalog.P2MPNetworkProductOffering")

    # Create network service
    network_service = network_svc.create_network_service(
        network_service_request={
            "type": NETWORK_SERVICE_TYPE_P2MP,
            "managing_account": account,
            "consuming_account": account,
            "billing_account": account,
            "product_offering": offering,
            "display_name": "ETree42",
            "ip_addresses": None,
        })

    assert network_service


@pytest.mark.django_db
def test_create_network_service__cloud():
    """Test creating a cloud network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    offering = baker.make(
        "catalog.CloudNetworkProductOffering",
        service_provider_workflow="exchange_first")

    # Create network service
    network_service = network_svc.create_network_service(
        network_service_request={
            "type": NETWORK_SERVICE_TYPE_CLOUD,
            "managing_account": account,
            "consuming_account": account,
            "billing_account": account,
            "product_offering": offering,
            "capacity": 1000,
            "cloud_key": "DEMOCGTAGTAGCTAACTAGTATGA",
        })

    # Assertions
    assert network_service
    assert network_service.diversity == offering.diversity
    assert network_service.capacity == 1000


@pytest.mark.django_db
def test_create_network_service__unsupported():
    """Test creating an unsupported network service"""
    account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    offering = baker.make("catalog.ExchangeLanNetworkProductOffering")

    with pytest.raises(Exception):
        network_svc.create_network_service(
            network_service_request={
                "type": NETWORK_SERVICE_TYPE_EXCHANGE_LAN,
                "managing_account": account,
                "consuming_account": account,
                "billing_account": account,
                "product_offering": offering,
                "name": "ExchangeLan DUS",
            })


@pytest.mark.django_db
def test_update_network_service__accounts():
    """Test updating a network service"""
    network_service = baker.make("service.P2PNetworkService")
    account = baker.make("crm.Account")

    network_service = network_svc.update_network_service(
        network_service=network_service,
        network_service_update={
            "managing_account": account,
            "consuming_account": account,
        })

    assert network_service.managing_account == account
    assert network_service.consuming_account == account


@pytest.mark.django_db
def test_update_network_service__billing_account():
    """Test updating the billing contact"""
    network_service = baker.make("service.P2PNetworkService")
    billing_account = baker.make(
        "crm.Account",
        billing_information=baker.make("crm.BillingInformation"))
    non_billing_account = baker.make("crm.Account")

    network_service = network_svc.update_network_service(
        network_service=network_service,
        network_service_update={
            "billing_account": billing_account,
        })

    assert network_service.billing_account == billing_account

    with pytest.raises(ValidationError):
        network_svc.update_network_service(
            network_service=network_service,
            network_service_update={
                "billing_account": non_billing_account,
            })


@pytest.mark.django_db
def test_update_network_service__common_attributes():
    """Common attributes update"""
    network_service = baker.make("service.P2PNetworkService")
    network_service = network_svc.update_network_service(
        network_service=network_service,
        network_service_update={
            "display_name": "name",
            "external_ref": "external_ref",
            "purchase_order": "purchase_order",
            "contract_ref": "contract_ref",
        })

    assert network_service.name == "name"
    assert network_service.external_ref == "external_ref"
    assert network_service.purchase_order == "purchase_order"
    assert network_service.contract_ref == "contract_ref"


@pytest.mark.django_db
def test_update_network_service__product():
    """Network service product change"""
    updates = [
        (baker.make("service.P2PNetworkService"),
         baker.make("catalog.P2PNetworkProductOffering")),
        (baker.make("service.MP2MPNetworkService"),
         baker.make("catalog.MP2MPNetworkProductOffering")),
        (baker.make("service.P2MPNetworkService"),
         baker.make("catalog.P2MPNetworkProductOffering")),
    ]

    for service, offering in updates:
        network_service = network_svc.update_network_service(
            network_service=service,
            network_service_update={
                "product_offering": offering,
            })

        assert network_service.product_offering == offering


@pytest.mark.django_db
def test_update_network_service__elan():
    """Update elan specific fields"""
    network_service = baker.make("service.MP2MPNetworkService")
    update = {
        "display_name": "Some Name 42"
    }

    network_service = network_svc.update_network_service(
        network_service=network_service,
        network_service_update=update)

    assert network_service.name == \
        update["display_name"]


@pytest.mark.django_db
def test_update_network_service__etree():
    """Update etree specific fields"""
    network_service = baker.make("service.P2MPNetworkService")

    # These are pretty much the same as with an elan
    update = {
        "display_name": "another name",
    }

    network_service = network_svc.update_network_service(
        network_service=network_service,
        network_service_update=update)

    assert network_service.name == update["display_name"]


@pytest.mark.django_db
def test__assign_ip_addresses():
    """Test ip address assign helper"""
    network_service = baker.make(
        "service.MP2MPNetworkService")
    ip4 = baker.make(
        "ipam.IpAddress",
        address="23.42.0.0",
        version=4,
        prefix_length=23)
    ip6 = baker.make(
        "ipam.IpAddress",
        address="fd42::0",
        version=6,
        prefix_length=48)

    network_svc._assign_ip_addresses(
        network_service=network_service,
        ip_addresses=[ip4, ip6])

    assert ip4 in network_service.ip_addresses.all()
    assert ip6 in network_service.ip_addresses.all()


@pytest.mark.django_db
def test__assign_ip_addresses__release():
    """Test ip address assign helper"""
    network_service = baker.make(
        "service.MP2MPNetworkService")
    ip4 = baker.make(
        "ipam.IpAddress",
        address="23.42.0.0",
        version=4,
        prefix_length=23,
        mp2mp_network_service=network_service)
    ip6 = baker.make(
        "ipam.IpAddress",
        address="fd42::0",
        version=6,
        prefix_length=48,
        mp2mp_network_service=network_service)

    # Update
    network_svc._assign_ip_addresses(
        network_service=network_service,
        ip_addresses=[ip6])

    assert not ip4 in network_service.ip_addresses.all()
    assert ip6 in network_service.ip_addresses.all()

    # Clear all
    network_svc._assign_ip_addresses(
        network_service=network_service,
        ip_addresses=[])

    assert not ip4 in network_service.ip_addresses.all()
    assert not ip6 in network_service.ip_addresses.all()


@pytest.mark.django_db
def test_get_network_service__by_service_id():
    """Test getting a single service by id"""
    service = baker.make("service.ExchangeLanNetworkService")
    service_ = network_svc.get_network_service(
        network_service=str(service.pk))
    assert service.pk == service_.pk

    with pytest.raises(Exception):
        network_svc.get_network_service()


@pytest.mark.django_db
def test_get_network_service_change_request():
    """Get network service change request"""
    req = baker.make("service.NetworkServiceChangeRequest")

    # By ID
    req1 = network_svc.get_network_service_change_request(
        network_service_change_request=str(req.pk))
    assert req1 == req

    # By network service
    req2 = network_svc.get_network_service_change_request(
        network_service=req.network_service.pk)
    assert req2 == req

    # By network service without change request
    with pytest.raises(ResourceNotFound):
        service = baker.make("service.P2PNetworkService")
        network_svc.get_network_service_change_request(
            network_service=service)


@pytest.mark.django_db
def test_create_network_service_change_request():
    """Create a change request"""
    offering_old = baker.make(
        "catalog.P2PNetworkProductOffering",
        bandwidth_min=1000,
        bandwidth_max=10000)
    offering_new = baker.make(
        "catalog.P2PNetworkProductOffering",
        bandwidth_min=10000,
        bandwidth_max=1000000)
    p2pvc = baker.make(
        "service.P2PNetworkService",
        product_offering=offering_old)
    req = network_svc.create_network_service_change_request(
        network_service=p2pvc,
        network_service_change_request={
            "capacity": 25000,
            "product_offering": offering_new,
        })
    assert req


@pytest.mark.django_db
def test_create_network_service_change_request__invalid_service():
    """Test with invalid network service type"""
    offering = baker.make(
        "catalog.P2MPNetworkProductOffering")
    service = baker.make(
        "service.P2MPNetworkService",
        product_offering=offering)
    with pytest.raises(ValidationError):
        network_svc.create_network_service_change_request(
            network_service=service,
            network_service_change_request={
                "capacity": None,
                "product_offering": offering,
            })


@pytest.mark.django_db
def test_create_network_service_change_request__presence():
    """Test with a change request already present"""
    service = baker.make(
        "service.P2MPNetworkService")
    baker.make(
        "service.NetworkServiceChangeRequest",
        network_service=service)
    with pytest.raises(ValidationError):
        network_svc.create_network_service_change_request(
            network_service=service,
            network_service_change_request={
                "capacity": None,
                "product_offering": service.product_offering,
            })


@pytest.mark.django_db
def test_delete_network_service_change_request():
    """Test removing a network service change request"""
    # By ID
    change_request = baker.make(
        "service.NetworkServiceChangeRequest")
    change_request = network_svc.delete_network_service_change_request(
        network_service_change_request=change_request.pk)
    assert change_request

    # By network service
    service = baker.make(
        "service.P2MPNetworkService")
    baker.make(
        "service.NetworkServiceChangeRequest",
        network_service=service)
    change_request = network_svc.delete_network_service_change_request(
        network_service=service.pk)
    assert change_request


@pytest.mark.django_db
def test_get_network_features():
    """Get all the network features"""
    feature = baker.make("service.RouteServerNetworkFeature")
    features = network_svc.get_network_features()
    assert feature in features
    # TODO: Add tests for filtering


@pytest.mark.django_db
def test_get_network_feature__cached():
    """Get a feature"""
    feature = baker.make("service.RouteServerNetworkFeature")
    result = network_svc.get_network_feature(
        network_feature=feature)

    assert result == feature


@pytest.mark.django_db
def test_get_network_feature__cached():
    """Get a feature"""
    feature = baker.make("service.RouteServerNetworkFeature")
    result = network_svc.get_network_feature(
        network_feature=feature.pk)

    assert result == feature


@pytest.mark.django_db
def test_assert_network_feature_available():
    """Test availability assertion for a given network feature"""
    s1 = baker.make("service.ExchangeLanNetworkService")
    s2 = baker.make("service.ExchangeLanNetworkService")
    f1 = baker.make("service.RouteServerNetworkFeature",
                     network_service=s1)
    f2 = baker.make("service.RouteServerNetworkFeature",
                    network_service=s2)

    # Network feature is available:
    network_svc.assert_network_feature_available(f1, s1)

    # Feature is not available
    with pytest.raises(exceptions.NetworkFeatureNotAvailable):
        network_svc.assert_network_feature_available(f2, s1)

