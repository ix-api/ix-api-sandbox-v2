
"""
Test vlan membership service
"""

import pytest
from model_bakery import baker

from jea import exceptions
from jea.service.services import membership as membership_svc


@pytest.mark.django_db
def test_get_network_service_configs():
    """Test getting network service configs"""
    account = baker.make("crm.Account")
    network_service = baker.make(
        "service.MP2MPNetworkService",
        managing_account=account,
        consuming_account=account)
    rule = baker.make(
        "service.AllowMemberJoiningRule",
        network_service=network_service,
        consuming_account=account,
        managing_account=account)

    # Create a network service config
    config = baker.make(
        "config.MP2MPNetworkServiceConfig",
        managing_account=account,
        consuming_account=account,
        network_service=network_service)

    assert config in membership_svc.get_network_service_configs(
        member_joining_rule=rule)


@pytest.mark.django_db
def test_is_member_joining_rule_in_use():
    """Test in_use check for member joining rules"""
    account = baker.make("crm.Account")
    network_service = baker.make(
        "service.MP2MPNetworkService",
        managing_account=account,
        consuming_account=account)
    rule = baker.make(
        "service.AllowMemberJoiningRule",
        network_service=network_service,
        consuming_account=account,
        managing_account=account)

    assert not membership_svc.is_member_joining_rule_in_use(
        member_joining_rule=rule)

    # Create a network service config
    baker.make(
        "config.MP2MPNetworkServiceConfig",
        managing_account=account,
        consuming_account=account,
        network_service=network_service)

    assert membership_svc.is_member_joining_rule_in_use(
        member_joining_rule=rule)


@pytest.mark.django_db
def test_get_member_joining_rules__network_service():
    """Test getting all member joining rules"""
    account = baker.make("crm.Account")
    network_service = baker.make(
        "service.MP2MPNetworkService",
        managing_account=account,
        consuming_account=account)
    related_rule = baker.make(
        "service.AllowMemberJoiningRule",
        network_service=network_service,
        consuming_account=account,
        managing_account=account)
    unrelated_rule = baker.make(
        "service.DenyMemberJoiningRule",
        consuming_account=account,
        managing_account=account)

    assert related_rule in membership_svc.get_member_joining_rules(
        network_service=network_service)
    assert not unrelated_rule in membership_svc.get_member_joining_rules(
        network_service=network_service)


@pytest.mark.django_db
def test_get_member_joining_rules__scoping_account():
    """Test getting all member joining rules"""
    account = baker.make("crm.Account")
    other_account = baker.make("crm.Account")
    network_service = baker.make(
        "service.MP2MPNetworkService",
        managing_account=account,
        consuming_account=account)
    related_rule = baker.make(
        "service.AllowMemberJoiningRule",
        network_service=network_service,
        consuming_account=account,
        managing_account=account)
    unrelated_rule = baker.make(
        "service.DenyMemberJoiningRule",
        consuming_account=other_account,
        managing_account=account)

    assert related_rule in membership_svc.get_member_joining_rules(
        scoping_account=account)
    assert not unrelated_rule in membership_svc.get_member_joining_rules(
        scoping_account=account)



@pytest.mark.django_db
def test_create_member_joining_rule__deny():
    """
    Test creating a deny member joining rule
    for a network service.
    """
    account = baker.make("crm.Account")
    network_service = baker.make(
        "service.MP2MPNetworkService",
        managing_account=account,
        consuming_account=account)

    rule = membership_svc.create_member_joining_rule(
        member_joining_rule_request={
            "type": "deny",
            "managing_account": account,
            "consuming_account": account,
            "network_service": network_service.pk,
        })

    assert rule

    # This should be not allowed, as elines don't support
    # member joining rules
    with pytest.raises(exceptions.ValidationError):
        membership_svc.create_member_joining_rule(
            member_joining_rule_request={
                "type": "deny",
                "managing_account": account,
                "consuming_account": account,
                "network_service": network_service.pk,
            })


@pytest.mark.django_db
def test_create_member_joining_rule__allow():
    """
    Test creating a allow member joining rule for a
    network service.
    """
    account = baker.make("crm.Account")
    account_a = baker.make("crm.Account")
    account_b = baker.make("crm.Account")
    network_service = baker.make(
        "service.MP2MPNetworkService",
        managing_account=account,
        consuming_account=account)

    # Account A
    rule = membership_svc.create_member_joining_rule(
        member_joining_rule_request={
            "type": "allow",
            "managing_account": account,
            "consuming_account": account_a,
            "capacity_min": 5000,
            "capacity_max": None,
            "network_service": network_service.pk,
        })

    assert rule

    # Account B
    rule = membership_svc.create_member_joining_rule(
        member_joining_rule_request={
            "type": "allow",
            "managing_account": account,
            "consuming_account": account_b,
            "capacity_min": 5000,
            "capacity_max": None,
            "network_service": network_service.pk,
        })

    assert rule

    # Account B again
    with pytest.raises(exceptions.ValidationError):
        membership_svc.create_member_joining_rule(
            member_joining_rule_request={
                "type": "allow",
                "managing_account": account,
                "consuming_account": account_b,
                "capacity_min": 5000,
                "capacity_max": None,
                "network_service": network_service.pk,
            })


@pytest.mark.django_db
def test_create_member_joining_rule__etree():
    """
    Test creating a member joining rule for an ETree
    network service.
    """
    account = baker.make("crm.Account")
    account_a = baker.make("crm.Account")
    account_b = baker.make("crm.Account")
    network_service = baker.make(
        "service.P2MPNetworkService",
        managing_account=account,
        consuming_account=account)

    # Account A
    rule = membership_svc.create_member_joining_rule(
        member_joining_rule_request={
            "type": "allow",
            "managing_account": account,
            "consuming_account": account_a,
            "capacity_min": 5000,
            "capacity_max": None,
            "network_service": network_service.pk,
        })

    assert rule

    # Account B
    rule = membership_svc.create_member_joining_rule(
        member_joining_rule_request={
            "type": "allow",
            "managing_account": account,
            "consuming_account": account_b,
            "capacity_min": 5000,
            "capacity_max": None,
            "network_service": network_service.pk,
        })

    assert rule

    # Account B again
    with pytest.raises(exceptions.ValidationError):
        membership_svc.create_member_joining_rule(
            member_joining_rule_request={
                "type": "allow",
                "managing_account": account,
                "consuming_account": account_b,
                "capacity_min": 5000,
                "capacity_max": None,
                "network_service": network_service.pk,
            })


@pytest.mark.django_db
def test_create_member_joining_rule__unsupported():
    """
    Test creating a member joining rule for an
    unsupported network service.
    """
    account = baker.make("crm.Account")
    network_service = baker.make(
        "service.ExchangeLanNetworkService",
        managing_account=account,
        consuming_account=account)

    with pytest.raises(exceptions.ValidationError):
        membership_svc.create_member_joining_rule(
            member_joining_rule_request={
                "type": "allow",
                "managing_account": account,
                "consuming_account": account,
                "capacity_min": 5000,
                "capacity_max": None,
                "network_service": network_service.pk,
            })


@pytest.mark.django_db
def test_update_member_joining_rule__consuming_account():
    """
    Test updating a member joining rule
    """
    account = baker.make("crm.Account")
    account_b = baker.make("crm.Account")
    network_service = baker.make(
        "service.MP2MPNetworkService",
        managing_account=account,
        consuming_account=account)
    rule = baker.make(
        "service.AllowMemberJoiningRule",
        network_service=network_service,
        consuming_account=account,
        managing_account=account)

    # This should be ok
    rule = membership_svc.update_member_joining_rule(
        member_joining_rule=rule,
        member_joining_rule_update={
            "consuming_account": account_b,
            "capacity_min": 8000,
            "capacity_max": None,
        })

    # Use this rule
    config = baker.make(
        "config.MP2MPNetworkServiceConfig",
        managing_account=account,
        consuming_account=account_b,
        network_service=network_service)

    # This should not be ok anymore
    with pytest.raises(exceptions.ValidationError):
        membership_svc.update_member_joining_rule(
            member_joining_rule=rule,
            member_joining_rule_update={
                "consuming_account": account,
            })


@pytest.mark.django_db
def test_update_member_joining_rule__capacity_max():
    """
    Test updating a member joining rule
    """
    account = baker.make("crm.Account")
    account_b = baker.make("crm.Account")
    network_service = baker.make(
        "service.P2MPNetworkService",
        managing_account=account,
        consuming_account=account)
    rule = baker.make(
        "service.AllowMemberJoiningRule",
        network_service=network_service,
        consuming_account=account,
        managing_account=account)

    # Update capacity max
    rule = membership_svc.update_member_joining_rule(
        member_joining_rule=rule,
        member_joining_rule_update={
            "capacity_min": 8000,
            "capacity_max": 10000,
        })

    # Use this rule with no given capacity
    config = baker.make(
        "config.P2MPNetworkServiceConfig",
        managing_account=account,
        consuming_account=account_b,
        network_service=network_service,
        capacity=None)

    # Update capacity max
    rule = membership_svc.update_member_joining_rule(
        member_joining_rule=rule,
        member_joining_rule_update={
            "capacity_min": 8000,
            "capacity_max": 9000,
        })

    # This should trigger a reset
    config.capacity = 9000
    config.save()

    # Update capacity max
    rule = membership_svc.update_member_joining_rule(
        member_joining_rule=rule,
        member_joining_rule_update={
            "capacity_min": 4000,
            "capacity_max": 5000,
        })

    config.refresh_from_db()
    assert config.capacity == 5000


@pytest.mark.django_db
def test_delete_member_joining_rule():
    """Test the removal of a rule"""
    account = baker.make("crm.Account")
    network_service = baker.make(
        "service.P2MPNetworkService",
        managing_account=account,
        consuming_account=account)
    rule = baker.make(
        "service.AllowMemberJoiningRule",
        network_service=network_service,
        consuming_account=account,
        managing_account=account)

    membership_svc.delete_member_joining_rule(
        member_joining_rule=rule)


@pytest.mark.django_db
def test_delete_member_joining_rule__in_use():
    """Test the removal of a rule"""
    account = baker.make("crm.Account")
    network_service = baker.make(
        "service.MP2MPNetworkService",
        managing_account=account,
        consuming_account=account)
    rule = baker.make(
        "service.AllowMemberJoiningRule",
        network_service=network_service,
        consuming_account=account,
        managing_account=account)

    # Use this rule
    config = baker.make(
        "config.MP2MPNetworkServiceConfig",
        managing_account=account,
        consuming_account=account,
        network_service=network_service)

    with pytest.raises(exceptions.ValidationError):
        membership_svc.delete_member_joining_rule(
            member_joining_rule=rule)
