
"""
JEA Service: Network

Access and manage all network services, like
Exchange Lans, Closed User Groups, etc...
"""

from copy import copy

from django.db.models.query import QuerySet
from ixapi_schema.v2.constants.catalog import (
    ServiceProviderWorkflow,
)
from ixapi_schema.v2.constants.service import (
    NETWORK_SERVICE_TYPE_P2P,
    NETWORK_SERVICE_TYPE_P2MP,
    NETWORK_SERVICE_TYPE_MP2MP,
    NETWORK_SERVICE_TYPE_CLOUD,
)

from jea.exceptions import (
    ResourceAccessDenied,
    ResourceNotFound,
    ValidationError,
)
from jea.stateful.models import State
from jea.service.models import (
    NetworkService,
    ExchangeLanNetworkService,
    P2PNetworkService,
    P2MPNetworkService,
    MP2MPNetworkService,
    CloudNetworkService,
    NetworkServiceChangeRequest,
    NetworkFeature,
)
from jea.catalog.models import (
    P2PNetworkProductOffering,
    P2MPNetworkProductOffering,
    MP2MPNetworkProductOffering,
    CloudNetworkProductOffering,
)
from jea.service.exceptions import (
    NetworkFeatureNotAvailable,
    ProductTypeMismatch,
)
from jea.service.filters import (
    NetworkServiceFilter,
    NetworkFeatureFilter,
)
from jea.crm.services import (
    accounts as accounts_svc,
    contacts as contacts_svc,
)
from jea.catalog.services import products as products_svc
from jea.ipam.services import ip_addresses as ip_addresses_svc


def get_network_services(
        scoping_account=None,
        filters=None) -> QuerySet:
    """
    List all network services and apply filters

    :param filters: A dict of filters.
    """
    # Make queryset:
    #
    # Allow for all exchange lan network services,
    # services within the account scope and services
    # which are public.

    # Legacy Network Services
    exchange_lans = NetworkServiceFilter(
        filters,
        queryset=NetworkService.objects.instance_of(
            ExchangeLanNetworkService)).qs

    # Scoped network services
    scoped_network_services = NetworkServiceFilter(
        filters,
        queryset=NetworkService.objects.filter(
            scoping_account=scoping_account)).qs

    # Public network services
    public_network_services = NetworkServiceFilter(
        filters,
        queryset=NetworkService.objects.filter(public=True)).qs

    # Make queryset
    queryset = exchange_lans | scoped_network_services | public_network_services

    return queryset.distinct()


def get_network_service(
        network_service=None,
        scoping_account=None,
    ) -> NetworkService:
    """
    Get a specific network service.
    Right now the supported lookup methods are:
     - service_id

    TODO: Add billing and owning account check for services
          which are account instances. E.g. MP2MP virtual circuits

    :param service_id: The primary key of the network service
    """
    if not network_service:
        raise ValidationError("A service identifier or object is required")

    if isinstance(network_service, NetworkService):
        return network_service

    return NetworkService.objects.get(pk=network_service)


def create_network_service(
        scoping_account=None,
        network_service_request=None,
    ) -> NetworkService:
    """
    Create a new network service.

    Only the creation of elan, eline, etree vc network
    services and cloud network services are supported right now.

    :param scoping_account: The scoping account managing network services
    :param network_service_request: A request for a new network service.
    """
    # Get related accounts
    managing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_service_request["managing_account"])
    consuming_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_service_request["consuming_account"])
    billing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_service_request["billing_account"])
    accounts_svc.assert_billing_information(account=billing_account)

    # Get product offering and make sure it matches the requested
    # network service.
    offering = products_svc.get_product_offering(
        scoping_account=scoping_account,
        product_offering=network_service_request["product_offering"])

    # Dispatch the creation of the network service based on
    # the given type.
    if network_service_request["type"] == NETWORK_SERVICE_TYPE_P2P:
        # Validate product
        if not isinstance(offering, P2PNetworkProductOffering):
            raise ProductTypeMismatch()

        return _create_p2p_network_service(
            scoping_account=scoping_account,
            managing_account=managing_account,
            consuming_account=consuming_account,
            billing_account=billing_account,
            product_offering=offering,
            network_service_request=network_service_request)

    if network_service_request["type"] == NETWORK_SERVICE_TYPE_MP2MP:
        if not isinstance(offering, MP2MPNetworkProductOffering):
            raise ProductTypeMismatch()

        return _create_mp2mp_network_service(
            scoping_account=scoping_account,
            managing_account=managing_account,
            consuming_account=consuming_account,
            billing_account=billing_account,
            product_offering=offering,
            network_service_request=network_service_request)

    if network_service_request["type"] == NETWORK_SERVICE_TYPE_P2MP:
        if not isinstance(offering, P2MPNetworkProductOffering):
            raise ProductTypeMismatch()

        return _create_p2mp_network_service(
            scoping_account=scoping_account,
            managing_account=managing_account,
            consuming_account=consuming_account,
            billing_account=billing_account,
            product_offering=offering,
            network_service_request=network_service_request)

    if network_service_request["type"] == NETWORK_SERVICE_TYPE_CLOUD:
        if not isinstance(offering, CloudNetworkProductOffering):
            raise ProductTypeMismatch()

        return _create_cloud_network_service(
            scoping_account=scoping_account,
            managing_account=managing_account,
            consuming_account=consuming_account,
            billing_account=billing_account,
            product_offering=offering,
            network_service_request=network_service_request)

    # We do not support the creation of any other network service
    # right now.
    raise NotImplementedError


def _create_p2p_network_service(
        scoping_account=None,
        managing_account=None,
        consuming_account=None,
        billing_account=None,
        product_offering=None,
        network_service_request=None,
    ) -> P2PNetworkService:
    """
    Create an ELine network service

    :param scoping_account: The scoping account managing network services
    :param managing_account: A resolved and validated managing account
    :param consuming_account: A validated consuming account
    :param billing_account: An account with billing information present
    :param product: A validated eline product
    :param network_service_request: A request for a new network service.
    """
    joining_member_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=network_service_request["joining_member_account"])


    # Let's create the eline network service!
    network_service = P2PNetworkService(
        state=State.REQUESTED,
        scoping_account=scoping_account,
        managing_account=managing_account,
        consuming_account=consuming_account,
        joining_member_account=joining_member_account,
        billing_account=billing_account,
        product_offering=product_offering,
        external_ref=network_service_request.get("external_ref"),
        purchase_order=network_service_request.get("purchase_order", ""),
        contract_ref=network_service_request.get("contract_ref"),
        public=False,
    )

    network_service.save()

    # Assign required contact roles: NOC, Implementation
    network_service.nsc_required_contact_roles.add(
        contacts_svc.get_default_role("noc"))
    network_service.nsc_required_contact_roles.add(
        contacts_svc.get_default_role("implementation"))

    return network_service


def _create_mp2mp_network_service(
        scoping_account=None,
        managing_account=None,
        consuming_account=None,
        billing_account=None,
        product_offering=None,
        network_service_request=None,
    ) -> MP2MPNetworkService:
    """
    Create an ELan network service

    :param scoping_account: The scoping account managing network services
    :param network_service_request: A request for a new network service.
    """
    # Let's create the mp2mp / elan network service!
    network_service = MP2MPNetworkService(
        state=State.REQUESTED,
        managing_account=managing_account,
        consuming_account=consuming_account,
        scoping_account=scoping_account,
        billing_account=billing_account,
        product_offering=product_offering,
        external_ref=network_service_request.get("external_ref"),
        purchase_order=network_service_request.get("purchase_order", ""),
        contract_ref=network_service_request.get("contract_ref"),
        public=network_service_request.get("public", False),
        display_name=network_service_request.get(
            "display_name", "new mp2mp network service"))
    network_service.save()

    # Assign required contact roles:
    # This is IX dependent - for now we require a NOC
    # and an Implementation contact
    network_service.nsc_required_contact_roles.add(
        contacts_svc.get_default_role("noc"))
    network_service.nsc_required_contact_roles.add(
        contacts_svc.get_default_role("implementation"))

    _assign_ip_addresses(
        network_service=network_service,
        ip_addresses=network_service_request.get("ip_addresses"),
        scoping_account=scoping_account)

    return network_service


def _create_p2mp_network_service(
        scoping_account=None,
        managing_account=None,
        consuming_account=None,
        billing_account=None,
        product_offering=None,
        network_service_request=None,
    ) -> P2MPNetworkService:
    """
    Create an ETree network service

    :param scoping_account: The scoping account managing network services
    :param network_service_request: A request for a new network service.
    """
    # Let's create the elan network service!
    network_service = P2MPNetworkService(
        state=State.REQUESTED,
        managing_account=managing_account,
        consuming_account=consuming_account,
        billing_account=billing_account,
        scoping_account=scoping_account,
        product_offering=product_offering,
        external_ref=network_service_request.get("external_ref"),
        purchase_order=network_service_request.get("purchase_order", ""),
        contract_ref=network_service_request.get("contract_ref"),
        public=network_service_request.get("public", False),
        display_name=network_service_request.get(
            "display_name", "new p2mp network service"))
    network_service.save()

    # Assign required contact roles: NOC, Implementation
    network_service.nsc_required_contact_roles.add(
        contacts_svc.get_default_role("noc"))
    network_service.nsc_required_contact_roles.add(
        contacts_svc.get_default_role("implementation"))

    _assign_ip_addresses(
        network_service=network_service,
        ip_addresses=network_service_request.get("ip_addresses"),
        scoping_account=scoping_account)

    return network_service


def _create_cloud_network_service(
        scoping_account=None,
        managing_account=None,
        consuming_account=None,
        billing_account=None,
        product_offering=None,
        network_service_request=None,
    ) -> CloudNetworkService:
    """
    Create a cloud network service.

    :param scoping_account: The scoping account managing network services
    :param managing_account: A resolved and validated managing account
    :param consuming_account: A validated consuming account
    :param billing_account: An account with billing information present
    :param product: A validated eline product
    :param network_service_request: A request for a new network service.
    """
    # Gather additional infomation
    cloud_key = network_service_request["cloud_key"]
    diversity = product_offering.diversity
    capacity = network_service_request.get("capacity")

    provider_ref = None
    workflow = product_offering.service_provider_workflow
    if workflow == ServiceProviderWorkflow.PROVIDER_FIRST:
        provider_ref = "PROVIDER-DEMO-REF-" + cloud_key

    # Check constraints
    products_svc.assert_capacity_constraints(
        scoping_account=scoping_account,
        product_offering=product_offering,
        capacity=capacity)

    products_svc.assert_valid_cloud_key(
        scoping_account=scoping_account,
        product_offering=product_offering,
        cloud_key=cloud_key)

    # Let's create the cloud network service!
    network_service = CloudNetworkService(
        state=State.REQUESTED,
        managing_account=managing_account,
        consuming_account=consuming_account,
        billing_account=billing_account,
        scoping_account=scoping_account,
        product_offering=product_offering,
        external_ref=network_service_request.get("external_ref"),
        purchase_order=network_service_request.get("purchase_order", ""),
        contract_ref=network_service_request.get("contract_ref"),
        cloud_key=cloud_key,
        provider_ref=provider_ref,
        capacity=capacity,
        diversity=diversity,
    )
    network_service.save()

    # Assign required contact roles: NOC, Implementation
    network_service.nsc_required_contact_roles.add(
        contacts_svc.get_default_role("noc"))
    network_service.nsc_required_contact_roles.add(
        contacts_svc.get_default_role("implementation"))

    return network_service


def _assign_ip_addresses(
        network_service=None,
        ip_addresses=None,
        scoping_account=None,
    ):
    """Use the ip addresses by the network service"""
    # Unbind all ip addresses
    for addr in network_service.ip_addresses.all():
        ip_addresses_svc.release_ip_address(
            ip_address=addr,
            scoping_account=scoping_account)

    if not ip_addresses:
        return # We are done here.

    # Bind new ip addresses
    for ip_address in ip_addresses:
        ip_addresses_svc.bind_ip_address(
            ip_address=ip_address,
            to=network_service,
            scoping_account=scoping_account)


def update_network_service(
        network_service=None,
        network_service_update=None,
        scoping_account=None,
    ):
    """
    Update a network service.

    :param network_service: The network service to update
    :param network_service_update: Updates to the network service
    :param scoping_account: A account context
    """
    network_service = _apply_network_service_update(
        network_service=network_service,
        network_service_update=network_service_update,
        scoping_account=scoping_account)

    return network_service


def _apply_network_service_update(
        network_service=None,
        network_service_update=None,
        scoping_account=None,
    ):
    """
    Dispatch the update of a network service to the
    type specific update hanlder.


    :param network_service: The network service to update
    :param network_service_update: Updates to the network service
    :param scoping_account: A account context
    """
    network_service = get_network_service(
        network_service=network_service,
        scoping_account=scoping_account)

    # We only allow for elan, eline and etree network
    # services to be updated
    if not isinstance(network_service, (
            P2PNetworkService,
            MP2MPNetworkService,
            P2MPNetworkService,
        )):
        raise NotImplementedError

    update_fields = network_service_update.keys()

    # Update related accounts
    if "managing_account" in update_fields:
        managing_account = accounts_svc.get_account(
            scoping_account=scoping_account,
            account=network_service_update["managing_account"])
        network_service.managing_account = managing_account

    if "consuming_account" in update_fields:
        consuming_account = accounts_svc.get_account(
            scoping_account=scoping_account,
            account=network_service_update["consuming_account"])
        network_service.consuming_account = consuming_account

    if "billing_account" in update_fields:
        billing_account = accounts_svc.get_account(
            scoping_account=scoping_account,
            account=network_service_update["billing_account"])
        accounts_svc.assert_billing_information(billing_account)
        network_service.billing_account = billing_account

    # Name
    if "display_name" in update_fields:
        network_service.name = network_service_update["display_name"]

    # Purchaseable
    if "external_ref" in update_fields:
        network_service.external_ref = \
            network_service_update["external_ref"]
    if "purchase_order" in update_fields:
        network_service.purchase_order = \
            network_service_update["purchase_order"]
    if "contract_ref" in update_fields:
        network_service.contract_ref = \
            network_service_update["contract_ref"]

    # Product change
    if "product_offering" in update_fields:
        offering = products_svc.get_product_offering(
            scoping_account=scoping_account,
            product_offering=network_service_update["product_offering"])
        # The product type needs to match
        if not isinstance(offering, type(network_service.product_offering)):
            raise ProductTypeMismatch()
        network_service.product_offering = offering

    # Dispatch updates for all other fields, which might
    # not be common across all network services.
    if isinstance(network_service, P2PNetworkService):
        return _update_p2p_network_service(
            update_fields=update_fields,
            network_service=network_service,
            network_service_update=network_service_update,
            scoping_account=scoping_account)
    if isinstance(network_service, MP2MPNetworkService):
        return _update_mp2mp_network_service(
            update_fields=update_fields,
            network_service=network_service,
            network_service_update=network_service_update,
            scoping_account=scoping_account)
    if isinstance(network_service, P2MPNetworkService):
        return _update_p2mp_network_service(
            update_fields=update_fields,
            network_service=network_service,
            network_service_update=network_service_update,
            scoping_account=scoping_account)


def _update_p2p_network_service(
        network_service=None,
        network_service_update=None,
        scoping_account=None,
        update_fields=None,
    ):
    """ Update eline specific fields."""
    # There none anymore.
    # Persist changes
    network_service.save()
    return network_service


def _update_mp2mp_network_service(
        network_service=None,
        network_service_update=None,
        scoping_account=None,
        update_fields=None,
    ):
    """
    Update elan specific fields and relations
    """

    # Ip address updates
    if "ip_addresses" in update_fields:
        _assign_ip_addresses(
            network_service=network_service,
            ip_addresses=network_service_update["ip_addresses"],
            scoping_account=scoping_account)

    network_service.save()

    return network_service


def _update_p2mp_network_service(
        network_service=None,
        network_service_update=None,
        scoping_account=None,
        update_fields=None,
    ):
    """
    Update elan specific fields and relations
    """
    # Ip address updates
    if "ip_addresses" in update_fields:
        _assign_ip_addresses(
            network_service=network_service,
            ip_addresses=network_service_update["ip_addresses"],
            scoping_account=scoping_account)

    network_service.save()

    return network_service


# DEPRECATED, will be removed in next version
def delete_network_service(
        network_service=None,
        scoping_account=None):
    """Request decomissioning of a network service"""
    network_service = get_network_service(
        network_service=network_service,
        scoping_account=scoping_account)

    # System network services can not be decomissioned
    if not network_service.scoping_account:
        raise ResourceAccessDenied(network_service)

    if not network_service.scoping_account == scoping_account:
        raise ResourceAccessDenied(network_service)

    # Change state
    network_service.state = State.DECOMMISSION_REQUESTED
    network_service.save()

    return network_service


def create_network_service_change_request(
        network_service=None,
        network_service_change_request=None,
        scoping_account=None):
    """Create a network service change request"""
    request = network_service_change_request
    network_service = get_network_service(
        network_service=network_service,
        scoping_account=scoping_account,
    )
    if not isinstance(network_service, P2PNetworkService):
        raise ValidationError(
            "NetworkService is not a p2p virtual circuit",
            field="network_service")

    # Check if we already have a pendeing change request
    if network_service.change_requests.exists():
        raise ValidationError(
            "There is already a pending change request",
            field="network_service")

    product_offering = products_svc.get_product_offering(
        product_offering=request["product_offering"],
        scoping_account=scoping_account)
    if not isinstance(product_offering, P2PNetworkProductOffering):
        raise ValidationError(
            "ProductOffering is not a p2p network product offering",
            field="product_offering")

    capacity = request.get("capacity")
    products_svc.assert_capacity_constraints(
        scoping_account=scoping_account,
        product_offering=product_offering,
        capacity=capacity)

    # Create change request
    change_request = NetworkServiceChangeRequest(
        network_service=network_service,
        product_offering=product_offering,
        capacity=capacity)
    change_request.save()

    # Make network service state pending
    if network_service.state == State.PRODUCTION:
        network_service.state = State.PRODUCTION_CHANGE_PENDING
        network_service.save()

    return change_request


def get_network_service_change_request(
        network_service=None,
        network_service_change_request=None,
        scoping_account=None):
    """Get network service change request"""
    if network_service:
        network_service = get_network_service(
            network_service=network_service,
            scoping_account=scoping_account)
        network_service_change_request = network_service \
            .change_requests.last()

    if not network_service and not network_service_change_request:
        raise ResourceNotFound()

    if not isinstance(
            network_service_change_request,
            NetworkServiceChangeRequest):
        network_service_change_request = \
            NetworkServiceChangeRequest.objects \
                .filter(pk=network_service_change_request) \
                .last()

    if not network_service_change_request:
        raise ResourceNotFound

    # Check permission
    if scoping_account:
        request_scope = network_service_change_request \
            .network_service \
            .scoping_account
        if not request_scope == scoping_account:
            raise ResourceAccessDenied(network_service)

    return network_service_change_request


def delete_network_service_change_request(
        network_service=None,
        network_service_change_request=None,
        scoping_account=None):
    """Remove a change request"""
    change_request = get_network_service_change_request(
        scoping_account=scoping_account,
        network_service=network_service,
        network_service_change_request=network_service_change_request)
    # Copy data and delete change request
    change_request_copy = copy(change_request)
    change_request.delete()
    return change_request_copy


def get_network_features(filters=None, **_):
    """
    Get a list of features.
    These might be filtered by network service.

    :param filters: A dict of filters.
    """
    # In case the network service is passed as an object,
    # we use the filter pk. TODO: patch filtering fields to
    # accept objects.
    if filters:
        network_service = filters.get("network_service")
        if network_service and isinstance(network_service, NetworkService):
            filters["network_service"] = network_service.pk

    filtered = NetworkFeatureFilter(filters)

    return filtered.qs


def get_network_feature(
        scoping_account=None,
        network_feature=None) -> NetworkFeature:
    """
    Retrieve a single feature by it's primary key

    :param feature: The pk of the feature
    """
    # Lookup strategies
    if network_feature and isinstance(network_feature, NetworkFeature):
        return network_feature

    return NetworkFeature.objects.get(pk=network_feature)


def assert_network_feature_available(
        network_feature=None,
        network_service=None,
        scoping_account=None):
    """
    Check for the availability of a feature in a given network
    service.

    :raises NetworkFeatureNotAvailable:
        In case the feature is not available

    :param scoping_account: The managing account
    :param network_feature: The network feature
    :param network_service: The service to check for feature
        availability
    """
    features = network_service.network_features.all()
    if network_feature not in features:
        raise NetworkFeatureNotAvailable(
            network_feature, network_service)
