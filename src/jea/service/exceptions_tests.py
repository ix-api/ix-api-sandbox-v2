
"""
Service Exceptions Tests
"""

from model_bakery import baker

from jea.service import exceptions


def test_network_service_not_available():
    """Test network service not available exception"""
    service = baker.prepare("service.ExchangeLanNetworkService")
    exc = exceptions.NetworkServiceNotAvailable(service)
    assert exc.detail


def test_network_feature_not_available():
    """test network feature not available exception"""
    feature = baker.prepare("service.RouteServerNetworkFeature")
    exc = exceptions.NetworkFeatureNotAvailable(feature)
    assert exc.detail


def test_network_feature_not_available_with_service():
    """Test feature not available for a network service"""
    feature = baker.prepare("service.RouteServerNetworkFeature")
    service = baker.prepare("service.ExchangeLanNetworkService")

    exc = exceptions.NetworkFeatureNotAvailable(
        feature, network_service=service)
    assert exc.detail


def test_member_joining_rule_in_use_exception():
    """Test member joining rule in use"""
    exc = exceptions.MemberJoiningRuleInUse()
    assert exc.detail

