
"""
Services Models
---------------

Provide models for describing services and service features.
"""

from typing import Optional
from uuid import uuid4

import enumfields
from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.contrib.postgres import fields as postgres_fields
from polymorphic.models import PolymorphicModel
from ixapi_schema.v2.constants.service import (
    NETWORK_SERVICE_TYPE_EXCHANGE_LAN,
    NETWORK_SERVICE_TYPE_P2P,
    NETWORK_SERVICE_TYPE_P2MP,
    NETWORK_SERVICE_TYPE_MP2MP,
    NETWORK_SERVICE_TYPE_CLOUD,

    NETWORK_FEATURE_TYPE_ROUTESERVER,

    MEMBER_JOINING_RULE_TYPE_ALLOW,
    MEMBER_JOINING_RULE_TYPE_DENY,
)

from jea.crm.models import (
    OwnableMixin,
    ManagedMixin,
    BillableMixin,
    InvoiceableMixin,
    AccountScopedMixin,
)
from jea.stateful.models import (
    StatefulMixin,
    StatefulPolymorphicManager,
)
from jea.ipam.models import (
    AddressFamilies,
)
from jea.config.models import (
    RouteServerSessionMode,
    BGPSessionType,
)
from utils.datastructures import reverse_mapping

#
# Network Service Membership
#
class MemberJoiningRule(
        OwnableMixin,
        ManagedMixin,
        AccountScopedMixin,
        PolymorphicModel,
    ):
    """A rule for members to join a network service."""
    network_service = models.ForeignKey(
        "NetworkService",
        related_name="member_joining_rules",
        on_delete=models.CASCADE)


class DenyMemberJoiningRule(MemberJoiningRule):
    """A rule for rejecting a member"""
    __polymorphic_type__ = MEMBER_JOINING_RULE_TYPE_DENY


class AllowMemberJoiningRule(MemberJoiningRule):
    """A rule for allowing to join the network service"""
    capacity_min = models.PositiveIntegerField(null=True)
    capacity_max = models.PositiveIntegerField(null=True)

    __polymorphic_type__ = MEMBER_JOINING_RULE_TYPE_ALLOW


MEMBER_JOINING_RULE_MODELS = {
    MEMBER_JOINING_RULE_TYPE_ALLOW: AllowMemberJoiningRule,
    MEMBER_JOINING_RULE_TYPE_DENY: DenyMemberJoiningRule,
}

#
# Network Services
#
class NetworkService(
        OwnableMixin,
        ManagedMixin,
        InvoiceableMixin,
        AccountScopedMixin,
        StatefulMixin,
        PolymorphicModel,
    ):
    """Polymorphic network service base"""
    product_offering = models.ForeignKey(
        "catalog.ProductOffering",
        related_name="network_services",
        on_delete=models.PROTECT)

    public = models.BooleanField(default=False)

    # Cancellation
    decommission_at = models.DateField(null=True, blank=True)
    charged_until = models.DateField(null=True, blank=True)

    # Roles
    nsc_required_contact_roles = models.ManyToManyField(
        "crm.Role",
        blank=True,
        related_name="required_by_network_services")

    objects = StatefulPolymorphicManager()

    @property
    def all_nsc_required_contact_roles(self):
        """Contact roles queryset shortcut"""
        return self.nsc_required_contact_roles.all()


class ExchangeLanNetworkService(NetworkService):
    """An exchange lan network service model"""

    name = models.CharField(max_length=40)
    metro_area_network = models.ForeignKey(
        "catalog.MetroAreaNetwork",
        on_delete=models.PROTECT)

    # External references
    peeringdb_ixid = models.PositiveIntegerField(null=True, blank=True)
    ixfdb_ixid = models.PositiveIntegerField(null=True, blank=True)

    # IP Addresses
    @property
    def subnet_v4(self):
        """Get the associated v4 subnet address"""
        return self.ip_addresses \
            .filter(version=4) \
            .active() \
            .first()

    @property
    def subnet_v6(self):
        """Get the assiciated v6 subnet"""
        return self.ip_addresses \
            .filter(version=6) \
            .active() \
            .first()

    def __str__(self):
        """Make string representation of exchange lan network service"""
        if self.metro_area_network:
            return (
                f"ExchangeLanNetworkService: {self.name} "
                f"({self.metro_area_network.name}) ({self.pk})"
            )
        return f"ExchangeLanNetworkService: {self.name} ({self.pk})"


    def __repr__(self):
        """Make shortrepresentation"""
        return f"<ExchangeLanNetworkService id='{self.pk}' name='{self.name}'>"

    class Meta:
        verbose_name = "ExchangeLAN Network Service"

    # Keep the referenced service type
    __polymorphic_type__ = NETWORK_SERVICE_TYPE_EXCHANGE_LAN


class EVPNetworkService(models.Model):
    """Base model for all ethernet virtual private network services"""
    display_name = models.CharField(default="", max_length=256)

    nsc_product_offerings = models.ManyToManyField("catalog.ProductOffering")

    class Meta:
        abstract = True


class MP2MPNetworkService(BillableMixin, EVPNetworkService, NetworkService):
    """
    MP2MP (E-Lan) Network Service

    This is a model for a multi point network, e.g. a
    closed user group.
    """
    def __str__(self):
        """E-Lan to string"""
        return "MP2MP Network Service"

    def __repr__(self):
        """MP2MP representation"""
        return f"<MP2MPNetworkService id='{self.pk}'>"

    class Meta:
        verbose_name = "MP2MP Network Service"

    __polymorphic_type__ = NETWORK_SERVICE_TYPE_MP2MP


class P2PNetworkService(BillableMixin, EVPNetworkService, NetworkService):
    """
    P2P (E-Line) Network Service

    A model for point to point networks.
    """
    joining_member_account = models.ForeignKey(
        "crm.Account",
        null=False,
        on_delete=models.PROTECT)

    availability_zones = models.ManyToManyField(
        "catalog.AvailabilityZone",
        related_name="p2p_network_services")

    # capacity = models.PositiveIntegerField()
    @property
    def capacity(self):
        """
        The capacity is derived from the configured connections,
        or rate limits in the network service config.
        """
        # This is a first draft.
        # TODO: Implement someting more meaningful.
        capacities = [c.capacity for c in self.configs.all() if c.capacity]
        if not capacities:
            return None
        return min(capacities)


    def __str__(self):
        """Eline to string"""
        return "P2P Network Service"


    def __repr__(self):
        """E-Line Service representation"""
        return f"<P2PNetworkService id='{self.pk}'>"

    class Meta:
        verbose_name = "P2P Network Service"

    __polymorphic_type__ = NETWORK_SERVICE_TYPE_P2P


class P2MPNetworkService(BillableMixin, EVPNetworkService, NetworkService):
    """
    P2MP (E-Tree) Network Service

    A multi point network service, where multiple leaf nodes
    are connected to one or more roots.
    """
    def __str__(self):
        """P2MP to string"""
        return "P2MP Network Service"

    def __repr__(self):
        """E-Tree Service representation"""
        return f"<P2MPNetworkService id='{self.pk}'>"

    class Meta:
        verbose_name = "P2MP Network Service"

    __polymorphic_type__ = NETWORK_SERVICE_TYPE_P2MP


class CloudNetworkService(BillableMixin, NetworkService):
    """A Cloud network service model"""
    cloud_key = models.CharField(max_length=512)
    provider_ref = models.CharField(null=True, max_length=512)
    diversity = models.PositiveIntegerField(default=1)
    capacity = models.PositiveIntegerField(null=True)

    availability_zones = models.ManyToManyField(
        "catalog.AvailabilityZone",
        related_name="cloud_network_services")

    def assign_provider_ref(self):
        """
        Generate a provider ref and assign it.
        This can be run as as change handler sideeffect.
        """
        self.provider_ref = str(uuid4())
        self.save()

        return self

    def __str__(self):
        """Cloud network service to string"""
        return "CloudService"

    def __repr__(self):
        """Closed user group representation"""
        return f"<CloudNetworkService id='{self.pk}'>"

    class Meta:
        verbose_name = "Cloud Network Service"

    __polymorphic_type__ = NETWORK_SERVICE_TYPE_CLOUD


# Type to class mapping
NETWORK_SERVICE_MODELS = {
    NETWORK_SERVICE_TYPE_EXCHANGE_LAN: ExchangeLanNetworkService,
    NETWORK_SERVICE_TYPE_P2P: P2PNetworkService,
    NETWORK_SERVICE_TYPE_P2MP: P2MPNetworkService,
    NETWORK_SERVICE_TYPE_MP2MP: MP2MPNetworkService,
    NETWORK_SERVICE_TYPE_CLOUD: CloudNetworkService,
}


# CloudNetworkService ChangeRequests
class NetworkServiceChangeRequest(models.Model):
    """A deferred change to a network service"""
    # Currently only p2p_vc network services support
    # this and the method could be more generalized.
    # There is a pull request to the ix-api-schema
    # describing a generalized variant.
    network_service = models.ForeignKey(
        NetworkService,
        related_name="change_requests",
        on_delete=models.CASCADE)

    product_offering = models.ForeignKey(
        "catalog.ProductOffering",
        related_name="network_service_change_requests",
        on_delete=models.CASCADE)

    capacity = models.PositiveIntegerField(null=True)

    class Meta:
        verbose_name = "Network Service Change Request"


#
# Features
#
class NetworkFeature(PolymorphicModel):
    """Polymorphic base feature"""
    name = models.CharField(max_length=80)
    required = models.BooleanField()

    network_service = models.ForeignKey(
        NetworkService,
        related_name="network_features",
        on_delete=models.CASCADE)

    nfc_required_contact_roles = models.ManyToManyField(
        "crm.Role",
        blank=True,
        related_name="required_by_network_features")

    @property
    def all_nfc_required_contact_roles(self):
        """Contact roles queryset shortcut"""
        return self.nfc_required_contact_roles.all()


class IXPSpecificFeatureFlag(models.Model):
    """
    A model describing some IXP specifig feature flag.
    Flags have a name and a description.
    """
    name = models.CharField(max_length=40)
    description = models.CharField(max_length=80)
    mandatory = models.BooleanField(default=False)

    network_feature = models.ForeignKey(
        NetworkFeature,
        related_name="ixp_specific_flags",
        on_delete=models.CASCADE)

    def __str__(self):
        """Feature flag as string"""
        return f"IXPFeatureFlag: {self.name}"

    def __repr__(self):
        """Feature flag representation"""
        return f"<IXPSpecificFeatureFlag id='{self.pk}' name='{self.name}'>"

    class Meta:
        verbose_name = "IXP-specific feature flag"


class RouteServerNetworkFeature(NetworkFeature):
    """A route server feature model"""
    asn = models.PositiveIntegerField(null=True)
    fqdn = models.CharField(max_length=80)
    looking_glass_url = models.URLField(null=True, blank=True)
    address_families = postgres_fields.ArrayField(
        enumfields.EnumField(AddressFamilies, max_length=10))

    session_mode = enumfields.EnumField(
        RouteServerSessionMode, max_length=20)
    available_bgp_session_types = postgres_fields.ArrayField(
        enumfields.EnumField(BGPSessionType, max_length=20))

    @property
    def ip_v4(self):
        """Get the first active IP v4 address."""
        return self.ip_addresses \
            .filter(version=4) \
            .active() \
            .first()

    @property
    def ip_v6(self):
        """Get the first active IP v6 address."""
        return self.ip_addresses \
            .filter(version=6) \
            .active() \
            .first()

    def __str__(self):
        """RouteServer Feature as string"""
        return f"RouteServer: {self.fqdn}"

    def __repr__(self):
        """RouteServer representation"""
        return f"<RouteServerFeature id='{self.pk}' fqdn='{self.fqdn}'>"

    class Meta:
        verbose_name = "RouteServer Feature"

    __polymorphic_type__ = NETWORK_FEATURE_TYPE_ROUTESERVER



# Type Mapping
NETWORK_FEATURE_MODELS = {
    NETWORK_FEATURE_TYPE_ROUTESERVER: RouteServerNetworkFeature,
}

