
"""
Services Models Tests
"""

from datetime import datetime, timezone

import pytest
from model_bakery import baker
from django.db.models import ProtectedError

from jea.service.models import (
    MemberJoiningRule,
    DenyMemberJoiningRule,
    AllowMemberJoiningRule,
    NetworkService,
    ExchangeLanNetworkService,
    P2PNetworkService,
    MP2MPNetworkService,
    P2MPNetworkService,
    CloudNetworkService,
    NetworkFeature,
    RouteServerNetworkFeature,
)

from jea.ipam.models import (
    IpAddress,
)


@pytest.mark.django_db
def test_member_joining_rule():
    """Test a member joining rule"""
    # This should work without failing:
    r1 = baker.make(DenyMemberJoiningRule)
    r2 = baker.make(AllowMemberJoiningRule,
                    capacity_min=0,
                    capacity_max=None)

    assert r1 in MemberJoiningRule.objects.all()
    assert r2 in MemberJoiningRule.objects.all()


@pytest.mark.django_db
def test_network_services():
    """Test polymorphic network services"""
    s_1 = baker.make(ExchangeLanNetworkService)
    s_2 = baker.make(MP2MPNetworkService)
    s_3 = baker.make(P2PNetworkService)
    s_4 = baker.make(MP2MPNetworkService)

    # Find polymorphic by id
    assert NetworkService.objects.get(pk=s_1.id)
    assert NetworkService.objects.get(pk=s_2.id)
    assert NetworkService.objects.get(pk=s_3.id)
    assert NetworkService.objects.get(pk=s_4.id)


@pytest.mark.django_db
def test_network_service_protected_relations():
    """Check protected relations"""
    s1 = baker.make(ExchangeLanNetworkService)

    consuming_account = s1.consuming_account
    managing_account = s1.managing_account
    offering = s1.product_offering

    assert consuming_account, \
        "There should be an assigned owning account"
    assert managing_account, \
        "There should be an assigned billing account"
    assert offering, \
        "There should be an assigned product offering"

    # Deletion should not be possible unless the offering
    # referenced by the service is gone
    with pytest.raises(ProtectedError):
        offering.delete()

    with pytest.raises(ProtectedError):
        managing_account.delete()

    with pytest.raises(ProtectedError):
        consuming_account.delete()

    # After deleting the service, we should be able
    # to remove the refences.
    s1.delete()
    offering.delete()
    managing_account.delete()
    consuming_account.delete()


@pytest.mark.django_db
def test_exchange_lan_network_service():
    """Test Exchange Lan Network Service"""
    # Check, that we can assign IP addresses
    service = baker.make(ExchangeLanNetworkService)
    ip = baker.make(IpAddress)
    service.ip_addresses.add(ip)
    baker.make(IpAddress, exchange_lan_network_service=service)

    assert service.ip_addresses.count() == 2 # one is already assigned


@pytest.mark.django_db
def test_exchange_lan_network_service_network_addresses_access():
    """Test Exchange Lan Network Service network address properties"""
    service = baker.make(ExchangeLanNetworkService)
    ip4 = baker.make(IpAddress, version=4)
    ip6 = baker.make(IpAddress, version=6)

    service.ip_addresses.add(ip4)
    service.ip_addresses.add(ip6)

    assert service.ip_addresses.count() == 2

    assert ip4 == service.subnet_v4
    assert ip6 == service.subnet_v6


@pytest.mark.django_db
def test_cloud_network_service():
    """Test cloud network service"""
    service = baker.make(CloudNetworkService)
    assert service


@pytest.mark.django_db
def test_network_service_change_request():
    """Test network service change request"""
    req = baker.make("service.NetworkServiceChangeRequest")
    assert req


@pytest.mark.django_db
def test_polymorphic_features():
    """Test polymorphic features model"""
    service = baker.make(NetworkService)
    feature = baker.make(
        RouteServerNetworkFeature,
        network_service=service)

    assert service.network_features.get(pk=feature.pk)


@pytest.mark.django_db
def test_service_feature_relation():
    """Test feature to service relation"""
    s = baker.make(NetworkService)
    f_1 = baker.make(RouteServerNetworkFeature, network_service=s)
    assert f_1 in list(s.network_features.all())
    # Delete network service, feature should be gone aswell.
    s.delete()
    with pytest.raises(NetworkFeature.DoesNotExist):
        f_1.refresh_from_db()


@pytest.mark.django_db
def test_feature_route_server_ipaddresses():
    """Test Routeserver Feature with IP addresses"""
    feature = baker.make(RouteServerNetworkFeature)
    ipp = baker.make(
        IpAddress,
        version=6,
        valid_not_after=datetime(
            1997, 1, 1, tzinfo=timezone.utc))
    ip4 = baker.make(IpAddress, version=4)
    ip6 = baker.make(IpAddress, version=6)
    feature.ip_addresses.add(ipp)
    feature.ip_addresses.add(ip4)
    feature.ip_addresses.add(ip6)

    assert feature.ip_v4 == ip4
    assert feature.ip_v6 == ip6
    assert ipp in feature.ip_addresses.all()


@pytest.mark.django_db
def test_feature_route_server_ipaddress_properties():
    """Test ip address property access"""
    feature = baker.make(RouteServerNetworkFeature)
    ip4 = baker.make(IpAddress, version=4)
    ip6 = baker.make(IpAddress, version=6)
    feature.ip_addresses.add(ip4)
    feature.ip_addresses.add(ip6)

    assert feature.ip_v4 == ip4
    assert feature.ip_v6 == ip6


@pytest.mark.django_db
def test_p2p_network_service__relations():
    """Test eline network service reltations"""
    billing_account = baker.make("crm.Account")
    service = baker.make(
        P2PNetworkService,
        billing_account=billing_account)

    with pytest.raises(ProtectedError):
        billing_account.delete()


@pytest.mark.django_db
def test_network_service__member_joining_rules():
    """Get the member joining rules of a network service"""
    account = baker.make("crm.Account")
    rule = baker.make(
        AllowMemberJoiningRule,
        consuming_account=account,
        managing_account=account)
    service = baker.make(
        MP2MPNetworkService,
        consuming_account=account,
        managing_account=account,
        member_joining_rules=[rule])

    assert rule in service.member_joining_rules.all()


@pytest.mark.django_db
def test_mp2mp_network_service__relations():
    """Test elan network service relations"""
    billing_account = baker.make("crm.Account")
    service = baker.make(
        MP2MPNetworkService,
        billing_account=billing_account)
    prefix = baker.make("ipam.IpAddress", mp2mp_network_service=service)

    with pytest.raises(ProtectedError):
        billing_account.delete()

    assert prefix in service.ip_addresses.all()


@pytest.mark.django_db
def test_p2mp_network_service__relations():
    """Test etree relations"""
    billing_account = baker.make("crm.Account")
    service = baker.make(
        P2MPNetworkService,
        billing_account=billing_account)

    with pytest.raises(ProtectedError):
        billing_account.delete()

