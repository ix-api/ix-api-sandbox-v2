"""
Network Services Change Handlers
"""

from datetime import timedelta

from ixapi_schema.v2 import schema

from jea.exceptions import UnableToFulfillError
from jea.stateful.changes import schedule_in, state_update
from jea.stateful.models import (
    Change,
    State,
)
from jea.service.models import (
    P2PNetworkService,
    P2MPNetworkService,
    MP2MPNetworkService,
)
from jea.service.services import network as network_svc
from jea.service.status.reducers import update_network_service_status


def create(change: Change):
    """Create a new network service"""
    # Validate data and let the service create a new
    # network service instance.
    serializer = schema.NetworkServiceRequest(data=change.data)
    serializer.is_valid(raise_exception=True)
    network_service = network_svc.create_network_service(
        network_service_request=serializer.validated_data,
        scoping_account=change.scoping_account)

    change.ref = network_service
    change.save()

    # Update status messages
    network_service = update_network_service_status(network_service)

    # Initialize service state
    network_service.state = State.REQUESTED
    network_service.save()

    return network_service, []


def update(change: Change):
    """Update a network service"""
    serializer = schema.NetworkServiceRequest(
        data=change.data, partial=change.partial)
    serializer.is_valid(raise_exception=True)

    # Update the network service
    network_service = network_svc.update_network_service(
        network_service=change.ref,
        network_service_update=serializer.validated_data,
        scoping_account=change.scoping_account)

    decommission_states = (State.DECOMMISSION_REQUESTED, State.DECOMMISSIONED)
    if network_service.state in decommission_states:
        # We do not allow updating the config
        raise UnableToFulfillError

    return network_service, []


def decommission(change: Change):
    """Decommission a network service"""
    network_service = network_svc.get_network_service(
        scoping_account=change.scoping_account,
        network_service=change.ref)

    if network_service.state == State.DECOMMISSION_REQUESTED:
        # Decommission network service and configs
        decommissions = [
            state_update(cfg, State.DECOMMISSIONED)
            for cfg in network_service.configs.all()
        ]
        return network_service, [
            state_update(network_service, State.DECOMMISSIONED),
        ] + decommissions


    network_service.state = State.DECOMMISSION_REQUESTED
    network_service.save()

    # This cascades to all network service configs
    for cfg in network_service.configs.all():
        cfg.state = State.DECOMMISSION_REQUESTED
        cfg.save()

    # Mark as decommission requested
    return network_service, [
        # Schedule decommssioning
        schedule_in(change, timedelta(seconds=5))
    ]
