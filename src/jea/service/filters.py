
"""
Service Filters
"""

import django_filters
from django.db import models

from utils import filters
from jea.crm import filters as crm_filters
from jea.stateful.filters import StatefulMixin
from jea.service import models as service_models
from jea.service.models import (
    NETWORK_SERVICE_MODELS,
    NETWORK_FEATURE_MODELS,
    NetworkService,
    NetworkFeature,
)


class NetworkServiceFilter(
        crm_filters.OwnableFilterMixin,
        StatefulMixin,
        django_filters.FilterSet,
    ):
    """Filter network services"""
    id = filters.BulkIdFilter()

    type = filters.PolymorphicModelTypeFilter(NETWORK_SERVICE_MODELS)

    pop = django_filters.CharFilter(method="filter_pop")
    def filter_pop(self, queryset, _name, value):
        """Filter by pop"""
        # TODO: Implement something meaningfull in here.
        #       What that is is still to be discussed
        return queryset

    #
    # Fields
    #
    class Meta:
        model = NetworkService
        fields = [
            "product_offering",
        ]



class NetworkFeatureFilter(django_filters.FilterSet):
    id = filters.BulkIdFilter()
    type = filters.PolymorphicModelTypeFilter(NETWORK_FEATURE_MODELS)

    # TODO: Generalize this fix.
    required = django_filters.CharFilter(method="filter_required")
    def filter_required(self, queryset, _name, value):
        """Filter required network features"""
        value = value.lower()
        bool_value = False
        if value == "true" or value=="1":
            bool_value = True

        return queryset.filter(required=bool_value)


    network_service = django_filters.CharFilter()

    class Meta:
        model = NetworkFeature
        fields = [
            "name",
        ]

