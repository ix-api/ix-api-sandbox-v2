"""
stateful test helpers
"""

from model_bakery import baker

def assert_save_change(change, *args):
    """Persist a change"""
    if not change.scoping_account_id:
        change.scoping_account = baker.make("crm.Account")
    change.save()
    assert change.pk
