"""
Change Dispatcher
-----------------

Process a change by calling the change handler
for a given resource with an action.

A change handler will return the tuple: obj, [list_of_changes]
The next state of the change is then set: While it is still in
the list of changes, the state will be kept. Otherwise the
change is either rejected, due to a validation error or explict
reject exception or applied when everything was ok.
"""

import logging

from django.db import transaction
from django.utils import timezone

from jea.api.v2.errors import problem_factory
from jea.exceptions import UnableToFulfillError
from jea.stateful.models import (
    State,
    Change,
    ChangeState,
    Create,
    Update,
    Decommission,
    SideEffect,
    StateUpdate,
    Severity,
    StatusMessage,
)
from jea.stateful.services import changes as changes_svc
from jea.crm.change_handlers import accounts as accounts_res
from jea.config.change_handlers import (
    network_service_configs as network_service_configs_res,
    network_feature_configs as network_feature_configs_res,
)
from jea.service.change_handlers import (
    network_services as network_services_res,
)

logger = logging.getLogger(__name__)

DECOMMISSION_STATES = (
    State.DECOMMISSION_REQUESTED,
    State.DECOMMISSIONED,
)

def process_pending():
    """Process all changes in queue"""
    for change in changes_svc.get_pending():
        process(change)


def is_schedule_ready(change):
    """Check if the change is within the schedule window"""
    now = timezone.now()
    if change.not_before and change.not_before >= now:
        return False
    if change.not_after and change.not_after < now:
        return False
    return True


def request(scoping_account, change):
    """Request a change"""
    # Assign scoping account
    if not change.scoping_account_id:
        change.scoping_account = scoping_account
    change.save()
    return process(change)


def process(change):
    """
    Try to apply the change and return result.
    """
    # Before executing the change, check if preconditions are met
    if not is_schedule_ready(change):
        return None  # Defer the change

    with change.locked():
        logger.info("apply %s", change)
        try:
            with transaction.atomic():
                res, next_changes = handle(change)
        except Exception as exc:
            # Make status from ixapi exception
            status = _status_from_exc(change, exc)
            status.save()
            change.refresh_from_db()
            change.state = ChangeState.REJECTED
            change.save()
            raise

        # No exception, get next state
        if change not in next_changes:
            change.state = ChangeState.APPLIED

        change.save()
        for next_change in next_changes:
            # Inherit scope if not specified otherwise
            if not next_change.scoping_account_id:
                next_change.scoping_account = change.scoping_account
            next_change.save()

    return res


def handle(change):
    """
    Apply action to resource by calling the handler
    with the current ref and change.
    """
    if isinstance(change, SideEffect):
        return _exec_sideeffect(change)
    if isinstance(change, StateUpdate):
        return _handle_state_change(change)

    handler = _resource_handler(change.resource)
    if isinstance(change, Create):
        res, next_changes = handler.create(change)
    elif isinstance(change, Update):
        # We do not allow updating the stateful object
        if change.ref.state in DECOMMISSION_STATES:
            raise UnableToFulfillError
        res, next_changes = handler.update(change)
    elif isinstance(change, Decommission):
        res, next_changes = handler.decommission(change)
    else:
        raise NotImplementedError

    return res, next_changes


def _resource_handler(resource):
    """Get the resource handler"""
    if resource == Change.ACCOUNT:
        return accounts_res
    if resource == Change.NETWORK_SERVICE:
        return network_services_res
    if resource == Change.NETWORK_SERVICE_CONFIG:
        return network_service_configs_res
    if resource == Change.NETWORK_FEATURE_CONFIG:
        return network_feature_configs_res

    raise NotImplementedError


def _exec_sideeffect(change):
    """Execute a side effect"""
    fun = getattr(change.ref, change.method)
    res = fun(*change.args, **change.kwargs)
    return res, []


def _handle_state_change(change):
    """State change handler"""
    obj = change.ref
    obj.state = change.next
    obj.save()
    return obj, []


def _status_from_exc(change, exc):
    """Make status from exception"""
    # Make status from problem
    problem = problem_factory.make_problem(exc)
    status = StatusMessage(
        severity=Severity.ERROR,
        ref=change,
        tag=problem.ptype,
        message=problem.title,
        attrs=problem.extra,
    )
    status.save()
    return status
