"""
Changes Service
"""

from jea.stateful.models import (
    Change,
    ChangeState,
)


def get_pending(ref=None, scoping_account=None):
    """Get pending changes"""
    changes = Change.objects \
        .filter(locked_at=None) \
        .exclude(state=ChangeState.APPLIED) \
        .exclude(state=ChangeState.REJECTED)

    if ref:
        changes = changes.filter(ref=ref)
    if scoping_account:
        changes = changes.filter(
            scoping_account=scoping_account)

    return changes


def get_rejected(ref=None, scoping_account=None):
    """Get pending changes"""
    changes = Change.objects \
        .filter(locked_at=None) \
        .filter(state=ChangeState.REJECTED)

    if ref:
        changes = changes.filter(ref=ref)
    if scoping_account:
        changes = changes.filter(
            scoping_account=scoping_account)

    return changes


def get_applied(ref=None, scoping_account=None):
    """Get pending changes"""
    changes = Change.objects \
        .filter(locked_at=None) \
        .filter(state=ChangeState.APPLIED)

    if ref:
        changes = changes.filter(ref=ref)
    if scoping_account:
        changes = changes.filter(
            scoping_account=scoping_account)

    return changes

