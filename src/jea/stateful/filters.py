"""
Stateful filters mixins
"""

from django_filters import (
    FilterSet,
    CharFilter,
)

from jea.stateful.models import State


class StatefulMixin(FilterSet):
    """Filter mixin to provide for state filters"""
    # State
    state = CharFilter(
        field_name="state", lookup_expr="iexact")

    state__is_not = CharFilter(
        field_name="state", lookup_expr="iexact", exclude=True)

    def filter_queryset(self, queryset):
        """
        Override filter queryset to exclude
        decommissioned objects.
        """
        if self.form.cleaned_data["state"]:
            return super().filter_queryset(queryset)
        if self.form.cleaned_data["state__is_not"]:
            return super().filter_queryset(queryset)

        # State filter fields will not apply, so we
        # modify the queryset to exclude decommissioned objects
        queryset = queryset.exclude(state=State.DECOMMISSIONED)
        return super().filter_queryset(queryset)
