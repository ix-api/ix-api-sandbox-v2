
"""
Models Tests
"""

import pytest
from model_bakery import baker

from jea.crm.models import Account
from jea.config.models import Connection, P2PNetworkServiceConfig
from jea.stateful.models import (
    Change,
    State,
    StatusMessage,
    Severity,
)


@pytest.mark.django_db
def test_change___repr__():
    """Test representation of changes"""
    change = baker.prepare(Change)
    assert isinstance(repr(change), str)


@pytest.mark.django_db
def test_change___str__():
    """Test representation of changes"""
    change = baker.prepare(Change)
    assert isinstance(str(change), str)


@pytest.mark.django_db
def test_stateful_mixin_status():
    """Test mixin with task"""
    # Create task and status
    change = baker.make(Change)
    status = baker.make(StatusMessage, ref=change)

    # Check if message relation holds
    messages = change.status.all()
    assert status in messages


def test_severity_levels():
    """Test severity"""
    assert Severity.EMERGENCY == 0
    assert Severity.DEBUG == 7


@pytest.mark.django_db
def test_stateful_managers():
    """Test stateful managers"""
    account = baker.make(
        Account, state=State.PRODUCTION)

    assert account in Account.objects.active.all()

    # Decommission account
    account.state = State.DECOMMISSIONED
    account.save()

    assert account not in Account.objects.active.all()
    assert account in Account.objects.with_decommissioned.all()


@pytest.mark.django_db
def test_polymorphic_stateful_managers():
    """Test polymorphic stateful managers"""
    cfg = baker.make(
        P2PNetworkServiceConfig, state=State.PRODUCTION)
    assert cfg in P2PNetworkServiceConfig.objects.active.all()

    # Decommission account
    cfg.state = State.DECOMMISSIONED
    cfg.save()

    assert cfg not in P2PNetworkServiceConfig.objects.active.all()
    assert cfg in P2PNetworkServiceConfig.objects.with_decommissioned.all()


@pytest.mark.django_db
def test_stateful_managers_relations():
    """Test stateful managers"""
    account = baker.make(
        Account, state=State.PRODUCTION)
    conn = baker.make(
        Connection,
        state=State.PRODUCTION,
        managing_account=account,
        consuming_account=account)
    cfg = baker.make(
        P2PNetworkServiceConfig,
        state=State.PRODUCTION,
        managing_account=account,
        consuming_account=account)

    # Decommission account
    account.state = State.DECOMMISSIONED
    account.save()

    assert conn in account.managed_connections.all()
    conn.state = State.DECOMMISSIONED
    conn.save()
    assert conn not in account.managed_connections.active.all()

    assert cfg in account.consumed_networkserviceconfigs.active.all()
    cfg.state = State.DECOMMISSIONED
    cfg.save()
    assert cfg not in account.consumed_networkserviceconfigs.active.all()
    assert cfg in account \
        .consumed_networkserviceconfigs \
        .with_decommissioned


@pytest.mark.django_db
def test_stateful_mixin__has_error_status():
    """Tests error status"""
    account = baker.make(Account, state=State.PRODUCTION)
    assert not account.status.exists()
    assert not account.has_error_status

    # Create warning
    baker.make(
        "stateful.StatusMessage",
        ref=account,
        severity=Severity.WARNING)

    assert account.status.exists()
    assert not account.has_error_status

    # Create error
    baker.make(
        "stateful.StatusMessage",
        ref=account,
        severity=Severity.ALERT)

    assert account.has_error_status
