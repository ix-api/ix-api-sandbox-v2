"""
Stateful
"""
import os
import logging

from django.apps import AppConfig
from django.utils.autoreload import DJANGO_AUTORELOAD_ENV


logger = logging.getLogger(__name__)

class StatefulAppConfig(AppConfig):
    name = "jea.stateful"
    verbose_name = "JEA :: Stateful"

    def ready(self):
        """Application startup"""
        logger.info("Initializing app: %s", self.name)

        from jea.stateful import signals

        # Only start processing when we are
        # in an autoreload / development context.
        # In production a dedicated instance should be spawned
        # as a management command.
        from jea.stateful import process
        if os.environ.get(DJANGO_AUTORELOAD_ENV) == 'true':
            logger.info("Starting development background processing...")
            process.start()
