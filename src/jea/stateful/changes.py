"""
Functions for managing Changes
"""

from datetime import timedelta

from django.utils import timezone

from jea.stateful.models import State, StateUpdate, SideEffect


def schedule_in(change, dt):
    """Update a changes schedule"""
    now = timezone.now()
    change.not_before = now + dt

    return change


def state_update(ref, next_state: State):
    """Request a state update"""
    return StateUpdate(
        ref=ref,
        prev=ref.state,
        next=next_state)


def side_effect(ref, method, args=[], kwargs={}):
    """Request a sideeffect on a model"""
    return SideEffect(
        ref=ref,
        method=method,
        args=args,
        kwargs=kwargs)

