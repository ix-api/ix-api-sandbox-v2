
"""
Stateful Models

Describe object state, status and actions.
Also Operations are described here.

Operations are tasks that can be scheduled to a
specific timeframe and can be executed by an agaent.
These are either sync- or asynchronous.

Agents however can be anything that are able to perform
a given task / operation. This includes software services,
human beings, robots and so on.
"""

import time
from enum import Enum
from contextlib import contextmanager

import enumfields
from django.db import models
from django.contrib.contenttypes import (
    fields as contenttype_fields,
    models as contenttype_models,
)
from django.core.serializers.json import DjangoJSONEncoder
from django.utils import timezone
from polymorphic.models import PolymorphicModel, PolymorphicManager


class State(Enum):
    """Enum containing all states the statemachine can transition"""
    REQUESTED = "requested"
    ALLOCATED = "allocated"

    TESTING = "testing"

    PRODUCTION = "production"
    PRODUCTION_CHANGE_PENDING = "production_change_pending"

    DECOMMISSION_REQUESTED = "decommission_requested"
    DECOMMISSIONED = "decommissioned"

    ARCHIVED = "archived"

    ERROR = "error"
    OPERATOR = "operator"
    SCHEDULED = "scheduled" # I'm not convinced about this.


class ChangeState(Enum):
    """Changes have a state"""
    REQUESTED = "requested"
    ACCEPTED = "accepted"
    REJECTED = "rejected"
    APPLIED = "applied"


class Severity:
    """Status Severity"""
    EMERGENCY = 0
    ALERT = 1
    CRITICAL = 2
    ERROR = 3
    WARNING = 4
    NOTICE = 5
    INFORMATIONAL = 6
    DEBUG = 7


class StatusMessage(models.Model):
    """
    Multiple messages can be assigned to an object
    representing additional status information
    e.g. why the current state is an error state.
    """
    severity = models.PositiveSmallIntegerField(default=1)

    message = models.CharField(max_length=255)
    tag = models.CharField(max_length=80)

    # Message attributes:
    # In general, if you are using any variable in the (rendered)
    # message, you should add it here.
    attrs = models.JSONField(
        encoder=DjangoJSONEncoder,
        default=dict)

    timestamp = models.DateTimeField(default=timezone.now)

    # References:
    # We make a polymorphic relation to stateful objects.
    ref_type = models.ForeignKey(contenttype_models.ContentType,
                                 on_delete=models.CASCADE)
    ref_id = models.PositiveIntegerField()
    ref = contenttype_fields.GenericForeignKey('ref_type', 'ref_id')

    class Meta:
        ordering = ["id"]


def next_change_serial():
    """Generate change serial"""
    return int(time.time() * 10e6)


class ChangeSerialField(models.BigIntegerField):
    """ChangeSerial updates on every save"""
    def pre_save(self, model_instance, add):
        """Set current serial"""
        serial = next_change_serial()
        model_instance.serial = serial
        return serial


class Change(PolymorphicModel):
    """
    A Change encodes an operation (create, update, decomission,
    state-change) in the polymorphic class with payload.
    """
    # Actions
    CREATE = "create"
    UPDATE = "update"
    DECOMMISSION = "decommission"
    STATE_UPDATE = "state_update"
    SIDEEFFECT = "side_effect"

    # Resources
    ACCOUNT = "account"
    CONNECTION = "connection"
    PORT = "port"
    NETWORK_SERVICE = "network_service"
    NETWORK_SERVICE_CONFIG = "network_service_config"
    NETWORK_FEATURE = "network_feature"
    NETWORK_FEATURE_CONFIG = "network_feature_config"

    # Fields
    state = enumfields.EnumField(
        ChangeState,
        default=ChangeState.REQUESTED,
        max_length=255)

    status = contenttype_fields.GenericRelation(
        StatusMessage,
        content_type_field="ref_type",
        object_id_field="ref_id")

    sequence = models.AutoField(primary_key=True)
    serial = ChangeSerialField(db_index=True)

    not_before = models.DateTimeField(null=True)
    not_after = models.DateTimeField(null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    locked_at = models.DateTimeField(null=True)
    locked_deadline = models.DateTimeField(null=True)

    resource = models.CharField(max_length=255)

    # This can be later extended to a full
    # consent mechanism.
    is_confirmed = models.BooleanField(default=False)

    # We make a polymorphic relation to the object we
    # are changing.
    ref_type = models.ForeignKey(contenttype_models.ContentType,
                                 on_delete=models.CASCADE,
                                 null=True)
    ref_id = models.PositiveIntegerField(null=True)
    ref = contenttype_fields.GenericForeignKey('ref_type', 'ref_id')

    scoping_account = models.ForeignKey(
        "crm.Account",
        on_delete=models.CASCADE)

    def __repr__(self):
        """Make representation"""
        return (f"<Change seq={self.sequence} serial={self.serial} "
                f"action={self.action} "
                f"ref_type={self.ref_type} ref_id={self.ref_id}>")

    def __str__(self):
        """Make string representation"""
        return (f"change [seq={self.sequence} serial={self.serial} "
                f"action={self.action} "
                f"ref_type={self.ref_type} ref_id={self.ref_id}]: "
                f"{self.payload}")

    def lock(self):
        """Lock the change for processing"""
        if self.pk:
            self.refresh_from_db()
        self.locked_at = timezone.now()
        self.save()
        return self

    def unlock(self):
        """Unlock the change after processing"""
        if self.pk:
            self.refresh_from_db()
        self.locked_at = None
        self.save()
        return self

    @contextmanager
    def locked(self):
        """Make a locked context"""
        self.lock()
        try:
            yield self
        finally:
            self.unlock()

    @property
    def action(self):
        """Derive action from class"""
        ptype = getattr(self, "__polymorphic_type__", None)
        if ptype:
            return ptype
        return "unknown"

    @property
    def payload(self):
        """Derive payload"""
        return "*no payload*"

    class Meta:
        ordering = ["serial"]


class Create(Change):
    """Encode a create operation"""
    __polymorphic_type__ = Change.CREATE
    data = models.JSONField(
        encoder=DjangoJSONEncoder,
        default=dict)

    @property
    def payload(self):
        """Derive payload"""
        return self.data


class Update(Change):
    """Encode an update"""
    __polymorphic_type__ = Change.UPDATE
    data = models.JSONField(
        encoder=DjangoJSONEncoder,
        default=dict)

    partial = models.BooleanField(default=False)

    @property
    def payload(self):
        """Derive payload"""
        return self.data


class Decommission(Change):
    """Encode a decommissioning"""
    decommission_at = models.DateField(null=True)
    __polymorphic_type__ = Change.DECOMMISSION


class StateUpdate(Change):
    """Change state"""
    __polymorphic_type__ = Change.STATE_UPDATE
    prev = enumfields.EnumField(
        State, max_length=255)
    next = enumfields.EnumField(
        State, max_length=255)

    @property
    def payload(self):
        """Derive payload"""
        return "prev: {}, next: {}".format(
            self.prev,
            self.next)


class SideEffect(Change):
    """Encode a side effect"""
    __polymorphic_type__ = Change.SIDEEFFECT
    method = models.CharField(max_length=255)
    args = models.JSONField(
        encoder=DjangoJSONEncoder,
        default=list)
    kwargs = models.JSONField(
        encoder=DjangoJSONEncoder,
        default=dict)

    @property
    def payload(self):
        """Derive payload"""
        return "@ref({}).{}({}, {})".format(
            type(self.ref).__name__,
            self.method,
            self.args,
            self.kwargs)


class StatefulManagerMixin:
    """
    Model manager for stateful objects.
    Decommissioned objects a are treated as soft-deleted.
    """
    @property
    def active(self):
        """Get queryset excluding decommissioned"""
        queryset = super().get_queryset()
        return queryset \
            .exclude(state=State.DECOMMISSIONED) \
            .exclude(state=State.DECOMMISSION_REQUESTED)

    @property
    def with_decommission_requested(self):
        """Get queryset excluding soft deleted"""
        queryset = super().get_queryset()
        return queryset \
            .exclude(state=State.DECOMMISSIONED)

    @property
    def with_decommissioned(self):
        """Get queryset including soft deleted"""
        return super().get_queryset()

    @property
    def decommissioned(self):
        """Get queryset of decommissioned objects"""
        return super().get_queryset().filter(
            state=State.DECOMMISSIONED)

    @property
    def decommission_requested(self):
        """Get queryset of decommissione requested objects"""
        return super().get_queryset().filter(
            state=State.DECOMMISSION_REQUESTED)


# Create stateful and polymorphic stateful manager.
# This sounds like some crossover between oldschool
# black metal and accounting.
StatefulManager = type("StatefulManager", (
    StatefulManagerMixin,
    models.Manager,
), {})


StatefulPolymorphicManager = type("StatefulPolymorphicManager", (
    StatefulManagerMixin,
    PolymorphicManager,
), {})


class StatefulMixin(models.Model):
    """
    Add state fields to a model and add reference to status
    messages.
    All stateful objects have a list of events documenting
    the state changes.
    """
    state = enumfields.EnumField(
        State,
        default=State.REQUESTED,
        max_length=255)

    status = contenttype_fields.GenericRelation(
        StatusMessage,
        content_type_field="ref_type",
        object_id_field="ref_id")

    changes = contenttype_fields.GenericRelation(
        Change,
        content_type_field="ref_type",
        object_id_field="ref_id")

    def add_status_info_text(self, text):
        """Add an info message"""
        return self.add_status_text(text, Severity.INFORMATIONAL)

    def add_status_notice_text(self, text):
        """Add an info message"""
        return self.add_status_text(text, Severity.NOTICE)

    def add_status_text(self, text, severity):
        msg = StatusMessage(
            ref=self,
            severity=severity,
            message=text)
        msg.save()
        return self


    def clear_status(self):
        """Delete all status messages (statefull sideeffect)"""
        return self.status.clear()

    @property
    def has_status(self):
        """Indicates the presence of any status"""
        return self.status.exists()

    @property
    def has_error_status(self):
        """
        Indicates the presence of a status with a
        severity < 4 (WARNING)
        """
        # return min(s.severity for s in self.status) < Severity.WARNING
        return self.status.filter(severity__lt=Severity.WARNING).exists()

    class Meta:
        abstract = True

