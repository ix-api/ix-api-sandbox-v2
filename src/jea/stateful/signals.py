
from django.dispatch import receiver
from django.db.models.signals import pre_delete

from jea.stateful.models import (
    StatefulMixin,
    Change,
)

@receiver(pre_delete)
def cascade_delete_weak_refs(sender, **kwargs):
    """
    The StatefulMixin is using generic references
    to the stateful object in the StatusMessages
    and Changes.

    However, this means that we have to take care of
    these objects
    """

    if not issubclass(sender, StatefulMixin):
        return

    obj = kwargs["instance"]
    for c in obj.changes.all():
        c.status.clear()
        c.delete()

    for s in obj.status.all():
        s.delete()

    obj.changes.clear()
    obj.status.clear()
