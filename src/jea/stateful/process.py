"""
Stateful Background Process
"""

import time
import logging
import traceback
from multiprocessing import Process

from django.db.utils import Error as DbError

from jea.exceptions import IxApiException
from jea.stateful import dispatch

logger = logging.getLogger(__name__)


def __proc_main__():
    """Process main"""
    logger.info("Starting stateful background tasks.")
    while True:
        try:
            dispatch.process_pending()
        except IxApiException:
            # This is kind of fine and we can ignore this.
            pass
        except DbError:
            logger.error(
                "Could not apply pending changes because of a database error.")
            logger.error("Is the database migrated?")
            time.sleep(0.5)  # Wait.
        except:
            print("Execption while applying change:")
            traceback.print_exc()
        finally:
            time.sleep(0.7777777)


def start():
    """Start process with backround executor"""
    proc = Process(target=__proc_main__, daemon=True)
    proc.start()
    return proc


def stop(proc):
    """Stop the process"""
    return proc.kill()
