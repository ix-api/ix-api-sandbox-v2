"""
Test change dispatcher
"""

import pytest
from mock import patch, MagicMock
from datetime import timedelta

from model_bakery import baker
from django.utils import timezone

from jea.stateful import dispatch
from jea.stateful.models import (
    Change,
    Create,
)


@pytest.mark.django_db
def test_is_schedule_ready__undscheduled():
    """Test window of change"""
    change = baker.make("stateful.Change")
    assert dispatch.is_schedule_ready(change)


@pytest.mark.django_db
def test_is_schedule_ready__deferred():
    """Test window of change"""
    change = baker.make(
        "stateful.Change",
        not_before=timezone.now() + timedelta(hours=1))
    assert not dispatch.is_schedule_ready(change)


@pytest.mark.django_db
def test_is_schedule_ready__expired():
    """Test window of change"""
    change = baker.make(
        "stateful.Change",
        not_after=timezone.now() - timedelta(hours=1))
    assert not dispatch.is_schedule_ready(change)


@pytest.mark.django_db
@patch("jea.stateful.dispatch.process")
def test_request(process):
    """Test dispatching a change"""
    change = Create(
        resource=Change.ACCOUNT)
    account = baker.make("crm.Account")
    res = dispatch.request(account, change)
    assert res
    assert process.called
    assert change.scoping_account == account


@pytest.mark.django_db
@patch("jea.stateful.dispatch.handle")
def test_process(handle):
    """Test change processing"""
    account = baker.make("crm.Account")
    change1 = Create(
        resource=Change.ACCOUNT,
        scoping_account=account)
    change2 = Create(
        resource=Change.ACCOUNT)

    handle.return_value = "new_account", [change2]
    res = dispatch.process(change1)
    assert res
    assert change2.scoping_account == account
