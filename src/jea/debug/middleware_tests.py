
from django.test.client import RequestFactory
from django.http import HttpResponse

from jea.debug.middleware import DebugMiddleware


def test_debug_middleware():
    """Test debug middleware"""
    def view(request):
        return HttpResponse("test")

    request = RequestFactory().get('/')
    response = DebugMiddleware(view)(request)
    assert response.status_code == 200

