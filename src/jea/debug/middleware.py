
from django.conf import settings

def DebugMiddleware(get_response):
    def middleware(request):
        """Intercept requests and dump it to the console"""
        if not settings.DEBUG_REQUESTS:
            return get_response(request)

        print("----- REQUEST -----")
        print(request.body)
        response = get_response(request)
        print("----- RESPONSE -----")
        print(response.content)

        return response

    return middleware

