import pytest
from model_bakery import baker


def test_account__str__():
    """Accounts should be representable in a string"""
    # Create a account with fixed id and name
    account = baker.prepare("crm.Account")
    assert str(account), "Account to string should be implemented"

def test_contact__str__():
    """Test contact string representation"""
    contact = baker.prepare("crm.Contact")
    assert str(contact)


def test_contact__repr__():
    """Test contact repr"""
    contact = baker.prepare("crm.Contact")
    assert repr(contact)

