"""
Change creators
"""

from jea.stateful.models import (
    Change,
    Create,
    Update,
    Decommission,
    StateUpdate,
    SideEffect,
)


def create_account(data) -> Change:
    """Create account create change"""
    return Create(resource=Change.ACCOUNT, data=data)


def update_account(account, data, partial=False) -> Change:
    """Update an account"""
    return Update(
        resource=Change.ACCOUNT,
        partial=partial,
        ref=account,
        data=data)


def decommission_account(account) -> Change:
    """Decommission an account"""
    return Decommission(
        resource=Change.ACCOUNT,
        ref=account)


def update_account_state(account, next_state) -> Change:
    """Create state change"""
    return StateUpdate(
        resource=Change.ACCOUNT,
        ref=account,
        prev=account.state,
        next=next_state)


def confirm_account_change(account, change):
    """Trigger a change confirmation"""
    return SideEffect(
        method="confirm_change",
        args=[change.pk],
        ref=account)
