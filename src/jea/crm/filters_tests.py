
"""
Test CRM filters
"""

import pytest
from model_bakery import baker

from jea.stateful.models import State
from jea.crm import models, filters


@pytest.mark.django_db
def test_account_filter__fields():
    """Test account filtering by fields"""
    c1 = baker.make(
        "crm.Account",
        state=State.PRODUCTION,
        _fill_optional=True)
    c2 = baker.make(
        "crm.Account",
        state=State.PRODUCTION,
        external_ref=c1.external_ref,
        _fill_optional=True)
    c3 = baker.make(
        "crm.Account",
        state=State.PRODUCTION,
        _fill_optional=True)


    filtered = filters.AccountFilter({
        "external_ref": str(c1.external_ref)
    })
    queryset = filtered.qs

    assert c1 in queryset
    assert c2 in queryset
    assert not c3 in queryset


@pytest.mark.django_db
def test_account_filter__billable():
    """Test billable filter"""
    billing_info = baker.make("crm.BillingInformation")
    c1 = baker.make("crm.Account")
    c2 = baker.make("crm.Account", billing_information=billing_info)

    filtered = filters.AccountFilter({
        "billable": True,
    }).qs
    assert c1 not in filtered
    assert c2 in filtered

    filtered = filters.AccountFilter({
        "billable": False,
    }).qs
    assert c1 in filtered
    assert c2 not in filtered

    filtered = filters.AccountFilter({}).qs
    assert c1 in filtered
    assert c2 in filtered


@pytest.mark.django_db
def test_account_filter__state():
    """Test account filtering by state"""
    c1 = baker.make("crm.Account", state="production")
    c2 = baker.make("crm.Account", state="testing")

    filtered = filters.AccountFilter({
        "state": "production",
    })
    queryset = filtered.qs

    assert c1 in queryset
    assert not c2 in queryset


@pytest.mark.django_db
def test_account_filter__bulk_id():
    """Test account filtering by primary key"""
    c1 = baker.make("crm.Account")
    c2 = baker.make("crm.Account")
    c3 = baker.make("crm.Account")

    filtered = filters.AccountFilter({
        "id": f"{c1.pk},{c3.pk}",
    })
    queryset = filtered.qs

    assert c1 in queryset
    assert not c2 in queryset

    assert c3 in queryset


@pytest.mark.django_db
def test_contact_filter__account():
    """Filter contact with account"""
    c1 = baker.make("crm.Account")
    c2 = baker.make("crm.Account")

    ct1 = baker.make("crm.Contact", consuming_account=c1)
    ct2 = baker.make("crm.Contact", consuming_account=c1)
    ct3 = baker.make("crm.Contact", consuming_account=c2)

    filtered = filters.ContactFilter({
        "consuming_account": str(c1.pk) ,
    })
    queryset = filtered.qs

    assert ct1 in queryset
    assert ct2 in queryset
    assert not ct3 in queryset


@pytest.mark.django_db
def test_contact_filter_roles():
    """Test polymorphic type filtering"""
    role_noc = baker.make("crm.Role", name="noc")
    role_impl = baker.make("crm.Role", name="implementation")

    contact1 = baker.make("crm.Contact")
    baker.make("crm.RoleAssignment", role=role_noc, contact=contact1)
    contact2 = baker.make("crm.Contact")
    baker.make("crm.RoleAssignment", role=role_impl, contact=contact2)
    contact3 = baker.make("crm.Contact")
    baker.make("crm.RoleAssignment", role=role_noc, contact=contact3)
    baker.make("crm.RoleAssignment", role=role_impl, contact=contact3)
    contact4 = baker.make("crm.Contact")

    # Implementation
    filtered = filters.ContactFilter({
        "role": "implementation",
    })
    queryset = filtered.qs

    assert not contact1 in queryset
    assert contact2 in queryset
    assert contact3 in queryset
    assert not contact4 in queryset

    # Noc
    filtered = filters.ContactFilter({
        "role": "noc",
    })
    queryset = filtered.qs

    assert contact1 in queryset
    assert not contact2 in queryset
    assert contact3 in queryset
    assert not contact4 in queryset

#
# Filter Mixins
#
@pytest.mark.django_db
def test_ownable_filter_mixin():
    """Test filter mixin with billing and owning account"""
    account_a = baker.make("crm.Account")
    account_b = baker.make("crm.Account")
    c1 = baker.make(
        "crm.Contact",
        managing_account=account_a,
        consuming_account=account_b)
    c2 = baker.make(
        "crm.Contact",
        managing_account=account_b,
        consuming_account=account_a)

    # First filter by owning account
    filtered = filters.OwnableFilterMixin(
        {"managing_account": account_a.pk},
        queryset=models.Contact.objects.all())
    assert c1 in filtered.qs
    assert not c2 in filtered.qs

    filtered = filters.OwnableFilterMixin(
        {"consuming_account": account_a.pk},
        queryset=models.Contact.objects.all())
    assert not c1 in filtered.qs
    assert c2 in filtered.qs

