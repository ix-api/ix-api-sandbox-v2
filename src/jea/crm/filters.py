
"""
Account and Contact filtering based on django filters.
"""

import django_filters
from django.db import models

from utils import filters
from jea.stateful.models import State
from jea.stateful.filters import StatefulMixin
from jea.crm import models as crm_models
from jea.crm.models import (
    Account,
    Role,
    Contact,
)


class OwnableFilterMixin(django_filters.FilterSet):
    """
    Define all filters for ownable objects as defined
    in `jea.crm.models.OwnableMixin`.
    """
    managing_account = django_filters.CharFilter(
        field_name="managing_account")
    consuming_account = django_filters.CharFilter(
        field_name="consuming_account")

    purchase_order = django_filters.CharFilter(
        field_name="purchase_order",
        lookup_expr="iexact")
    contract_ref = django_filters.CharFilter(
        field_name="contract_ref",
        lookup_expr="iexact")


class AccountFilter(
        StatefulMixin,
        django_filters.FilterSet):
    """Filter accounts"""
    id = filters.BulkIdFilter()

    billable = django_filters.BooleanFilter(
        method="filter_billable")
    def filter_billable(self, queryset, _name, value):
        """Filter accounts with billing_information"""
        print("filter billable:", value)
        if value:
            print("exclude non billing information")
            return queryset.exclude(billing_information=None)
        return queryset.filter(billing_information=None)

    class Meta:
        model = Account
        fields = [
            "managing_account",
            "external_ref",
            "name",
        ]

        filter_overrides = {
            models.CharField: {
                'filter_class':
                    django_filters.CharFilter,
                'extra': lambda f: {
                    'lookup_expr': 'iexact',
                },
            },
        }


class ContactFilter(OwnableFilterMixin, django_filters.FilterSet):
    """Filter contacts"""
    # Field filters
    id = filters.BulkIdFilter()

    role = django_filters.CharFilter(
        method="filter_role")
    def filter_role(self, queryset, _name, value):
        """Filter roles"""
        # Get role by name
        role = Role.objects.get(name=value)
        return queryset.filter(role_assignments__role=role)


    class Meta:
        model = Contact
        fields = [
            "external_ref",
        ]

