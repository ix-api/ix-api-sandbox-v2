
"""
Admin Interface for Accounts and Contacts
"""

from django.contrib.admin import (
    ModelAdmin,
    StackedInline,
)

from jea import admin
from jea.crm.models import (
    Account,
    Contact,
    Role,
    Address,
    RoleAssignment,
    BillingInformation,
    Address,
)

class RoleAssignmentsInline(StackedInline):
    """RoleAssignment inline admin"""
    model = RoleAssignment
    extra = 1
    fk_name = "contact"


class ContactAdmin(ModelAdmin):
    """Admin for contacts"""
    model = Contact
    inlines = (RoleAssignmentsInline, )


class AddressAdmin(ModelAdmin):
    """Address Models"""
    model = Address


class RoleAdmin(ModelAdmin):
    """A model admin for roles"""
    model = Role


class ContactInline(StackedInline):
    """Contact Inline Admin"""
    model = Contact
    extra = 0
    fk_name = "consuming_account"


class RoleAssignmentsAdmin(ModelAdmin):
    """A model admin for role assignments"""
    model = RoleAssignment


class BillingInformationAdmin(ModelAdmin):
    """Billing information"""
    model = BillingInformation


class AccountAdmin(ModelAdmin):
    """The account admin"""
    inlines = (
        ContactInline,
    )


admin.site.register(Account, AccountAdmin)
admin.site.register(Contact, ContactAdmin)
admin.site.register(Role, RoleAdmin)
admin.site.register(Address, AddressAdmin)
admin.site.register(BillingInformation, BillingInformationAdmin)
admin.site.register(RoleAssignment, RoleAssignmentsAdmin)
