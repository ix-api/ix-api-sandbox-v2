"""
Test change creators
"""

import pytest
from model_bakery import baker

from jea.stateful.models import State
from jea.stateful.test.checks import assert_save_change
from jea.crm import changes


@pytest.mark.django_db
def test_create_account():
    """Create account"""
    change = changes.create_account({})
    assert_save_change(change)


@pytest.mark.django_db
def test_update_account_state():
    """Change account state"""
    account = baker.make("crm.Account")
    change = changes.update_account_state(
        account, State.PRODUCTION)
    assert change.next == State.PRODUCTION
    assert_save_change(change)
