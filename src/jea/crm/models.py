
from typing import List

from django.db import models
from django.core.exceptions import ValidationError
from django.contrib.postgres import fields as postgres_fields
from polymorphic.models import PolymorphicModel

from jea.stateful.models import StatefulMixin, StatefulManager


#
# Mixins
#

class BillableMixin(models.Model):
    """
    A billable is linked to an account through
        billed_<modelname>
    A billable has a billing account
    """
    billing_account = models.ForeignKey(
        "crm.Account",
        related_name="billed_%(class)ss",
        on_delete=models.PROTECT)

    class Meta:
        abstract = True


class ManagedMixin(models.Model):
    """
    An entity is managed by another account
    """
    managing_account = models.ForeignKey(
        "crm.Account",
        related_name="managed_%(class)ss",
        on_delete=models.PROTECT)

    class Meta:
        abstract = True


class ManagedCascadeMixin(models.Model):
    """
    An entity is managed by another account,
    but deleting the account will propagate to the entity.
    """
    managing_account = models.ForeignKey(
        "crm.Account",
        related_name="managed_%(class)ss",
        on_delete=models.CASCADE)

    class Meta:
        abstract = True



class ManagedOptionalMixin(models.Model):
    """
    An entity can be managed by another account
    """
    managing_account = models.ForeignKey(
        "crm.Account",
        related_name="managed_%(class)ss",
        null=True,
        on_delete=models.PROTECT)

    class Meta:
        abstract = True


class ReferencableMixin(models.Model):
    """
    The entity includes a field with an external
    reference. This is an opaque string.
    """
    external_ref = models.CharField(
        max_length=128,
        default=None,
        null=True,
        blank=True)

    class Meta:
        abstract = True


class OwnableMixin(ReferencableMixin, models.Model):
    """
    This mixin adds ownership to a model:
    Managing and consuming accounts are referenced.

    An optional external reference
    can be used for linking to foreign datasource.
    """
    consuming_account = models.ForeignKey(
        "crm.Account",
        related_name="consumed_%(class)ss",
        on_delete=models.PROTECT)

    class Meta:
        abstract = True


class ContactableMixin(models.Model):
    """
    Models with this trait have a role_assignments attribute.
    """
    # Contact role assignments
    role_assignments = models.ManyToManyField(
        "crm.RoleAssignment",
        related_name="%(class)ss")

    class Meta:
        abstract = True


class InvoiceableMixin(models.Model):
    """
    This trait marks objects as billable and
    adds required fields.

    Invoiceable objects are contactable.
    """
    purchase_order = models.CharField(max_length=80, default="", blank=True)
    contract_ref = models.CharField(
        max_length=128,
        null=True,
        blank=True)

    class Meta:
        abstract = True


class AccountScopedMixin(models.Model):
    """
    Scoped objects can have a related `scoping_account`.
    Permission checks should be done on this relation.

    The default permission if the `scoping_account` is None,
    is read only. This will be the case with most "system"
    entities, like the exchange lan network service provided
    by the IX.

    Rationale: Why are we using 'None' in this case and
               not an IXP account?
    Granting access to a shared object (like a network service)
    requires a M:N relationship. Preferrably with an explicit
    permission like "read/write".

    This still can be introduced in the future, but for now
    the simpler assumption is sufficient.
    """
    scoping_account = models.ForeignKey(
        "crm.Account",
        null=True,
        blank=True,
        related_name="scoped_%(class)ss",
        on_delete=models.PROTECT)

    class Meta:
        abstract = True

#
# Models
#
class Account(
        StatefulMixin,
        AccountScopedMixin,
        ManagedOptionalMixin,
        models.Model):
    """
    A JEA Account.
    The account is one of the entry points to the graph.
    """
    name = models.CharField(max_length=128)

    legal_name = models.CharField(
        max_length=128,
        blank=True, null=True)

    external_ref = models.CharField(
        max_length=128,
        null=True,
        blank=True)

    address = models.OneToOneField(
        "crm.Address",
        related_name="account",
        on_delete=models.CASCADE)

    billing_information = models.OneToOneField(
        "crm.BillingInformation",
        related_name="account",
        null=True,
        blank=True,
        on_delete=models.SET_NULL)

    discoverable = models.BooleanField(default=False)

    # Informal list of metro area networks the account
    # is present in.
    metro_area_network_presence = models.ManyToManyField(
        "catalog.MetroAreaNetwork",
        blank=True)

    # Custom manager
    objects = StatefulManager()

    def confirm_change(self, change_id):
        """Confirm change"""
        change = self.changes.get(pk=change_id)
        change.is_confirmed = True
        change.save()

    def __str__(self):
        return "{} ({})".format(self.name, self.pk)


class BillingInformation(models.Model):
    """Account billing information"""
    name = models.CharField(max_length=128)
    address = models.OneToOneField(
        "crm.Address",
        related_name="billing_information",
        on_delete=models.CASCADE)
    vat_number = models.CharField(
        null=True, blank=True,
        max_length=20)


    def __str__(self):
        return "{} ({})".format(self.name, self.pk)


class Address(models.Model):
    """An address model"""
    country = models.CharField(max_length=2)
    locality = models.CharField(max_length=128)
    region = models.CharField(
        max_length=128,
        null=True,
        blank=True)
    street_address = models.CharField(max_length=80)
    postal_code = models.CharField(max_length=128)
    post_office_box_number = models.CharField(max_length=128,
                                              null=True,
                                              blank=True)

    def __str__(self):
        """Address to string"""
        return f"{self.street_address}, {self.locality}, {self.region}"

#
# Contacts and Roles
#
class Contact(
        OwnableMixin,
        ManagedCascadeMixin,
        AccountScopedMixin,
        models.Model,
    ):
    """A contact"""
    name = models.CharField(max_length=128, null=True, blank=True)
    telephone = models.CharField(max_length=128, null=True, blank=True)
    email = models.CharField(max_length=128, null=True, blank=True)

    @property
    def roles(self):
        """Roles accessor"""
        # Get releated roles queryset
        return Role.objects.filter(role_assignments__contact_id=self.pk)

    def __str__(self):
        """Contact to string"""
        cstr = ""
        if self.name:
            cstr += f"{self.name}, "
        if self.email:
            cstr += f"{self.email}, "
        if self.telephone:
            cstr += f"Tel: {self.telephone} "
        cstr += f"({self.pk})"
        return cstr


class Role(models.Model):
    """
    A Role has a name and many contacts.
    It might be required by various (network-) services or -features.
    """
    name = models.CharField(max_length=80)
    required_fields = postgres_fields.ArrayField(
        models.CharField(max_length=80))

    def clean(self):
        """Validate properties before saving"""
        fields_available = ['name', 'telephone', 'email']
        for field in self.required_fields:
            if field not in fields_available:
                fnames = ", ".join(fields_available)
                raise ValidationError(
                    f"Field {field} is not in available fields: {fnames}")


    def __str__(self):
        """Role to string"""
        return f"{self.name} ({self.pk})"


class RoleAssignment(models.Model):
    """
    RoleAssignments model connects the Role and the Contact
    and can be referenced.
    """
    contact = models.ForeignKey(
        Contact,
        related_name="role_assignments",
        on_delete=models.PROTECT)
    role = models.ForeignKey(
        Role,
        related_name="role_assignments",
        on_delete=models.CASCADE)

    def __str__(self):
        """Assignment to string"""
        return f"{self.role} assigned to {self.contact} ({self.pk})"
