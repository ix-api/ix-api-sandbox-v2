
"""
Accounts Service
"""

from typing import Iterable, Optional

from django.db import transaction
from django.contrib.contenttypes.models import (
    ContentType,
)

from jea.exceptions import ResourceAccessDenied, ValidationError
from jea.stateful.models import (
    State,
    StatusMessage,
)
from jea.crm.models import (
    Account,
    Address,
    Contact,
    RoleAssignment,
    BillingInformation,
    BillableMixin,
    ManagedMixin,
    ManagedOptionalMixin,
)
from jea.crm.filters import (
    AccountFilter,
)
from jea.stateful.models import (
    Change,
)

def get_accounts(
        scoping_account: Account = None,
        include_discoverable=False,
        filters=None,
    ) -> Iterable[Account]:
    """
    Get the accounts. If there is a managing account
    context, limit the result set to the scope of the
    given account.

    :param scoping_account: The account scope if present
    :param filters: A dict with filter paramters. This will be applied to
                    the subtrees queryset.
    :param include_discoverable: Allow for discoverable accounts to be
        part of the result set.
    """
    queryset = AccountFilter(filters).qs

    # Limit to scoping account
    if scoping_account:
        current_account = queryset.filter(pk=scoping_account.pk)
        queryset = queryset.filter(scoping_account=scoping_account)
        queryset |= current_account

    # Add all discoverable accounts, however: these
    # are limited to production state accounts.
    if include_discoverable and not filters:
        discoverable = AccountFilter(filters).qs \
            .filter(discoverable=True) \
            .filter(state="production")
        queryset |= discoverable

    return queryset.distinct()


def get_account(
        scoping_account: Account = None,
        account=None,
        allow_discoverable=False,
    ) -> Optional[Account]:
    """
    Retrieve a account from the repository.
    Implemented lookups:
        - account_id

    :param account_id: The identifier of the account
    :param allow_discoverable: Include discoverable accounts.
        CAVEAT be extra cautios when assigning a account
        to a resource.

    :raises ResourceAccessDenied: When permission checks fail
    """
    if not account:
        return None

    # Resolve account
    if not isinstance(account, Account):
        account = Account.objects.get(pk=account)

    # If we allow for discoverable accounts, we skip the
    # permission check. HOWEVER! be aware, that you need
    # to make sure the account can be assigned to a resource
    # and the assignment is wanted.
    if allow_discoverable and account.discoverable:
        return account

    # Perform permission check
    if scoping_account and \
        account.pk != scoping_account.pk and \
        account.scoping_account_id != scoping_account.pk:
        raise ResourceAccessDenied(account)

    return account


def _create_address(address_request) -> Address:
    """
    Create an address from request data

    :param address_request: Validated address data
    """
    address = Address(
        country=address_request["country"],
        locality=address_request["locality"],
        region=address_request.get("region"),
        postal_code=address_request["postal_code"],
        street_address=address_request["street_address"],
        post_office_box_number=address_request.get("post_office_box_number"))
    address.save()

    return address


def _update_address(address: Address, address_update) -> Address:
    """
    Update an existing address

    :param address: The address to update
    :param address_update: Validated address update data
    """
    update_fields = address_update.keys()
    if "country" in update_fields:
        address.country = address_update["country"]
    if "locality" in update_fields:
        address.locality = address_update["locality"]
    if "region" in update_fields:
        address.region = address_update["region"]
    if "postal_code" in update_fields:
        address.postal_code = address_update["postal_code"]
    if "street_address" in update_fields:
        address.street_address = address_update["street_address"]
    if "post_office_box_number" in update_fields:
        address.post_office_box_number = \
            address_update["post_office_box_number"]
    address.save()

    return address


def _create_billing_information(
        billing_information_request=None,
    ) -> BillingInformation:
    """
    Create billing information, add addres and
    associte with account.

    :param billing_information_request: A dict with billing information.
    :param account: The associtated account
    """
    # Create billing address
    billing_address_request = billing_information_request["address"]
    billing_address = _create_address(billing_address_request)

    # Create billing information
    billing_information = BillingInformation(
        name=billing_information_request["name"],
        vat_number=billing_information_request.get("vat_number"),
        address=billing_address)
    billing_information.save()

    return billing_information


def create_account(
        scoping_account: Account = None,
        account_request=None,
    ) -> Account:
    """
    Create a account in the database.

    :param scoping_account: An optional account managing this account.
    :param account_input: Validated account input data

    :return: a freshly created account
    """
    managing_account = account_request.get(
        "managing_account", scoping_account)
    managing_account = get_account(
        account=managing_account,
        scoping_account=scoping_account)

    # Store address
    address_request = account_request["address"]
    address = _create_address(address_request)

    # Store billing information if present
    billing_request = account_request.get("billing_information")
    billing_information = None
    if billing_request:
        billing_information = _create_billing_information(billing_request)

    # Persist account
    account = Account(
        managing_account=managing_account,
        name=account_request["name"],
        legal_name=account_request.get("legal_name"),
        external_ref=account_request.get("external_ref"),
        address=address,
        billing_information=billing_information,
        scoping_account=scoping_account)
    account.save()

    # Assign many to many relations
    account.metro_area_network_presence.set(
        account_request.get("metro_area_network_presence", []))

    return account


def update_account(
        scoping_account: Account = None,
        account: Account = None,
        account_update=None,
    ) -> Account:
    """
    Update a given account in the database.

    :param account_update: A validated account update.
    :param scoping_account: The current managing account.
    """
    account = get_account(scoping_account=scoping_account,
                          account=account)

    update_fields = account_update.keys()

    # Set properties
    if "name" in update_fields:
        account.name = account_update["name"]
    if "legal_name" in update_fields:
        account.legal_name = account_update["legal_name"]
    if "external_ref" in update_fields:
        account.external_ref = account_update["external_ref"]
    if "discoverable" in update_fields:
        account.discoverable = account_update["discoverable"]
    if "parent" in update_fields:
        parent = get_account(
            scoping_account=scoping_account,
            account=account_update["parent"])
        account.parent = parent
    if "address" in update_fields:
        _update_address(
            account.address,
            account_update["address"])
    if "billing_information" in update_fields:
        if account.billing_information:
            account.billing_information.delete()

        # Update billing information
        billing_update = account_update["billing_information"]
        if billing_update is None:
            assert_billing_information_not_in_use(account)
            billing_information = None
        else:
            # We can do this, because we do not allow for partial
            # updates in nested updates. At least for now.
            billing_information = _create_billing_information(billing_update)

        account.billing_information = billing_information

    # Update MetroAreaNetwork Presence
    if "metro_area_network_presence" in update_fields:
        networks = account_update["metro_area_network_presence"]
        account.metro_area_network_presence.set(networks)

    account.save()
    return account


@transaction.atomic
def delete_account(
        scoping_account=None,
        account=None,
    ):
    """
    Destroy a account.
    Might fail, if protected models are affected.

    :param scoping_account: The account managing the
        account marked for deletion.
    :param account: The account to delete.
        Can be an id, which will be resolved.
    """
    account = get_account(scoping_account=scoping_account,
                          account=account)

    # Check if the account is in use somewhere as managing
    # or billing account.
    managed_models = [
        *ManagedMixin.__subclasses__(),
        *ManagedOptionalMixin.__subclasses__(),
    ]
    managed = []
    for m in managed_models:
        for obj in m.objects.filter(managing_account=account):
            state = getattr(obj, "state", "")
            if state == State.DECOMMISSIONED:
                obj.delete()
            else:
                managed.append(obj)

    if managed:
        print(managed)
        raise ValidationError(
            "The account is in use as a `managing_account` "
            "and can not be deleted.",
            field="managing_account")

    # Delete all contacts and role assignments
    RoleAssignment.objects.filter(contact__consuming_account=account).delete()
    Contact.objects.filter(consuming_account=account).delete()

    # Remove other dependencies
    ct = ContentType.objects.get_for_model(account)
    for c in Change.objects.filter(scoping_account=account):
        c.delete()
    for c in Change.objects.filter(ref_id=account.pk, ref_type=ct):
        c.delete()

    # Remove account
    account.delete()

    return account


def assert_billing_information(account=None):
    """
    Test if an account has billing information present.
    If not raise an exception.

    :param account: The account to check

    :raises ValidationError: in case no billing information is present.
    """
    if not account.billing_information:
        raise ValidationError(
            "The account is lacking billing information and "
            "can not be used as billing account.",
            field="billing_account")


def assert_billing_information_not_in_use(account=None):
    """
    Check, that our billing account is not in use by
    any entity.

    :param account: An account with billing information present

    :raises ValidationError: when the account is used as billing
        account in somewhere.
    """
    if any(M.objects.filter(billing_account=account)
           for M in BillableMixin.__subclasses__()):
        # The account is used as billing account somewhere
        raise ValidationError(
            "The account is used as billing_account and the "
            "billing information can not be removed.",
            field="billing_information")

