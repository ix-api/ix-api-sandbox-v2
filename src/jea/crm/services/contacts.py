
"""
A service for contact handling
"""

import logging
import base64
from typing import Optional, Any, List
from copy import deepcopy

from django.db.models import ProtectedError

from jea.exceptions import (
    ResourceAccessDenied,
    ValidationError,
    UnableToFulfillError,
)
from jea.crm.filters import (
    ContactFilter,
)
from jea.crm.exceptions import (
    RoleAssignmentInUse,
    RequiredContactRolesInvalid,
)
from jea.crm.models import (
    Account,
    Contact,
    Role,
    RoleAssignment,
    ContactableMixin,
)
from jea.crm.services import accounts as accounts_svc


logger = logging.getLogger(__name__)


def get_contacts(
        scoping_account: Account = None,
        filters: dict = None,
    ):
    """
    Get a list of filtered contacts.
    A managing account is required.

    :param scoping_account: The current managing account
    :param filters: An optional dict of query filter params
    """
    if not scoping_account:
        raise ValidationError(
            "Not allowed without managed scope", field="scoping_account")

    contacts = scoping_account.scoped_contacts.all()
    filtered = ContactFilter(filters, queryset=contacts)

    return filtered.qs


def get_contact(
        scoping_account: Account = None,
        contact: Any = None,
    ) -> Contact:
    """
    Get a account contact identified by the primary key.

    :param scoping_account: The managing account.
    :param pk: The contact's primary key

    :raises: Contact.DoesNotExist
    :raises: ResourceAccessDenied
    """
    # Resolve contact
    if not isinstance(contact, Contact):
        contact = Contact.objects.get(pk=contact)

    # Permission check
    if scoping_account:
        if contact.scoping_account_id != scoping_account.pk:
            raise ResourceAccessDenied(contact)

    return contact


def create_contact(
        contact_request=None,
        scoping_account=None,
    ) -> Contact:
    """
    Create a account contact

    :param account: An account instance
    :param contact_request: Contact data. *MUST* be validated.
    """
    # Permission check on referenced accounts
    managing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=contact_request["managing_account"])
    consuming_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=contact_request["consuming_account"])

    # Create contact
    contact = Contact(
        name=contact_request.get("name"),
        telephone=contact_request.get("telephone"),
        email=contact_request.get("email"),
        managing_account=managing_account,
        consuming_account=consuming_account,
        scoping_account=scoping_account)
    contact.save()

    return contact


def update_contact(
        scoping_account: Account = None,
        contact: Contact = None,
        contact_update=None,
    ):
    """
    Update a account contact.

    :param contact: The contact to update
    :param contact_update: The validated! update data
    :param scoping_account: A scope limiting account
    """
    contact = get_contact(
        scoping_account=scoping_account,
        contact=contact)
    update_fields = contact_update.keys()

    # Update contact relations
    if "managing_account" in update_fields:
        managing_account = accounts_svc.get_account(
            scoping_account=scoping_account,
            account=contact_update["managing_account"])
        contact.managing_account = managing_account

    if "consuming_account" in update_fields:
        consuming_account = accounts_svc.get_account(
            scoping_account=scoping_account,
            account=contact_update["consuming_account"])
        contact.consuming_account = consuming_account

    # Update contact attributes
    if "name" in update_fields:
        contact.name = contact_update["name"]
    if "telephone" in update_fields:
        contact.telephone = contact_update["telephone"]
    if "email" in update_fields:
        contact.email = contact_update["email"]

    contact.save()

    return contact


def delete_contact(
        scoping_account=None,
        contact=None,
    ):
    """
    Delete contact either by providing a contact object or
    by passing the id.

    :param scoping_account: The current managing account scope
    :param contact: The contact to delete
    """
    contact = get_contact(
        scoping_account=scoping_account,
        contact=contact)

    contact_data = deepcopy(contact)
    try:
        contact.delete()
    except ProtectedError:
        raise UnableToFulfillError()

    return contact


#
# Roles
#
DEFAULT_ROLES = [
    "noc",
    "implementation",
]


def assert_presence_of_contact_roles(
        required_contact_roles=None,
        role_assignments: List[RoleAssignment] = None,
        scoping_account=None):
    """
    Check if all required contact roles are present
    through role assignments.

    :param required_contact_roles: A list of required contact roles
    :param role_assignments: A list of role assignments.
    :param scoping_account: An optional account scope.

    :raises RequiredContactRolesInvalid: in case a required
        contact type is missing or not required present.
    """
    if not required_contact_roles:
        return  # There is nothing required

    # Permission check
    role_assignments = [
        get_role_assignment(
            role_assignment=assignment,
            scoping_account=scoping_account)
        for assignment in role_assignments or []]

    # All roles in provided role assignments
    assigned_roles = [assignment.role for assignment in role_assignments]

    for role in required_contact_roles:
        if not role in assigned_roles:
            # We checked all assignments, however non
            # has actually the role required.
            raise RequiredContactRolesInvalid()


def get_roles():
    """
    Get all roles available
    """
    # Make sure default roles are present
    for role in DEFAULT_ROLES:
        get_default_role(role)

    return Role.objects.all()


def get_role(name=None, role=None):
    """
    Get a role by an identifier or name.

    :param name: A role name.
    :param role: A role identifier, or role
    """
    # Make sure default roles are present
    for default_role in DEFAULT_ROLES:
        get_default_role(default_role)

    if name:
        return Role.objects.get(name=name)

    if isinstance(role, Role):
        return role

    return Role.objects.get(pk=role)


def assign_role(
        role=None,
        contact=None,
        scoping_account=None,
    ):
    """
    Assign a role to the contact, if the requied
    fields are present.

    :param role: The role to assign
    :param contact: The contact which should assume the role
    :param scoping_account: The current account context
    """
    role = get_role(role=role)
    contact = get_contact(
        contact=contact,
        scoping_account=scoping_account)

    # Check presence of required fields
    for field in role.required_fields:
        value = getattr(contact, field, None)
        if not value:
            raise ValidationError(
                "The field is required.", field=field)

    # Everythings fine? Great! Create a cole assignment.
    assignment = RoleAssignment(
        contact=contact,
        role=role)
    assignment.save()

    return assignment


def is_role_assignment_in_use(
        role_assignment=None,
        scoping_account=None):
    """Test if the role assignment is in use"""
    role_assignment = get_role_assignment(
        role_assignment=role_assignment,
        scoping_account=scoping_account)
    # Check all related
    for rel in role_assignment._meta.related_objects:
        # Get related field and query for values
        # for example: role_assignment.networkserviceconfigs.exists()
        field = getattr(role_assignment, rel.get_accessor_name())
        if field.exists():
            return True

    return False


def destroy_role_assignment(
        role_assignment=None,
        scoping_account=None,
    ):
    """
    Remove a role assignment.

    :param role_assignment: The assignment to remove
    :param scoping_account: An account scope
    """
    role_assignment = get_role_assignment(
        role_assignment=role_assignment,
        scoping_account=scoping_account)

    if is_role_assignment_in_use(
            role_assignment=role_assignment,
            scoping_account=scoping_account):
        raise RoleAssignmentInUse

    role_assignment.delete()

    return role_assignment


def get_role_assignments(
        scoping_account=None,
    ):
    """
    Get all role assignments for the given account.

    :param scoping_account: The current scoping account
    """
    assignments = RoleAssignment.objects.filter(
        contact__scoping_account=scoping_account)

    return assignments


def get_role_assignment(
        role_assignment=None,
        scoping_account=None,
    ):
    """
    Get a role assignment

    :param scoping_account: Limit to this account
    :param role_assignment: A role assignment identifier
    """
    if not isinstance(role_assignment, RoleAssignment):
        # Resolve object
        role_assignment = RoleAssignment.objects \
            .prefetch_related("contact") \
            .prefetch_related("contact__scoping_account") \
            .get(pk=role_assignment)

    # Permission check
    if scoping_account and \
        role_assignment.contact and \
        role_assignment.contact.scoping_account != scoping_account:
        raise ResourceAccessDenied(role_assignment)

    return role_assignment


def get_default_role(name: str) -> Role:
    """
    Get a default role.

    The following roles and their required fields
    are predefined in the ix-api sandbox:

        noc:
            - email
            - telephone
        implementation:
            - name
            - telephone
            - email

    :param name: A default role name
    """
    if name == "noc":
        return _get_default_role_noc()
    if name == "implementation":
        return _get_default_role_implementation()

    raise ValueError("{} is not a valid default role.".format(
        name))


def _get_default_role_noc() -> Role:
    """Get NOC default role"""
    role, created = Role.objects.get_or_create(
        name="noc",
        required_fields=["email", "telephone"])
    if created:
        logger.debug("Initialized default role: noc")

    return role


def _get_default_role_implementation() -> Role:
    """Get implementation default role"""
    role, created = Role.objects.get_or_create(
        name="implementation",
        required_fields=["email", "telephone"])

    if created:
        logger.debug("Initialized default role: implementation")

    return role

