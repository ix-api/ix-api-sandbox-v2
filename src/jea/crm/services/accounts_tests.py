
"""
Accounts Services Tests
"""

import pytest
from model_bakery import baker
from django.core.exceptions import PermissionDenied
from django.db.models.deletion import ProtectedError

from jea.exceptions import ResourceAccessDenied, ValidationError
from jea.config.models import (
    ExchangeLanNetworkServiceConfig,
)
from jea.crm.models import (
    Account,
    Contact,
    Role,
    RoleAssignment,
)
from jea.crm.services import accounts as accounts_svc


@pytest.mark.django_db
def test_get_accounts():
    """Test account queryset"""
    manager = baker.make(Account, name="manager_1")
    c1 = baker.make(Account, name="c1", scoping_account=manager)
    c2 = baker.make(Account, name="c2")

    accounts = accounts_svc.get_accounts()
    assert manager in accounts
    assert c1 in accounts
    assert c2 in accounts

    accounts = accounts_svc.get_accounts(scoping_account=manager)
    assert manager in accounts
    assert c1 in accounts
    assert not c2 in accounts

    accounts = accounts_svc.get_accounts(scoping_account=c2)
    assert not manager in accounts
    assert not c1 in accounts
    assert c2 in accounts


@pytest.mark.django_db
def test_create_account():
    """Try to create a new account"""
    # A simple test account
    generator = baker.prepare(Account)

    # Now create the account without a parent
    request = {
        "name": "SubAccount" + generator.name[:23],
        "address": {
            "country": "DE",
            "locality": "Berlin",
            "postal_code": "11011",
            "street_address": "Strassenweg 9",
        },
    }

    subaccount = accounts_svc.create_account(
        account_request=request)

    account = baker.make(
        Account,
        name="root account",
        scoping_account=subaccount)

    # Create with manager
    request = {
        "name": "SubAccount" + generator.name[:23],
        "parent": account,
        "address": {
            "country": "DE",
            "locality": "Berlin",
            "postal_code": "11011",
            "street_address": "Strassenweg 9",
        },
    }
    account = accounts_svc.create_account(
        scoping_account=subaccount,
        account_request=request)
    assert account.scoping_account.pk == subaccount.pk


@pytest.mark.django_db
def test_create_billing_account():
    """Try to create a new account with billing information"""
    # A simple test account
    generator = baker.prepare(Account)
    request = {
        "name": "Internet Co.",
        "legal_name": "Internet und Net GmbH" + generator.name[:12],
        "address": {
            "country": "DE",
            "locality": "Berlin",
            "postal_code": "11011",
            "street_address": "Strassenweg 9",
        },
        "billing_information": {
            "name": "Peering Management GmbH",
            "address": {
                "country": "DE",
                "locality": "Berlin",
                "postal_code": "11011",
                "street_address": "Strassenweg 9b",
            },
        }
    }

    account = accounts_svc.create_account(
        account_request=request)

    # Check if the account can be used for billing
    accounts_svc.assert_billing_information(account)


@pytest.mark.django_db
def test_get_account__no_check():
    """Test retrieving a single account"""
    c1 = baker.make(Account)
    account = accounts_svc.get_account(
        account=c1)
    assert account.pk == c1.pk


@pytest.mark.django_db
def test_get_account__by_id():
    """Test getting the account with only the id present"""
    c1 = baker.make(Account)
    account = accounts_svc.get_account(
        account=str(c1.pk))
    assert account.pk == c1.pk


@pytest.mark.django_db
def test_get_account__permission_check():
    """Test retrieving a single account, perform a permission check"""
    m1 = baker.make(Account, name="manager1")
    c1 = baker.make(Account, scoping_account=m1)
    c2 = baker.make(Account)

    # Objects are resolved, only perform a permission check
    account = accounts_svc.get_account(
        scoping_account=m1,
        account=c1)
    assert account == c1

    with pytest.raises(ResourceAccessDenied):
        accounts_svc.get_account(
            scoping_account=m1,
            account=c2.pk)


@pytest.mark.django_db
def test_update_account():
    """Test updating a account"""
    manager = baker.make(Account)
    account = baker.make(Account, scoping_account=manager)

    update = {
        "external_ref": "fnorf2000",
        "name": "foo",
    }

    account = accounts_svc.update_account(
        scoping_account=manager,
        account=account,
        account_update=update)

    assert account.external_ref == "fnorf2000"
    assert account.name == "foo"


@pytest.mark.django_db
def test_update_billing_information():
    """
    Test updating the billing information in an account
    """
    account = baker.make(Account)

    # Make sure, this account can't be used for billing
    with pytest.raises(ValidationError):
        accounts_svc.assert_billing_information(account)

    # Update with billing information
    account = accounts_svc.update_account(
        account=account,
        account_update={
            "billing_information": {
                "name": "billing name",
                "address": {
                    "country": "GB",
                    "locality": "Cardiff",
                    "region": "Wales",
                    "postal_code": "CF10 FOO",
                    "street_address": "A Street",
                },
            }
        })

    # This account should be able to be used for billing
    accounts_svc.assert_billing_information(account)

    # Remove billing information
    account = accounts_svc.update_account(
        account=account,
        account_update={
            "billing_information": None,
        })

    # However, not after removal of the billing information
    with pytest.raises(ValidationError):
        accounts_svc.assert_billing_information(account)


@pytest.mark.django_db
def test_update_billing_information__in_use():
    """
    Prevent removal of billing information, in case
    the account is used somewhere as billing account.
    """
    account = baker.make(
        Account,
        billing_information=baker.make("crm.BillingInformation"))

    # Make sure, this account can be used for billing
    accounts_svc.assert_billing_information(account)

    # Use the account in a connection
    service = baker.make(
        "config.Connection",
        billing_account=account)

    # We should not be ok with the removal of billing information
    with pytest.raises(ValidationError):
        accounts_svc.update_account(
            account=account,
            account_update={
                "billing_information": None,
            })


@pytest.mark.django_db()
def test_assert_billing_information():
    """Test billing information presence assertation"""
    account = baker.make(Account)
    billing_account = baker.make(
        Account,
        billing_information=baker.make("crm.BillingInformation"))

    accounts_svc.assert_billing_information(billing_account)

    with pytest.raises(ValidationError):
        accounts_svc.assert_billing_information(account)


@pytest.mark.django_db()
def test_assert_billing_information_not_in_use():
    """Test assertion of billing account not in use"""
    account = baker.make(
        Account,
        billing_information=baker.make("crm.BillingInformation"))

    # Use the account in a connection
    service = baker.make(
        "config.Connection",
        billing_account=account)


@pytest.mark.django_db()
def test_delete_account__simple():
    """Test account deletion without dependencies"""
    account = baker.make(Account)
    accounts_svc.delete_account(account=account)

    with pytest.raises(Account.DoesNotExist):
        account.refresh_from_db()


@pytest.mark.django_db()
def test_delete_account__cascade_relations():
    """Test account delete with simple dependencies"""
    managing_account = baker.make(Account)
    account = baker.make(Account)
    role = baker.make(Role)
    contact = baker.make(
        Contact,
        consuming_account=account,
        managing_account=managing_account)
    ra = baker.make(
        RoleAssignment,
        role=role,
        contact=contact)

    accounts_svc.delete_account(account=account)
    with pytest.raises(Account.DoesNotExist):
        account.refresh_from_db()
    with pytest.raises(Contact.DoesNotExist):
        contact.refresh_from_db()


@pytest.mark.django_db()
def test_delete_account__protected_relations():
    """Test account delete with protected dependencies"""
    account = baker.make(Account)
    role = baker.make(Role)
    contact = baker.make(
        Contact,
        consuming_account=account,
        managing_account=account)
    ra = baker.make(
        RoleAssignment,
        role=role,
        contact=contact)
    nsc = baker.make(
        ExchangeLanNetworkServiceConfig,
        state="production",
        managing_account=account,
        consuming_account=account)

    with pytest.raises(ValidationError): # Account in use
        accounts_svc.delete_account(account=account)

    contact.refresh_from_db() # should still be present

    # A decommissioned nsc should be removed
    nsc.state = "decommissioned"
    nsc.save()

    accounts_svc.delete_account(account=account)

    with pytest.raises(Account.DoesNotExist):
        account.refresh_from_db()

    with pytest.raises(Contact.DoesNotExist):
        contact.refresh_from_db()

