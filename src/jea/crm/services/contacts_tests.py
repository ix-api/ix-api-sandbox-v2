
"""
Contacts Service Tests
"""

import pytest
import mock
from model_bakery import baker

from jea.crm.models import Contact
from jea.crm.exceptions import RoleAssignmentInUse
from jea.crm.services import contacts as contacts_svc
from jea.crm.exceptions import RequiredContactRolesInvalid, ValidationError


@pytest.mark.django_db
def test_get_contacts():
    """Get all contacts within managing account scope"""
    m1 = baker.make("crm.Account")
    c1 = baker.make("crm.Contact", scoping_account=m1)
    c2 = baker.make("crm.Contact")

    contacts = contacts_svc.get_contacts(
        scoping_account=m1)
    assert c1 in contacts
    assert not c2 in contacts


def test_get_contacts__missing_scoping_account():
    """Test getting contacts without a managing account"""
    with pytest.raises(Exception):
        contacts_svc.get_contacts()


@pytest.mark.django_db
def test_create_account_contact_data_type():
    """Test Contact Creation"""
    account = baker.make("crm.Account")
    managing_account = baker.make("crm.Account", scoping_account=account)
    consuming_account = baker.make("crm.Account", scoping_account=account)
    contact_data = {
        "email": "email@addr.ess",
        "telephone": "123456",
        "managing_account": managing_account.pk,
        "consuming_account": consuming_account.pk,
    }

    # Assume contact_data is validated.
    contact = contacts_svc.create_contact(
        scoping_account=account,
        contact_request=contact_data)
    assert contact.pk, \
        "Contact should have an assigned primary key"


@pytest.mark.django_db
def test_create_account_contact_fixed_type():
    """Test Contact Creation"""
    account = baker.make("crm.Account")
    managing_account = baker.make("crm.Account", scoping_account=account)
    consuming_account = baker.make("crm.Account", scoping_account=account)
    contact_data = {
        "email": "email@addr.ess",
        "telephone": "123456",
        "managing_account": managing_account,
        "consuming_account": consuming_account,
    }

    contact = contacts_svc.create_contact(
        scoping_account=account,
        contact_request=contact_data,
    )

    assert contact.pk, \
        "Contact should have an assigned primary key"


@pytest.mark.django_db
def test_update_contact():
    """Test updating a contact"""
    account = baker.make("crm.Account")
    managing_account = baker.make("crm.Account", scoping_account=account)
    consuming_account = baker.make("crm.Account", scoping_account=account)
    contact_data = {
        "email": "email@addr.ess",
        "telephone": "123456",
        "managing_account": managing_account,
        "consuming_account": consuming_account,
    }

    contact = contacts_svc.create_contact(
        scoping_account=account,
        contact_request=contact_data)

    contact = contacts_svc.update_contact(
        scoping_account=account,
        contact=contact,
        contact_update={
            "telephone": "0000",
        })

    assert contact.telephone == "0000"

    # managing account id and type should be ignored from the update
    contact = contacts_svc.update_contact(
        scoping_account=contact.scoping_account,
        contact=contact,
        contact_update={
            "scoping_account_id": "F00",
        })

    assert contact.scoping_account_id != "F00", \
        "Account id should not be reassigned in update"


@pytest.mark.django_db
def test_delete_contact():
    """Test contact removal"""
    account = baker.make("crm.Account")
    contact = baker.make("crm.Contact", scoping_account=account)

    contacts_svc.delete_contact(
        scoping_account=contact.scoping_account,
        contact=contact)

    with pytest.raises(Contact.DoesNotExist):
        contact.refresh_from_db()



@pytest.mark.django_db
def test_assert_presence_of_contact_roles():
    """Test required contact roles validation"""
    # This should work
    noc_role = baker.make("crm.Role", name="noc")
    impl_role = baker.make("crm.Role", name="implementation")

    contact = baker.make("crm.Contact")

    # Assign role
    assignments = [
        baker.make("crm.RoleAssignment", role=noc_role, contact=contact),
        baker.make("crm.RoleAssignment", role=impl_role, contact=contact)
    ]

    required = [impl_role, noc_role]
    contacts_svc.assert_presence_of_contact_roles(
        required_contact_roles=required,
        role_assignments=assignments)

    # This should, well, not.
    contact = baker.make("crm.Contact")
    required = [noc_role]
    assignments = [
        baker.make("crm.RoleAssignment", role=impl_role, contact=contact),
    ]
    with pytest.raises(RequiredContactRolesInvalid):
        contacts_svc.assert_presence_of_contact_roles(
            required_contact_roles=required,
            role_assignments=assignments)


@pytest.mark.django_db
def test_assign_role():
    """Test assigning a role"""
    role = baker.make(
        "crm.Role",
        required_fields=["name"])

    valid_contact = baker.make("crm.Contact", name="present")
    invalid_contact = baker.make("crm.Contact", name=None)

    # This should work
    contacts_svc.assign_role(
        contact=valid_contact,
        role=role)

    assert role in valid_contact.roles.all()

    # This however should not work
    with pytest.raises(ValidationError):
        contacts_svc.assign_role(
            contact=invalid_contact,
            role=role)


@pytest.mark.django_db
def test_destroy_role_assignment():
    """Test removing a role assignment"""
    role = baker.make("crm.Role")
    contact = baker.make("crm.Contact")
    assignment = baker.make(
        "crm.RoleAssignment",
        role=role,
        contact=contact)

    # Use assignment to trigger an error
    conn = baker.make(
        "config.Connection",
        role_assignments=[assignment])

    # Remove the role
    with pytest.raises(RoleAssignmentInUse):
        contacts_svc.destroy_role_assignment(
            role_assignment=assignment)

    conn.role_assignments.set([])
    conn.save()

    # Delete role assignment
    contacts_svc.destroy_role_assignment(
        role_assignment=assignment)

    assert not role in contact.role_assignments.filter(role=role)


@pytest.mark.django_db
def test_get_role_assignments():
    """Test getting all role assignment within an account scope"""
    account = baker.make("crm.Account")
    noc_contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=account)
    noc_assignment = baker.make(
        "crm.RoleAssignment",
        contact=noc_contact,
        role=contacts_svc.get_default_role("noc"))

    implementation_contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=account)
    implementation_assignment = baker.make(
        "crm.RoleAssignment",
        contact=implementation_contact,
        role=contacts_svc.get_default_role("implementation"))

    # Get all assignments
    assignments = contacts_svc.get_role_assignments(
        scoping_account=account)

    assert assignments, "There should be assignments present"
    assert implementation_assignment in assignments
    assert noc_assignment in assignments

    contact_pks = [a.contact_id for a in assignments]
    assert noc_contact.pk in contact_pks
    assert implementation_contact.pk in contact_pks


@pytest.mark.django_db
def test_get_role_assignment():
    """Test getting a single role assignment"""
    account = baker.make("crm.Account")
    role = contacts_svc.get_default_role("noc")
    contact = baker.make(
        "crm.Contact",
        scoping_account=account,
        consuming_account=account)
    assignment = baker.make(
        "crm.RoleAssignment",
        role=role,
        contact=contact)

    # Retrieve assignment
    assignment = contacts_svc.get_role_assignment(
        scoping_account=account,
        role_assignment=assignment.pk)

    assert assignment

