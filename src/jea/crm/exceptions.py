"""
CRM Exceptions
"""

from jea.exceptions import (
    ConditionAssertionError,
    ValidationError,
)


class RoleAssignmentInUse(ConditionAssertionError):
    """The role assignment is in use"""
    default_detail = ("The contact role assignment is currently in use.")
    default_code = "contact_in_use"


class RequiredContactRolesInvalid(ValidationError):
    """Required contact roles are missing"""
    default_detail = ("The config requires at least one contact, with "
                      "all roles required from corresponding network-service "
                      "or feature.")
    default_code = "required_contact_role_assignments_invalid"
    default_field = "role_assignments"
