import logging

from django.apps import AppConfig

logger = logging.getLogger(__name__)

class CrmConfig(AppConfig):
    """Sandbox CRM application"""
    name = "jea.crm"

    verbose_name = "Sandbox :: CRM"

    def ready(self):
        """Application startup"""
        logger.info("Initializing app: {}".format(self.name))

