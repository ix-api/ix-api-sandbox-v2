
"""
CRM Resource Handlers
"""

import time
import logging
from datetime import timedelta

from ixapi_schema.v2.schema import (
    AccountRequest,
)

from jea.crm.services import accounts as accounts_svc
from jea.config.changes import (
    decommission_network_service_config,
    decommission_network_feature_config,
)
from jea.crm.changes import (
    confirm_account_change,
    update_account_state,
)
from jea.stateful.models import (
    Change,
    ChangeState,
    StatusMessage,
    State,
)
from jea.stateful.changes import (
    schedule_in,
)
from jea.crm.models import (
    Account,
    Address,
    BillingInformation,
)


logger = logging.getLogger(__name__)


def create(change):
    """
    Create a new account from validated data.
    """
    serializer = AccountRequest(data=change.data)
    serializer.is_valid(raise_exception=True)
    validated_data = serializer.validated_data

    # Use scoping account as manager as fallback
    if not validated_data["managing_account"]:
        validated_data["managing_account"] = change.scoping_account

    account = accounts_svc.create_account(
        scoping_account=change.scoping_account,
        account_request=validated_data)
    change.ref = account

    return account, [
        # We created the initial account, we can change it's state
        # to production next:
        update_account_state(account, State.PRODUCTION)
    ]


def update(change):
    """Update an account"""
    account = change.ref

    serializer = AccountRequest(
        data=change.data,
        partial=change.partial)
    serializer.is_valid(raise_exception=True)
    validated_data = serializer.validated_data
    update_keys = validated_data.keys()

    # When billing information is updated, we need to defer
    # this util we get confirmation from our backoffice.
    if change.is_confirmed:
        # All good. Update with billing info.
        account = accounts_svc.update_account(
            scoping_account=change.scoping_account,
            account=account,
            account_update=validated_data)
        # We go back to production again
        return account, [
            update_account_state(account, State.PRODUCTION),
        ]

    # When no critical data is to update, we can just
    # apply this.
    if "billing_info" not in update_keys and \
        "address" not in update_keys:
        # Update account and be done
        account = accounts_svc.update_account(
            scoping_account=change.scoping_account,
            account=account,
            account_update=validated_data)
        return account, []

    # We have changes, that require a deferred task.
    # Update without billing info or address
    deferred = []
    if "billing_info" in update_keys:
        del validated_data["billing_info"]
    if "address" in update_keys:
        del validated_data["address"]

    # Set account state to pending updates
    if not account.state == State.PRODUCTION_CHANGE_PENDING:
        # We defer the confirmation and set the state
        # to pending.
        deferred += [
            schedule_in(
                confirm_account_change(account, change),
                timedelta(seconds=10))
        ]
        account.state = State.PRODUCTION_CHANGE_PENDING
        account.save()

    account = accounts_svc.update_account(
        scoping_account=change.scoping_account,
        account=account,
        account_update=validated_data)

    # We defer the confirmation to a side effect and
    # keep this change open.
    return account, deferred + [
        schedule_in(change, timedelta(seconds=5)),
    ]


def decommission(change):
    """Decommission an account"""
    account = change.ref
    actions = []

    # Check that we actually can decommission - e.g. the account
    # is not managing other network services etc...
    # if .. account.in_use()... ...


    # Eventually we will mark this account as decomissioned

    return account, actions
