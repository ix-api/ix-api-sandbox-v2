"""
Test accounts change handlers
"""

import pytest
from model_bakery import baker

from jea.crm.change_handlers import accounts as accounts_res
from jea.crm.changes import (
    create_account,
)


@pytest.mark.django_db
def test_create():
    """Apply a create change"""
    scoping_account = baker.make("crm.Account")
    change = create_account({
        "name": "some account",
        "address": {
            "street_address": "23 Streetway",
            "country": "GB",
            "locality": "Townvillage",
            "postal_code": "388CJ",
        },
    })
    change.scoping_account = scoping_account

    # Apply to accounts resource
    account, changes = accounts_res.create(change)
    assert account
