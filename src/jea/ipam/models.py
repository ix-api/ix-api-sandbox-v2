
"""
Ip Address Management
---------------------

Define models for managing ip address across products,
services, features, and so on.
"""

import enumfields
from django.db import models
from django.db.models import Q
from django.utils import timezone
from ixapi_schema.v1.constants.ipam import (
    IpVersion,
    AddressFamilies,
)

from jea.config import models as access_models
from jea.service import models as service_models
from jea.crm.models import (
    OwnableMixin,
    ManagedMixin,
    AccountScopedMixin,
)


class IpAddressQuerySet(models.QuerySet):
    """IpAddress addional filters"""
    def active_at(self, t):
        """Filter IP addresses valid at this date"""
        return self \
            .exclude(valid_not_before__gt=t) \
            .filter(
                Q(valid_not_after=None) |
                Q(valid_not_after__gt=t))

    def active(self):
        """Filter IP addreses valid right now"""
        return self.active_at(timezone.now())


class IpAddressManager(models.Manager):
    """IpAddress manager adds default filters"""
    def get_queryset(self):
        """Get custom ip address query set"""
        return IpAddressQuerySet(self.model, using=self._db)

    def active_at(self, t):
        """Get queryset with filter"""
        return self.get_queryset().active_at(t)

    def valid(self):
        """Get queryset with valid filter"""
        return self.get_queryset().active()


class IpAddress(
        ManagedMixin,
        OwnableMixin,
        AccountScopedMixin,
        models.Model,
    ):
    """
    An ip address model.

    Stores the ip, a assigned fqdn and
    the prefix length of the address.

    Ip addresses can be owned by a account. And can be managed.
    """
    address = models.GenericIPAddressField()
    prefix_length = models.PositiveSmallIntegerField()
    version = enumfields.EnumIntegerField(IpVersion)

    fqdn = models.CharField(max_length=100, null=True, blank=True)

    assigned_at = models.DateTimeField(default=timezone.now)

    valid_not_before = models.DateTimeField(default=timezone.now)
    valid_not_after = models.DateTimeField(null=True, blank=True)

    ixp_allocated = models.BooleanField(default=False)

    # Relations
    #  - Services
    #
    # TODO: We need to make this many to many as an
    #       ip address now can be used at multiple points
    # This makes the assigned_rel property useless.
    #
    exchange_lan_network_service = models.ForeignKey(
        "service.ExchangeLanNetworkService",
        null=True, blank=True,
        related_name="ip_addresses",
        on_delete=models.CASCADE)

    p2mp_network_service = models.ForeignKey(
        "service.P2MPNetworkService",
        null=True,
        related_name="ip_addresses",
        on_delete=models.CASCADE)

    mp2mp_network_service = models.ForeignKey(
        "service.MP2MPNetworkService",
        null=True,
        related_name="ip_addresses",
        on_delete=models.CASCADE)

    # - Service Access
    exchange_lan_network_service_config = models.ForeignKey(
        "config.ExchangeLanNetworkServiceConfig",
        null=True, blank=True,
        related_name="ip_addresses",
        on_delete=models.CASCADE)

    # - Features
    route_server_network_feature = models.ForeignKey(
        "service.RouteServerNetworkFeature",
        null=True, blank=True,
        related_name="ip_addresses",
        on_delete=models.CASCADE)


    #################################################
    ## Custom default manager for queyset extension #
    #################################################
    objects = IpAddressManager()

    def __str__(self):
        """IP address as string"""
        if self.version.value == 4 and self.prefix_length == 32:
            return str(self.address)
        if self.version.value == 6 and self.prefix_length == 128:
            return str(self.address)
        # CIDR notation
        return "{}/{}".format(self.address, self.prefix_length)

    def __repr__(self):
        """Mac representation"""
        return f"<IpAddress id='{self.pk}' ip='{self.address}'>"


    @property
    def assigned_rel(self):
        """
        Get the assigned relation, if there is any.
        """
        if self.exchange_lan_network_service:
            return self.exchange_lan_network_service
        if self.exchange_lan_network_service_config:
            return self.exchange_lan_network_service_config
        if self.mp2mp_network_service:
            return self.mp2mp_network_service
        if self.p2mp_network_service:
            return self.p2mp_network_service
        if self.route_server_network_feature:
            return self.route_server_network_feature

    @assigned_rel.setter
    def assigned_rel(self, rel):
        """
        Set the assgined relation
        """
        # unbind all
        self.exchange_lan_network_service = None
        self.exchange_lan_network_service_config = None
        self.mp2mp_network_service = None
        self.p2mp_network_service = None
        self.route_server_network_feature = None

        if rel is None:
            return # We are done here

        # bind field
        if isinstance(rel, service_models.ExchangeLanNetworkService):
            self.exchange_lan_network_service = rel
        elif isinstance(rel, service_models.MP2MPNetworkService):
            self.mp2mp_network_service = rel
        elif isinstance(rel, service_models.P2MPNetworkService):
            self.p2mp_network_service = rel
        elif isinstance(rel, service_models.RouteServerNetworkFeature):
            self.route_server_network_feature = rel
        elif isinstance(rel, access_models.ExchangeLanNetworkServiceConfig):
            self.exchange_lan_network_service_config = rel
        else:
            raise ValueError("Unsupported related object: {}".format(
                type(rel)))

    @property
    def in_use(self):
        """
        An ip address is in use if it was assigned
        to a (network) service, feature or config.
        """
        return self.assigned_rel is not None

    class Meta:
        verbose_name = "IP Address"
        verbose_name_plural = "IP Addresses"


class MacAddress(
        ManagedMixin,
        OwnableMixin,
        AccountScopedMixin,
        models.Model,
    ):
    """
    A MAC address model: This model has ownership traits and
    is manageable.

    The ownership is defined by the billing and owning account,
    which might be a reseller / subaccount.
    """
    address = models.CharField(max_length=17)

    assigned_at = models.DateTimeField(default=timezone.now)
    valid_not_before = models.DateTimeField(default=timezone.now)
    valid_not_after = models.DateTimeField(null=True, blank=False)

    # Relations
    # - Service Access
    exchange_lan_network_service_configs = models.ManyToManyField(
        "config.ExchangeLanNetworkServiceConfig",
        related_name="mac_addresses")

    p2p_network_service_configs = models.ManyToManyField(
        "config.P2PNetworkServiceConfig",
        related_name="mac_addresses")

    p2p_network_service_configs = models.ManyToManyField(
        "config.P2MPNetworkServiceConfig",
        related_name="mac_addresses")

    mp2mp_network_service_configs = models.ManyToManyField(
        "config.MP2MPNetworkServiceConfig",
        related_name="mac_addresses")


    def __str__(self):
        """Mac address as string"""
        return self.address

    def __repr__(self):
        """Mac representation"""
        return f"<MacAddress id='{self.pk}' mac='{self.address}'>"


    class Meta:
        verbose_name = "MAC Address"
        verbose_name_plural = "MAC Addresses"

