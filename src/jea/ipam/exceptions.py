"""
IP and Mac address related exceptions
"""

from jea import exceptions


class IpAddressInUse(exceptions.ValidationError):
    """
    Raised when accidentally trying to reassign
    the ip address object.
    """
    default_field = "ip_address"
    default_detail = "The IpAddress is currently in use."


