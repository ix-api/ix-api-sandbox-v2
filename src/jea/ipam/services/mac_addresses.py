
"""
MAC Addresses Services
----------------------

Manage mac addresses. Schedule mac changes and
get all mac addresses relevant for the account.
"""

from django.core.exceptions import ValidationError
from django.utils import timezone

from jea.exceptions import ResourceAccessDenied
from jea.ipam.models import MacAddress
from jea.ipam.filters import MacAddressFilter
from jea.config import models as access_models
from jea.config.services import configs as configs_svc
from jea.crm.services import accounts as accounts_svc


def get_mac_addresses(
        scoping_account=None,
        filters=None,
    ):
    """
    Get all related mac addresses for the account.

    :param scoping_account: The account mananging the address
    :param filters: A mapping of filter keys to values.
    """
    if scoping_account:
        macs = scoping_account.scoped_macaddresss.all()
    else:
        macs = MacAddress.objects.all()

    # And now: Filter these mac addresses
    filtered = MacAddressFilter(
        filters, queryset=macs)

    return filtered.qs


def get_mac_address(
        scoping_account=None,
        mac_address=None,
    ):
    """
    Retrieve a single mac address. Perform ownership test.
    """
    # Get mac address from managed account queryset, if
    # present.
    if not isinstance(mac_address, MacAddress):
        macs = get_mac_addresses(
            scoping_account=scoping_account)
        mac_address = macs.get(pk=mac_address)

    # Permission check
    if scoping_account and \
        mac_address.scoping_account_id != scoping_account.pk:
        raise ResourceAccessDenied(mac_address)

    return mac_address


def create_mac_address(
        scoping_account=None,
        mac_address_input=None,
    ) -> MacAddress:
    """
    Create a mac address object.
    """
    if not scoping_account:
        raise ValidationError("A managing account is required.")

    if not mac_address_input.get("managing_account"):
        raise ValidationError("A billing account is required.")

    if not mac_address_input.get("consuming_account"):
        raise ValidationError("An owning account is required.")

    # Fetch related accounts and check permissions
    managing_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=mac_address_input["managing_account"])

    consuming_account = accounts_svc.get_account(
        scoping_account=scoping_account,
        account=mac_address_input["consuming_account"])

    # Check date constraints
    valid_not_before = mac_address_input.get("valid_not_before")
    valid_not_after = mac_address_input.get("valid_not_after")

    # We don't allow creating things in the past
    if not valid_not_before or valid_not_before < timezone.now():
        valid_not_before = timezone.now()

    # Make new MAC address
    mac_address = MacAddress(
        scoping_account=scoping_account,
        managing_account=managing_account,
        consuming_account=consuming_account,
        external_ref=mac_address_input["external_ref"],
        address=mac_address_input["address"],
        valid_not_before=valid_not_before,
        valid_not_after=valid_not_after)
    mac_address.save()

    return mac_address


def assign_mac_address(
        scoping_account=None,
        mac_address_input=None,
    ):
    """
    Assign a mac address.
    Right now the supported target is a network service.
    In the future others might be required aswell.
    """
    # Check ownership of related service
    config = configs_svc.get_network_service_config(
        scoping_account=scoping_account,
        network_service_config=mac_address_input["network_service_config"])

    # Load related accounts
    managing_account = mac_address_input.get("managing_account")
    consuming_account = mac_address_input.get("consuming_account")
    if not managing_account:
        managing_account = config.managing_account
    if not consuming_account:
        consuming_account = config.consuming_account
    managing_account = accounts_svc.get_account(
        account=managing_account,
        scoping_account=scoping_account)
    consuming_account = accounts_svc.get_account(
        account=consuming_account,
        scoping_account=scoping_account)

    mac_address = mac_address_input.get("mac_address")
    if mac_address:
        # Ownership test
        mac_address = get_mac_address(
            scoping_account=scoping_account,
            mac_address=mac_address)

    if not mac_address:
        # Create a fresh mac from input. Or try to.
        valid_not_before = mac_address_input.get("valid_not_before")
        valid_not_after = mac_address_input.get("valid_not_after")
        if not valid_not_before or valid_not_before < timezone.now():
            valid_not_before = timezone.now()

        mac_address = MacAddress(
            address=mac_address_input["address"],
            valid_not_before=mac_address_input["valid_not_before"],
            valid_not_after=mac_address_input["valid_not_after"],
            scoping_account=scoping_account,
            managing_account=managing_account,
            consuming_account=consuming_account)
        mac_address.save()


    # Assign service by type
    if isinstance(config, access_models.ExchangeLanNetworkServiceConfig):
        mac_address.exchange_lan_network_service_configs.add(config)
    elif isinstance(config, access_models.MP2MPNetworkServiceConfig):
        mac_address.mp2mp_network_service_configs.add(config)
    else:
        raise ValidationError("Invalid network service.")

    return mac_address


def remove_mac_address(
        scoping_account=None,
        mac_address=None,
    ):
    """
    Remove a mac address from the network.

    :param managing_account: The account responisble for related object.
    :param consuming_account: The owning account of the related object.
    :param mac_address: A mac address object or an identifier.
    """
    mac_address = get_mac_address(
        scoping_account=scoping_account,
        mac_address=mac_address)

    # Check if mac address is in use
    if mac_address.exchange_lan_network_service_configs.all() or \
        mac_address.mp2mp_network_service_configs.all():
        raise ValidationError("MAC address is currently in use")

    mac_address.delete()
    return mac_address

