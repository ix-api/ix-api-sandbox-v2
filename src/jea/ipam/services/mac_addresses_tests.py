
"""
Test mac addresses service
"""

import pytest
from model_bakery import baker
from django.utils import timezone
from django.core.exceptions import ValidationError

from jea.exceptions import ResourceAccessDenied
from jea.ipam.services import mac_addresses as mac_addresses_svc
from jea.ipam.models import MacAddress


@pytest.mark.django_db
def test_list_mac_addresses__base_queryset():
    """
    Get base queryset for account mac addresses
    """
    c1 = baker.make("crm.Account")
    c2 = baker.make("crm.Account", scoping_account=c1)

    sc1 = baker.make("config.ExchangeLanNetworkServiceConfig",
        scoping_account=c1,
        managing_account=c1,
        consuming_account=c2)
    sc2 = baker.make("config.MP2MPNetworkServiceConfig",
        scoping_account=c1,
        managing_account=c1,
        consuming_account=c2)
    sc3 = baker.make("config.MP2MPNetworkServiceConfig")

    # Assign mac addresses
    mac1 = baker.make("ipam.MacAddress",
        scoping_account=c1,
        managing_account=c1,
        consuming_account=c2)
    mac1.exchange_lan_network_service_configs.add(sc1)

    mac2 = baker.make("ipam.MacAddress",
        scoping_account=c1,
        managing_account=c1,
        consuming_account=c2)
    mac2.mp2mp_network_service_configs.add(sc2)
    mac3 = baker.make("ipam.MacAddress",
        scoping_account=c2,
        managing_account=c1,
        consuming_account=c1)
    mac3.mp2mp_network_service_configs.add(sc3)

    # Get account mac addresses
    mac_addresses = mac_addresses_svc.get_mac_addresses(
        scoping_account=c1)

    assert mac1 in mac_addresses
    assert mac2 in mac_addresses
    assert not mac3 in mac_addresses


@pytest.mark.django_db
def test_get_mac_address():
    """Get a single mac address"""
    m1 = baker.make("crm.Account")
    c1 = baker.make("crm.Account", scoping_account=m1)
    c2 = baker.make("crm.Account", scoping_account=m1)

    sc1 = baker.make("config.ExchangeLanNetworkServiceConfig",
        scoping_account=m1,
        managing_account=c1,
        consuming_account=c1)
    sc2 = baker.make("config.MP2MPNetworkServiceConfig",
        scoping_account=m1,
        managing_account=c2,
        consuming_account=c2)

    # Assign mac addresses
    mac1 = baker.make("ipam.MacAddress",
        scoping_account=m1,
        managing_account=c1,
        consuming_account=c1)
    mac2 = baker.make("ipam.MacAddress",
        scoping_account=m1,
        managing_account=c2,
        consuming_account=c2)

    # Query mac addresses
    assert mac_addresses_svc.get_mac_address(
        scoping_account=m1,
        mac_address=mac1) == mac1

    assert mac_addresses_svc.get_mac_address(
        scoping_account=m1,
        mac_address=mac1.pk) == mac1

    with pytest.raises(MacAddress.DoesNotExist):
        mac_addresses_svc.get_mac_address(
            scoping_account=c2,
            mac_address=mac1.pk)

    with pytest.raises(ResourceAccessDenied):
        mac_addresses_svc.get_mac_address(
            scoping_account=c2,
            mac_address=mac1)


@pytest.mark.django_db
def test_create_mac_address():
    """Test creating a mac address"""
    c = baker.make("crm.Account")
    mac = mac_addresses_svc.create_mac_address(
        mac_address_input={
            "address": "11:22:33:44:55:66",
            "valid_not_before": timezone.now(),
            "valid_not_after": None,
            "external_ref": "f00",
            "managing_account": c.pk,
            "consuming_account": c.pk,
        },
        scoping_account=c)

    assert mac
    assert mac.pk


@pytest.mark.django_db
def test_assign_mac_address__exchange_lan_network_service():
    """Assign a mac address to a service (config)"""
    c = baker.make("crm.Account")
    sc1 = baker.make(
        "config.ExchangeLanNetworkServiceConfig",
        scoping_account=c, managing_account=c, consuming_account=c)
    mac = baker.make(
        "ipam.MacAddress",
        scoping_account=sc1.scoping_account,
        managing_account=sc1.scoping_account,
        consuming_account=sc1.scoping_account)

    mac_address = mac_addresses_svc.assign_mac_address(
        mac_address_input={
            "mac_address": mac.pk,
            "network_service_config": sc1,
        },
        scoping_account=sc1.scoping_account)

    assert sc1 in mac_address.exchange_lan_network_service_configs.all()


@pytest.mark.django_db
def test_remove_mac_address():
    """Remove a mac address"""
    s = baker.make("config.ExchangeLanNetworkServiceConfig")
    mac = baker.make("ipam.MacAddress",
        exchange_lan_network_service_configs=[s])

    with pytest.raises(ValidationError):
        mac_addresses_svc.remove_mac_address(
            mac_address=mac)

    s.mac_addresses.clear()
    mac = mac_addresses_svc.remove_mac_address(
        mac_address=mac)

    assert not mac.pk
