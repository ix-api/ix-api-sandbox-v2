
"""
IP Addresses Services
---------------------

Allocate ip addresses and find all ip addresses relevant
for the account.
"""
from typing import Optional, Iterable
import ipaddress

from django.core.exceptions import PermissionDenied
from django.utils import timezone

from jea.exceptions import ResourceAccessDenied, ValidationError
from jea.ipam.exceptions import IpAddressInUse
from jea.ipam.models import IpAddress
from jea.crm.services import accounts as accounts_svc
from jea.config import models as access_models
from jea.service.services import network as network_svc
from jea.ipam.filters import IpAddressFilter


def get_ip_addresses(
        scoping_account=None,
        filters=None,
    ) -> Iterable[IpAddress]:
    """
    Get all ip addresses the managing account can config.

    :param scoping_account: The managing account of the session
    :param filters: A mapping of filter attributes and values
    """
    # For a managing account this includes all of
    # the managed ip addressed + all where scoping_account
    # is None
    queryset = IpAddress.objects.filter(
        scoping_account__isnull=True)

    if scoping_account:
        queryset |= scoping_account.scoped_ipaddresss.all()

    filtered = IpAddressFilter(filters, queryset=queryset)

    return filtered.qs


def get_ip_address(
        scoping_account=None,
        ip_address=None,
    ) -> Optional[IpAddress]:
    """
    Resolve a single ip address object. Perform ownership test
    in case a `scoping_account` is present.

    :param scoping_account: A account managing the ip address object
    :param ip_address: An ip address identifier or object

    :raises ResourceAccessDenied: When ownership test fails.
    """
    if not ip_address:
        return None

    if not isinstance(ip_address, IpAddress):
        ip_address = IpAddress.objects.get(pk=ip_address)

    # Check permission
    if scoping_account and \
        ip_address.scoping_account and \
        ip_address.scoping_account != scoping_account:
        raise ResourceAccessDenied(ip_address)

    return ip_address


def get_assigned_exchange_lan_network_service_ips(
        scoping_account=None,
        exchange_lan_network_service=None
    ):
    """Get all ip addresses in an exchange lan"""
    network_service = network_svc.get_network_service(
        scoping_account=scoping_account,
        network_service=exchange_lan_network_service)

    return IpAddress.objects.filter(
        exchange_lan_network_service_config__network_service= \
            network_service)


def add_ip_address(
        scoping_account=None,
        ip_address_request=None,
    ) -> IpAddress:
    """
    Add an IP address record (prefix or host address).

    :param scoping_account: A account managing the ip address object
    :param ip_address_request: An IP address request.
    """
    # Get managing and owning account
    managing_account = accounts_svc.get_account(
        account=ip_address_request["managing_account"],
        scoping_account=scoping_account)
    consuming_account = accounts_svc.get_account(
        account=ip_address_request["consuming_account"],
        scoping_account=scoping_account)

    # Create an ip address object
    ip_address = IpAddress(
        version=ip_address_request["version"],
        address=ip_address_request["address"],
        prefix_length=ip_address_request["prefix_length"],
        fqdn=ip_address_request.get("fqdn"),
        valid_not_before=ip_address_request.get(
            "valid_not_before", timezone.now()),
        valid_not_after=ip_address_request.get("valid_not_after"),
        managing_account=managing_account,
        consuming_account=consuming_account)
    ip_address.save()

    return ip_address


def bind_ip_address(
        to=None,
        ip_address=None,
        scoping_account=None,
    ):
    """
    Assign a ip address to a (network) service, feature,
    config, ... and so on.

    :param to: An object the ip address can be bound to
    :param ip_address: The ip address object or identifier
    :param scoping_account: A scope limiting account
    """
    ip_address = get_ip_address(
        ip_address=ip_address,
        scoping_account=scoping_account)

    if ip_address.in_use and ip_address.assigned_rel != to:
        raise IpAddressInUse()

    # Assign related object
    ip_address.assigned_rel = to
    ip_address.save()

    return ip_address


def release_ip_address(
        ip_address=None,
        scoping_account=None,
    ):
    """
    Release an IP address by undoing all references.

    :param ip_address: The ip address object to release
    :param scoping_account: A scope limiting account
    """
    ip_address = get_ip_address(
        ip_address=ip_address,
        scoping_account=scoping_account)

    ip_address.assigned_rel = None
    ip_address.save()

    return ip_address


def allocate_ip_address(
        scoping_account=None,
        network_service_config=None,
        version=4,
    ):
    """
    Allocate an ip address for a network service configuration
    or network feature configuration.
    """
    if scoping_account and \
        network_service_config.scoping_account != scoping_account:
        raise ResourceAccessDenied(network_service_config)

    if isinstance(network_service_config,
                  access_models.ExchangeLanNetworkServiceConfig):
        ip_addr = _allocate_exchange_lan_network_service_config_ip_address(
            network_service_config, version,
            scoping_account=scoping_account)
    else:
        raise NotImplementedError

    return ip_addr


def _allocate_exchange_lan_network_service_config_ip_address(
        exchange_lan_network_service_config,
        version,
        scoping_account=None,
    ):
    """
    Allocate an ip address from the exchange lan service ip range

    TODO: The alogrithm can be optimized.
    """
    network_service = exchange_lan_network_service_config.network_service


    # Get the network address
    ip = network_service.ip_addresses.filter(version=version).first()
    if not ip:
        raise ValidationError(
            "Network prefix is not configured for exchange lan")

    net = ipaddress.ip_network(f"{ip.address}/{ip.prefix_length}")

    # get all allocated ips for the exchange lan
    allocated = get_assigned_exchange_lan_network_service_ips(
        exchange_lan_network_service=network_service)

    allocated_hosts = {ipaddress.ip_address(f"{ip.address}")
                       for ip in allocated}

    if version == 4:
        net_hosts = {host for host in net.hosts()}
        prefix_len = 32
    else:
        gen = net.hosts()
        net_hosts = {gen.send(None) for _ in range(1000)}
        prefix_len = 128

    available = net_hosts - allocated_hosts
    if not available:
        raise ValidationError(
            "IP pool for exchange lan exhausted")

    addr = available.pop()

    # Allocate address
    ip_address = IpAddress(
        ixp_allocated=True,
        version=version,
        address=str(addr),
        prefix_length=prefix_len,
        scoping_account=scoping_account,
        managing_account=exchange_lan_network_service_config.managing_account,
        consuming_account=exchange_lan_network_service_config.consuming_account,
        exchange_lan_network_service_config=\
            exchange_lan_network_service_config)
    ip_address.save()

    return ip_address


def _update_ip_address_address(
        ip_address=None,
        ip_address_update=None,
    ):
    """
    Update the ip address address (and version) field.
    Also update the prefix length.

    :param ip_address: An ip address model object
    :param ip_address_update: A dict with validated update data
    """
    # Get the updated version or the current
    # if the update does not change it.
    version = ip_address_update.get(
        "version", ip_address.version)
    address = ip_address_update.get(
        "address", ip_address.address)
    prefix_length = ip_address_update.get(
        "prefix_length", ip_address.prefix_length)

    # Are we allowed to make updates to this?
    if ip_address.ixp_allocated:
        if ip_address.address != address or \
            ip_address.prefix_length != prefix_length or \
            ip_address.version != version:
                raise PermissionDenied(
                    "You may not change allocated addresses.")

    # Validate ip address
    try:
        addr = ipaddress.ip_address(address)
    except ValueError as e:
        raise ValidationError(e, field="address")

    # Check that version and ip matches
    if addr.version != version.value:
        raise ValidationError(
            "Version of ip does not match address.",
            field="version")

    # Validate usage specific rules
    # TODO

    # Update address
    ip_address.version = version
    ip_address.address = str(addr)

    return ip_address


def _update_ip_address_expiration(
        ip_address=None,
        ip_address_update=None,
    ):
    """
    Change the expiration date of an ip address object.

    :param ip_address: An IpAddress model object
    :param ip_address_update: Validated update data
    """
    valid_not_before = ip_address_update.get(
        "valid_not_before", ip_address.valid_not_before)
    valid_not_after = ip_address_update.get(
        "valid_not_after", ip_address.valid_not_after)

    # Are we allowed to make this change?
    if ip_address.ixp_allocated:
        if valid_not_before != ip_address.valid_not_before or \
            valid_not_after != ip_address.valid_not_after:
                raise PermissionDenied(
                    "You may not change allocated addresses.")

    if valid_not_after and valid_not_after < valid_not_before:
        raise ValidationError(
            "The expiration date might not be before the "
            "date the address becomes valid.", field="valid_not_after")

    if valid_not_after and valid_not_after < timezone.now():
        raise ValidationError(
            "The expiration date must not be in the past.",
            field="valid_not_after")

    # Set the expiration date
    ip_address.valid_not_before = valid_not_before
    ip_address.valid_not_after = valid_not_after

    return ip_address


def update_ip_address(
        scoping_account=None,
        ip_address=None,
        ip_address_update=None,
    ):
    """
    Update an ip address.

    :param scoping_account: A account managing the ip address
    :param ip_address: The ip address to update
    :param ip_address_input: The update data
    """
    ip_address = get_ip_address(
        scoping_account=scoping_account,
        ip_address=ip_address)

    # Check for write capabilities
    if ip_address.scoping_account != scoping_account:
        raise ResourceAccessDenied(ip_address)

    update_keys = set(ip_address_update.keys())

    # Update ownership attributes
    if "purchase_order" in update_keys:
        ip_address.purchase_order = ip_address_update["purchase_order"]
    if "managing_account" in update_keys:
        managing_account = accounts_svc.get_account(
            account=ip_address_update["managing_account"],
            scoping_account=scoping_account)
        ip_address.managing_account = managing_account
    if "consuming_account" in update_keys:
        consuming_account = accounts_svc.get_account(
            account=ip_address_update["consuming_account"],
            scoping_account=scoping_account)
        ip_address.consuming_account = consuming_account

    # Update FQDN
    if "fqdn" in update_keys:
        ip_address.fqdn = ip_address_update["fqdn"]

    # Update address and version on non ixp allocated
    # ip addresses
    if any(k in update_keys for k in {"version", "address", "prefix_length"}):
        ip_address = _update_ip_address_address(
            ip_address=ip_address,
            ip_address_update=ip_address_update)

    if "valid_not_before" in update_keys or "valid_not_after" in update_keys:
        ip_address = _update_ip_address_expiration(
            ip_address=ip_address,
            ip_address_update=ip_address_update)

    # Persist changes
    ip_address.save()

    return ip_address


def delete_ip_address(
        scoping_account=None,
        ip_address=None,
    ):
    """
    Delete an IP address record.

    :param scoping_account: A account managing the ip address
    :param ip_address: The ip address to remove
    """
    ip_address = get_ip_address(
        scoping_account=scoping_account,
        ip_address=ip_address)

    # Check for write capabilities
    if ip_address.scoping_account != scoping_account:
        raise ResourceAccessDenied(ip_address)

    # Can the ip address be removed by the user?
    if ip_address.ixp_allocated:
        raise PermissionDenied(
            "You are not allowed to remove the IP address")

    ip_address.delete()

    return ip_address

