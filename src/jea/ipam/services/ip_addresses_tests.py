
"""
IP Address Service Tests
"""

from datetime import datetime, timedelta

import pytest
from model_bakery import baker
from django.core.exceptions import PermissionDenied
from django.utils import timezone

from jea.exceptions import ResourceAccessDenied, ValidationError
from jea.ipam.models import IpVersion
from jea.ipam.services import (
    ip_addresses as ip_addresses_svc,
)


@pytest.mark.django_db
def test_get_ip_addresses__base_queryset__service_features():
    """Test base queryset construction"""
    s1 = baker.make("service.ExchangeLanNetworkService")
    f1 = baker.make("service.RouteServerNetworkFeature")
    f3 = baker.make("service.RouteServerNetworkFeature")

    # Baseline IP
    ip0_manager = baker.make("crm.Account")
    ip0 = baker.make("ipam.IpAddress", scoping_account=ip0_manager)

    # Assign ips
    ip1 = baker.make("ipam.IpAddress",
        exchange_lan_network_service=s1)
    ip2 = baker.make("ipam.IpAddress",
        route_server_network_feature=f1)
    ip3 = baker.make("ipam.IpAddress",
        route_server_network_feature=f3)

    # Test queryset construction
    qs = ip_addresses_svc.get_ip_addresses()

    assert not ip0 in qs, "Unrelated address should be filtered"

    # All other should be included.
    assert ip1 in qs
    assert ip2 in qs
    assert ip3 in qs


@pytest.mark.django_db
def test_get_ip_addresses__base_queryset__configs():
    """Test base queryset construction with service and feature configs."""
    c1 = baker.make("crm.Account")
    c2 = baker.make("crm.Account", scoping_account=c1)

    sc1 = baker.make("config.ExchangeLanNetworkServiceConfig",
        scoping_account=c2,
        managing_account=c1,
        consuming_account=c2)

    ip2 = baker.make("ipam.IpAddress")
    fc1 = baker.make(
        "config.RouteServerNetworkFeatureConfig",
        ip=ip2,
        scoping_account=c2,
        managing_account=c1,
        consuming_account=c2)

    # Assign ips
    ip0_manager = baker.make("crm.Account")
    ip0 = baker.make("ipam.IpAddress", scoping_account=ip0_manager)

    ip1 = baker.make("ipam.IpAddress",
        exchange_lan_network_service_config=sc1)

    # Make queryset
    qs = ip_addresses_svc.get_ip_addresses()

    assert not ip0 in qs, "Unrelated ip should be filtered"

    # Again, all other should be included
    assert ip1 in qs
    assert ip2 in qs


@pytest.mark.django_db
def test_get_ip_address():
    """Get a ip address, test ownership"""
    account = baker.make("crm.Account")
    service_config = baker.make(
        "config.ExchangeLanNetworkServiceConfig",
        scoping_account=account,
        managing_account=account,
        consuming_account=account)
    ip1 = baker.make(
        "ipam.IpAddress",
        scoping_account=account,
        exchange_lan_network_service_config=service_config)
    other_account = baker.make("crm.Account")
    ip2 = baker.make("ipam.IpAddress",
        scoping_account=other_account)

    assert ip_addresses_svc.get_ip_address(
        scoping_account=account,
        ip_address=ip1)

    assert ip_addresses_svc.get_ip_address(
        ip_address=ip2.pk)

    with pytest.raises(ResourceAccessDenied):
        ip_addresses_svc.get_ip_address(
            scoping_account=account,
            ip_address=ip2)


@pytest.mark.django_db
def test_add_ip_address():
    """Test adding an ip address record"""
    account = baker.make("crm.Account")
    ip = ip_addresses_svc.add_ip_address(
        scoping_account=account,
        ip_address_request={
           "version":  IpVersion(4),
           "address": "1.2.3.4",
           "prefix_length": 32,
           "managing_account": account,
           "consuming_account": account,
        })

    assert ip


@pytest.mark.django_db
def test_bind_ip_address():
    """Test binding an ip address to a service or config"""
    ip = baker.make("ipam.IpAddress")
    elan = baker.make("service.MP2MPNetworkService")

    ip = ip_addresses_svc.bind_ip_address(
        ip_address=ip,
        to=elan)

    assert ip.assigned_rel == elan

    # Try this again and it should fail
    etree = baker.make("service.P2MPNetworkService")
    with pytest.raises(ValidationError):
        ip_addresses_svc.bind_ip_address(
            ip_address=ip,
            to=etree)


@pytest.mark.django_db
def test_release_ip_address():
    """Test releasing the ip address"""
    elan = baker.make("service.MP2MPNetworkService")
    ip = baker.make(
        "ipam.IpAddress",
        mp2mp_network_service=elan)

    ip = ip_addresses_svc.release_ip_address(ip_address=ip)
    assert not ip.in_use


@pytest.mark.django_db
def test_get_assigned_exchange_lan_network_service_ips():
    """
    Test listing all ip addresses for a given exchange
    lan network service.
    """
    service = baker.make("service.ExchangeLanNetworkService")
    service_config = baker.make(
        "config.ExchangeLanNetworkServiceConfig",
        network_service=service)
    ip1 = baker.make(
        "ipam.IpAddress",
        exchange_lan_network_service_config=service_config)
    ip2 = baker.make("ipam.IpAddress")

    ips = ip_addresses_svc.get_assigned_exchange_lan_network_service_ips(
        exchange_lan_network_service=service)

    assert ip1 in ips
    assert not ip2 in ips


@pytest.mark.django_db
def test_allocate_exchange_lan_config_ip_address_v4():
    """Test allocating ip addresses for an excange lan"""
    account = baker.make("crm.Account")
    exchange_lan = baker.make("service.ExchangeLanNetworkService")
    net_ip = baker.make(
        "ipam.IpAddress",
        exchange_lan_network_service=exchange_lan,
        version=4,
        address="100.100.100.0", prefix_length=24)

    config = baker.make(
        "config.ExchangeLanNetworkServiceConfig",
        scoping_account=account,
        network_service=exchange_lan)

    # Allocate address
    ip4 = ip_addresses_svc \
        ._allocate_exchange_lan_network_service_config_ip_address(
            config, 4, account)

    assert ip4
    assert ip4.pk


@pytest.mark.django_db
def test_allocate_exchange_lan_config_ip_address():
    """Test allocating ip addresses for an excange lan"""
    account = baker.make("crm.Account")
    exchange_lan = baker.make("service.ExchangeLanNetworkService")
    net_ip = baker.make("ipam.IpAddress",
        exchange_lan_network_service=exchange_lan,
        version=6,
        address="1000:23:42::0", prefix_length=64)

    config = baker.make("config.ExchangeLanNetworkServiceConfig",
        scoping_account=account,
        network_service=exchange_lan)

    # Allocate address
    ip6 = ip_addresses_svc \
        ._allocate_exchange_lan_network_service_config_ip_address(
            config, 6, account)

    assert ip6
    assert ip6.pk


@pytest.mark.django_db
def test_update_ip_address__address():
    """Test updating the ip address"""
    ip = baker.make(
        "ipam.IpAddress",
        version=IpVersion(4),
        prefix_length=32,
        address="10.23.42.1")

    ip = ip_addresses_svc.update_ip_address(
        ip_address=ip,
        ip_address_update={
            "address": "23.42.42.10",
        })

    assert ip.address == "23.42.42.10"

    with pytest.raises(ValidationError):
        ip_addresses_svc.update_ip_address(
            ip_address=ip,
            ip_address_update={
                "address": "fe23::42",
            })


@pytest.mark.django_db
def test_update_ip_address__version():
    """Test updating an ip address with version change"""
    ip = baker.make(
        "ipam.IpAddress",
        version=IpVersion(4),
        prefix_length=32,
        address="10.23.42.1")

    ip = ip_addresses_svc.update_ip_address(
        ip_address=ip,
        ip_address_update={
            "address": "fe23::42",
            "version": IpVersion(6),
        })

    assert ip.version.value == 6
    assert ip.address == "fe23::42"


@pytest.mark.django_db
def test_update_ip_address__ixp_allocated():
    """Test updating an allocated ip address"""
    ip = baker.make(
        "ipam.IpAddress",
        ixp_allocated=True,
        version=IpVersion(4),
        prefix_length=32,
        address="10.23.42.1")

    with pytest.raises(PermissionDenied):
        ip_addresses_svc.update_ip_address(
            ip_address=ip,
            ip_address_update={
                "address": "100.200.120.2",
            })

    with pytest.raises(PermissionDenied):
        ip_addresses_svc.update_ip_address(
            ip_address=ip,
            ip_address_update={
                "prefix_length": 16,
            })

    with pytest.raises(PermissionDenied):
        ip_addresses_svc.update_ip_address(
            ip_address=ip,
            ip_address_update={
                "valid_not_after": datetime.now(),
            })

    # Not changing anything shuold work though
    ip_addresses_svc.update_ip_address(
        ip_address=ip,
        ip_address_update={
            "address": "10.23.42.1",
        })


@pytest.mark.django_db
def test_update_ip_address__fqdn():
    """Test updating an ip address object"""
    next_fqdn = "foo.bar42.net"
    account = baker.make("crm.Account")
    ip = baker.make("ipam.IpAddress")
    ip = ip_addresses_svc.update_ip_address(
        ip_address=ip,
        ip_address_update={
            "fqdn": next_fqdn,
            "managing_account": account,
            "consuming_account": account,
            "purchase_order": "foo23",
        })

    assert ip.fqdn == next_fqdn


@pytest.mark.django_db
def test_update_ip_address__expiration():
    """Test updating the expiration date"""
    expires_at = timezone.now() + timedelta(days=10)
    ip = baker.make("ipam.IpAddress")

    # Set an expiration date
    ip = ip_addresses_svc.update_ip_address(
        ip_address=ip,
        ip_address_update={
            "valid_not_after": expires_at,
        })
    assert ip.valid_not_after == expires_at

    # Do not expire address
    ip = ip_addresses_svc.update_ip_address(
        ip_address=ip,
        ip_address_update={
            "valid_not_after": None,
        })

    assert not ip.valid_not_after

    # Try to expire in the past
    with pytest.raises(ValidationError):
        ip_addresses_svc.update_ip_address(
            ip_address=ip,
            ip_address_update={
                "valid_not_after": timezone.now() - timedelta(days=23),
            })


@pytest.mark.django_db
def test_delete_ip_address():
    """Delete an ip address"""
    # Account Allocated
    ip = baker.make(
        "ipam.IpAddress", ixp_allocated=False)
    ip_addresses_svc.delete_ip_address(ip_address=ip)

    # IXP Allocated
    ip = baker.make(
        "ipam.IpAddress", ixp_allocated=True)
    with pytest.raises(PermissionDenied):
        ip_addresses_svc.delete_ip_address(ip_address=ip)

