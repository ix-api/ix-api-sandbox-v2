
"""
IP and Mac address filters
"""

import django_filters
from django.db import models

from jea.ipam.models import (
    IpAddress,
    MacAddress,
)
from utils import filters
from utils.datastructures import must_list



class IpAddressFilter(django_filters.FilterSet):
    """Filters for ip addresses (and prefixes)"""
    id = filters.BulkIdFilter()

    network_service = django_filters.CharFilter(
        method="filter_network_service")
    def filter_network_service(self, queryset, _name, value):
        """Filter by a list of network services"""
        value = must_list(value)
        if not value:
            return queryset

        return queryset.filter(exchange_lan_network_service__in=value)


    network_service_config = django_filters.CharFilter(
        method="filter_network_service_config")
    def filter_network_service_config(self, queryset, _name, value):
        """Filter by network service configs"""
        value = must_list(value)
        if not value:
            return queryset

        return queryset.filter(
            exchange_lan_network_service_config__in=value)

    network_feature = django_filters.CharFilter(
        method="filter_network_feature")
    def filter_network_feature(self, queryset, _name, value):
        """Filter by network feature"""
        value = must_list(value)
        if not value:
            return queryset
        qs = queryset.filter(
            route_server_network_feature__in=value)

        return qs


    network_feature_config = django_filters.CharFilter(
        method="filter_network_feature_config")
    def filter_network_feature_config(self, queryset, _name, value):
        """Filter by network feature configuration"""
        value = must_list(value)
        if not value:
            return queryset

        return queryset.filter(
            route_server_network_feature_config__in=value)


    version = django_filters.NumberFilter()

    class Meta:
        model = IpAddress
        fields = [
            "fqdn",
            "version",
            "prefix_length",
            "valid_not_before",
            "valid_not_after",
            "exchange_lan_network_service",
            "route_server_network_feature",
            "exchange_lan_network_service_config",
            "managing_account",
            "consuming_account",
        ]

        filter_overrides = {
            models.CharField: {
                'filter_class':
                    django_filters.CharFilter,
                'extra': lambda f: {
                    'lookup_expr': 'iexact',
                },
            },
        }


class MacAddressFilter(django_filters.FilterSet):
    """
    Filter mac addresses.
    """
    id = filters.BulkIdFilter()

    network_service_config = django_filters.CharFilter(
        method="filter_network_service_config")
    def filter_network_service_config(self, queryset, _name, value):
        """Filter by a list of network service configs"""
        value = must_list(value)
        if not value:
            return queryset

        qs = queryset.filter(
            exchange_lan_network_service_configs__in=value)
        qs |= queryset.filter(
            mp2mp_network_service_configs__in=value)

        return qs


    class Meta:
        model = MacAddress
        fields = [
            "address",
            "assigned_at",
            "valid_not_before",
            "valid_not_after",
            "managing_account",
            "consuming_account",
        ]

        filter_overrides = {
            models.CharField: {
                'filter_class':
                    django_filters.CharFilter,
                'extra': lambda f: {
                    'lookup_expr': 'iexact',
                },
            },
        }
