
from django.contrib.admin import AdminSite


class JeaAdminSite(AdminSite):
    """
    Override some settings in the django admin site
    """
    site_title = 'Joint External API'
    site_header = 'JEA administration'
    index_title = 'Joint External API administration'

site = JeaAdminSite()

