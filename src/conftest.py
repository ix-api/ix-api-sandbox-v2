"""
Pytest configuration
"""

from model_bakery import baker


# Register custom fields
baker.generators.add(
    'jea.stateful.models.ChangeSerialField',
    'jea.stateful.models.next_change_serial')
