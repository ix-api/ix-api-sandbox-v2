"""
JEA URL Configuration
"""
from django.urls import include, path

from jea import admin
from jea.public import urls as public_urls
from jea.api.v2 import urls as api_v2_urls
from jea.api.ctrl import urls as api_ctrl_urls
from jea.auth import urls as auth_urls

urlpatterns = [
    path(r"", include(public_urls)),
    path(r"admin/", admin.site.urls),
    path(r"auth/", include(auth_urls)),
    path(r"api/v2/", include(api_v2_urls)),
    path(r"api/ctrl/", include(api_ctrl_urls)),
]
