"""
Django settings for jea sandbox project
=======================================

This file configures the django application.
However, for application settings, please checkout the configuration
file located in:

    /etc/jea/sandbox.conf

Or relative to your projects root:

    etc/jea/sandbox.conf

where you can override the default settings.

"""

import os
import sys
import glob
import configparser


# Default configurations:
#
# These are the defaults, for use with a local docker development setup.
# It is _NOT RECOMMENDED_ to edit this file.
# Use the etc/jea/sandbox.conf configuration to change settings.

DEFAULT_SECRET_KEY = "nl@rmf&yeb7i(k6uwzkan7sv)vz+)vhg0hwa+c+l8&9xg-7+p_"
DEFAULT_CONFIG = {
    "jea": {
        "debug": "true",
        "secret_key": DEFAULT_SECRET_KEY,
        "log_level": "INFO"
    },
    "database": {
        "engine": "django.db.backends.postgresql_psycopg2",
        "name": "postgres",
        "user": "postgres",
        "host": "db",
        "port": 5432
    }
}


# Parse configuration(s)
config = configparser.ConfigParser()
config.read_dict(DEFAULT_CONFIG)
config.read([
    "./etc/jea/sandbox.conf",
    "/etc/jea/sandbox.conf"
])

# Get additional config overrides from the environment
JEA_SBX_DB_NAME = os.environ.get(
    "DB_NAME", config["database"]["name"])
JEA_SBX_DB_USER = os.environ.get(
    "DB_USER", config["database"]["user"])
JEA_SBX_DB_HOST = os.environ.get(
    "DB_HOST", config["database"]["host"])
JEA_SBX_DB_PORT = os.environ.get(
    "DB_PORT", config["database"]["port"])
JEA_SBX_DB_PASSWORD = os.environ.get(
    "DB_PASSWORD", config["database"].get("password"))

# Are we running in a test setting
TESTING = False

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Add local / temporary library path
LOCAL_LIB_DIR = os.path.join(BASE_DIR, "..", "local")
if os.path.exists(LOCAL_LIB_DIR):
    print("Local libraries available!")
    for lib in glob.glob(os.path.join(LOCAL_LIB_DIR, "*")):
        # Make available for imports
        print(" - Adding local library path: {}".format(lib))
        sys.path.insert(0, lib)


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = config["jea"].getboolean("debug", True)

DEBUG_REQUESTS = os.environ.get("IXAPI_SANDBOX_DEBUG_REQUESTS", "false").lower() == "true"

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config["jea"]["secret_key"]
if SECRET_KEY == DEFAULT_SECRET_KEY and not DEBUG:
    print("ERROR: You must change the secret key in a production environment")
    sys.exit(-1)


# SECURITY WARNING: This should be enabled if publically deployed.
#   As the primary usecase of the sandbox is local / development use,
#   the default allowed_hosts is `any`.
ALLOWED_HOSTS = os.environ.get(
    "ALLOWED_HOSTS",
    config["jea"].get("allowed_hosts", "*")
).split(",")


ERROR_DOCS_BASE_URL = config["jea"].get(
    "error_docs_base", "https://errors.ix-api.net/v2/")


DEFAULT_AUTO_FIELD = "django.db.models.AutoField"


# Application definition
INSTALLED_APPS = [
    'polymorphic',
    'django_filters',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'jea.public',
    'jea.stateful',
    'jea.auth',
    'jea.ipam',
    'jea.catalog',
    'jea.service',
    'jea.config',
    'jea.crm',
    'jea.ctrl',
    'jea.api.v2',

    'rest_framework'
]


MIDDLEWARE = [
    'jea.debug.middleware.DebugMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware'
]

ROOT_URLCONF = 'backend.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages'
            ],
        },
    },
]

WSGI_APPLICATION = 'backend.wsgi.application'
# Database
DATABASES = {
    'default': {
        'ENGINE': config['database']['engine'],
        'NAME': JEA_SBX_DB_NAME,
        'PASSWORD': JEA_SBX_DB_PASSWORD,
        'USER': JEA_SBX_DB_USER,
        'HOST': JEA_SBX_DB_HOST,
        'PORT': JEA_SBX_DB_PORT,
    }
}

# Auth user
AUTH_USER_MODEL = "jea_auth.User"

LOGIN_URL = "login"

# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_TZ = True


# Static files (CSS, JavaScript, Images)
STATIC_URL = '/static/'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(asctime)s %(levelname)s %(name)-20s %(module)s %(process)d %(thread)d %(message)s',
        },
        'simple': {
            'format': '%(asctime)s %(levelname)-8s %(name)-20s| %(message)s',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': config['jea']['log_level'],
        },
        'jea': {
            'handlers': ['console'],
            'level': config['jea']['log_level'],
        },
    },
}

#
# Sandbox Version
#
VERSION = "unknown"
try:
    with open(BASE_DIR + "/../VERSION") as f:
        VERSION = str(f.read()).strip()
except Exception as e:
    print("Could not get sandbox version information")

# Update jedi settings for tab completion to work
# within a containerized environment.
if DEBUG:
    try:
        import tempfile
        import jedi
        cache_dir = os.path.join(
            tempfile.gettempdir(), "jea-sandbox/jedi")
        jedi.settings.cache_directory = cache_dir
    except ImportError:
        pass
