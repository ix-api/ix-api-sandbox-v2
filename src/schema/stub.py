
import enum

PORT_STATE_UP = "up"
PORT_STATE_DOWN = "down"
PORT_STATE_ERROR = "error"

class PortState(enum.Enum):
    UP = PORT_STATE_UP
    DOWN = PORT_STATE_DOWN
    ERROR = PORT_STATE_ERROR

