
#
# JEA :: SANDBOX
# --------------
#
# Makefile for project tasks
#

VERSION ?= dev


#
# Development
#

volumes:
	docker volume create --name=ixapi-sandbox-v2_postgres_dev

clean:
	find . -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete

#
# Production / Deployment
#
build_prod:
	docker build . \
		-t registry.gitlab.com/ix-api/ix-api-sandbox-v2 \
		-t registry.gitlab.com/ix-api/ix-api-sandbox-v2:$(VERSION)

push_prod:
	docker push registry.gitlab.com/ix-api/ix-api-sandbox-v2:$(VERSION)

push_prod_latest:
	docker push registry.gitlab.com/ix-api/ix-api-sandbox-v2:latest

up: volumes
	docker-compose  up

migrate:
	./bin/sandbox migrate

superuser:
	./bin/sandbox createsuperuser

bootstrap:
	./bin/sandbox bootstrap
