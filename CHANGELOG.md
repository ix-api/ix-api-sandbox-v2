
# 2.3.1
Updated dependencies,
Updated postgres image in docker-compose.yml
NOTICE: you have to recreate the develoment volume:

    docker volume rm ixapi-sandbox-v2_postgres_dev
    docker volume create --name=ixapi-sandbox-v2_postgres_dev

Added support for statistics


# 2.3.0 (2023-06-21)
Updated Dockerimage
Updated to schema release 2.5.0

New: support for OAuth2.0

Added support for availability_zones

# 2.2.2 (2023-01-04)

Bugs: `subscriber_side_demarcs` field was not saved.
Updated Schema version: 2.4.2, added required properties.

# 2.1.5 (2022-03-09)

Upgraded all dependencies. Please note, that it is required
to recreate the database and apply migrations and bootstrapping again.


# 0.0.1 - Initial Commit


